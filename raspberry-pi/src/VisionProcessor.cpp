#include "VisionProcessor.h"
#include "RectanglePacket.h"

#include <opencv2/opencv.hpp>
#include <chrono>
#include <thread>
#include <iostream>
#include <fstream>

#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


VisionProcessor::VisionProcessor()
   : m_captures()
   , m_currCam(0)
   , m_stopRequest(false)
   , m_config()
{
   std::cout << "Starting processor..." << std::endl;

   Json::Reader reader;
   std::ifstream configFile("config.json", std::ifstream::binary);
   bool parseSuccess = reader.parse(configFile, m_config, false);
   if (!parseSuccess)
   {
      printf("Config file contains errors: %s\n", reader.getFormatedErrorMessages().c_str());
   }

   /*for (int i = 0; i < m_config.get("cameraCount", 1).asInt(); i++)
   {
      std::cout << "Initializing camera " << i << std::endl;
      this->initCamera(i);
   }*/

   this->initSocket();

   m_thread = std::thread(&VisionProcessor::thread_body, this);
   m_recvThread = std::thread(&VisionProcessor::recv_thread_body, this);
   // Define TEST_THREAD to test switching cameras.
#ifdef TEST_THREAD
   m_testThread = std::thread(&VisionProcessor::test_thread_body, this);
#endif
}

VisionProcessor::~VisionProcessor()
{
   m_stopRequest = true;
   m_thread.join();
   m_recvThread.join();

   /*for (VideoCapture *vc : m_captures)
   {
      if (vc->isOpened())
      {
         vc->release();
      }
   }*/
}

void VisionProcessor::initSocket(void)
{
   std::memset(&m_roboRio, 0, sizeof(m_roboRio));

   m_roboRio.sin_family = AF_INET;
   m_roboRio.sin_port = htons(m_config.get("port", 4061).asInt());
   m_roboRio.sin_addr.s_addr = inet_addr(m_config.get("ip", "10.40.61.21").asCString());

   m_socket = socket(AF_INET, SOCK_DGRAM, 0);
}

VideoCapture * VisionProcessor::initCamera(int camNum)
{
   VideoCapture *cap = new VideoCapture(camNum);

   char command[80];
   sprintf(command, "v4l2-ctl -d /dev/video%i -c exposure_auto=%i -c exposure_absolute=%i",
           camNum,
           m_config.get("exposureAuto", 1).asInt(),
           m_config.get("exposureAbsolute", 19).asInt());
   system(command);

   if (!cap->isOpened())
   {
      printf("uh oh\n");
   }

   cap->set(CV_CAP_PROP_FRAME_WIDTH, m_config.get("frameWidth", 640).asInt());
   cap->set(CV_CAP_PROP_FRAME_HEIGHT, m_config.get("frameHeight", 480).asInt());

   //m_captures.push_back(cap);

   return cap;
}

void VisionProcessor::thread_body(void)   
{
   // Color info for the tape
   double hue[] = {
      m_config.get("hslLow", 78.0).asDouble(),
      m_config.get("hslHigh", 92.0).asDouble()
   };
   double sat[] = {
      m_config.get("satLow", 172.0).asDouble(),
      m_config.get("satHigh", 255.0).asDouble()
   };
   double lum[] = {
      44.0,
      255.0
   };
   Scalar hslUpper = Scalar(hue[1], lum[1], sat[1]);
   Scalar hslLower = Scalar(hue[0], lum[0], sat[0]);

   int minWidth = m_config.get("rectMinWidth", 5).asInt();
   int maxWidth = m_config.get("rectMaxWidth", 1000).asInt();
   int minHeight = m_config.get("rectMinHeight", 5).asInt();
   int maxHeight = m_config.get("rectMaxHeight", 1000).asInt();

   Psb::Subsystem::Vision::RectanglePacket rectanglePacket;

   // Pre-allocate the resource
   Mat frame;
   Mat hslThresholdOutput;
   std::vector<std::vector<Point>> contours;
   std::vector<Vec4i> hierarchy;
   std::vector<std::vector<Point>> output;
   
   uint8_t *pDataBuffer = new uint8_t[32 * 1024];
   size_t numTxBytes = 0;

   VideoCapture *cap = NULL;
   int usingCam = -1;

   while (!m_stopRequest)
   {
      if (usingCam != m_currCam) {
         if (cap != NULL) {
            cap->release();
            delete cap;
         }
         cap = initCamera(m_currCam);
         usingCam = m_currCam;
      }

      // Reset our results
      contours.clear();
      hierarchy.clear();

      // Step 1: Grab a frame from the camera and store for our use
      //m_captures[m_currCam]->grab();
      //*(m_captures[m_currCam]) >> frame;  // Default frame size is 480 rows and 640 cols
      cap->grab();
      *cap >> frame;

      // Step 2: Resize the image to help with processing time.
      //         Need to optimize these numbers.
      //resize(frame, resizedFrame, Size(), 0.25, 0.25, INTER_LINEAR);

      // Step 3: Search through the resized frame for the green tape
      cvtColor(frame, hslThresholdOutput, COLOR_BGR2HLS);
      inRange(hslThresholdOutput, hslLower, hslUpper, hslThresholdOutput);

      // Step 4: Search for contours (lines) in our image. At this point the
      //         hslThresholdOuput should be a white / black image with
      //         black pixel where the tape is.
      findContours(hslThresholdOutput, contours, hierarchy, RETR_LIST, CHAIN_APPROX_SIMPLE);

      // Step 5: Build rectangles from the contours. Only save the rectangles
      //         that meet our criteria.
      rectanglePacket.clear();
      for (std::vector<Point> contour: contours)
      {
         // Build a rectangle from the contour
         Rect rect = boundingRect(contour);

         if (rect.width < minWidth || rect.width > maxWidth) continue;
         if (rect.height < minHeight || rect.height > maxHeight) continue;
         printf("%i %i %i %i %i %i\n", minWidth, maxWidth, minHeight, maxHeight, rect.width, rect.height);

         rectanglePacket.addRectangle(rect);
      }

      // Create a bogus packet if we don't come up with any rectangles.
      if (rectanglePacket.getNumRectangles() == 0)
      {
         Rect rect(1, 1, 1, 1);
         rectanglePacket.addRectangle(rect);
      }

      // Don't know how it would be zero at this point, but check anyway.
      if (rectanglePacket.getNumRectangles() > 0)
      {
         //std::cout << "Num of rectangles found = " << rectanglePacket.getNumRectangles() << std::endl;
         numTxBytes = rectanglePacket.serialize(pDataBuffer, 32 * 1024);
         sendto(m_socket, pDataBuffer, numTxBytes, 0, (struct sockaddr *)&m_roboRio, sizeof(m_roboRio));
         //std::cout << "Bytes sent = " << numTxBytes << std::endl;
      }
   }

   delete [] pDataBuffer;
}

void VisionProcessor::recv_thread_body(void)
{
   int receive = 0;
   std::cout << "Start receive body" << std::endl;

   struct sockaddr_in socketInfo;
   std::memset(&socketInfo, 0, sizeof(socketInfo));

   socketInfo.sin_family = AF_INET;
   socketInfo.sin_port = htons(4061);
   socketInfo.sin_addr.s_addr = htonl(INADDR_ANY);

   int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
   if (sock == -1)
   {
      int localError = errno;
      exit(1);
   }

   if (-1 == bind(sock, (struct sockaddr*)&socketInfo, sizeof(socketInfo)))
   {
      int localError = errno;
      exit(1);
   }

   while (!m_stopRequest)
   {
      int ret = recv(sock, &receive, sizeof(receive), 0);
      if (ret > 0) {
         int newCam = ntohl(receive);
         if (newCam != m_currCam) {
            std::cout << "Received data: " << newCam << std::endl;
            m_currCam = newCam;
         }
      }
   }
}

void VisionProcessor::test_thread_body(void) {
   bool flip = true;
   while (true) {
      std::cout << (flip ? "Flip" : "Flop") << std::endl;
      m_currCam = flip;
      flip = !flip;
      usleep(1000000);
   }
}

