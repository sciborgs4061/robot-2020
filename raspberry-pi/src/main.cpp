#include <iostream>
#include <memory>
#include <thread>
#include <chrono>

#include "VisionProcessor.h"

int main(int argc, char* argv[])
{
	std::unique_ptr<VisionProcessor> p_vision(new VisionProcessor());

	while (true)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(5000));
	}

	return 0;
}
