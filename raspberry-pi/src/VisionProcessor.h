#pragma once

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <opencv2/opencv.hpp>
#include <thread>
using namespace cv;

#include "json/json.h"


class VisionProcessor
{
	public:
		VisionProcessor();
		~VisionProcessor();

	public:
		void thread_body(void);
      void recv_thread_body(void);
      void test_thread_body(void);
	protected:
		void initSocket(void);
      VideoCapture *initCamera(int);

	private:
      std::vector<VideoCapture *> m_captures;
      int m_currCam;
		std::thread m_thread;
      std::thread m_recvThread;
      std::thread m_testThread;
		bool m_stopRequest;
		struct sockaddr_in m_roboRio;
		int m_socket;
		Json::Value m_config;
};
