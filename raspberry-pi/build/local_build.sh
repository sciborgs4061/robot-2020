#!/bin/bash
if [ $HOSTNAME == "raspberrypi" ]; then
	cmake ..
	make -j3
else
	printf "You can only build this on the PI!\n"
	exit -1
fi
