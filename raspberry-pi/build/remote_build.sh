#!/bin/bash
ADDRESS=10.40.61.18
sshpass -p 'raspberry' rsync -avz --copy-links --exclude 'build*' ../. pi@${ADDRESS}:/home/pi/psb-visiond/.
sshpass -p 'raspberry' rsync -avz local_build.sh pi@${ADDRESS}:/home/pi/psb-visiond/build/.
sshpass -p 'raspberry' ssh pi@${ADDRESS} "cd /home/pi/psb-visiond/build; ./local_build.sh"

