//
// Inputs
//
Inputs
{
	Mat source0;
}

//
// Variables
//
Outputs
{
	Mat hslThresholdOutput;
	ContoursReport findContoursOutput;
	ContoursReport filterContoursOutput;
}

//
// Steps
//

Step HSL_Threshold0
{
    Mat hslThresholdInput = source0;
    List hslThresholdHue = [51.798561151079134, 101.67235494880548];
    List hslThresholdSaturation = [116.95143884892086, 255.0];
    List hslThresholdLuminance = [43.57014388489208, 255.0];

    hslThreshold(hslThresholdInput, hslThresholdHue, hslThresholdSaturation, hslThresholdLuminance, hslThresholdOutput);
}

Step Find_Contours0
{
    Mat findContoursInput = hslThresholdOutput;
    Boolean findContoursExternalOnly = false;

    findContours(findContoursInput, findContoursExternalOnly, findContoursOutput);
}

Step Filter_Contours0
{
    ContoursReport filterContoursContours = findContoursOutput;
    Double filterContoursMinArea = 4.0;
    Double filterContoursMinPerimeter = 2.0;
    Double filterContoursMinWidth = 0.0;
    Double filterContoursMaxWidth = 9999.0;
    Double filterContoursMinHeight = 0.0;
    Double filterContoursMaxHeight = 999.0;
    List filterContoursSolidity = [0.0, 100.0];
    Double filterContoursMaxVertices = 2.147483647E9;
    Double filterContoursMinVertices = 0.0;
    Double filterContoursMinRatio = 0.2;
    Double filterContoursMaxRatio = 1.0;

    filterContours(filterContoursContours, filterContoursMinArea, filterContoursMinPerimeter, filterContoursMinWidth, filterContoursMaxWidth, filterContoursMinHeight, filterContoursMaxHeight, filterContoursSolidity, filterContoursMaxVertices, filterContoursMinVertices, filterContoursMinRatio, filterContoursMaxRatio, filterContoursOutput);
}

Step NTPublish_ContoursReport0
{
    ContoursReport ntpublishContoursreportData = filterContoursOutput;
    String ntpublishContoursreportName = "myContoursReport";
    Boolean ntpublishContoursreportPublishArea = true;
    Boolean ntpublishContoursreportPublishCenterx = true;
    Boolean ntpublishContoursreportPublishCentery = true;
    Boolean ntpublishContoursreportPublishWidth = true;
    Boolean ntpublishContoursreportPublishHeight = true;
    Boolean ntpublishContoursreportPublishSolidity = true;

    ntpublishContoursreport(ntpublishContoursreportData, ntpublishContoursreportName, ntpublishContoursreportPublishArea, ntpublishContoursreportPublishCenterx, ntpublishContoursreportPublishCentery, ntpublishContoursreportPublishWidth, ntpublishContoursreportPublishHeight, ntpublishContoursreportPublishSolidity, );
}




