#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <json/json.h>

#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>
using namespace std;

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

#define PORTNUM 5805
// #define HOSTNAME "localhost"

int main(int argc, char *argv[])
{
    if (!(argc == 2 || argc == 3))
    {
       printf("Usage: %s ipaddr [subsystem filter]\n",argv[0]);
       return 1;
    }

    std::string subsystem_filter;
    if (argc == 3)
    {
       subsystem_filter = argv[2];
    }

    // try to connect and stay connected as much as possible
    do
    {
      int sockfd, n;
      struct sockaddr_in serv_addr;
      struct hostent *server;

      char json_buffer[256];
      sockfd = socket(AF_INET, SOCK_STREAM, 0);
      if (sockfd < 0)
      {
         error("ERROR opening socket");
      }
      server = gethostbyname(argv[1]);
      if (server == NULL)
      {
         fprintf(stderr,"ERROR, no such host\n");
         exit(0);
      }
      bzero((char *) &serv_addr, sizeof(serv_addr));
      serv_addr.sin_family = AF_INET;
      bcopy((char *)server->h_addr, 
            (char *)&serv_addr.sin_addr.s_addr,
            server->h_length);
      serv_addr.sin_port = htons(PORTNUM);

      bool not_connected = true;
      // I want to try to connect until I get connected
      while (not_connected)
      {
         if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
         {
            perror("ERROR connecting: ");
            printf("  ...is %s available for connecting to?\n",argv[1]);
            sleep(1);
            continue;
         }
         else
         {
            not_connected = false;
         }
      }

      bool found_delim = false;
      std::string json_desc = "";

      while (!found_delim)
      {
         bzero(json_buffer,sizeof(json_buffer));
         n = read(sockfd,json_buffer,sizeof(json_buffer)-1);
         if (n < 0)
         {
            error("ERROR reading from socket");
         }
         // this will exist in there somewhere...even if it
         // was cut in half in the read
         char *location = strstr(json_buffer,"=====");
         if (location != NULL)
         {
            // found the delimiter
            json_desc += json_buffer;
            // hack of the end of the string that has the delimiter
            // on it and then it should be valid JSON.
            // Then you can load it into JSONCpp and then start
            // ripping it apart.
            json_desc.erase(json_desc.find("====="));

            // printf("FOUND IT: \n---------------\n%s",json_desc.c_str());
            // printf("\n------------------\n");
            found_delim = true;
         }
         else
         {
            json_desc += json_buffer;
            // printf("still reading: bytes: %d  Value:%s\n",n,json_buffer);
         }
      }

      printf("%s",json_desc.c_str());
      // load the json description
      Json::Value root;
      Json::Reader reader;
      if (!reader.parse(json_desc,root))
      {
         error("Error parsing JSON");
      }

      // You can then find each of the data types in each
      // of the sections (in order) and rip the data
      // out of the stream and save it off.

      // figure out how much data is coming in by looking at the JSON
      // description
      int databufsize = 0;
      const Json::Value ss = root["subsystems"];

      for ( int ss_idx = 0; ss_idx < ss.size(); ++ss_idx )
      {
         const Json::Value data = ss[ss_idx]["ss_data"];
         for ( int data_idx = 0; data_idx < data.size(); ++data_idx )
         {
            /*
            std::string type = data[data_idx]["type"].asString();
            if (type == "bool")     databufsize += 8;
            if (type == "double")   databufsize += 8;
            if (type == "int32_t")  databufsize += 8;
            if (type == "char[32]") databufsize += 32;
            if (type == "memblk_t") databufsize += 256+8;
            */

            int type_size = data[data_idx]["size"].asInt();
            databufsize += type_size;
         }
      }

      char *databuf = new char[databufsize];

      while (true)
      {
         bzero(databuf,databufsize);
         n = read(sockfd,databuf,databufsize);
         if (n <= 0)
         {
            perror("ERROR reading from socket:");
            break;
         }
         printf("\n=========================\n   bytes read/needed: %d/%d\n\n",n,databufsize);

         const Json::Value ss = root["subsystems"];
         int offset = 0;
         for ( int ss_idx = 0; ss_idx < ss.size(); ++ss_idx )
         {
            bool dump_string = false;
            if (subsystem_filter.length() != 0)
            {
               dump_string = (ss[ss_idx]["ss_name"].asString() == subsystem_filter);
            }
            else
            {
               dump_string = true;
            }

            if (dump_string)
            {
               cout << "\n** " << ss[ss_idx]["ss_name"].asString() << " **\n";
            }

            const Json::Value data = ss[ss_idx]["ss_data"];
            std::stringstream row;
            row.setf( std::ios_base::fixed, std::ios_base::floatfield );

            for ( int data_idx = 0; data_idx < data.size(); ++data_idx )
            {
               std::string type = data[data_idx]["type"].asString();
               int type_size = data[data_idx]["size"].asInt();

               // If the type_size for this item is zero size we can just
               // skip it all together
               if (type_size == 0)
               {
                  continue;
               }

               row << "  " << right;
               if (type == "bool")
               {
                  row << setw(7) << *(bool *)(databuf + offset);
               }
               if (type == "double")
               {
                  row << setw(7) << std::setprecision(4) << *(double*)(databuf + offset);
               }
               if (type == "int32_t")
               {
                  row << setw(7) << *(int32_t*)(databuf + offset);
               }
               if (type == "char[32]")
               {
                  row << "      " << (char*)(databuf + offset);
               }
               if (type == "memblk_t")
               {
                  row << "led";
               }
               offset += type_size;

               row << " : ";
               row << left << setw(40) << data[data_idx]["var_name"].asString();
               if (row.str().length() > 60)
               {
                  // dump the stream if it is really long
                  if (dump_string)
                  {
                     cout << row.str() << "\n";
                  }
                  // and then clear it
                  row.str("");
               }
               else
               {
                  // add a tab and just roll with it
                  row << "\t";
               }
            }
            if (row.str().length() > 0)
            {
               // dump the last of the row incase something is left in there
               if (dump_string)
               {
                  cout << row.str() << "\n";
               }
               // and then clear it
               row.str("");
            }
         }
      }
      delete databuf;

      close(sockfd);
    } while(true);  // I want to try to connect again if I can

    return 0;
}
