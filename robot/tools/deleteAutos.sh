#!/bin/bash

echo "Starting Roborio Delete Auto Script!"  

source   ./tools/rioComms.sh

getIps
getNextGoodIp
	
if [ ! -z "$goodIp" ]
then
	sshtmp admin@$goodIp rm ../lvuser/psb_settings/Auto_*.xml
fi


