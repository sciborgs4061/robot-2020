#!/bin/bash


echo "Starting Roborio SSH Script!"  



source   ./tools/rioComms.sh

getIps


# configurable vars
sleepBeforeReconnecting=5


# keep re-connecting forever
while true
do


	getNextGoodIp
	
	if [ ! -z "$goodIp" ]
	then
   		sshtmp admin@$goodIp 
	fi

	echo
	echo "Connection to Roborio failed. Retrying in $sleepBeforeReconnecting second(s)..."
	echo

	sleep $sleepBeforeReconnecting

done




