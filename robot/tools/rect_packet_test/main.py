import getopt
import socket
import struct
import sys

addr = ("127.0.0.1", 4061)
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
packets = 4
rects = 1

if len(sys.argv) > 1:
   packets = int(sys.argv[1])
if len(sys.argv) > 2:
   rects = int(sys.argv[2])

i = 1
for _ in range(packets):
   bytes = bytearray()
   for _ in range(rects):
      for _ in range(4):
         bytes += bytearray(struct.pack("i", i))
         i += 1
   sock.sendto(bytes, addr)

