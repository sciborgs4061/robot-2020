#!/usr/bin/env python3

import sys, getopt, hashlib, os
import socket, tempfile

LOCAL_MANIFESTS = { "frc"    : "manifests/frc.dir",
              "natinst": "manifests/natinst.dir" }

REMOTE_DIRS = { "frc"     : "/usr/local/frc/lib",
                "natinst" : "/usr/local/natinst/lib" ,
                "frcbin" : "/usr/local/frc/bin" ,
                "lvuser" : "/home/lvuser/" }

ROBOT_ADDRESSES = [ "172.22.11.2",
                    "10.40.61.2",
                    "10.40.61.36",
					"10.40.61.21",
                    "roboRIO-4061-FRC.local" ]


def md5sum(filename):
	hash_md5 = hashlib.md5()
	try:
		with open(filename, "rb") as f:
			for chunk in iter(lambda: f.read(4096), b""):
				hash_md5.update(chunk)

	except (FileNotFoundError, IOError):
		return None

	return hash_md5.hexdigest()


# Build the md5 sum list for the libraries
def buildLocalChecksumLists(lib_dir):
	print("######################################################")
	print("Building local manifest...")
	ret_dict = {}
	ret_dict["frc"] = {}
	ret_dict["natinst"] = {}

	with open(LOCAL_MANIFESTS["frc"]) as f:
		frc_libs = f.read().splitlines()

	with open(LOCAL_MANIFESTS["natinst"]) as f:
		natinst_libs = f.read().splitlines()

	for lib in frc_libs:
		ret_dict["frc"][lib] = md5sum(os.path.join(lib_dir, lib))

	for lib in natinst_libs:
		ret_dict["natinst"][lib] = md5sum(os.path.join(lib_dir, lib))

	print('\tSuccess!\n')

	return ret_dict

def buildRemoteChecksumList(temp_dir):
	print("######################################################")
	print("Building remote manifest...")
	ret_dict = {}
	ret_dict["frc"] = {}
	ret_dict["natinst"] = {}

	with open(os.path.join(temp_dir, "frc.dir")) as f:
		frc_libs = f.read().splitlines()

	for entry in frc_libs:
		grab = entry.rsplit('\\',1)
		if len(grab) > 1:
			pair=entry.rsplit('\\', 1)[1].split('=')
			ret_dict["frc"][pair[0]] = pair[1]
	print('\tSuccess!')
	return ret_dict

def findRoboRio():
	ret = None
	print("######################################################")
	for addr in ROBOT_ADDRESSES:
		print('Searching for RoboRIO at: {0}'.format(addr))
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		try:
			s.connect((addr, 22))
			print('\tSuccess!')
			ret = addr
		except socket.error as e:
			print('\tFailed: {0}'.format(e))
		s.close()
		print('\n')
		if ret is not None: return ret
	return ret

# Fetch the manivest file
def fetchManifestFiles(remote_host):
	credentials = "admin@" + remote_host
	temp_dir = tempfile.mkdtemp()

	print("######################################################")
	print("Pulling {0}...".format(REMOTE_MANIFESTS["frc"]))
	os.system('sshpass -p "" scp "%s:%s" "%s" > /dev/null 2>&1' % (credentials , REMOTE_MANIFESTS["frc"], os.path.join(temp_dir, "frc.dir")))
	print('\tSuccess!\n')

	return temp_dir

def getFilesToUpdate(local_manifest, remote_manifest):
	update_list = []
	for key in local_manifest["frc"].keys():
		if key in remote_manifest["frc"]:
			if local_manifest["frc"][key] != remote_manifest["frc"][key]:
				update_list.append(key)
		else:
			# Local library missing, make sure we add
			update_list.append(key)

def updateFiles(file_list):
	print("######################################################")

	if len(file_file) is 0:
		print("No files to update!")
		print("\n")
		return

def copyFileToRemote(addr, local_dir, remote_dir, file_name):
	os.system('sshpass -p "" scp "%s" "%s:%s" >/dev/null 2>&1' % (os.path.join(local_dir, file_name), 'admin@' + addr, os.path.join(remote_dir, file_name)))

def copyLibToRemote(addr, local_dir, remote_dir, file_name):
	os.system('sshpass -p "" scp "%s" "%s:%s" >/dev/null 2>&1' % (os.path.join(local_dir, file_name), 'admin@' + addr, os.path.join(remote_dir, file_name)))

def pushFRCLibs(roborio_addr, local_dir):
	with open(LOCAL_MANIFESTS["frc"]) as f:
		frc_libs = f.read().splitlines()

	print("\n\n")
	print("######################################################")
	print("Force copying all FRC Libraries")
	
	for lib in frc_libs:
		print("Copying %s..." % lib)
		copyLibToRemote(roborio_addr, local_dir, REMOTE_DIRS["frc"], lib)

def pushNILibs(roborio_addr, local_dir):
	with open(LOCAL_MANIFESTS["natinst"]) as f:
		ni_libs = f.read().splitlines()

	print("\n\n")
	print("######################################################")
	print("Force copying all National Instrument Libraries")

	for lib in ni_libs:
		print("Copying %s to %s" % (lib, REMOTE_DIRS["natinst"]))
		copyLibToRemote(roborio_addr, local_dir, REMOTE_DIRS["natinst"], lib)


def main(argv):
	local_lib_dir = None
	roborio_addr = None
	force = False
	try:
		opts, args = getopt.getopt(argv,"h",["check","lib_dir=","roborio_addr=","force_update"])
	except getopt.GetoptError:
		print('Help')
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print('test.py -i <inputfile> -o <outputfile>')
			sys.exit()
		elif opt in ("--check"):
			inputfile = "hahaha"
		elif opt in ("--lib_dir"):
			local_lib_dir = arg
		elif opt in ("--roborio_addr"):
			roborio_addr = arg
		elif opt in ("--force_update"):
			force = True
			
			

	print('\n\n')
	if roborio_addr is None:
		roborio_addr = findRoboRio()

	if (force is True):
		pushFRCLibs(roborio_addr, local_lib_dir)
		pushNILibs(roborio_addr, local_lib_dir)
		copyFileToRemote(roborio_addr, ".", REMOTE_DIRS["frcbin"], "netconsole-host")
		copyFileToRemote(roborio_addr, ".", REMOTE_DIRS["lvuser"], "robotCommand")

	else:
		local_manifest = buildLocalChecksumLists(local_lib_dir)
		remote_manifest = buildRemoteChecksumList(temp_dir)

		list_to_update = getFilesToUpdate(local_manifest, remote_manifest)

		print(list_to_update)
		print(temp_dir)

	


if __name__ == "__main__":
	main(sys.argv[1:])

