#!/bin/bash

echo "Starting Roborio Logs Script!"  

# configurable vars
sleepBeforeReconnecting=5

source   ./tools/rioComms.sh

getIps


# keep re-connecting forever
while true
do
	getNextGoodIp
	
	if [ ! -z "$goodIp" ]
	then
   		sshtmp admin@$goodIp tail -f ../lvuser/FRC_UserProgram.log
	fi
		
	echo
	echo "Connection to Roborio failed. Retrying in $sleepBeforeReconnecting second(s)..."
	echo

	sleep $sleepBeforeReconnecting

done




