#!/bin/bash
echo "Importing rio comms"


##########################
# Figure out ip stuff
######################

getIps()
{
	# configurable vars
	teamNumb="4061"  # Note: the current parse code only works on 4 digit team numbers
	
	# array of default ips to try connecting to 
	ips=("roboRIO-${teamNumb}-FRC" "10.${teamNumb:0:2}.${teamNumb:2}.2" "172.22.11.2" )
	
	# if user wants a custom ip then use that instead of default array of ips
	read -p 'Custom ip?' custIp
	
	if [ "$custIp" != "" ]
	then
		ips=($custIp)
	fi
}


# temp wrapper functions that disable security  (dangerous but probably ok for our purposes)
sshtmp()
{
    ssh -o "ConnectTimeout 3" \
        -o "StrictHostKeyChecking no" \
        -o "UserKnownHostsFile /dev/null" \
             "$@"
}
scptmp()
{
    scp -o "ConnectTimeout 3" \
        -o "StrictHostKeyChecking no" \
        -o "UserKnownHostsFile /dev/null" \
        	 "$@"
}



################################
# things that we can do
#############################


delete_auto()
{
	echo ""
	echo "+--------------------------------------------------------------------"
	echo "| deleting auto programs..."
	echo "+--------------------------------------------------------------------"
	sshtmp admin@$i rm ../lvuser/psb_settings/Auto_*.xml
}

deploy_config()
{
	echo ""
	echo "+--------------------------------------------------------------------"
	echo "| make sure the directory exists..."
	echo "+--------------------------------------------------------------------"
	sshtmp admin@${1} "mkdir -p /home/lvuser/psb_settings"
	sshtmp admin@${1} "rm -f /home/lvuser/psb_settings/*.xml"
	echo "+--------------------------------------------------------------------"
	echo "| Copying over configs..."
	echo "+--------------------------------------------------------------------"
	scptmp src/subsystems/the_robot/config/*.xml admin@${1}:/home/lvuser/psb_settings
	echo "+--------------------------------------------------------------------"
	echo "| Killing the console host program..."
	echo "+--------------------------------------------------------------------"
	sshtmp admin@${1} "killall frcUserProgram"
	echo "+--------------------------------------------------------------------"
	echo "| Starting program..."
	echo "+--------------------------------------------------------------------"
	sshtmp admin@${1} ". /etc/profile.d/natinst-path.sh; chmod a+x /home/lvuser/frcUserProgram;   /usr/local/frc/bin/frcKillRobot.sh -t -r"
	echo "+--------------------------------------------------------------------"
	echo "| done..."
	echo "+--------------------------------------------------------------------"
}


deploy_auto()
{
	echo ""
	echo "+--------------------------------------------------------------------"
	echo "| make sure the directory exists..."
	echo "+--------------------------------------------------------------------"
	sshtmp admin@${1} "mkdir -p /home/lvuser/psb_settings"
	sshtmp admin@${1} "rm -f /home/lvuser/psb_settings/Auto_*.xml"
	echo "+--------------------------------------------------------------------"
	echo "| Copying over auto programs..."
	echo "+--------------------------------------------------------------------"
	scptmp src/subsystems/the_robot/config/Auto_*.xml admin@${1}:/home/lvuser/psb_settings
	echo "+--------------------------------------------------------------------"
	echo "| Killing the console host program..."
	echo "+--------------------------------------------------------------------"
	sshtmp admin@${1} "killall frcUserProgram"
	echo "+--------------------------------------------------------------------"
	echo "| Starting program..."
	echo "+--------------------------------------------------------------------"
	sshtmp admin@${1} ". /etc/profile.d/natinst-path.sh; chmod a+x /home/lvuser/frcUserProgram;  /usr/local/frc/bin/frcKillRobot.sh -t -r"
	echo "+--------------------------------------------------------------------"
	echo "| done..."
	echo "+--------------------------------------------------------------------"
}




# finds the next valid rio ip by sshing to it
getNextGoodIp()
{
	
	# try connecting to each ip
	for i in "${ips[@]}"
	do
   		: 
   		echo "Connecting to ${i}"
   		
		if ! sshtmp -q admin@${i} exit
		then
			echo "Failed Connecting"
		else
			goodIp=$i
			echo "Found Connection!!"
			break
		fi

	done
	
}

