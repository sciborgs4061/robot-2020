#!/bin/bash

echo "Starting Roborio Deploy Auto Script!"  

source   ./tools/rioComms.sh

getIps
getNextGoodIp
	
if [ ! -z "$goodIp" ]
then
   	deploy_auto $goodIp 
fi
