// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

#define BUILD_IT
#include "PsbRobotDashstreamInterface.h"

// don't want to build it again if someone happens to
// include things again
#undef BUILD_IT

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
//#include <frc/WPILib.h>
#include "frc/TimedRobot.h"
#include "frc/livewindow/LiveWindow.h"
#include "networktables/NetworkTable.h"
#include "networktables/NetworkTableInstance.h"
#include <PsbLogger.h>
#include <PsbLogDataUdpSender.h>
#include <GlobalOiWpiStore.h>
#include <GlobalSiWpiStore.h>
#include <GlobalAiWpiStore.h>
#include <GlobalCiStore.h>
#include <GlobalIiWpiStore.h>
#include <GlobalMiStore.h>
#include <frc/smartdashboard/SmartDashboard.h>
#include <frc/Timer.h>
#include "SciBorgRobot.h"
#include "RobotSetup.h"
#include <iostream>

// /////////////////////////////////////////////////////////////////////////////
// Defines(s)
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Static Data Init
// /////////////////////////////////////////////////////////////////////////////

/// @todo
/// Make the CTOR take no arguments, and then have it internally load an xml
/// config file that tells it where to send the log data
#ifndef X86_PLATFORM
Psb::LogDataUdpSender sv_udpSender(0x0a283de0, 17654);  // 10.40.61.224:17654
#else // X86_PLATFORM
// on the x86 throw the logs to the localhost
Psb::LogDataUdpSender sv_udpSender(0x7f000001, 17654);  // 127.0.0.1:17654
#endif // X86_PLATFORM

const char* GetGitCommitHash();

// /////////////////////////////////////////////////////////////////////////////
// Namespace
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {

////////////////////////////////////////////////////////////////////////////////
SciBorgRobot::SciBorgRobot()
   : frc::TimedRobot(10_ms)
   , mp_autonomousController(NULL)
   , mp_dashstreamController(NULL)
{
   // Create all singletons
   Logger::setDataSender(&sv_udpSender);
   Logger::instance();
   PSB_LOG_DEBUG3(0);
   std::cout << "Starting SciBorgRobot code with " << GetGitCommitHash() << std::endl;
   // Create all WPI Handle Storage Objects
   Subsystem::GlobalOiWpiStore::instance();
   Subsystem::GlobalSiWpiStore::instance();
   Subsystem::GlobalAiWpiStore::instance();

   Subsystem::GlobalCiStore::instance();
   Subsystem::GlobalMiStore::instance();

   // Initialize SmartDashboard
   SmartDashboard::init();

   mp_dashstreamIntf = new Psb::Subsystem::PSBRobot::DashstreamInterface;
   m_data = (Psb::Subsystem::PSBRobot::DashstreamInterface::DashstreamPOD_t *)mp_dashstreamIntf->getData();
   memset(m_data,0,sizeof(Psb::Subsystem::PSBRobot::DashstreamInterface::DashstreamPOD_t));
			  
   mp_dashstreamController = new Subsystem::Dashstream::Controller();

   // Put the robot subsystem in the dashstream so we can watch what
   // it is doing
   mp_dashstreamController->addSubsystem(getDashstreamInterface());

   // call the robot specific function to add all the season specific
   // subsystems to the list
   AddSubsystems(m_subsystems);

   // Init the robot dashstream interface
   getDashstreamInterface()->init();

   // now that they are created...init them all...
   // and add them to the dashstream controller
   for (auto p_action : m_subsystems)
   {
      if (p_action->getDashstreamInterface() != NULL)
      {
         p_action->getDashstreamInterface()->init();
         mp_dashstreamController->addSubsystem(p_action->getDashstreamInterface());
      }
      p_action->init();
   }

   // Create autonomous and add it to the dashstream
   // We create this last because right now the auto programs are all loaded in
   // the constructor
   mp_autonomousController = new Subsystem::Autonomous::Controller();
   mp_dashstreamController->addSubsystem(mp_autonomousController->getDashstreamInterface());

   // init the mirror store to wire all the publishers and subscribers together
   Subsystem::GlobalMiStore::instance()->init();
}


////////////////////////////////////////////////////////////////////////////////
SciBorgRobot::~SciBorgRobot()
{

   delete mp_dashstreamIntf;
   mp_dashstreamIntf = NULL;

   // Delete each of the controller objects
   delete mp_dashstreamController;
   mp_dashstreamController = NULL;

   delete mp_autonomousController;
   mp_autonomousController = NULL;

   // clean up all the subsystems
   for (auto p_action : m_subsystems)
   {
      delete p_action;
   }

   delete Subsystem::GlobalAiWpiStore::instance();
   delete Subsystem::GlobalSiWpiStore::instance();
   delete Subsystem::GlobalOiWpiStore::instance();
   delete Subsystem::GlobalCiStore::instance();
}

////////////////////////////////////////////////////////////////////////////////
Psb::Subsystem::Dashstream::BaseInterface_I *
SciBorgRobot::getDashstreamInterface(void) 
{
   return mp_dashstreamIntf;
}

////////////////////////////////////////////////////////////////////////////////
void
SciBorgRobot::RobotInit()
{
  LiveWindow::GetInstance()->DisableAllTelemetry();
  PSB_LOG_DEBUG3(0);
}


////////////////////////////////////////////////////////////////////////////////
void
SciBorgRobot::DisabledInit()
{
   // At the beginning of Disabled (so every time we start or get disabled)
   // we want to reload all the settings from disk.  Calling refresh on the CI
   // store will do that.
   Subsystem::GlobalCiStore::instance()->refresh();

   // Reset all the AIs just to make sure they are all 0
   Subsystem::GlobalAiWpiStore::instance()->reset();

   mp_autonomousController->transitionToState(Psb::Subsystem::SUBSYSTEM_DISABLED);
   mp_autonomousController->setFrcMatchState(Psb::Subsystem::MATCH_STATE_DISABLED);

   // Display new state on dashboard
   strcpy(m_data->m_robot_state,"Disabled");
   m_data->m_robot_state_number = 0;
   // //////////////////////////////////////////////////////////////////////////
   // Ensure that all subystems are disabled
   // This will be overridden in Teleop mode when we actually read the real
   // values from the Button Box.  When disabled, a subsystem is expected to
   // reset its internal values to a known state.
   // //////////////////////////////////////////////////////////////////////////

   // go through and disable all the controllers
   for (auto p_action : m_subsystems)
   {
      p_action->setEnabledState(Psb::Subsystem::DISABLED);
      p_action->transitionToState(Psb::Subsystem::SUBSYSTEM_DISABLED);
   }
   Subsystem::GlobalMiStore::instance()->mirror();
}
// time from one loop start to the next
#define CAPTURE_CYCLE_TIME {\
  double currentTime = frc::Timer::GetFPGATimestamp();\
  m_data->m_robot_cycle_time = 1000*(currentTime - m_loopStatsTime); \
  m_loopStatsTime = currentTime;\
}

// time taken by the current periodic function
#define CAPTURE_LOOP_TIME {\
    m_data->m_robot_loop_time =  1000*(frc::Timer::GetFPGATimestamp() - m_loopStatsTime); \
}
////////////////////////////////////////////////////////////////////////////////
void
SciBorgRobot::DisabledPeriodic()
{
#ifndef X86_PLATFORM
   auto table = nt::NetworkTableInstance::GetDefault().GetTable("Limelight");
   table->PutNumber("ledMode", 1);
   table->PutNumber("camMode", 0);
   table->PutNumber("stream", 0);
#endif // X86_PLATFORM
   CAPTURE_CYCLE_TIME
   
   Subsystem::GlobalOiWpiStore::instance()->refresh();

   // update auto in disabled mode because it actually does work there...
   // it tracks the auto program...
   mp_autonomousController->update();

   // refresh all the sensors so we can see them on the dashstream
   // and in the logger
   Subsystem::GlobalSiWpiStore::instance()->refresh();

   // let the subsystems do something in disabled state...most will have
   // nothing todo
   for (auto p_action : m_subsystems)
   {
      p_action->update();
      Subsystem::GlobalMiStore::instance()->mirror();
   }

   // flush any indicators and the dashstream...which should really
   // start being an indicator someday
   Subsystem::GlobalIiWpiStore::instance()->flush();
   mp_dashstreamController->update();
   CAPTURE_LOOP_TIME
}


////////////////////////////////////////////////////////////////////////////////
void
SciBorgRobot::AutonomousInit()
{
   mp_autonomousController->setFrcMatchState(Psb::Subsystem::MATCH_STATE_AUTONOMOUS);
   mp_autonomousController->transitionToState(Psb::Subsystem::SUBSYSTEM_AUTOMATIC);

   // Display robot state on dashboard
   strcpy(m_data->m_robot_state,"Autonomous");
   m_data->m_robot_state_number = 1;

   // go through and change state on all the controllers
   for (auto p_action : m_subsystems)
   {
      p_action->transitionToState(Psb::Subsystem::SUBSYSTEM_AUTOMATIC);
   }
   Subsystem::GlobalMiStore::instance()->mirror();
}

////////////////////////////////////////////////////////////////////////////////
void
SciBorgRobot::AutonomousPeriodic()
{
   CAPTURE_CYCLE_TIME
   // of course...we do need to call the update function on autonomous
   mp_autonomousController->update();

   // DON'T refresh the OI's...because that is the job of the autonomous "update"

   // grab all the sensor inputs.
   Subsystem::GlobalSiWpiStore::instance()->refresh();

   // walk through the subsystems and do the work that needs done
   for (auto p_action : m_subsystems)
   {
      p_action->update();
      Subsystem::GlobalMiStore::instance()->mirror();
   }

   // now push the outputs to the robot and push indicators
   Subsystem::GlobalAiWpiStore::instance()->flush();
   Subsystem::GlobalIiWpiStore::instance()->flush();

   // Finally, send dashstream data to the dashboard laptop
   mp_dashstreamController->update();
   CAPTURE_LOOP_TIME
}


////////////////////////////////////////////////////////////////////////////////
void
SciBorgRobot::TeleopInit()
{
   // Reset all the AIs so they don't have any lingering
   // values leftover from the autonomous run
   Subsystem::GlobalAiWpiStore::instance()->reset();

   // Pull all the real values from the OIs so they are all what are
   // actively set.  We need to do this here because autonomous can
   // mess with any of the values so there is no telling what state
   // they are logically in and we want them to be what they really are.
   Subsystem::GlobalOiWpiStore::instance()->refresh();

   mp_autonomousController->setFrcMatchState(Psb::Subsystem::MATCH_STATE_TELEOPERATED);
   mp_autonomousController->transitionToState(Psb::Subsystem::SUBSYSTEM_MANUAL);

   // Display robot state on dashboard
   strcpy(m_data->m_robot_state,"Teleop");
   m_data->m_robot_state_number = 2;
   // go through and change state on all the controllers
   for (auto p_action : m_subsystems)
   {
      p_action->transitionToState(Psb::Subsystem::SUBSYSTEM_MANUAL);
   }

   Subsystem::GlobalMiStore::instance()->mirror();
}

////////////////////////////////////////////////////////////////////////////////
void
SciBorgRobot::TeleopPeriodic()
{
   CAPTURE_CYCLE_TIME  
   /// if everything is in the global OI/AI/SI stores then we
   /// we can really just update all of those...do the updates
   /// on each controller....and then flush the AI globally

   Subsystem::GlobalOiWpiStore::instance()->refresh();
   Subsystem::GlobalSiWpiStore::instance()->refresh();

   // walk through the subsystems and do the work that needs done
   for (auto p_action : m_subsystems)
   {
      p_action->update();
      Subsystem::GlobalMiStore::instance()->mirror();
   }

   // now push the outputs to the robot
   Subsystem::GlobalAiWpiStore::instance()->flush();
   Subsystem::GlobalIiWpiStore::instance()->flush();

   // dump the data so we can see it
   mp_dashstreamController->update();
   CAPTURE_LOOP_TIME
}


////////////////////////////////////////////////////////////////////////////////
void
SciBorgRobot::TestInit()
{
   // NOTE: This used to pass in TELEOPERATED...but maybe that was a bug
   mp_autonomousController->setFrcMatchState(Psb::Subsystem::MATCH_STATE_TEST);

   // Display robot state on dashboard
   strcpy(m_data->m_robot_state,"Test");
   m_data->m_robot_state_number = 3;
   PSB_LOG_DEBUG3(0);
}


////////////////////////////////////////////////////////////////////////////////
void
SciBorgRobot::TestPeriodic()
{
   CAPTURE_CYCLE_TIME  
   PSB_LOG_DEBUG3(0);
   CAPTURE_LOOP_TIME
}

} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////
