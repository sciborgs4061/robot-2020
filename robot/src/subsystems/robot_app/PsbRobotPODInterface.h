// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

START_NEW_SS(PSBRobot)

   SS_CHAR32_POD_DATA(m_robot_state, "SS",
                      "Robot State", "State of the robot")

   SS_POD_PUBLISH_INT32(m_robot_state_number,"robot_state",
                       "Robot State Number", "Numeric State of the robot")

   SS_DOUBLE_POD_DATA(m_robot_cycle_time, "SS",
                      "Cycle Time", "Time from start of one processing cycle to the next")

   SS_DOUBLE_POD_DATA(m_robot_loop_time, "SS",
                      "Loop Time", "Time actually used in the processing cycle")

  
  
  
   SS_POD_PUBLISH_DOUBLE(vision_distance_to_target, "vision_distance_to_target",
                      "calculated distance to target", "calculated distance to target (zero if no target)")

   SS_POD_PUBLISH_INT32(arm_lift_encoder_publish, "arm_lift_encoder_publish",
                  "arm lift encoder publish", "DIO 4, DIO 5 ")
   SS_POD_PUBLISH_INT32(arm_tilt_encoder_publish, "arm_tilt_encoder_publish",
                  "arm tilt encoder publish", "DIO 7, DIO 8 ")
   // pull in the PDP diagnostics for this years robot
#define PDP_DIAGNOSTICS
   #include "TheRobotMappings.h"
#undef PDP_DIAGNOSTICS

END_SS



