// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __PsbMovingAverage_h__
#define __PsbMovingAverage_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbMacros.h>


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This class provides a simple interface to track the passage of time.
////////////////////////////////////////////////////////////////////////////////
class MovingAverage
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief 
      /// Constructor
      ///
      /// @param[in] num_values   Number of values to average over
      //////////////////////////////////////////////////////////////////////////
      MovingAverage(int32_t num_values = 15)
         : m_num_values(num_values),
           m_running_total(0.0)
      {
      }

      //////////////////////////////////////////////////////////////////////////
      /// @brief 
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      ~MovingAverage(void)
      {
      }

      //////////////////////////////////////////////////////////////////////////
      /// @brief 
      /// Allows you to set the number of values, after construction
      ///
      /// @param[in] num_values   Number of values to average over
      //////////////////////////////////////////////////////////////////////////
      void config(int32_t num_values)
      {
         m_num_values = num_values;
      }

      //////////////////////////////////////////////////////////////////////////
      /// @brief 
      /// Takes a new value, and returns the new moving average
      ///
      /// @param[in] max_num_values   Number of values to average over
      //////////////////////////////////////////////////////////////////////////
      double update(double new_value)
      {
         if (m_values.size() >= m_num_values)
         {
            // subtract what is on the front if we have too many items
            m_running_total -= m_values.front();
            m_values.pop_front();
         }

         m_running_total += new_value;
         m_values.push_back(new_value);

         return m_running_total/(double)m_values.size();
      }

   private:
      //////////////////////////////////////////////////////////////////////////
      /// @brief 
      /// The maximum number of values that you will calculate the average
      /// over
      //////////////////////////////////////////////////////////////////////////
      uint32_t m_num_values;

      //////////////////////////////////////////////////////////////////////////
      /// @brief 
      /// The sum of all the values
      //////////////////////////////////////////////////////////////////////////
      double m_running_total;

      //////////////////////////////////////////////////////////////////////////
      /// @brief 
      /// A list of all the values
      //////////////////////////////////////////////////////////////////////////
      std::list<double> m_values;

   private:
      DISALLOW_COPY_CTOR(MovingAverage);
      DISALLOW_ASSIGNMENT_OPER(MovingAverage);
}; // END class MovingAverage


} // END Psb Namespace


#endif // __PsbMovingAverage_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

