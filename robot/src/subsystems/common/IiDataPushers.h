////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __IiDataPushers_h__
#define __IiDataPushers_h__

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <frc/SPI.h>
#include <PsbLogger.h>
#include "PsbMacros.h"
#include "PsbSubsystemTypes.h"

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////  
namespace Psb {
namespace Subsystem {

////////////////////////////////////////////////////////////////////////////////
/// @brief 
/// Provides an interface for the AI to push data to the physical objects
///   from the controllers that have the values to push.
////////////////////////////////////////////////////////////////////////////////
class IiDataPusher
{
   public:
      IiDataPusher(void) {}
      virtual ~IiDataPusher(void) {}

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   The function that will actually push the data
      ///     must be overridden by the derived class
      ////////////////////////////////////////////////////////////////////////// 
      virtual void pushData(void) = 0;

   private:
      DISALLOW_COPY_CTOR(IiDataPusher);
      DISALLOW_ASSIGNMENT_OPER(IiDataPusher);
}; // END class IiDataGrabber


////////////////////////////////////////////////////////////////////////////////
/// @todo
/// Document me!!!
///
/// @todo
/// Should this really be part of a common data type pusher?
////////////////////////////////////////////////////////////////////////////////
class LPD8806LedStripDataPusher : public IiDataPusher
{
   public:
      ////////////////////////////////////////////////////////////////////////// 
      LPD8806LedStripDataPusher(void *mem) 
        : m_memory(mem)
        , m_spiDevice(nullptr)
      {
      }

      ////////////////////////////////////////////////////////////////////////// 
      void setSpiDevice(frc::SPI * spiDevice)
      {
          m_spiDevice = spiDevice;
      }

      ////////////////////////////////////////////////////////////////////////// 
      virtual ~LPD8806LedStripDataPusher(void) {}

      ////////////////////////////////////////////////////////////////////////// 
      virtual void pushData(void)
      {
         // The memory coming in is a pointer to a mem-block structure
         memblk_t *p_memblk = reinterpret_cast<memblk_t *>(m_memory);

         // Push the data out to the SPI device
         m_spiDevice->Write(p_memblk->block, p_memblk->length);
      }

   private:
      void *m_memory;
      frc::SPI *m_spiDevice;

   private:
      DISALLOW_COPY_CTOR(LPD8806LedStripDataPusher);
      DISALLOW_ASSIGNMENT_OPER(LPD8806LedStripDataPusher);
}; // END class LPD8806LedStripDataPusher


} // END Subsystem namespace
} // END Psb namespace

#undef FILE_NUM // 0xACACACAC

#endif // __IiDataPushers_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// ///////////////////////////////////////////////////////////////////////////// 

