////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __SiDataGrabbers_h__
#define __SiDataGrabbers_h__

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
//#include <frc/WPILib.h>
#include <AHRS.h>
#include "PsbMacros.h"
#include "I2CBreakout.h"
#include "frc/AnalogInput.h"
#include "frc/AnalogPotentiometer.h"
#include "frc/DigitalInput.h"
#include "frc/Encoder.h"
#include "frc/PowerDistributionPanel.h"
#include "frc/I2C.h"

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////  
namespace Psb {
namespace Subsystem {

////////////////////////////////////////////////////////////////////////////////
/// @brief 
/// Provides an interface for the OI to grab data from physical objects
/// and put that data where it needs to go
////////////////////////////////////////////////////////////////////////////////
class SiDataGrabber
{
   public:
      SiDataGrabber(void) {}
      virtual ~SiDataGrabber(void) {}

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   The function that will actually grab the data from the sensor.
      ///   Must be overridden by the derived class
      ////////////////////////////////////////////////////////////////////////// 
      virtual void grabData(void) = 0;

   private:
      DISALLOW_COPY_CTOR(SiDataGrabber);
      DISALLOW_ASSIGNMENT_OPER(SiDataGrabber);

}; // END class SiDataGrabber

////////////////////////////////////////////////////////////////////////////////
/// @brief
///   Class to grab data from a DigitalInput (like a LimitSwitch).
////////////////////////////////////////////////////////////////////////////////
class DigitalInputDataGrabber : public SiDataGrabber
{
   public:
      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Constructor for DigitalInputDataGrabber.
      ////////////////////////////////////////////////////////////////////////// 
      DigitalInputDataGrabber(bool *mem) 
        : m_memory(mem)
        , m_digitalInput(NULL)
      {
         // left blank on purpose
      }

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Set the DigitalInput of the data grabber.
      ////////////////////////////////////////////////////////////////////////// 
      void setDigitalInput(DigitalInput *di)
      {
         m_digitalInput = di;
      }

      ////////////////////////////////////////////////////////////////////////// 
      virtual ~DigitalInputDataGrabber(void) {}

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   the function that will actually grab the data
      ////////////////////////////////////////////////////////////////////////// 
      virtual void grabData(void)
      {
         (*((bool *)m_memory)) = m_digitalInput->Get();
      }

   private:
      DISALLOW_COPY_CTOR(DigitalInputDataGrabber);
      DISALLOW_ASSIGNMENT_OPER(DigitalInputDataGrabber);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   The memory location that we store the value in
      ////////////////////////////////////////////////////////////////////////// 
      void *m_memory;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   The pointer to the digitalInput
      ////////////////////////////////////////////////////////////////////////// 
      DigitalInput *m_digitalInput;

}; // END class DigitalInputDataGrabber

////////////////////////////////////////////////////////////////////////////////
/// @brief
///   Class to grab data from an Encoder.
////////////////////////////////////////////////////////////////////////////////
class EncoderDataGrabber : public SiDataGrabber
{
   public:

      typedef enum {
           RATE
         , COUNT
      } wanted_data_t;

      typedef enum {
         DOUBLE_DATA,
         UINT_DATA
      } data_type_t;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Constructor for EncoderDataGrabber.
      ////////////////////////////////////////////////////////////////////////// 
      EncoderDataGrabber(int32_t *mem, wanted_data_t wanted) 
        : m_memory(mem)
        , m_encoder(NULL)
        , m_wanted(wanted)
      {
         // left blank on purpose
      }

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Constructor for EncoderDataGrabber.
      ////////////////////////////////////////////////////////////////////////// 
      EncoderDataGrabber(double *mem, wanted_data_t wanted) 
        : m_memory(mem)
        , m_encoder(NULL)
        , m_wanted(wanted)
      {
         // left blank on purpose
      }

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Set the encoder of the data grabber.
      ////////////////////////////////////////////////////////////////////////// 
      void setEncoder(Encoder *encoder)
      {
         m_encoder = encoder;
      }

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Destructor for EncoderDataGrabber.
      ////////////////////////////////////////////////////////////////////////// 
      virtual ~EncoderDataGrabber(void) {}

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Function that will grab the data.
      ////////////////////////////////////////////////////////////////////////// 
      virtual void grabData(void)
      {
         switch (m_wanted)
         {
            case COUNT:
            {
               (*((int32_t *)m_memory)) = m_encoder->Get();
               break;
            }
            case RATE:
            {
               double rate = m_encoder->GetRate();
               if (rate > -10000.0 && rate < 10000.0)
               {
                  (*((double *)m_memory)) = m_encoder->GetRate();
               }
               else
               {
                  (*((double *)m_memory)) = 0;
               }
               break;
            }
            // default:
         }
    
      }

   private:
      DISALLOW_COPY_CTOR(EncoderDataGrabber);
      DISALLOW_ASSIGNMENT_OPER(EncoderDataGrabber);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   The memory location that we store the value in.
      ////////////////////////////////////////////////////////////////////////// 
      void *m_memory;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   The pointer to the Encoder.
      ////////////////////////////////////////////////////////////////////////// 
      Encoder *m_encoder;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   The pointer to the Encoder.
      ////////////////////////////////////////////////////////////////////////// 
      wanted_data_t m_wanted;

}; // END class EncoderDataGrabber

////////////////////////////////////////////////////////////////////////////////
/// @brief
///   Class to grab data from an AnalogInput.
////////////////////////////////////////////////////////////////////////////////
class AnalogInputDataGrabber : public SiDataGrabber
{
   public:
      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Constructor for AnalogInputDataGrabber.
      ////////////////////////////////////////////////////////////////////////// 
      AnalogInputDataGrabber(int32_t *mem) 
        : m_memory(mem)
        , m_analogInput(NULL)
      {
         // left blank on purpose
      }

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Set the AnalogInput of the data grabber.
      //////////////////////////////////////////////////////////////////////////
      void setInput(AnalogInput *analogInput)
      {
         m_analogInput = analogInput;
      }

      ////////////////////////////////////////////////////////////////////////// 
      virtual ~AnalogInputDataGrabber(void) {}

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Function that will grab the data.
      ////////////////////////////////////////////////////////////////////////// 
      virtual void grabData(void)
      {
         (*((int32_t *)m_memory)) = m_analogInput->GetValue();
      }

   private:
      DISALLOW_COPY_CTOR(AnalogInputDataGrabber);
      DISALLOW_ASSIGNMENT_OPER(AnalogInputDataGrabber);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   The memory location that we store the value in.
      ////////////////////////////////////////////////////////////////////////// 
      void *m_memory;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   The pointer to the AnalogInput from WPILib.
      ////////////////////////////////////////////////////////////////////////// 
      AnalogInput *m_analogInput;

}; // END class AnalogInputDataGrabber

////////////////////////////////////////////////////////////////////////////////
/// @brief
///   Class to grab data from an AnalogPotentiometer.
////////////////////////////////////////////////////////////////////////////////
class AnalogPotDataGrabber : public SiDataGrabber
{
   public:
      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Constructor for AnalogPotDataGrabber.
      ////////////////////////////////////////////////////////////////////////// 
      AnalogPotDataGrabber(double *mem) 
        : m_memory(mem)
        , m_analogPot(NULL)
      {
         // left blank on purpose
      }

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Set the AnalogPotentiometer of the data grabber.
      //////////////////////////////////////////////////////////////////////////
      void setInput(AnalogPotentiometer *analogPot)
      {
         m_analogPot = analogPot;
      }

      ////////////////////////////////////////////////////////////////////////// 
      virtual ~AnalogPotDataGrabber(void) {}

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Function that will grab the data.
      ////////////////////////////////////////////////////////////////////////// 
      virtual void grabData(void)
      {
         (*((double *)m_memory)) = m_analogPot->Get();
      }

   private:
      DISALLOW_COPY_CTOR(AnalogPotDataGrabber);
      DISALLOW_ASSIGNMENT_OPER(AnalogPotDataGrabber);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   The memory location that we store the value in.
      ////////////////////////////////////////////////////////////////////////// 
      void *m_memory;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   The pointer to the AnalogPotentiometer from WPILib.
      ////////////////////////////////////////////////////////////////////////// 
      AnalogPotentiometer *m_analogPot;

}; // END class AnalogPotDataGrabber

////////////////////////////////////////////////////////////////////////////////
/// @brief
///   Class to grab data from a Navigation Sensor.
////////////////////////////////////////////////////////////////////////////////
class NavigationDataGrabber : public SiDataGrabber
{
   public:

      // the order of these matter...don't change them
      typedef enum {
           IS_CONNECTED
         , IS_MOVING
         , IS_CALIBRATING
         , YAW
         , PITCH
         , ROLL
         , ADJUSTED_ANGLE // home grown angle calculation
         , SNAPSHOTTED_ANGLE // a snapshot of the homegrown angle
         , ANGLE
         , ANGULAR_VELOCITY
         , ACCELERATION_X
         , ACCELERATION_Y
         , VELOCITY_X
         , VELOCITY_Y
         , DISPLACEMENT_X
         , DISPLACEMENT_Y
      } wanted_data_t;

      typedef enum {
         DOUBLE_DATA,
         BOOL_DATA
      } data_type_t;

      static uint8_t const UPDATE_RATE_HZ = 200;

      ////////////////////////////////////////////////////////////////////////// 
      NavigationDataGrabber( double *mem,
                          wanted_data_t wanted)
        : m_memory(mem)
        , m_wanted_data(wanted)
        , m_type(DOUBLE_DATA)
        , m_navigation(NULL)
      {
         // left blank on purpose
      }

      ////////////////////////////////////////////////////////////////////////// 
      NavigationDataGrabber( bool *mem,
                          wanted_data_t wanted)
        : m_memory(mem)
        , m_wanted_data(wanted)
        , m_type(BOOL_DATA)
        , m_navigation(NULL)
      {
         // left blank on purpose
      }

      ////////////////////////////////////////////////////////////////////////// 
      void setNavigation(AHRS *nav)
      {
         m_navigation = nav;
      }

      ////////////////////////////////////////////////////////////////////////// 
      virtual ~NavigationDataGrabber(void) {}

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   the function that will actually grab the data
      ///   must be overridden by the derived class
      ////////////////////////////////////////////////////////////////////////// 
      virtual void grabData(void)
      {
#ifndef X86_PLATFORM
         // If this is a new update, update the displacement.
         double update_count = m_navigation->GetUpdateCount();
         if (update_count > s_old_update_count)
         {
            // If a new message arrived, update the linear displacement.
            m_navigation->UpdateDisplacement(m_navigation->GetWorldLinearAccelX(),
                                             m_navigation->GetWorldLinearAccelY(),
                                             UPDATE_RATE_HZ,
                                             m_navigation->IsMoving());

            s_old_update_count = update_count;
         }

         /*
         printf("grabData:%f,%d,%d,%d,%f\n",
               update_count,
               m_navigation->IsConnected(),
               m_navigation->IsMoving(),
               m_navigation->IsCalibrating(),
               m_navigation->GetAngle());
         */
         switch (m_wanted_data)
         {
            case IS_CONNECTED:
              (*((bool *)m_memory)) = m_navigation->IsConnected();
              break;
            case IS_MOVING:
              (*((bool *)m_memory)) = m_navigation->IsMoving();
              break;
            case IS_CALIBRATING:
              (*((bool *)m_memory)) = m_navigation->IsCalibrating();
              break;
            case YAW:
              (*((double *)m_memory)) = (double)m_navigation->GetYaw();
              break;
            case PITCH:
              (*((double *)m_memory)) = (double)m_navigation->GetPitch();
              break;
            case ROLL:
              (*((double *)m_memory)) = (double)m_navigation->GetRoll();
              break;
            case ADJUSTED_ANGLE:
              (*((double *)m_memory)) = getAdjustedAngle();
              break;
            case SNAPSHOTTED_ANGLE:
              (*((double *)m_memory)) = s_angle_snapshot;
              break;
            case ANGLE:
              (*((double *)m_memory)) = (double)m_navigation->GetAngle();
              break;
            case ANGULAR_VELOCITY:
              (*((double *)m_memory)) = (double)m_navigation->GetRate();
              break;
            case ACCELERATION_X:
              (*((double *)m_memory)) = (double)m_navigation->GetWorldLinearAccelX();
              break;
            case ACCELERATION_Y:
              (*((double *)m_memory)) = (double)m_navigation->GetWorldLinearAccelY();
              break;
            case VELOCITY_X:
              (*((double *)m_memory)) = (double)m_navigation->GetVelocityX();
              break;
            case VELOCITY_Y:
              (*((double *)m_memory)) = (double)m_navigation->GetVelocityY();
              break;
            case DISPLACEMENT_X:
              (*((double *)m_memory)) = (double)m_navigation->GetDisplacementX();
              break;
            case DISPLACEMENT_Y:
              (*((double *)m_memory)) = (double)m_navigation->GetDisplacementY();
              break;
            // default:
         };
#endif // X86_PLATFORM	 
      }

      ////////////////////////////////////////////////////////////////////////// 
      void setValue(bool data)
      {
         if (m_type == BOOL_DATA)
         {
            (*((bool *)m_memory)) = data;
         } else {
            assert(false);
         }
      }

      ////////////////////////////////////////////////////////////////////////// 
      void setValue(double data)
      {
         if (m_type == DOUBLE_DATA)
         {
            (*((double *)m_memory)) = data;
         } else {
            assert(false);
         }
      }

      ////////////////////////////////////////////////////////////////////////// 
      void snapshot(void)
      {
         s_angle_snapshot = getAdjustedAngle();
      }

      ////////////////////////////////////////////////////////////////////////// 
      static double get_snapshot(void)
      {
         return s_angle_snapshot;
      }

   private:
      DISALLOW_COPY_CTOR(NavigationDataGrabber);
      DISALLOW_ASSIGNMENT_OPER(NavigationDataGrabber);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   This function deals with zero crossings for the gyro angle
      ///   and produces a total rotation value that doesn't roll over
      ///   which is really what you want when you just want to turn
      ///   by some number of degrees.
      ////////////////////////////////////////////////////////////////////////// 
      double getAdjustedAngle(void)
      {
         double cur_angle = 0.0;
#ifndef X86_PLATFORM	 
	 cur_angle = (double)m_navigation->GetAngle();
#endif // X86_PLATFORM
	 
         // first time...so init
         if (s_last_angle >= 600.0)
         {
            s_adjusted_angle = cur_angle;
            s_last_angle = cur_angle;
            s_zero_crossing_adjustment = 0.0;
         }
         else
         {
            // Need to look for going from the 300's to just above (or equal) zero
            // which is a zero crossing in the forward direction
            // We are in the upper range and close to a zero crossing
            // I have a range in here because if we are really turning
            // quickly  I would worry about missing the window.  Of couse
            // you can't just do m_last_angle <= 360 beucase 0 matches.
            if (s_last_angle <= 360.0 && s_last_angle > 300.0)
            {
               // also check a range here...because 360 > 0
               if (cur_angle >= 0.0 && cur_angle < 50.0)
               {
                  // we have crossed 360 back to 0...so deal with it
                  s_zero_crossing_adjustment += 360.0;
               }
            }

            // Need to look for going from lower 50's to just under (or equal) 360
            // which is a zero crossing in the backwards direction
            // We are in the lower range and close to a zero crossing
            // I have a range in here because if we are really turning
            // quickly  I would worry about missing the window.  Of couse
            // you can't just do m_last_angle >= 0 beucase 360 matches.
            if (s_last_angle >= 0.0 && s_last_angle < 50.0)
            {
               // also check a range here...because 0 is < 360
               if (cur_angle <= 360.0 && cur_angle >= 300.0)
               {
                  // we have crossed 0 back to 360...so deal with it
                  s_zero_crossing_adjustment += -360.0;
               }
            }
         }

         s_last_angle = cur_angle;

         s_adjusted_angle = cur_angle + s_zero_crossing_adjustment;

         return s_adjusted_angle;
      }


      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   The memory block that we store the values in.
      ////////////////////////////////////////////////////////////////////////// 
      void *m_memory;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   type of the data that we want out of the joystick
      ////////////////////////////////////////////////////////////////////////// 
      wanted_data_t m_wanted_data;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   type of the data
      ////////////////////////////////////////////////////////////////////////// 
      data_type_t m_type;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   adjjusted angle value...used when calcing adjusted angle to remove
      ///   zero crossings
      ////////////////////////////////////////////////////////////////////////// 
      static double s_adjusted_angle;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   last angle value...used to calc adjusted angle to remove zero
      //    crossings
      ////////////////////////////////////////////////////////////////////////// 
      static double s_last_angle;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   this is the adjustment to handle zero crossings.
      ////////////////////////////////////////////////////////////////////////// 
      static double s_zero_crossing_adjustment;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   old update count
      ////////////////////////////////////////////////////////////////////////// 
      static double s_old_update_count;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   a place to store a snapshot of an angle for anyone to use later
      ////////////////////////////////////////////////////////////////////////// 
      static double s_angle_snapshot;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   The pointer to the Navigation sensor.
      ////////////////////////////////////////////////////////////////////////// 
      AHRS *m_navigation;

}; // END class NavDataGrabber

////////////////////////////////////////////////////////////////////////////////
/// @brief
///   Class to grab data from the PDP
////////////////////////////////////////////////////////////////////////////////
class PDPDataGrabber : public SiDataGrabber
{
   public:
      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Constructor for AnalogInputDataGrabber.
      ////////////////////////////////////////////////////////////////////////// 
      PDPDataGrabber(double *mem, uint8_t channel)
        : m_memory(mem)
        , m_channel(channel)
      {
         // left blank on purpose
      }

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Set the AnalogInput of the data grabber.
      //////////////////////////////////////////////////////////////////////////
      void setPDP(PowerDistributionPanel *pdp)
      {
         m_pdp = pdp;
      }

      ////////////////////////////////////////////////////////////////////////// 
      virtual ~PDPDataGrabber(void) {}

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Function that will grab the data.
      ////////////////////////////////////////////////////////////////////////// 
      virtual void grabData(void)
      {
         (*((double *)m_memory)) = m_pdp->GetCurrent(m_channel);
      }

   private:
      DISALLOW_COPY_CTOR(PDPDataGrabber);
      DISALLOW_ASSIGNMENT_OPER(PDPDataGrabber);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   The memory location that we store the value in.
      ////////////////////////////////////////////////////////////////////////// 
      void *m_memory;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   The channel number
      ////////////////////////////////////////////////////////////////////////// 
      uint8_t m_channel;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   The pointer to the AnalogInput from WPILib.
      ////////////////////////////////////////////////////////////////////////// 
      PowerDistributionPanel *m_pdp;
}; // END class PDPDataGrabber

} // END Subsystem namespace

} // END Psb namespace


#endif //__SiDataGrabbers_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// ///////////////////////////////////////////////////////////////////////////// 

