

#include "SiDataGrabbers.h"

namespace Psb {
namespace Subsystem {

double NavigationDataGrabber::s_adjusted_angle = 0.0;
double NavigationDataGrabber::s_angle_snapshot = 0.0;
double NavigationDataGrabber::s_last_angle = 700.0;
double NavigationDataGrabber::s_zero_crossing_adjustment = 0.0;

} // END Subsystem namespace
} // END Psb namespace


