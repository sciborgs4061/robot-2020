// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#include "PsbSlewController.h"

// #include <iostream>
// using namespace std;

///////////////////////////////////////////////////////////////////////////////
PsbSlewController::PsbSlewController(double deadband,
                                     double slowdown_distance,
                                     double speed_min,
                                     double speed_max,
                                     double zero_point)
   : m_zero_point(zero_point)
   , m_deadband(deadband)
   , m_slowdown_distance(slowdown_distance)
   , m_speed_min(speed_min)
   , m_speed_max(speed_max)
{
}

///////////////////////////////////////////////////////////////////////////////
void PsbSlewController::config(double deadband,
                               double slowdown_distance,
                               double speed_min,
                               double speed_max,
                               double zero_point)
{
   m_zero_point = zero_point;
   m_deadband = deadband;
   m_slowdown_distance = slowdown_distance;
   m_speed_min = speed_min;
   m_speed_max = speed_max;
}

///////////////////////////////////////////////////////////////////////////////
double PsbSlewController::go(double desired_pos, double curr_pos)
{
   double motor_speed = 0.0;
   double calculated_position = curr_pos - m_zero_point;

   // calculate the distance away from the desired position
   double distance_away = calculated_position - desired_pos;

   // this is here to keep the code clean, but track the true direction
   // we need to be moving...assume forwards
   double direction_fix = -1.0;

   // make the value positive if it isn't
   if (distance_away < 0)
   {
      distance_away *= -1;
      // but now we want to go backwards
      direction_fix = 1.0;
   }

   // default motor multiplier to 1 and adjust if we are close to the target
   double motor_mult = 1.0;

   // calculate a motor scaling to slow the motor if we are approaching
   // the deadband
   if (distance_away <= m_slowdown_distance)
   {
      motor_mult = distance_away / m_slowdown_distance;
   }

   // check the deadband
   if (distance_away <= m_deadband)
   {
      // we are within the the deadband...stop the motor
      motor_speed = 0.0;
      // cerr << "LOCKED IN: " << endl;
   }
   else
   {
      // if you are not in the deadband...you need to move the motor
      // the right speed
      motor_speed = m_speed_max * motor_mult;

      // check if we are below our min speed and adjust if needed
      if (motor_speed < m_speed_min)
      {
         motor_speed = m_speed_min;
      }

      // now make it go the right direction
      motor_speed *= direction_fix;
   }

   /*
   cerr << "DistAway: " << distance_away << endl;
   cerr << "MotorMult: " << motor_mult << endl;
   cerr << "MotorSpeed: " << motor_speed << endl;
   cerr << "CalcPos: " << calculated_position << endl;
   cerr << "desired_pos: " << desired_pos << endl;
   cerr << "curr_pos: " << curr_pos << endl;
   cerr << "slowdown: " << m_slowdown_distance << endl;
   cerr << "deadband: " << m_deadband << endl;
   cerr << "minspeed: " << m_speed_min << endl;
   cerr << "maxspeed: " << m_speed_max << endl;
   cerr << endl;
   */

   return motor_speed;
}

