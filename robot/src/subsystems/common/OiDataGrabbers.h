////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __OiDataGrabbers_h__
#define __OiDataGrabbers_h__

////////////////////////////////////////////////////////////////////////////////
///
/// @todo
/// The fact that this file implements all of its classes in the interface
/// overly imposes the WPI interface onto the supposedly WPI-agnostic clients.
/// This implementation should be hidden in a .cpp file with ZERO WPI interfaces
/// pulled into this file.
///
////////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string>
#include <assert.h>

#include <frc/Joystick.h>
#include <frc/DriverStation.h>

#include <PsbLogger.h>
#include <PsbSubsystemTypes.h>
#include <PsbStringConverter.h>
#include <PsbMacros.h>

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////  
namespace Psb {
namespace Subsystem {

////////////////////////////////////////////////////////////////////////////////
///
/// Macro to use when hooking into the OI Global Store
///
///  Example:
///     GlobalOiWpiStore *oi = GlobalOiWpiStore::instance();
///     oi->attach(JOYSTICK_DG(sstest1_throttle, RAW_AXIS_1, JOYSTICK_ZERO));
///
////////////////////////////////////////////////////////////////////////////////
#define JOYSTICK_DG(var_name,thing_to_grab,joystick_name) \
             new JoystickDataGrabber(#var_name, &(m_data->var_name), \
                                     JoystickDataGrabber::thing_to_grab), \
             GlobalOiWpiStore::joystick_name

////////////////////////////////////////////////////////////////////////////////
/// @brief 
/// Provides an interface for the OI to grab data from physical objects
/// and put that data where it needs to go
////////////////////////////////////////////////////////////////////////////////
class OiDataGrabber
{
   public:
      OiDataGrabber(std::string const &name)
      : m_name(name)
      {}
      virtual ~OiDataGrabber(void) {}

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   returns the name of the grabber
      ////////////////////////////////////////////////////////////////////////// 
      std::string const &getName(void) const
      {
         return m_name;
      }

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   the function that will actually grab the data
      ///   must be overridden by the derived class
      ////////////////////////////////////////////////////////////////////////// 
      virtual void grabData(void) = 0;

   private:

      std::string const m_name;

      DISALLOW_COPY_CTOR(OiDataGrabber);
      DISALLOW_ASSIGNMENT_OPER(OiDataGrabber);
}; // END class OiDataGrabber


////////////////////////////////////////////////////////////////////////////////
/// @brief
///   Class to grab data from a Joystick
////////////////////////////////////////////////////////////////////////////////
class JoystickDataGrabber : public OiDataGrabber
{
   public:
      friend class JoystickDataFaker;

      // the order of these matter...don't change them
      typedef enum {
           RAW_AXIS_0    
         , RAW_AXIS_1   
         , RAW_AXIS_2   
         , RAW_AXIS_3 
         , RAW_AXIS_4
         , RAW_AXIS_5
         , RAW_BUTTON_1
         , RAW_BUTTON_2
         , RAW_BUTTON_3
         , RAW_BUTTON_4
         , RAW_BUTTON_5
         , RAW_BUTTON_6
         , RAW_BUTTON_7
         , RAW_BUTTON_8
         , RAW_BUTTON_9
         , RAW_BUTTON_10
         , RAW_BUTTON_11
         , RAW_BUTTON_12
         , RAW_BUTTON_13
         , RAW_BUTTON_14
         , RAW_BUTTON_15
         , RAW_BUTTON_16
      } wanted_data_t;

      typedef enum {
         DOUBLE_DATA,
         BOOL_DATA
      } data_type_t;

      ////////////////////////////////////////////////////////////////////////// 
      JoystickDataGrabber(std::string const &name,
                          double *mem,
                          wanted_data_t wanted)
        : OiDataGrabber(name)
        , m_memory(mem)
        , m_wanted_data(wanted)
        , m_type((m_wanted_data < RAW_BUTTON_1) ? DOUBLE_DATA : BOOL_DATA)
        , m_joystick(NULL)
      {
         // left blank on purpose
      }

      ////////////////////////////////////////////////////////////////////////// 
      JoystickDataGrabber(std::string const &name,
                          bool *mem,
                          wanted_data_t wanted)
        : OiDataGrabber(name)
        , m_memory(mem)
        , m_wanted_data(wanted)
        , m_type((m_wanted_data < RAW_BUTTON_1) ? DOUBLE_DATA : BOOL_DATA)
        , m_joystick(NULL)
      {
         // left blank on purpose
      }

      ////////////////////////////////////////////////////////////////////////// 
      void setJoystick(frc::Joystick *joy)
      {
         m_joystick = joy;
      }

      ////////////////////////////////////////////////////////////////////////// 
      virtual ~JoystickDataGrabber(void) {}

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   the function that will actually grab the data
      ///   must be overridden by the derived class
      ////////////////////////////////////////////////////////////////////////// 
      virtual void grabData(void)
      {
         // boy...this switch is kinda lame...we should fix it.
         // just make the values for the functions calls be based
         // off the m_wanted_data enum
         switch (m_wanted_data)
         {
            case RAW_AXIS_0:
              (*((double *)m_memory)) = (double)m_joystick->GetRawAxis(0);
              break;
            case RAW_AXIS_1:
              (*((double *)m_memory)) = (double)m_joystick->GetRawAxis(1);
              break;
            case RAW_AXIS_2:
              (*((double *)m_memory)) = (double)m_joystick->GetRawAxis(2);
              break;
            case RAW_AXIS_3:
              (*((double *)m_memory)) = (double)m_joystick->GetRawAxis(3);
              break;
            case RAW_AXIS_4:
              (*((double *)m_memory)) = (double)m_joystick->GetRawAxis(4);
              break;
            case RAW_AXIS_5:
              (*((double *)m_memory)) = (double)m_joystick->GetRawAxis(5);
              break;
            case RAW_BUTTON_1:
              (*((bool *)m_memory)) = m_joystick->GetRawButton(1);
              break;
            case RAW_BUTTON_2:
              (*((bool *)m_memory)) = m_joystick->GetRawButton(2);
              break;
            case RAW_BUTTON_3:
              (*((bool *)m_memory)) = m_joystick->GetRawButton(3);
              break;
            case RAW_BUTTON_4:
              (*((bool *)m_memory)) = m_joystick->GetRawButton(4);
              break;
            case RAW_BUTTON_5:
              (*((bool *)m_memory)) = m_joystick->GetRawButton(5);
              break;
            case RAW_BUTTON_6:
              (*((bool *)m_memory)) = m_joystick->GetRawButton(6);
              break;
            case RAW_BUTTON_7:
              (*((bool *)m_memory)) = m_joystick->GetRawButton(7);
              break;
            case RAW_BUTTON_8:
              (*((bool *)m_memory)) = m_joystick->GetRawButton(8);
              break;
            case RAW_BUTTON_9:
              (*((bool *)m_memory)) = m_joystick->GetRawButton(9);
              break;
            case RAW_BUTTON_10:
              (*((bool *)m_memory)) = m_joystick->GetRawButton(10);
              break;
            case RAW_BUTTON_11:
              (*((bool *)m_memory)) = m_joystick->GetRawButton(11);
              break;
            case RAW_BUTTON_12:
              (*((bool *)m_memory)) = m_joystick->GetRawButton(12);
              break;
            case RAW_BUTTON_13:
              (*((bool *)m_memory)) = m_joystick->GetRawButton(13);
              break;
            case RAW_BUTTON_14:
              (*((bool *)m_memory)) = m_joystick->GetRawButton(14);
              break;
            case RAW_BUTTON_15:
              (*((bool *)m_memory)) = m_joystick->GetRawButton(15);
              break;
            case RAW_BUTTON_16:
              (*((bool *)m_memory)) = m_joystick->GetRawButton(16);
              break;
            // default:
         };
      }

      ////////////////////////////////////////////////////////////////////////// 
      void setValue(bool data)
      {
         if (m_type == BOOL_DATA)
         {
            (*((bool *)m_memory)) = data;
         } else {
            assert(false);
         }
      }

      ////////////////////////////////////////////////////////////////////////// 
      void setValue(double data)
      {
         if (m_type == DOUBLE_DATA)
         {
            (*((double *)m_memory)) = data;
         } else {
            assert(false);
         }
      }


   private:
      DISALLOW_COPY_CTOR(JoystickDataGrabber);
      DISALLOW_ASSIGNMENT_OPER(JoystickDataGrabber);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   the memory location that we store the value in
      ////////////////////////////////////////////////////////////////////////// 
      void *m_memory;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   type of the data that we want out of the joystick
      ////////////////////////////////////////////////////////////////////////// 
      wanted_data_t m_wanted_data;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   type of the data
      ////////////////////////////////////////////////////////////////////////// 
      data_type_t m_type;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   The pointer to the joystick
      ////////////////////////////////////////////////////////////////////////// 
      frc::Joystick *m_joystick;
}; // END class JoystickDataGrabber


////////////////////////////////////////////////////////////////////////////////
/// @todo
/// Document ME!
////////////////////////////////////////////////////////////////////////////////
class JoystickDataFaker
{
   public:
      ////////////////////////////////////////////////////////////////////////// 
      JoystickDataFaker(JoystickDataGrabber *grabber, std::string const &value)
         : m_grabber(grabber)
         , m_double_value(0.0)
         , m_bool_value(false)
      {
         if (m_grabber->m_type == JoystickDataGrabber::DOUBLE_DATA)
         {
            m_double_value = StringConverter::toFloat(value, 0.0f);
         }
         else
         {
            m_bool_value = StringConverter::toBoolean(value, 0);
         }
      }

      ////////////////////////////////////////////////////////////////////////// 
      void fake_it(void)
      {
         if (m_grabber->m_type == JoystickDataGrabber::DOUBLE_DATA)
         {
            m_grabber->setValue(m_double_value);
         }
         else
         {
            m_grabber->setValue(m_bool_value);
         }
      }

   private:

      JoystickDataGrabber *m_grabber;
      double m_double_value;
      bool m_bool_value;
}; // END class JoystickDataFaker


////////////////////////////////////////////////////////////////////////////////
/// @brief
///   Class to grab the DS Attached state
////////////////////////////////////////////////////////////////////////////////
class DsStateDataGrabber : public OiDataGrabber
{
   public:
      ////////////////////////////////////////////////////////////////////////// 
      DsStateDataGrabber(bool *mem)
        : OiDataGrabber(__FUNCTION__)
        , m_memory(mem)
      {
         // left blank on purpose
      }

      ////////////////////////////////////////////////////////////////////////// 
      virtual ~DsStateDataGrabber(void) {}

      ////////////////////////////////////////////////////////////////////////// 
      /// @see OiDataGrabber::grabData()
      ////////////////////////////////////////////////////////////////////////// 
      virtual void grabData(void)
      {
         (*((bool *)m_memory)) = frc::DriverStation::GetInstance().IsDSAttached();
      }

   private:
      DISALLOW_COPY_CTOR(DsStateDataGrabber);
      DISALLOW_ASSIGNMENT_OPER(DsStateDataGrabber);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   the memory location that we store the value in
      ////////////////////////////////////////////////////////////////////////// 
      void *m_memory;
}; // END class DsStateDataGrabber


////////////////////////////////////////////////////////////////////////////////
/// @brief
///   Class to grab the Alliance Color
////////////////////////////////////////////////////////////////////////////////
class AllianceColorDataGrabber : public OiDataGrabber
{
   public:
      ////////////////////////////////////////////////////////////////////////// 
      AllianceColorDataGrabber(alliance_t *mem)
        : OiDataGrabber(__FUNCTION__)
        , m_memory(mem)
      {
         // left blank on purpose
      }

      ////////////////////////////////////////////////////////////////////////// 
      virtual ~AllianceColorDataGrabber(void) {}

      ////////////////////////////////////////////////////////////////////////// 
      /// @see OiDataGrabber::grabData()
      ////////////////////////////////////////////////////////////////////////// 
      virtual void grabData(void)
      {
         /// @todo
         /// There is an assumed 1-1 mapping between the WPI enum value for the
         /// alliance color and ours.  If this changes, then this data grabber
         /// is toast!
         (*((alliance_t *)m_memory)) =
            static_cast<alliance_t>(frc::DriverStation::GetInstance().GetAlliance());
      }

   private:
      DISALLOW_COPY_CTOR(AllianceColorDataGrabber);
      DISALLOW_ASSIGNMENT_OPER(AllianceColorDataGrabber);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   the memory location that we store the value in
      ////////////////////////////////////////////////////////////////////////// 
      void *m_memory;
}; // END class AllianceColorDataGrabber

} // END Subsystem namespace
} // END Psb namespace

#endif //__OiDataGrabbers_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// ///////////////////////////////////////////////////////////////////////////// 

