// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string>
#include <climits>
#include <cmath>
#include <PsbLogger.h>
#include "PsbNumberManipulator.h"



// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {


////////////////////////////////////////////////////////////////////////////////
double
NumberManipulator::applyDeadZone(double value, double deadzone)
{
	double temp = (abs(value) < deadzone ? 0 : (abs(value) - deadzone) * (value / abs(value)));

	if (deadzone > 0) temp /= 1 - deadzone;

	return temp;
}


////////////////////////////////////////////////////////////////////////////////
double
NumberManipulator::limitRate(
   double newInput,
   long newTime,
   double prevInput,
   long prevTime,
   double rising_slew_rate,
   double falling_slew_rate)
{
  

   

   double deltaInput = (newInput - prevInput);
   double deriva = deltaInput/(newTime - prevTime);//derivative of value over time


   if (deriva > rising_slew_rate)
   {
      return deltaInput * rising_slew_rate + prevInput;
   }
   else if (deriva < falling_slew_rate)
   {
      return deltaInput * falling_slew_rate + prevInput;
   }
   else// if between slew rates its good as is
   {
      return newInput;
   }
   

            

}


} // END Psb Namespace


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

