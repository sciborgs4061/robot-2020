// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

//////////////
#ifndef SUBSYSTEM_NAMESPACE_NAME
   #error "You must define SUBSYSTEM_NAMESPACE_NAME before including this file"
#endif // SUBSYSTEM_NAMESPACE_NAME
#ifndef INCLUDE_FILE
   #error "You must define INCLUDE_FILE before including this file"
#endif // INCLUDE_FILE
#ifndef SUBSYSTEM_SID
   #error "You must define SUBSYSTEM_SID before including this file"
#endif // SUBSYSTEM_SID
#ifndef SUBSYSTEM_DIV
   #error "You must define SUBSYSTEM_DIV before including this file"
#endif // SUBSYSTEM_DIV


///////////  undefine all macros  ///////////////
#undef START_NEW_SS
#undef SS_CHAR32_POD_DATA
#undef SS_MEMBLK_POD_DATA
#undef SS_INT32_POD_DATA
#undef SS_POD_PDP_CURRENT
#undef SS_POD_ENCODER_COUNT
#undef SS_POD_ENCODER_RATE
#undef SS_POD_RELAY
#undef SS_POD_DIGITAL_OUTPUT
#undef SS_POD_ANALOG_INPUT
#undef SS_POD_ANALOG_POT
#undef SS_BOOL_POD_DATA
#undef SS_POD_DIGITAL_INPUT
#undef SS_POD_NAVX_BOOL
#undef SS_POD_JOYSTICK_BUTTON
#undef SS_DOUBLE_POD_DATA
#undef SS_POD_NAVX_DOUBLE
#undef SS_POD_VISION_BOOL
#undef SS_POD_VISION_INT
#undef SS_POD_VISION_DOUBLE
#undef SS_POD_JOYSTICK_STICK
#undef SS_POD_SPEED_CONTROLLER
#undef SS_POD_CONFIG_DOUBLE
#undef SS_POD_CONFIG_INT
#undef SS_POD_CONFIG_BOOL
#undef SS_POD_PUBLISH_DOUBLE
#undef SS_POD_PUBLISH_BOOL
#undef SS_POD_PUBLISH_INT32
#undef SS_POD_MIRROR_DOUBLE
#undef SS_POD_MIRROR_BOOL
#undef SS_POD_MIRROR_INT32
#undef END_SS

////////  NOW Define new ones //////////////



////////////////////////////////////////////////////////
#ifdef BUILD_JSON_DESCRIPTION

#define START_NEW_SS(SS_NAME) \
char const Dashstream_POD_Description[] = "\
\n    { \
\n      \"ss_name\": \"" #SS_NAME "\", \
\n      \"ss_data\": [ \
\n        { \
\n           \"var_name\": \"placeholder\", \
\n           \"size\": 0 \
\n        }"

#define SS_CHAR32_POD_DATA(SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": " #INTF_TYPE ", \
\n           \"type\": \"char[32]\", \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"size\": 32, \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

#define SS_MEMBLK_POD_DATA(SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": " #INTF_TYPE ", \
\n           \"type\": \"memblk_t\", \
\n           \"size\": 264, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

#define SS_INT32_POD_DATA(SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": " #INTF_TYPE ", \
\n           \"type\": \"int32_t\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

// ENCODER_COUNT TYPE...looks like an int32
#define SS_POD_ENCODER_COUNT(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"SI\", \
\n           \"type\": \"int32_t\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

// ENCODER_RATE TYPE...looks like a double
#define SS_POD_ENCODER_RATE(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"SI\", \
\n           \"type\": \"double\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

// PDP_CURRENT TYPE...looks like a double
#define SS_POD_PDP_CURRENT(SS_VAR_NAME,CURRENT_INDEX,SS_DISPLAY_NAME) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"SI\", \
\n           \"type\": \"double\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DISPLAY_NAME " \
\n        }"

// ANALOG_INPUT TYPE...looks like an int32
#define SS_POD_ANALOG_INPUT(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"SI\", \
\n           \"type\": \"int32_t\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

// ANALOG_POT TYPE...looks like a double
#define SS_POD_ANALOG_POT(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"SI\", \
\n           \"type\": \"double\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

// RELAY TYPE...looks like an int32
#define SS_POD_RELAY(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"SI\", \
\n           \"type\": \"int32_t\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

// DIGITAL_OUTPUT TYPE...looks like an bool
#define SS_POD_DIGITAL_OUTPUT(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"SI\", \
\n           \"type\": \"bool\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

// CONFIG_INT TYPE...looks like a int32
#define SS_POD_CONFIG_INT(SS_VAR_NAME,DEFAULT_VALUE,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"CI\", \
\n           \"type\": \"int32_t\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

#define SS_BOOL_POD_DATA(SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": " #INTF_TYPE ", \
\n           \"type\": \"bool\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

// DIGITAL_INPUT TYPE...looks like a bool
#define SS_POD_DIGITAL_INPUT(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"SI\", \
\n           \"type\": \"bool\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

// NAVX_BOOL TYPE...looks like a bool
#define SS_POD_NAVX_BOOL(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"SI\", \
\n           \"type\": \"bool\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

// CONFIG_BOOL TYPE...looks like a bool
#define SS_POD_CONFIG_BOOL(SS_VAR_NAME,DEFAULT_VALUE,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"CI\", \
\n           \"type\": \"bool\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

// JOYSTICK_BUTTON TYPE...looks like a bool
#define SS_POD_JOYSTICK_BUTTON(SS_VAR_NAME,BUTTON_NAME,JOYSTICK_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"SI\", \
\n           \"type\": \"bool\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

#define SS_DOUBLE_POD_DATA(SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": " #INTF_TYPE ", \
\n           \"type\": \"double\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

// NAVX_DOUBLE TYPE...looks like a double
#define SS_POD_NAVX_DOUBLE(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"SI\", \
\n           \"type\": \"double\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"


// VISION_BOOL TYPE...looks like a bool
#define SS_POD_VISION_BOOL(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"SI\", \
\n           \"type\": \"bool\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

// VISION_INT TYPE...looks like a int
#define SS_POD_VISION_INT(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"SI\", \
\n           \"type\": \"int32_t\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

// VISION_DOUBLE TYPE...looks like a double
#define SS_POD_VISION_DOUBLE(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"SI\", \
\n           \"type\": \"double\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

// JOYSTICK_STICK TYPE...looks like a double
#define SS_POD_JOYSTICK_STICK(SS_VAR_NAME,STICK_NAME,JOYSTICK_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"SI\", \
\n           \"type\": \"double\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

// SPEED_CONTOLLER TYPE...looks like a double
#define SS_POD_SPEED_CONTROLLER(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"AI\", \
\n           \"type\": \"double\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

// CONFIG_DOUBLE TYPE...looks like a double
#define SS_POD_CONFIG_DOUBLE(SS_VAR_NAME,DEFAULT_VALUE,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"CI\", \
\n           \"type\": \"double\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

// MIRROR_DOUBLE TYPE...looks like a double
#define SS_POD_MIRROR_DOUBLE(SS_VAR_NAME,SS_MIRROR_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"MI\", \
\n           \"type\": \"double\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

// PUBLISH_DOUBLE TYPE...looks like a double
#define SS_POD_PUBLISH_DOUBLE(SS_VAR_NAME,SS_MIRROR_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"MI\", \
\n           \"type\": \"double\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

// MIRROR_INT32 TYPE...looks like an int
#define SS_POD_MIRROR_INT32(SS_VAR_NAME,SS_MIRROR_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"MI\", \
\n           \"type\": \"int32_t\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

// PUBLISH_INT32 TYPE...looks like an int
#define SS_POD_PUBLISH_INT32(SS_VAR_NAME,SS_MIRROR_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"MI\", \
\n           \"type\": \"int32_t\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

// MIRROR_BOOL TYPE...looks like a bool
#define SS_POD_MIRROR_BOOL(SS_VAR_NAME,SS_MIRROR_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"MI\", \
\n           \"type\": \"bool\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

// PUBLISH_BOOL TYPE...looks like a bool
#define SS_POD_PUBLISH_BOOL(SS_VAR_NAME,SS_MIRROR_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n       ,{ \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": \"MI\", \
\n           \"type\": \"bool\", \
\n           \"size\": 8, \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

#define END_SS "\
\n      ] \
\n    } \
\n"; \

namespace Psb {
namespace Subsystem {
namespace SUBSYSTEM_NAMESPACE_NAME {

#include INCLUDE_FILE

char const *get_Dashstream_POD_Description(void)
{
   return Dashstream_POD_Description;
}

} /* END namespace THE_SUBSYSTEM_NAMESPACE_NAME */ \
} /* END namespace Subsystem */ \
} /* END namespace Psb */

#endif // BUILD_JSON_DESCRIPTION
////////////////////////////////////////////////////////




////////////////////////////////////////////////////////
#ifdef BUILD_POD_CPP_CODE

#include <stdlib.h>
#include "PsbMacros.h"
#include "DashstreamBaseInterface_I.h"
#include "GlobalAiWpiStore.h"
#include "AiDataPushers.h"
#include "GlobalSiWpiStore.h"
#include "VisionDataGrabber.h"
#include "GlobalOiWpiStore.h"
#include "GlobalCiStore.h"
#include "GlobalMiStore.h"
#include "ConfigItem.h"
#include <string.h>

namespace Psb {
namespace Subsystem {
namespace SUBSYSTEM_NAMESPACE_NAME {

char const *get_Dashstream_POD_Description(void);

class DashstreamInterface : public Psb::Subsystem::Dashstream::BaseInterface_I
{
   public:
      DashstreamInterface(void)
         : BaseInterface_I()
         , m_data()
      {
         /* Start with clean data */
         memset(&m_data,0,sizeof(m_data));
      }

#ifndef X86_PLATFORM
// #pragma pack(push,1)
#else // X86_PLATFORM
// #pragma pack(1)
#endif // X86_PLATFORM
      typedef struct \
      {

#define START_NEW_SS(SS_NAME)
#define SS_CHAR32_POD_DATA(SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         char SS_VAR_NAME[32];
#define SS_MEMBLK_POD_DATA(SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         memblk_t SS_VAR_NAME; \
         char pad_##SS_VAR_NAME[4];

#define SS_INT32_POD_DATA(SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         int SS_VAR_NAME; \
         char pad_##SS_VAR_NAME[4];
#define SS_POD_PDP_CURRENT(SS_VAR_NAME,CURRENT_INDEX,SS_DISPLAY_NAME) \
         double SS_VAR_NAME;
#define SS_POD_ENCODER_COUNT(SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         int SS_VAR_NAME; \
         char pad_##SS_VAR_NAME[4];
#define SS_POD_ENCODER_RATE(SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         double SS_VAR_NAME;
#define SS_POD_RELAY(SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         int SS_VAR_NAME; \
         char pad_##SS_VAR_NAME[4];
#define SS_POD_DIGITAL_OUTPUT(SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         bool SS_VAR_NAME; \
         char pad_##SS_VAR_NAME[7];
#define SS_POD_ANALOG_INPUT(SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         int SS_VAR_NAME; \
         char pad_##SS_VAR_NAME[4];
#define SS_POD_ANALOG_POT(SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         double SS_VAR_NAME;
#define SS_POD_CONFIG_INT(SS_VAR_NAME,DEFAULT_VALUE,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         int SS_VAR_NAME; \
         char pad_##SS_VAR_NAME[4];

#define SS_BOOL_POD_DATA(SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         bool SS_VAR_NAME; \
         char pad_##SS_VAR_NAME[7];
#define SS_POD_DIGITAL_INPUT(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         bool SS_VAR_NAME; \
         char pad_##SS_VAR_NAME[7];
#define SS_POD_NAVX_BOOL(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         bool SS_VAR_NAME; \
         char pad_##SS_VAR_NAME[7];
#define SS_POD_JOYSTICK_BUTTON(SS_VAR_NAME,BUTTON_NAME,JOYSTICK_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         bool SS_VAR_NAME; \
         char pad_##SS_VAR_NAME[7];
#define SS_POD_CONFIG_BOOL(SS_VAR_NAME,DEFAULT_VALUE,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         bool SS_VAR_NAME; \
         char pad_##SS_VAR_NAME[7];

#define SS_DOUBLE_POD_DATA(SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         double SS_VAR_NAME;
#define SS_POD_NAVX_DOUBLE(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         double SS_VAR_NAME;
#define SS_POD_VISION_DOUBLE(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         double SS_VAR_NAME;
#define SS_POD_VISION_BOOL(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         bool SS_VAR_NAME; \
         char pad_##SS_VAR_NAME[7];
#define SS_POD_VISION_INT(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         int SS_VAR_NAME; \
         char pad_##SS_VAR_NAME[4];
#define SS_POD_JOYSTICK_STICK(SS_VAR_NAME,STICK_NAME,JOYSTICK_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         double SS_VAR_NAME;
#define SS_POD_SPEED_CONTROLLER(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         double SS_VAR_NAME;
#define SS_POD_CONFIG_DOUBLE(SS_VAR_NAME,DEFAULT_VALUE,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         double SS_VAR_NAME;

#define SS_POD_PUBLISH_DOUBLE(SS_VAR_NAME,SS_MIRROR_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         double SS_VAR_NAME;
#define SS_POD_PUBLISH_INT32(SS_VAR_NAME,SS_MIRROR_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         int SS_VAR_NAME; \
         char pad_##SS_VAR_NAME[4];
#define SS_POD_PUBLISH_BOOL(SS_VAR_NAME,SS_MIRROR_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         bool SS_VAR_NAME; \
         char pad_##SS_VAR_NAME[7];
#define SS_POD_MIRROR_DOUBLE(SS_VAR_NAME,SS_MIRROR_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         double SS_VAR_NAME;
#define SS_POD_MIRROR_INT32(SS_VAR_NAME,SS_MIRROR_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         int SS_VAR_NAME; \
         char pad_##SS_VAR_NAME[4];
#define SS_POD_MIRROR_BOOL(SS_VAR_NAME,SS_MIRROR_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         bool SS_VAR_NAME; \
         char pad_##SS_VAR_NAME[7];

#define END_SS

#include INCLUDE_FILE

      } DashstreamPOD_t;
#ifndef X86_PLATFORM
// #pragma pack(pop)
#else // X86_PLATFORM
// #pragma pack()
#endif // X86_PLATFORM

      virtual ~DashstreamInterface(void) { /* Intentionally left empty */ }
      virtual char const *getDescription(void) const { return get_Dashstream_POD_Description(); }
      // virtual void const *getData(void) const { return &m_data; }
      virtual void *getData(void) { return &m_data; }
      virtual size_t dataSize(void) const { return sizeof(m_data); }
      virtual uint8_t SID(void) const { return SUBSYSTEM_SID; }
      virtual uint8_t DIV(void) const { return SUBSYSTEM_DIV; }

      void init(void)
      {
#undef SS_CHAR32_POD_DATA
#undef SS_MEMBLK_POD_DATA
#undef SS_INT32_POD_DATA
#undef SS_POD_RELAY
#undef SS_POD_DIGITAL_OUTPUT
#undef SS_POD_PDP_CURRENT
#undef SS_POD_ENCODER_COUNT
#undef SS_POD_ENCODER_RATE
#undef SS_POD_ANALOG_INPUT
#undef SS_POD_ANALOG_POT
#undef SS_BOOL_POD_DATA
#undef SS_POD_DIGITAL_INPUT
#undef SS_POD_NAVX_BOOL
#undef SS_POD_JOYSTICK_BUTTON
#undef SS_DOUBLE_POD_DATA
#undef SS_POD_NAVX_DOUBLE
#undef SS_POD_VISION_BOOL
#undef SS_POD_VISION_INT
#undef SS_POD_VISION_DOUBLE
#undef SS_POD_JOYSTICK_STICK
#undef SS_POD_SPEED_CONTROLLER
#undef SS_POD_CONFIG_DOUBLE
#undef SS_POD_CONFIG_INT
#undef SS_POD_CONFIG_BOOL
#undef SS_POD_PUBLISH_DOUBLE
#undef SS_POD_PUBLISH_BOOL
#undef SS_POD_PUBLISH_INT32
#undef SS_POD_MIRROR_DOUBLE
#undef SS_POD_MIRROR_BOOL
#undef SS_POD_MIRROR_INT32

         // empty declarations for these so they compile...but they don't
         // actually connect into any pushers/grabbers
         // Their purpose is to give you a memory location that you can
         // use in your subsystem that will also show up through
         // the dashstream interface
         // If you want to do something like mirror/publish/config any data
         // then you need to use some of the other macros
#define SS_CHAR32_POD_DATA(SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION)
#define SS_INT32_POD_DATA(SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION)
#define SS_BOOL_POD_DATA(SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION)
#define SS_DOUBLE_POD_DATA(SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION)

#define SS_POD_DIGITAL_INPUT(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalSiWpiStore *si = GlobalSiWpiStore::instance(); \
            si->attach(new DigitalInputDataGrabber(&(m_data.SS_VAR_NAME)), \
              GlobalSiWpiStore::ENUM_NAME); \
         }

#define SS_POD_PDP_CURRENT(SS_VAR_NAME,CURRENT_INDEX,SS_DISPLAY_NAME) \
         { \
            GlobalSiWpiStore *si = GlobalSiWpiStore::instance(); \
            si->attach(new PDPDataGrabber(&(m_data.SS_VAR_NAME), CURRENT_INDEX)); \
         }

#define SS_POD_ENCODER_COUNT(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalSiWpiStore *si = GlobalSiWpiStore::instance(); \
            si->attach(new EncoderDataGrabber(&(m_data.SS_VAR_NAME), EncoderDataGrabber::COUNT), \
              GlobalSiWpiStore::ENUM_NAME); \
         }

#define SS_POD_ENCODER_RATE(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalSiWpiStore *si = GlobalSiWpiStore::instance(); \
            si->attach(new EncoderDataGrabber(&(m_data.SS_VAR_NAME), EncoderDataGrabber::RATE), \
              GlobalSiWpiStore::ENUM_NAME); \
         }

#define SS_POD_ANALOG_INPUT(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalSiWpiStore *si = GlobalSiWpiStore::instance(); \
            si->attach(new AnalogInputDataGrabber(&(m_data.SS_VAR_NAME)), \
              GlobalSiWpiStore::ENUM_NAME); \
         }

#define SS_POD_ANALOG_POT(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalSiWpiStore *si = GlobalSiWpiStore::instance(); \
            si->attach(new AnalogPotDataGrabber(&(m_data.SS_VAR_NAME)), \
              GlobalSiWpiStore::ENUM_NAME); \
         }

#define SS_POD_NAVX_DOUBLE(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalSiWpiStore *si = GlobalSiWpiStore::instance(); \
            si->attach(new NavigationDataGrabber(&(m_data.SS_VAR_NAME), \
              NavigationDataGrabber::ENUM_NAME)); \
         }


#define SS_POD_VISION_BOOL(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalSiWpiStore *si = GlobalSiWpiStore::instance(); \
            si->attach(new Vision::VisionDataGrabber(&(m_data.SS_VAR_NAME), \
              Vision::VisionDataGrabber::ENUM_NAME)); \
         }

#define SS_POD_VISION_INT(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalSiWpiStore *si = GlobalSiWpiStore::instance(); \
            si->attach(new Vision::VisionDataGrabber(&(m_data.SS_VAR_NAME), \
              Vision::VisionDataGrabber::ENUM_NAME)); \
         }

#define SS_POD_VISION_DOUBLE(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalSiWpiStore *si = GlobalSiWpiStore::instance(); \
            si->attach(new Vision::VisionDataGrabber(&(m_data.SS_VAR_NAME), \
              Vision::VisionDataGrabber::ENUM_NAME)); \
         }

#define SS_POD_NAVX_BOOL(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalSiWpiStore *si = GlobalSiWpiStore::instance(); \
            si->attach(new NavigationDataGrabber(&(m_data.SS_VAR_NAME), \
              NavigationDataGrabber::ENUM_NAME)); \
         }

#define SS_POD_RELAY(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalAiWpiStore *ai = GlobalAiWpiStore::instance(); \
            ai->attach(new RelayDataPusher(&(m_data.SS_VAR_NAME)), \
              GlobalAiWpiStore::ENUM_NAME); \
         }

#define SS_POD_DIGITAL_OUTPUT(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalAiWpiStore *ai = GlobalAiWpiStore::instance(); \
            ai->attach(new DigitalOutputDataPusher(&(m_data.SS_VAR_NAME)), \
              GlobalAiWpiStore::ENUM_NAME); \
         }

#define SS_POD_JOYSTICK_BUTTON(SS_VAR_NAME,BUTTON_NAME,JOYSTICK_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalOiWpiStore *oi = GlobalOiWpiStore::instance(); \
            oi->attach(new JoystickDataGrabber(#SS_VAR_NAME, &(m_data.SS_VAR_NAME), JoystickDataGrabber::BUTTON_NAME), \
              GlobalOiWpiStore::JOYSTICK_NAME); \
         }

#define SS_POD_JOYSTICK_STICK(SS_VAR_NAME,STICK_NAME,JOYSTICK_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalOiWpiStore *oi = GlobalOiWpiStore::instance(); \
            oi->attach(new JoystickDataGrabber(#SS_VAR_NAME, &(m_data.SS_VAR_NAME), JoystickDataGrabber::STICK_NAME), \
              GlobalOiWpiStore::JOYSTICK_NAME); \
         }

#define SS_POD_SPEED_CONTROLLER(SS_VAR_NAME,ENUM_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalAiWpiStore *ai = GlobalAiWpiStore::instance(); \
            ai->attach(new SpeedControllerDataPusher(&(m_data.SS_VAR_NAME)), \
              GlobalAiWpiStore::ENUM_NAME); \
         }

#define SS_POD_CONFIG_INT(SS_VAR_NAME,DEFAULT_VALUE,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalCiStore *ci = GlobalCiStore::instance(); \
            ci->attach(new ConfigItem(#SS_VAR_NAME, &(m_data.SS_VAR_NAME), DEFAULT_VALUE)); \
         }

#define SS_POD_CONFIG_DOUBLE(SS_VAR_NAME,DEFAULT_VALUE,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalCiStore *ci = GlobalCiStore::instance(); \
            ci->attach(new ConfigItem(#SS_VAR_NAME, &(m_data.SS_VAR_NAME), DEFAULT_VALUE)); \
         }

#define SS_POD_CONFIG_BOOL(SS_VAR_NAME,DEFAULT_VALUE,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalCiStore *ci = GlobalCiStore::instance(); \
            ci->attach(new ConfigItem(#SS_VAR_NAME, &(m_data.SS_VAR_NAME), DEFAULT_VALUE)); \
         }

#define SS_POD_MIRROR_DOUBLE(SS_VAR_NAME,SS_MIRROR_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalMiStore *mi = GlobalMiStore::instance(); \
            mi->mirror(SS_MIRROR_NAME,new DataMirror<double>(&(m_data.SS_VAR_NAME))); \
         }

#define SS_POD_MIRROR_INT32(SS_VAR_NAME,SS_MIRROR_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalMiStore *mi = GlobalMiStore::instance(); \
            mi->mirror(SS_MIRROR_NAME,new DataMirror<int32_t>(&(m_data.SS_VAR_NAME))); \
         }

#define SS_POD_MIRROR_BOOL(SS_VAR_NAME,SS_MIRROR_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalMiStore *mi = GlobalMiStore::instance(); \
            mi->mirror(SS_MIRROR_NAME,new DataMirror<bool>(&(m_data.SS_VAR_NAME))); \
         }

#define SS_POD_PUBLISH_DOUBLE(SS_VAR_NAME,SS_MIRROR_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalMiStore *mi = GlobalMiStore::instance(); \
            mi->publish(SS_MIRROR_NAME,new DataPublisher<double>(&(m_data.SS_VAR_NAME))); \
         }

#define SS_POD_PUBLISH_INT32(SS_VAR_NAME,SS_MIRROR_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalMiStore *mi = GlobalMiStore::instance(); \
            mi->publish(SS_MIRROR_NAME,new DataPublisher<int32_t>(&(m_data.SS_VAR_NAME))); \
         }

#define SS_POD_PUBLISH_BOOL(SS_VAR_NAME,SS_MIRROR_NAME,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         { \
            GlobalMiStore *mi = GlobalMiStore::instance(); \
            mi->publish(SS_MIRROR_NAME,new DataPublisher<bool>(&(m_data.SS_VAR_NAME))); \
         }

#include INCLUDE_FILE

      }

   private:
      DISALLOW_COPY_CTOR(DashstreamInterface);
      DISALLOW_ASSIGNMENT_OPER(DashstreamInterface);

      DashstreamPOD_t m_data;
};

} /* END namespace Dashstream */
} /* END namespace Subsystem */
} /* END namespace Psb */

#endif // BUILD_POD_CPP_CODE
////////////////////////////////////////////////////////

