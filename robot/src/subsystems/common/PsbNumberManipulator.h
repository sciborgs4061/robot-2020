// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __PsbNumberManipulator_h__
#define __PsbNumberManipulator_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include "PsbMacros.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This class contains static methods that manipulate numbers
////////////////////////////////////////////////////////////////////////////////
class NumberManipulator
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// applies a deadzone to something like a joystick input and scales the 
      /// output to still be able reach the range [-1, 1].
      ///
      /// @remarks
      /// needs the values to be in their proper ranges to work
      ///
      /// @param[in] value  the [-1, 1] ranged double that we are applying the 
      ///                  deadband to
      /// @param[in] deadzone  the deadzone to apply. Is in range [0, 1]
      //////////////////////////////////////////////////////////////////////////
      static double applyDeadZone(double value, double deadzone);


      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// limits the rate of change of a value 
      ///
      /// @param[in] newInput  the new value of the value being rate limited
      /// @param[in] newTime  the time of the new value
      /// @param[in] prevInput the previous input of the value being rate limited
      /// @param[in] prevTime  the time when the previous input was taken
      /// @param[in] rising_slew_rate  the largest rising rate (positive value)
      /// @param[in] falling_slew_rate  the largest falling rate (negative value)
      //////////////////////////////////////////////////////////////////////////
      static double limitRate(double newInput,
                                                long newTime,
                                                double prevInput,
                                                long prevTime,
                                                double rising_slew_rate,
                                                double falling_slew_rate);

   private:
      DISALLOW_DEFAULT_CTOR(NumberManipulator);
      DISALLOW_COPY_CTOR(NumberManipulator);
      DISALLOW_ASSIGNMENT_OPER(NumberManipulator);
}; // END class NumberManipulator


} // END Psb Namespace


#endif // __PsbNumberManipulator_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////