////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include "ConfigItem.h"
#include <PsbStringConverter.h>
#include <string>
#include "PsbMacros.h"
#include "GlobalCiStore.h"

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {

//////////////////////////////////////////////////////////////////////////
ConfigItem::ConfigItem(std::string const &name,
                       int32_t *mem,
                       int32_t def)
  : m_name(name)
  , m_memory(mem)
  , m_type(INT32_DATA)
  , m_int32_t_default(def)
{
   // left blank on purpose
}

//////////////////////////////////////////////////////////////////////////
ConfigItem::ConfigItem(std::string const &name,
                       bool *mem,
                       bool def)
  : m_name(name)
  , m_memory(mem)
  , m_type(BOOL_DATA)
  , m_bool_default(def)
{
   // left blank on purpose
}

//////////////////////////////////////////////////////////////////////////
ConfigItem::ConfigItem(std::string const &name,
                       double *mem,
                       double def)
  : m_name(name)
  , m_memory(mem)
  , m_type(DOUBLE_DATA)
  , m_double_default(def)
{
   // left blank on purpose
}

//////////////////////////////////////////////////////////////////////////
ConfigItem::~ConfigItem(void) {}

//////////////////////////////////////////////////////////////////////////
/// @brief
///   Returns the name of the config
//////////////////////////////////////////////////////////////////////////
std::string const &ConfigItem::getName(void) const
{
   return m_name;
}

//////////////////////////////////////////////////////////////////////////
/// @brief
///   Function that grabs the config values.
//////////////////////////////////////////////////////////////////////////
void ConfigItem::grabData(void)
{
   auto *ci = GlobalCiStore::instance();
   if (m_type == INT32_DATA)
   {
      if (ci->m_configValues.count(m_name))
      {
         *((int32_t*) m_memory) = stoi(ci->m_configValues[m_name]);
      }
      else
      {
         *((int32_t*) m_memory) = m_int32_t_default;
      }
   }
   else if (m_type == BOOL_DATA)
   {
      if (ci->m_configValues.count(m_name))
      {
         if (ci->m_configValues[m_name] == "true")
         {
            *((bool*) m_memory) = true;
         }
         else if (ci->m_configValues[m_name] == "false")
         {
            *((bool*) m_memory) = false;
         }
      }
      else
      {
         *((bool*) m_memory) = m_bool_default;
      }
   }
   else if (m_type == DOUBLE_DATA)
   {
      if (ci->m_configValues.count(m_name))
      {
         *((double*) m_memory) = stod(ci->m_configValues[m_name]);
      }
      else
      {
         *((double*) m_memory) = m_double_default;
      }
   }
}

} // END Subsystem namespace
} // END Psb namespace

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////
