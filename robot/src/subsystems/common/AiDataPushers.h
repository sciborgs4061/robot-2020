////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __AiDataPushers_h__
#define __AiDataPushers_h__

// DEBUG
#include <PsbLogger.h>

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include "PsbMacros.h"
#include "frc/SpeedController.h"
#include "frc/Relay.h"
#include "frc/DigitalOutput.h"

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////  
namespace Psb {
namespace Subsystem {

////////////////////////////////////////////////////////////////////////////////
///
/// Macro to use when hooking into the AI Global Store to use when
/// you want to hook up a speed controller
///
///  Example:
///     GlobalAiWpiStore *ai = GlobalAiWpiStore::instance();
///     ai->attach(SPEED_CONTROLLER_DP(sstest1_motor_1_speed, LEFT_DRIVE_MOTOR));
///
////////////////////////////////////////////////////////////////////////////////
#define SPEED_CONTROLLER_DP(var_name,where_to_push) \
              new SpeedControllerDataPusher(&(m_data->var_name)), \
              GlobalAiWpiStore::where_to_push

////////////////////////////////////////////////////////////////////////////////
///
/// Macro to use when hooking into the AI Global Store to use when
/// you want to hook up a Relay
///
///  Example:
///     GlobalAiWpiStore *ai = GlobalAiWpiStore::instance();
///     ai->attach(RELAY_DP(sstest1_relay, RELAY_ENUM_NAME));
///
////////////////////////////////////////////////////////////////////////////////
#define RELAY_DP(var_name,where_to_push) \
              new RelayDataPusher(&(m_data->var_name)), \
              GlobalAiWpiStore::where_to_push

////////////////////////////////////////////////////////////////////////////////
/// @brief 
/// Provides an interface for the AI to push data to the physical objects
///   from the controllers that have the values to push
////////////////////////////////////////////////////////////////////////////////
class AiDataPusher
{
   public:
      AiDataPusher(void) {}
      virtual ~AiDataPusher(void) {}

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   The function that will actually push the data to the actuator
      ///     that is being controlled. This must be overridden by the
      ///     derived class.
      ////////////////////////////////////////////////////////////////////////// 
      virtual void pushData(void) = 0;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   This needs to write a value of 0 back to the memory value
      ///     that is driving this pusher and then call pushData.
      ////////////////////////////////////////////////////////////////////////// 
      virtual void reset(void) = 0;

   private:
      DISALLOW_COPY_CTOR(AiDataPusher);
      DISALLOW_ASSIGNMENT_OPER(AiDataPusher);

}; // END class OiDataGrabber

/////////////////////////////////////////////////////////////////////////
/// @brief
///   Class to push a double value to a speed controller
class SpeedControllerDataPusher : public AiDataPusher
{
   public:
      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Constructor for SpeedControllerDataPusher.
      /// @param mem Memory location.
      ////////////////////////////////////////////////////////////////////////// 
      SpeedControllerDataPusher(double *mem) 
        : m_memory(mem)
        , m_speedController(NULL)
      {
         // left blank on purpose
      }

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Destructor for SpeedControllerDataPusher.
      ////////////////////////////////////////////////////////////////////////// 
      virtual ~SpeedControllerDataPusher(void) {}

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Set the Speed Controller.
      /// @param speedController Pointer to SpeedController.
      ////////////////////////////////////////////////////////////////////////// 
      void setSpeedController(frc::SpeedController *speedController)
      {
         m_speedController = speedController;
      }

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Reset the mapped memory and the speed controller to 0.0.
      ////////////////////////////////////////////////////////////////////////// 
      virtual void reset(void)
      {
         // write the memory value that we were handed to 0.0 to reset it
         *((double *)m_memory) = 0.0;
         // and then push the data out the the SpeedController
         pushData();
      }

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   This writes the mapped memory value out the the associated
      ///     speed controller.
      ////////////////////////////////////////////////////////////////////////// 
      virtual void pushData(void)
      {
         m_speedController->Set(*((double *)m_memory));
      }

   private:
      DISALLOW_COPY_CTOR(SpeedControllerDataPusher);
      DISALLOW_ASSIGNMENT_OPER(SpeedControllerDataPusher);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Pointer to the memory location that this class uses for the
      ///     value it writes to the speed controller that is mapped here.
      ////////////////////////////////////////////////////////////////////////// 
      void *m_memory;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Pointer to the speed controller.
      ////////////////////////////////////////////////////////////////////////// 
      frc::SpeedController *m_speedController;

}; // END class SpeedControllerDataPusher

/////////////////////////////////////////////////////////////////////////
/// @brief
///   Class to push an int32_t value to a relay
class RelayDataPusher : public AiDataPusher
{
   public:
      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Constructor for RelayDataPusher.
      /// @param mem Memory location.
      ////////////////////////////////////////////////////////////////////////// 
      RelayDataPusher(int32_t *mem) 
        : m_memory(mem)
        , m_relay(NULL)
      {
         // left blank on purpose
      }

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Destructor for RelayDataPusher.
      ////////////////////////////////////////////////////////////////////////// 
      virtual ~RelayDataPusher(void) {}

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Set the Relay.
      /// @param relay Pointer to Relay.
      ////////////////////////////////////////////////////////////////////////// 
      void setRelay(frc::Relay *relay)
      {
         m_relay = relay;
      }

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Reset the mapped memory and the relay to 0.0.
      ////////////////////////////////////////////////////////////////////////// 
      virtual void reset(void)
      {
         // write the memory value that we were handed to 0.0 to reset it
         *((int32_t *)m_memory) = 0;
         // and then push the data out the the Relay
         pushData();
      }

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   This writes the mapped memory value out the the associated
      ///     Relay.
      ////////////////////////////////////////////////////////////////////////// 
      virtual void pushData(void)
      {
         // this is a bit of a hack.  The Relay class can take one of
         // four values that are enumerated.  Right now we only want
         // one of two values and will simply have the user write either
         // 0 or 1 to this.
         m_relay->Set((frc::Relay::Value)*((int32_t *)m_memory));
      }

   private:
      DISALLOW_COPY_CTOR(RelayDataPusher);
      DISALLOW_ASSIGNMENT_OPER(RelayDataPusher);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Pointer to the memory location that this class uses for the
      ///     value it writes to the relay that is mapped here.
      ////////////////////////////////////////////////////////////////////////// 
      void *m_memory;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Pointer to the relay.
      ////////////////////////////////////////////////////////////////////////// 
      frc::Relay *m_relay;

}; // END class RelayDataPusher

/////////////////////////////////////////////////////////////////////////
/// @brief
///   Class to push an bool value to a DigitalOutput
class DigitalOutputDataPusher : public AiDataPusher
{
   public:
      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Constructor for DigitalOutputDataPusher.
      /// @param mem Memory location.
      ////////////////////////////////////////////////////////////////////////// 
      DigitalOutputDataPusher(bool *mem)
        : m_memory(mem)
        , m_do(NULL)
      {
         // left blank on purpose
      }

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Destructor for DigitalOutputDataPusher.
      ////////////////////////////////////////////////////////////////////////// 
      virtual ~DigitalOutputDataPusher(void) {}

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Set the DigitalOutput.
      /// @param the_do Pointer to DigitalOutput.
      ////////////////////////////////////////////////////////////////////////// 
      void setDigitalOutput(frc::DigitalOutput *the_do)
      {
         m_do = the_do;
      }

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Reset the mapped memory and the DO to 0.0.
      ////////////////////////////////////////////////////////////////////////// 
      virtual void reset(void)
      {
         // write the memory value that we were handed to false to reset it
         *((bool *)m_memory) = false;
         // and then push the data out the the DigitalOutput
         pushData();
      }

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   This writes the mapped memory value out the the associated
      ///     DigitalOutput.
      ////////////////////////////////////////////////////////////////////////// 
      virtual void pushData(void)
      {
         m_do->Set(*((bool *)m_memory));
      }

   private:
      DISALLOW_COPY_CTOR(DigitalOutputDataPusher);
      DISALLOW_ASSIGNMENT_OPER(DigitalOutputDataPusher);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Pointer to the memory location that this class uses for the
      ///     value it writes to the relay that is mapped here.
      ////////////////////////////////////////////////////////////////////////// 
      void *m_memory;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Pointer to the DO.
      ////////////////////////////////////////////////////////////////////////// 
      frc::DigitalOutput *m_do;

}; // END class DigitalOutputDataPusher


} // END Subsystem namespace
} // END Psb namespace


#endif // __AiDataPushers_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// ///////////////////////////////////////////////////////////////////////////// 

