////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __AutonomousBooleanTester_h__
#define __AutonomousBooleanTester_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbMacros.h>
#include <string.h>
#include "AutonomousCondition.h"
#include "AutonomousConditionFactory.h"
#include "AutonomousXmlDescriptions.h"
#include "OiDataGrabbers.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
   namespace Subsystem {
      namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// The BooleanTester object will provide a true indication
/// via the isConditionMet() function when the robot has found the ifthis to be 
/// true
///
/// @remarks
/// 
////////////////////////////////////////////////////////////////////////////////
         class BooleanTester : public Condition
         {
         public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Constructor
      ///
      /// @param[in] m_ifthis       value we look at to test if true or false
      /// @param[in] m_target       what we want the ifthis to be in order to
      ///                           return true
      ///
      //////////////////////////////////////////////////////////////////////////
            BooleanTester(std::string ifthis,                              
             std::string target,
             std::string store);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
            virtual ~BooleanTester(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see Condition::initialize()
      //////////////////////////////////////////////////////////////////////////
            virtual void initialize(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see Condition::isConditionMet()
      //////////////////////////////////////////////////////////////////////////
            virtual bool isConditionMet(void);

         private:

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// value we look at to test if true or false
      //////////////////////////////////////////////////////////////////////////
            bool m_ifthis;
            
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// this is what we want the ifthis to be in order to return true
      //////////////////////////////////////////////////////////////////////////
            bool m_target;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// mirror member var for setting and checking if auto makes debug autput
      //////////////////////////////////////////////////////////////////////////
            bool m_autoDebugEnabler;
            

         private:
      // Hidden from public view
            DISALLOW_DEFAULT_CTOR(BooleanTester);
            DISALLOW_COPY_CTOR(BooleanTester);
            DISALLOW_ASSIGNMENT_OPER(BooleanTester);
}; // END class BooleanTester

// /////////////////////////////////////////////////////////////////////////////
// @brief
// The class that will create the above BooleanTester when needed
// /////////////////////////////////////////////////////////////////////////////
class BooleanTesterCreator :
public Autonomous::ConditionFactory::ConditionCreator
{
public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief Constructor to create the Creator
      //////////////////////////////////////////////////////////////////////////
   BooleanTesterCreator(void)
   {
   }

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Builds and returns a Conditions object based on the description
      /// given.
      ///
      /// @param[in] rc_desc   The condition description information
      //////////////////////////////////////////////////////////////////////////
   virtual Autonomous::Condition*
   createCondition(Autonomous::condition_description_t const & rc_desc);

private:
      // Hidden from public view
   DISALLOW_COPY_CTOR(BooleanTesterCreator);
   DISALLOW_ASSIGNMENT_OPER(BooleanTesterCreator);

}; // END class BooleanTesterCreator
} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb

#endif  // __AutonomousBooleanTester_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////


