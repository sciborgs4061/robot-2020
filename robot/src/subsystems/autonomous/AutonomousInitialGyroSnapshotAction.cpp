////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbLogger.h>
#include "GlobalSiWpiStore.h"
#include "AutonomousInitialGyroSnapshotAction.h"
#include "SiDataGrabbers.h"
#include <assert.h>


// /////////////////////////////////////////////////////////////////////////////
// Defines(s)
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {

////////////////////////////////////////////////////////////////////////////////
InitialGyroSnapshotAction::InitialGyroSnapshotAction(action_description_t const&)
   : Action(__FUNCTION__)
{
}

////////////////////////////////////////////////////////////////////////////////
InitialGyroSnapshotAction::~InitialGyroSnapshotAction(void)
{
   // Intentionally left empty
}

////////////////////////////////////////////////////////////////////////////////
void
InitialGyroSnapshotAction::initialize(void)
{
   // tell the nav data grabber to snapshot the current angle
   // ... lots of stuff to setup to just get the right handle
   // to the right thing...
   double tmp;
   NavigationDataGrabber *nav =
   new NavigationDataGrabber(&tmp, NavigationDataGrabber::SNAPSHOTTED_ANGLE);

   GlobalSiWpiStore *si = GlobalSiWpiStore::instance();
   si->attach(nav);

   // now that it is attached...I can tell it to snapshot
   nav->snapshot();
}

////////////////////////////////////////////////////////////////////////////////
void
InitialGyroSnapshotAction::perform(void)
{
}


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

