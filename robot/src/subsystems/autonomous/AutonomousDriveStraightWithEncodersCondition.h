////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __AutonomousDriveStraightWithEncodersCondition_h__
#define __AutonomousDriveStraightWithEncodersCondition_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbMacros.h>
#include <string.h>
#include "AutonomousCondition.h"
#include "AutonomousConditionFactory.h"
#include "AutonomousXmlDescriptions.h"
#include "OiDataGrabbers.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// The DriveStraightWithEncodersCondition object will provide a true indication
/// via the isConditionMet() function when the robot has rotated through the
/// prescribed angle.
/// This Condition actually CONTROLS the heading value.
///
/// @remarks
/// Positive sweep angles indicate a clockwise rotation, negative indicate CCW.
////////////////////////////////////////////////////////////////////////////////
class DriveStraightWithEncodersCondition : public Condition
{
   public:

      ////////////////////////////////////////////////////////////////////////////////
      /// @todo DOCUMENTATION
      ////////////////////////////////////////////////////////////////////////////////
      enum encoder_selection_t
      {
         SELECT_BOTH_ENCODERS,    ///< Left & Right encoders must both hit the tick target (last one governs)
         SELECT_LEFT_ENCODER,     ///< Only Left encoder must hit the tick target
         SELECT_RIGHT_ENCODER,    ///< Only Right encoder must hit the tick target
         SELECT_EITHER_ENCODER,   ///< Left Or Right must hist the tick target
         SELECT_NEITHER_ENCODER,  ///< Immediately return as true

         // If neither encoder is selected, then we will indicate that the
         //  condition is met immediately.
         ENCODER_SELECTION_INVALID
      };

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Constructor
      ///
      /// @param[in] sweep_angle    The expected angle that the robot will rotate
      /// @param[in] slowdown_angle The angle delta to the goal at which point
      ///                           the heading will start to slow down
      /// @param[in] heading        The value of the heading to use to turn the bot
      /// @param[in] minimum_heading The minimum value we will let the heading get to
      /// @param[in] deadband       The deadband value that will let us stop moving
      ///                           and call the condition complete
      /// @param[in] store_name the store to get the encoder values from
      //////////////////////////////////////////////////////////////////////////
      DriveStraightWithEncodersCondition(double sweep_angle,
                                         double slowdown_angle,
                                         double heading,
                                         double minimum_heading,
                                         double angle_deadband,
                                         std::string heading_oi_name,
                                         std::string store_name,
                                         std::string left_encoder_si_name,
                                         std::string right_encoder_si_name,
                                         encoder_selection_t selection,
                                         double left_ticks,
                                         double right_ticks
                                         );

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~DriveStraightWithEncodersCondition(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see Condition::initialize()
      //////////////////////////////////////////////////////////////////////////
      virtual void initialize(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see Condition::isConditionMet()
      //////////////////////////////////////////////////////////////////////////
      virtual bool isConditionMet(void);

   private:


      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Keeps the robot going straight
      //////////////////////////////////////////////////////////////////////////
      void adjustDriveAngle(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Current reading of the Gyro
      //////////////////////////////////////////////////////////////////////////
      double m_current_angle;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// We keep track of the Gyro's starting angle for our rotation
      /// calculations rather than resetting it.
      //////////////////////////////////////////////////////////////////////////
      double m_starting_angle;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This is the target angle.
      //////////////////////////////////////////////////////////////////////////
      double m_sweep_angle;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The heading value that will be used to do the turning
      //////////////////////////////////////////////////////////////////////////
      double m_heading;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The point that start slowing the bots turn down
      //////////////////////////////////////////////////////////////////////////
      double m_slowdown_angle;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The minimum heading value we will use.
      //////////////////////////////////////////////////////////////////////////
      double m_minimum_heading;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The range that, once inside it, we will call the condition done
      /// and set the heading to 0
      //////////////////////////////////////////////////////////////////////////
      double m_angle_deadband;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The data grabber pointer
      //////////////////////////////////////////////////////////////////////////
      JoystickDataGrabber* m_grabber;

      double m_left_drive_encoder;
      double m_right_drive_encoder;
      double m_leftEncoderInitialTickCount;
      double m_rightEncoderInitialTickCount;
      double m_leftEncoderTargetTickCount;
      double m_rightEncoderTargetTickCount;
      encoder_selection_t m_encoderSelection;

   private:
      // Hidden from public view
      DISALLOW_DEFAULT_CTOR(DriveStraightWithEncodersCondition);
      DISALLOW_COPY_CTOR(DriveStraightWithEncodersCondition);
      DISALLOW_ASSIGNMENT_OPER(DriveStraightWithEncodersCondition);
}; // END class DriveStraightWithEncodersCondition

// /////////////////////////////////////////////////////////////////////////////
// @brief
// The class that will create the above RotateByAngleCondition when needed
// /////////////////////////////////////////////////////////////////////////////
class DriveStraightWithEncodersConditionCreator :
        public Autonomous::ConditionFactory::ConditionCreator
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief Constructor to create the Creator
      //////////////////////////////////////////////////////////////////////////
      DriveStraightWithEncodersConditionCreator(void)
      {
      }

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Builds and returns a Conditions object based on the description
      /// given.
      ///
      /// @param[in] rc_desc   The condition description information
      //////////////////////////////////////////////////////////////////////////
      virtual Autonomous::Condition*
              createCondition(Autonomous::condition_description_t const& rc_desc);

   private:
      // Hidden from public view
      DISALLOW_COPY_CTOR(DriveStraightWithEncodersConditionCreator);
      DISALLOW_ASSIGNMENT_OPER(DriveStraightWithEncodersConditionCreator);

}; // END class DriveStraightWithEncodersConditionCreator
} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb

#endif  // __AutonomousDriveStraightWithEncodersCondition

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////


