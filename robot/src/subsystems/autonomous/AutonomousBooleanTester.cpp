////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//
////////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbLogger.h>
#include "AutonomousBooleanTester.h"
#include "PsbStringConverter.h"
#include "SiDataGrabbers.h"
#include "MiDataMirrors.h"
#include "GlobalSiWpiStore.h"
#include "GlobalMiStore.h"


#include <iostream>
using namespace std;

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
BooleanTester::
BooleanTester(std::string ifthis,                              
                               std::string target,
                               std::string store )


: Condition(__FUNCTION__)      
{

  m_target = false;
  if (target == "true")
  {
    m_target = true; 
  }

  
//get value from stores
  m_ifthis = true;//if input is invalid it will default to true
  if (store == "sensor")
  {
    GlobalSiWpiStore *si = GlobalSiWpiStore::instance();
    cout<<"made it"<<endl;
    si->attach(new DigitalInputDataGrabber(&(m_ifthis)), ifthis); 
  }
  else if (store == "mirror")
  {
    GlobalMiStore *mi = GlobalMiStore::instance();
    mi->mirror(ifthis,new DataMirror<bool>(&(m_ifthis)));
  }



  // debug output checker
  Subsystem::GlobalMiStore::instance()->mirror("autonomous_debugDataEnabler",new DataMirror<bool>(&(m_autoDebugEnabler)));
}


////////////////////////////////////////////////////////////////////////////////
BooleanTester::~BooleanTester(void)
{
   // Intentionally left empty
}


////////////////////////////////////////////////////////////////////////////////
void
BooleanTester::initialize(void)
{
   
}

////////////////////////////////////////////////////////////////////////////////
bool
BooleanTester::isConditionMet(void)
{
	//compare variable from the store to what we want (true or false)

   bool is_condition_met = false;

      if (m_ifthis == m_target)
      {
         is_condition_met = true;
      }      


      // debug output 
      if (m_autoDebugEnabler)
      {
        std::cout << "@AutoDebug - " << "BooleanTester, desired: " << "\"" << m_target << "\"" << "    actual: " << "\"" << m_ifthis << "\"" << std::endl;
      }



   return is_condition_met;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//                       CONDITION CREATOR
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

Autonomous::Condition* BooleanTesterCreator::createCondition(
                Autonomous::condition_description_t const& rc_desc)
{
   std::string ifthis = rc_desc.params.at("ifthis");
   std::string target = rc_desc.params.at("target");
   std::string store = rc_desc.params.at("store");
   
   
   return new BooleanTester(ifthis,                                             
                                             target,
                                             store);
}


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////
