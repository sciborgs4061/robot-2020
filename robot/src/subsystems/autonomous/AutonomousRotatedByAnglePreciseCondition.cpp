////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//
////////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbLogger.h>
#include "AutonomousRotatedByAnglePreciseCondition.h"
#include "PsbStringConverter.h"
#include "SiDataGrabbers.h"
#include "GlobalSiWpiStore.h"
#include "GlobalOiWpiStore.h"
#include <iostream>

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
RotatedByAnglePreciseCondition::
RotatedByAnglePreciseCondition(double sweep_angle,
                               double slowdown_angle,
                               double heading,
                               double minimum_heading,
                               double deadband,
                               std::string oi_name)
   : Condition(__FUNCTION__)
   , m_current_angle(0.0f)
   , m_starting_angle(0.0f)
   , m_sweep_angle(sweep_angle)
   , m_heading(heading)
   , m_slowdown_angle(slowdown_angle)
   , m_minimum_heading(minimum_heading)
   , m_deadband(deadband)
   , m_grabber(NULL)
{
   GlobalSiWpiStore *si = GlobalSiWpiStore::instance();
   si->attach(new NavigationDataGrabber(&(m_current_angle),
              NavigationDataGrabber::ADJUSTED_ANGLE));
   GlobalOiWpiStore *oi = GlobalOiWpiStore::instance();
   m_grabber = static_cast<JoystickDataGrabber *>(oi->getGrabberByName(oi_name.c_str()));
}


////////////////////////////////////////////////////////////////////////////////
RotatedByAnglePreciseCondition::~RotatedByAnglePreciseCondition(void)
{
   // Intentionally left empty
}


////////////////////////////////////////////////////////////////////////////////
void
RotatedByAnglePreciseCondition::initialize(void)
{
   // Note the current heading angle instead of resetting the gyro
   // m_starting_angle = m_current_angle;
   m_starting_angle = NavigationDataGrabber::get_snapshot();
}

////////////////////////////////////////////////////////////////////////////////
bool
RotatedByAnglePreciseCondition::isConditionMet(void)
{
   bool is_condition_met = false;
   double actual_angle = m_current_angle - m_starting_angle;
   double distance_away = m_sweep_angle - actual_angle;
   double correction_value = 1.0;
   double calculated_heading = m_heading;

   if (distance_away < 0)
   {
      distance_away *= -1.0;
      correction_value = -1.0;
   }

   if (distance_away <= m_slowdown_angle)
   {
      calculated_heading = (distance_away / m_slowdown_angle) * m_heading;
      if (calculated_heading < m_minimum_heading)
      {
         calculated_heading = m_minimum_heading;
      }
   }

   if (distance_away < m_deadband)
   {
      calculated_heading = 0.0;
      is_condition_met = true;
   }
   else
   {
      is_condition_met = false;
   }

   m_grabber->setValue(calculated_heading * correction_value);



   return is_condition_met;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//                       CONDITION CREATOR
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

Autonomous::Condition* RotatedByAnglePreciseConditionCreator::createCondition(
                Autonomous::condition_description_t const& rc_desc)
{
   double sweep_angle =
      Psb::StringConverter::toFloat(rc_desc.params.at("sweep_angle").c_str(), 0);
   double slowdown_angle =
      Psb::StringConverter::toFloat(rc_desc.params.at("slowdown_angle").c_str(), 10);
   double heading =
      Psb::StringConverter::toFloat(rc_desc.params.at("heading").c_str(), 1.0);
   double minimum_heading =
      Psb::StringConverter::toFloat(rc_desc.params.at("minimum_heading").c_str(), 0.2);
   double deadband =
      Psb::StringConverter::toFloat(rc_desc.params.at("deadband").c_str(), 1.0);
   std::string oi_name = rc_desc.params.at("oi_name");

   return new RotatedByAnglePreciseCondition(sweep_angle,
                                             slowdown_angle,
                                             heading,
                                             minimum_heading,
                                             deadband,
                                             oi_name);
}


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////
