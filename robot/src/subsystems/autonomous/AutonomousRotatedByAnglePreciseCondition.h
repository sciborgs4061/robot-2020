////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __AutonomousRotatedByAnglePreciseCondition_h__
#define __AutonomousRotatedByAnglePreciseCondition_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbMacros.h>
#include <string.h>
#include "AutonomousCondition.h"
#include "AutonomousConditionFactory.h"
#include "AutonomousXmlDescriptions.h"
#include "OiDataGrabbers.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// The RotatedByAnglePreciseCondition object will provide a true indication
/// via the isConditionMet() function when the robot has rotated through the
/// prescribed angle.
/// This Condition actually CONTROLS the heading value.
///
/// @remarks
/// Positive sweep angles indicate a clockwise rotation, negative indicate CCW.
////////////////////////////////////////////////////////////////////////////////
class RotatedByAnglePreciseCondition : public Condition
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Constructor
      ///
      /// @param[in] sweep_angle    The expected angle that the robot will rotate
      /// @param[in] slowdown_angle The angle delta to the goal at which point
      ///                           the heading will start to slow down
      /// @param[in] heading        The value of the heading to use to turn the bot
      /// @param[in] minimum_heading The minimum value we will let the heading get to
      /// @param[in] deadband       The deadband value that will let us stop moving
      ///                           and call the condition complete
      //////////////////////////////////////////////////////////////////////////
      RotatedByAnglePreciseCondition(double sweep_angle,
                                     double slowdown_angle,
                                     double heading,
                                     double minimum_heading,
                                     double deadband,
                                     std::string oi_name);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~RotatedByAnglePreciseCondition(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see Condition::initialize()
      //////////////////////////////////////////////////////////////////////////
      virtual void initialize(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see Condition::isConditionMet()
      //////////////////////////////////////////////////////////////////////////
      virtual bool isConditionMet(void);

   private:

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Current reading of the Gyro
      //////////////////////////////////////////////////////////////////////////
      double m_current_angle;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// We keep track of the Gyro's starting angle for our rotation
      /// calculations rather than resetting it.
      //////////////////////////////////////////////////////////////////////////
      double m_starting_angle;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This is the target sweep angle.
      //////////////////////////////////////////////////////////////////////////
      double m_sweep_angle;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The heading value that will be used to do the turning
      //////////////////////////////////////////////////////////////////////////
      double m_heading;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The point that start slowing the bots turn down
      //////////////////////////////////////////////////////////////////////////
      double m_slowdown_angle;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The minimum heading value we will use.
      //////////////////////////////////////////////////////////////////////////
      double m_minimum_heading;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The range that, once inside it, we will call the condition done
      /// and set the heading to 0
      //////////////////////////////////////////////////////////////////////////
      double m_deadband;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The data grabber pointer
      //////////////////////////////////////////////////////////////////////////
      JoystickDataGrabber* m_grabber;

   private:
      // Hidden from public view
      DISALLOW_DEFAULT_CTOR(RotatedByAnglePreciseCondition);
      DISALLOW_COPY_CTOR(RotatedByAnglePreciseCondition);
      DISALLOW_ASSIGNMENT_OPER(RotatedByAnglePreciseCondition);
}; // END class RotatedByAnglePreciseCondition

// /////////////////////////////////////////////////////////////////////////////
// @brief
// The class that will create the above RotateByAngleCondition when needed
// /////////////////////////////////////////////////////////////////////////////
class RotatedByAnglePreciseConditionCreator :
        public Autonomous::ConditionFactory::ConditionCreator
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief Constructor to create the Creator
      //////////////////////////////////////////////////////////////////////////
      RotatedByAnglePreciseConditionCreator(void)
      {
      }

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Builds and returns a Conditions object based on the description
      /// given.
      ///
      /// @param[in] rc_desc   The condition description information
      //////////////////////////////////////////////////////////////////////////
      virtual Autonomous::Condition*
              createCondition(Autonomous::condition_description_t const& rc_desc);

   private:
      // Hidden from public view
      DISALLOW_COPY_CTOR(RotatedByAnglePreciseConditionCreator);
      DISALLOW_ASSIGNMENT_OPER(RotatedByAnglePreciseConditionCreator);

}; // END class RotateByAngleConditionCreator
} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb

#endif  // __AutonomousRotatedByAnglePreciseCondition_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////


