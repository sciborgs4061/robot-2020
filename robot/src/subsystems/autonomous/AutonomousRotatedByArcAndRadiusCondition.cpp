////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//
////////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbLogger.h>
#include "AutonomousRotatedByArcAndRadiusCondition.h"
#include "AutonomousDriveStraightWithEncodersCondition.h"// for encoder enum selection stuff
#include "PsbStringConverter.h"
#include "SiDataGrabbers.h"
#include "GlobalMiStore.h"
#include "GlobalOiWpiStore.h"
#include "GlobalSiWpiStore.h"
#include "GlobalCiStore.h"
#include "ConfigItem.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
RotatedByArcAndRadiusCondition:://-
RotatedByArcAndRadiusCondition(
                              double arc, 
                              double radius, 
                              std::string store_name, 
                              std::string left_encoder_name, 
                              std::string right_encoder_name,
                              DriveStraightWithEncodersCondition::encoder_selection_t encoder_selection,
                              std::string heading_oi_name,
                              double deadband,
                              double slowdown_ratio,
                              double min_heading,
                              double max_heading)
   : Condition(__FUNCTION__)
   , m_encoder_selection(encoder_selection)
   , m_slowdown_ratio(slowdown_ratio)
   , m_min_heading(min_heading)
   , m_max_heading(max_heading)
   , m_deadband(deadband)
{


   ///////////////////////////////////
   // Connect store variables
   ///////////////////////////////


   GlobalOiWpiStore *oi = GlobalOiWpiStore::instance();
   m_grabber = static_cast<JoystickDataGrabber *>(oi->getGrabberByName(heading_oi_name.c_str()));



   // hook up to the desired encoders from the desired store
   if (store_name == "mi")
   {
      GlobalMiStore *mi = GlobalMiStore::instance();
      mi->mirror(left_encoder_name, new DataMirror<double>(&(m_left_encoder)));
      mi->mirror(right_encoder_name, new DataMirror<double>(&(m_right_encoder)));
   }
   else if (store_name == "si")// if we want si store 
   {
      GlobalSiWpiStore *si = GlobalSiWpiStore::instance();
      si->attach(new EncoderDataGrabber(&(m_left_encoder), EncoderDataGrabber::COUNT),
                  left_encoder_name);
      si->attach(new EncoderDataGrabber(&(m_right_encoder), EncoderDataGrabber::COUNT),
                  right_encoder_name);
   }
   else
   {
      //error?
   }


   


   /////////////////////////////////////////
   // calc encoder arcs and encoder ratio
   ///////////////////////////////////////

   // fetch the trackwidth from the configs
   GlobalCiStore *ci = GlobalCiStore::instance();
   //ci->attach(new ConfigItem("autonomous_trackwidth", &(m_trackWidth_config), 0.0));
   double halfTrackWidth = 11.0;// m_trackWidth_config/2;



   double rightRadius = (radius - halfTrackWidth);
   double leftRadius = (radius + halfTrackWidth);


   // calc ratio
   m_leftToRightRatio = leftRadius/rightRadius;


   

   // figure out left and right encoder lengths (arcs) also
   // don't devide by zero. @TODO what do we do if this is zero apart from not crash?
   if (radius != 0)
   {
      double angle = arc/radius;

      m_desired_right_ticks = rightRadius * angle;
      m_desired_left_ticks = leftRadius * angle;
   }

   std::cout << "desired right ticks:" + std::to_string(m_desired_right_ticks) << std::endl;
   std::cout << "desired left ticks:" + std::to_string(m_desired_left_ticks) << std::endl;


   // figure out the sign of the output
   m_default_correction_value = (arc < 0) ? 1: -1;

}


////////////////////////////////////////////////////////////////////////////////
RotatedByArcAndRadiusCondition::~RotatedByArcAndRadiusCondition(void)
{
   // Intentionally left empty
}


////////////////////////////////////////////////////////////////////////////////
void
RotatedByArcAndRadiusCondition::initialize(void)
{

   // Get the initial Encoder counts
   m_left_initial_ticks = m_left_encoder;
   m_right_initial_ticks = m_right_encoder;

}


////////////////////////////////////////////////////////////////////////////////
bool
RotatedByArcAndRadiusCondition::isConditionMet(void)
{
   bool is_condition_met = false;
   
   // Now, get the instantaneous encoder counts...
   double left_ticks  = m_left_encoder;
   double right_ticks = m_right_encoder;


   // get change in encoder counts
   double traveledLeftTick = left_ticks - m_left_initial_ticks;
   double traveledRightTick = right_ticks - m_right_initial_ticks;


   ///////////////////////////////////////
   // figure out if we've gone far enough
   ///////////////////////////////////////


   bool left_target_hit =
      (m_desired_left_ticks >= 0)
      ? (traveledLeftTick >= m_desired_left_ticks)
      : (traveledLeftTick <= m_desired_left_ticks);

   bool right_target_hit =
      (m_desired_right_ticks >= 0)
      ? (traveledRightTick >= m_desired_right_ticks)
      : (traveledRightTick <= m_desired_right_ticks);

   switch (m_encoder_selection)
   {
      case DriveStraightWithEncodersCondition::encoder_selection_t::SELECT_BOTH_ENCODERS:
      {
         // Both left and right targets must be hit
         is_condition_met = 
         ( (true == left_target_hit) &&
           (true == right_target_hit) );
      } break;

      case DriveStraightWithEncodersCondition::encoder_selection_t::SELECT_LEFT_ENCODER:
      {
         // Just the left target must be hit
         is_condition_met = (true == left_target_hit);
      } break;

      case DriveStraightWithEncodersCondition::encoder_selection_t::SELECT_RIGHT_ENCODER:
      {
         // Just the right target must be hit
         is_condition_met = (true == right_target_hit);
      } break;

      case DriveStraightWithEncodersCondition::encoder_selection_t::SELECT_EITHER_ENCODER:
      {
         // Either the left OR the right target must be hit
         is_condition_met = 
         ( (true == left_target_hit) ||
           (true == right_target_hit) );
      } break;

      case DriveStraightWithEncodersCondition::encoder_selection_t::SELECT_NEITHER_ENCODER:
      {
         is_condition_met = true;
      } break;

      default:
      {
        // unsupported encoder type. 
         is_condition_met = true;
      } break;
   }


   // if we are done then just finish instead of making a new heading
   if (is_condition_met)
   {
      m_grabber->setValue(0.0);
      return true;
   }
     



   ///////////////////////////////////
   // Figure out new heading
   /////////////////////////////////


   // if we are going to divide by zero give up
   if (traveledRightTick == 0)
   {
      return false;
   }

   double current_ratio = traveledLeftTick/traveledRightTick;
   double distance_away = current_ratio - m_leftToRightRatio;

   //std::cout <<"Distance From Desired Ratio:" + std::to_string(distance_away) + "\t Current Ratio:" + std::to_string(current_ratio) + "\t Desired Ratio:" + std::to_string(m_leftToRightRatio) <<std::endl;
   //std::cout <<"Left Traveled:" + std::to_string(traveledLeftTick) + "\t left desired:" + std::to_string(m_desired_left_ticks) <<std::endl;
   //std::cout <<"right Traveled:" + std::to_string(traveledRightTick) + "\t right desired:" + std::to_string(m_desired_right_ticks) <<std::endl;

   double correction_value = m_default_correction_value;// if the ticks go backward then correct normally otherwise mult by negative 1
   double calculated_heading = m_max_heading;
   
   if (distance_away < 0)
   {
      distance_away *= -1.0;
      correction_value *= -1.0;
   }

   if (distance_away <= m_deadband)// if in deadband set heading to zero
   {
     // std::cout <<"In Deadband! "  + std::to_string(m_deadband) + " >= " + std::to_string(distance_away)<<std::endl; 
      calculated_heading = 0;
   }
   else if (distance_away <= m_slowdown_ratio)
   {
      //std::cout <<"In Slowband! "  + std::to_string(m_slowdown_ratio) + " >= " + std::to_string(distance_away); 
      
      
      calculated_heading = (distance_away / m_slowdown_ratio) * m_max_heading;

      if (calculated_heading < m_min_heading)
      {
         calculated_heading = m_min_heading;
      }


      //std::cout << ", Outputing: " + std::to_string(calculated_heading) <<std::endl;
   }
   


   // push the new heading out to the joystick
   m_grabber->setValue(calculated_heading * correction_value);

   
   return false;
}




////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//                       CONDITION CREATOR
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
static inline
DriveStraightWithEncodersCondition::encoder_selection_t
convertStringToEncoderSelection(std::string& rc_string)
{
   // Default to unknown - should NEVER happen
   DriveStraightWithEncodersCondition::encoder_selection_t selection = DriveStraightWithEncodersCondition::ENCODER_SELECTION_INVALID;

   if      ("SELECT_BOTH_ENCODERS" == rc_string)   { selection = DriveStraightWithEncodersCondition::SELECT_BOTH_ENCODERS; }
   else if ("SELECT_LEFT_ENCODER" == rc_string)    { selection = DriveStraightWithEncodersCondition::SELECT_LEFT_ENCODER; }
   else if ("SELECT_RIGHT_ENCODER" == rc_string)   { selection = DriveStraightWithEncodersCondition::SELECT_RIGHT_ENCODER; }
   else if ("SELECT_EITHER_ENCODER" == rc_string)  { selection = DriveStraightWithEncodersCondition::SELECT_EITHER_ENCODER; }
   else if ("SELECT_NEITHER_ENCODER" == rc_string) { selection = DriveStraightWithEncodersCondition::SELECT_NEITHER_ENCODER; }
   else                                            { selection = DriveStraightWithEncodersCondition::SELECT_BOTH_ENCODERS; }

   return selection;
}


Autonomous::Condition* RotatedByArcAndRadiusConditionCreator::createCondition(
                Autonomous::condition_description_t const& rc_desc)
{
   double arc =
      Psb::StringConverter::toFloat(rc_desc.params.at("arc").c_str(), 0);
   double radius =
      Psb::StringConverter::toFloat(rc_desc.params.at("radius").c_str(), 0);
   
   std::string store_name = rc_desc.params.at("store_name");
   std::string left_encoder_name  = rc_desc.params.at("left_encoder_name");
   std::string right_encoder_name = rc_desc.params.at("right_encoder_name");
   std::string oi_name = rc_desc.params.at("heading_oi_name");



   std::string selection_str = rc_desc.params.at("enc_selection");

   DriveStraightWithEncodersCondition::encoder_selection_t encoder_selection = convertStringToEncoderSelection(selection_str);

   double slowdown_ratio = Psb::StringConverter::toFloat(rc_desc.params.at("slowdown_ratio").c_str(), 0);// slow down ratio of right to the left encoder count
   double max_heading = Psb::StringConverter::toFloat(rc_desc.params.at("max_heading").c_str(), 0);
   double min_heading = Psb::StringConverter::toFloat(rc_desc.params.at("min_heading").c_str(), 0);
   double deadband = Psb::StringConverter::toFloat(rc_desc.params.at("deadband_ratio").c_str(), 1);

   return new RotatedByArcAndRadiusCondition(
      arc,
      radius,
      store_name,
      left_encoder_name, 
      right_encoder_name,
      encoder_selection,
      oi_name,
      deadband,
      slowdown_ratio,
      min_heading,
      max_heading);
}


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////
