// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// This is a good place to actually build the dashstream interface
// /////////////////////////////////////////////////////////////////////////////
#define BUILD_IT
#include "AutonomousDashstreamInterface.h"

// don't want to build it again if someone happens to
// include things again
#undef BUILD_IT

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <cstddef>
#include <cassert>
#include <iostream>
#include <sys/types.h>
#include <dirent.h>
#include <PsbLogger.h>
#include <frc/shuffleboard/Shuffleboard.h>
#include <frc/shuffleboard/BuiltInWidgets.h>
#include "AutonomousController.h"
#include "AutonomousXmlProgram.h"
#include "GlobalOiWpiStore.h"
#include "OiDataGrabbers.h"
#include "GlobalCiStore.h"
#include "AutonomousRotatedByAngleCondition.h"
#include "AutonomousRotatedByAnglePreciseCondition.h"
#include "AutonomousDriveStraightWithEncodersCondition.h"
#include "AutonomousBooleanTester.h"
#include "AutonomousRotatedByArcAndRadiusCondition.h"



////////////////////////////////////////////////////////////////////////////////
// Define(s)
////////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
Controller::Controller(void)
   : Psb::Subsystem::Controller_I()
   , m_configurationIntf()
   , m_frcMatchState(MATCH_STATE_DISABLED)
   , m_programTable()
   , m_lockedInProgramIndex(0u)
   , m_lockedInProgramName("")
   , mp_dashstreamIntf(NULL)
   , m_data(NULL)
{

   loadConditionalCreators();

   mp_dashstreamIntf = new DashstreamInterface;
   m_data = (DashstreamInterface::DashstreamPOD_t *)mp_dashstreamIntf->getData();
   memset(m_data, 0, sizeof(DashstreamInterface::DashstreamPOD_t));


   // add grid to contain auto stuff and failed auto string
   frc::ShuffleboardLayout& autoGridLayout =
   frc::Shuffleboard::GetTab("Autonomous").GetLayout("Auto Debug", "Grid Layout")
            .WithSize(3, 3).WithPosition(2, 0);

   m_shuffleboard_failed_program = autoGridLayout.Add("Failed Program", "N/A").GetEntry();
   
   
  

   // Read subsystem settings from the XML file
   m_configurationIntf.load();

   mp_dashstreamIntf->init();

   std::list<std::string> short_prgm_name_list;

   DIR *dir = opendir(determineProgramsLocation().c_str());
   struct dirent *dirent;

   if (dir != NULL) {
      while ((dirent = readdir(dir)) != NULL) {
         if ((strlen(dirent->d_name) >= 5) && (strncmp("Auto_", dirent->d_name, 5) == 0)) {
            short_prgm_name_list.push_back(std::string(dirent->d_name));
         }
      }
   }

   short_prgm_name_list.sort();


   // initialize auto programs catching any bad ones in the proccess 
   int i = 0;
   for (auto name : short_prgm_name_list)
   {
      // Build the proper program file path
      std::string xml_filename = determineProgramsLocation() + name;

      // Create the program & initialize it - VERY IMPORTANT to do this just once
      XmlProgram* p_program = NULL;
      try {
         p_program = new XmlProgram(xml_filename);
      }
      catch (...)
      {
         // bad XML most likely...throw error and quit processing
         std::cout << "BAD XML in program: " << xml_filename << std::endl;
#ifndef __APPLE__
         m_failedProgramName = basename(xml_filename.c_str());
         
        
#else
         // macOS doesn't have basename.
         m_failedProgramName = xml_filename.c_str();
#endif
         m_data->autonomous_found_bad_xml = 1;
         break;
      }
      p_program->initialize();

      // Add the program to our table
      std::string num = name.substr(5);
      std::size_t underscore = num.find("_");
      num = num.substr(0, underscore);
      m_programTable.push_back(p_program);
      
      // add program names to auto selector with it's associated index
      m_shuffleboard_autoChooser.AddOption(std::to_string(i) + " - " + p_program->getName(), std::to_string(i));
      i++;
   }


   if (m_programTable.size() == 0) 
   {

      m_lockedInProgramName  = "No Programs Found";
      m_lockedInProgramIndex = 0;

      m_programTable.push_back(new XmlProgram("/NonExistantProgram.xml"));
   }
   else
   {
      m_shuffleboard_autoChooser.SetDefaultOption("0 - " + m_programTable.front()->getName(), std::to_string(0));
   }


   
   
   

   
   

   frc::Shuffleboard::GetTab("Autonomous").Add("Auto Chooser", m_shuffleboard_autoChooser).WithSize(3, 1).WithPosition(0, 0)
      .WithWidget(frc::BuiltInWidgets::kComboBoxChooser);

  
   // push failed program out to shuffleboard
   m_shuffleboard_failed_program.SetString(m_failedProgramName.c_str());


   // take the local members and shove them into the data values
   // so we can see them on the dashboard
   strncpy(m_data->autonomous_lockedProgram, m_lockedInProgramName.c_str(), 31);
   strncpy(m_data->autonomous_failedProgram, m_failedProgramName.c_str(), 31);
   
   m_data->autonomous_lockedProgram[31] = '\0';
   m_data->autonomous_failedProgram[31] = '\0';
}

////////////////////////////////////////////////////////////////////////////////
Controller::~Controller(void)
{
   for (auto const &program : m_programTable)
   {
      delete program;
   }
}


////////////////////////////////////////////////////////////////////////////////
Psb::Subsystem::Dashstream::BaseInterface_I *
Controller::getDashstreamInterface(void)
{
   return mp_dashstreamIntf;
}

////////////////////////////////////////////////////////////////////////////////
void
Controller::init(void)
{
}

////////////////////////////////////////////////////////////////////////////////
void Controller::loadConditionalCreators(void)
{
   ConditionFactory::addConditionCreator("RotatedByAngleCondition",
         new RotatedByAngleConditionCreator());
   ConditionFactory::addConditionCreator("RotatedByAnglePreciseCondition",
         new RotatedByAnglePreciseConditionCreator());
   ConditionFactory::addConditionCreator("DriveStraightWithEncodersCondition",
         new DriveStraightWithEncodersConditionCreator());
   ConditionFactory::addConditionCreator("RotatedByArcAndRadiusCondition",
         new RotatedByArcAndRadiusConditionCreator());
   ConditionFactory::addConditionCreator("BooleanTester",
         new BooleanTesterCreator());

}

////////////////////////////////////////////////////////////////////////////////
void
Controller::performManualUpdate(void)
{

}

////////////////////////////////////////////////////////////////////////////////
void
Controller::performAutoUpdate(void)
{
   switch (m_frcMatchState)
   {
      case MATCH_STATE_DISABLED:
      {
         // There's a lot to do here, rely on the helper
         // this->performUpdateDisabled();
      }
      break;

      case MATCH_STATE_AUTONOMOUS:
      {         

         
         //update our specific oi for skipping rest of auto
        // Subsystem::GlobalOiWpiStore::instance()->getGrabberByName("autonomous_teleopEnabler")->grabData();


         if (m_teleopInAuto == true /*|| m_data->autonomous_teleopEnabler*/)// if we wan't to stop auto
      	{

            if (m_teleopInAuto == false)//If first time stopping auto, Then reset stores
            {
               // Reset all the AIs so they don't have any lingering
   		      // values leftover from the autonomous run
  	            Subsystem::GlobalAiWpiStore::instance()->reset();

   	   	   // Pull all the real values from the OIs so they are all what are
   	   	   // actively set.  We need to do this here because autonomous can
  		         // mess with any of the values so there is no telling what state
 		         // they are logically in and we want them to be what they really are.
  	   	      Subsystem::GlobalOiWpiStore::instance()->refresh();

  		         //Note /\ If we can figure out how to save the Ai and Oi store and 
  		         //stop all timers involved in auto when we stop and resume when we continue
  		         // we should be able to continue a auto program from where we left off
               

               m_teleopInAuto = true;
               
            }
            else//Run Teleop
            {
            
               //we want teleop so we need to update oi
               Subsystem::GlobalOiWpiStore::instance()->refresh();

            }
      		
      	}
      	else// if we wan't to continue Auto
         {
        
           if (MATCH_STATE_DISABLED == m_lastFrcMatchState)
           {
             // We just landed in autonomous, re-init the program!
             m_programTable[m_lockedInProgramIndex]->reinitialize();
           }      
     
           m_programTable[m_lockedInProgramIndex]->run();
      
         }
      

      }
      break;

      case MATCH_STATE_TELEOPERATED:
      {
         // This is a coding defect!
         // Don't run the autonomous subsystem in TELEOP!
         PSB_LOG_ERROR(0);
      }
      break;

      case MATCH_STATE_TEST:
      {
         // This is a coding defect!
         // Don't run the autonomous subsystem in TEST!
         PSB_LOG_ERROR(0);
      }
      break;

      default:
      {
         // This is a coding defect!
         PSB_LOG_ERROR(1, Psb::LOG_TYPE_INT32, m_frcMatchState);
      }
      break;

   }

   // sync these up
   m_lastFrcMatchState = m_frcMatchState;
}

////////////////////////////////////////////////////////////////////////////////
void
Controller::transitionToState(subsystem_state_t new_state)
{
   // keep track of the old state just in case
   subsystem_state_t prev_state = m_state;

   // update the base class member so the update() function calls
   // the right thing
   m_state = new_state;

   /// now do whatever else it is you need to do during a transiton
   switch(m_state)
   {
      case Psb::Subsystem::SUBSYSTEM_AUTOMATIC:
      {
         switch(prev_state)
         {
            case Psb::Subsystem::SUBSYSTEM_AUTOMATIC:
            break;

            case Psb::Subsystem::SUBSYSTEM_DISABLED:
            break;

            case Psb::Subsystem::SUBSYSTEM_MANUAL:
            break;

            default:
            {
               PSB_LOG_ERROR(2,
                             Psb::LOG_TYPE_INT32, m_state,
                             Psb::LOG_TYPE_INT32, new_state);
               m_state = Psb::Subsystem::SUBSYSTEM_DISABLED;
            } break;
         }
      }
      break;
      case Psb::Subsystem::SUBSYSTEM_DISABLED:
      {
         switch(prev_state)
         {
            case Psb::Subsystem::SUBSYSTEM_AUTOMATIC:
            break;

            case Psb::Subsystem::SUBSYSTEM_DISABLED:
            break;

            case Psb::Subsystem::SUBSYSTEM_MANUAL:
            break;

            default:
            {
               PSB_LOG_ERROR(2,
                             Psb::LOG_TYPE_INT32, m_state,
                             Psb::LOG_TYPE_INT32, new_state);
               m_state = Psb::Subsystem::SUBSYSTEM_DISABLED;
            } break;
         }
      }
      break;
      case Psb::Subsystem::SUBSYSTEM_MANUAL:
      {
         switch(prev_state)
         {
            case Psb::Subsystem::SUBSYSTEM_AUTOMATIC:
            break;

            case Psb::Subsystem::SUBSYSTEM_DISABLED:
            break;

            case Psb::Subsystem::SUBSYSTEM_MANUAL:
            break;

            default:
            {
               // Unknown state!  This is a coding defect!
               PSB_LOG_ERROR(2,
                             Psb::LOG_TYPE_INT32, m_state,
                             Psb::LOG_TYPE_INT32, new_state);
               m_state = Psb::Subsystem::SUBSYSTEM_DISABLED;
            } break;
         }
      }
      break;

      default:
      {
         // Unknown state!  This is a coding defect!
         PSB_LOG_ERROR(1,
                       Psb::LOG_TYPE_INT32, m_state,
                       Psb::LOG_TYPE_INT32, new_state);
         m_state = Psb::Subsystem::SUBSYSTEM_DISABLED;
      }
      break;
   }
}

////////////////////////////////////////////////////////////////////////////////
void Controller::setEnabledState(enabled_state_t /*new_state*/)
{
}

//////////////////////////////////////////////////////////////////////////
void Controller::setFrcMatchState(frc_match_state_t new_state)
{
   // keep the old one around for comparison
   m_lastFrcMatchState = m_frcMatchState;
   m_frcMatchState = new_state;
}

////////////////////////////////////////////////////////////////////////////////
void
Controller::performDisabledUpdate(void)
{

   //check if we want to enable teleop in auto
   m_teleopInAuto = false;//m_data->autonomous_teleopEnabler;


   // //////////////////////////////////////////////////////////////////////////////
   // parse desired auto index from the shuffleboard combo box
   // /////////////////////////////////////////////////////////////////////////////

   try
   {
      m_lockedInProgramIndex = std::stoi(m_shuffleboard_autoChooser.GetSelected().c_str());
   }
   catch (...)
   {
      //std::cout<<"failed to parse auto index"<<std::endl;
   }
  
   

   // //////////////////////////////////////////////////////////////////////////
   // Update our selected/locked program names
   // //////////////////////////////////////////////////////////////////////////

   m_lockedInProgramName  = "";
   m_lockedInProgramName += std::to_string(m_lockedInProgramIndex);
   m_lockedInProgramName += " - ";
   m_lockedInProgramName += m_programTable[m_lockedInProgramIndex]->getName();

   // Cap size if needed
   if (m_lockedInProgramName.size() > 31) { m_lockedInProgramName.resize(31); }

   // take the local members and shove them into the data values
   // so we can see them on the dashboard
   strncpy(m_data->autonomous_lockedProgram, m_lockedInProgramName.c_str(), 31);
   m_data->autonomous_lockedProgram[31] = '\0';

  


}



////////////////////////////////////////////////////////////////////////////////
std::string
Controller::getLockedProgramName(void) const
{
   return m_lockedInProgramName;
}



} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////
