////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////

START_NEW_SS(Autonomous)
   //////   OI   //////
               

   //////  SS   //////
   SS_POD_PUBLISH_BOOL(autonomous_found_bad_xml, "bad_auto_xml",
               "Bad Auto XML", "There is bad XML in one Auto Program")
      SS_CHAR32_POD_DATA(autonomous_lockedProgram, "SS",
                  "Locked Auto Program", "Currently locked in Auto Program")
   SS_CHAR32_POD_DATA(autonomous_failedProgram, "SS",
               "Failed Auto Program", "Bad xml in Auto Program")

   SS_POD_CONFIG_DOUBLE(autonomous_trackwidth, 22.0f, "trackwidth of robot", "N/A")

   // use this to control debug in auto if you add auto debug please use this to determine whether its on or off
   SS_POD_PUBLISH_BOOL(autonomous_debugDataEnabler, "debugDataEnabler_auto_xml",
               "auto debug data enabler", "when true output current program name etc")

   
/*   SS_POD_JOYSTICK_BUTTON(autonomous_teleopEnabler, RAW_BUTTON_9, CCI_TWO,
                          "teleop Enabled in Auto", "CCI 1, button 9")*/
END_SS



