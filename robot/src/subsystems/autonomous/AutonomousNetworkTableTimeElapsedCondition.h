////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __AutonomousShuffleElapsedCondition_h__
#define __AutonomousShuffleElapsedCondition_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbMacros.h>
#include <frc/shuffleboard/Shuffleboard.h>
#include "AutonomousXmlDescriptions.h"
#include <PsbStringConverter.h>
#include "AutonomousCondition.h"


// /////////////////////////////////////////////////////////////////////////////
// Forward Declaration(s)
// /////////////////////////////////////////////////////////////////////////////
namespace frc { class Timer; }


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// The NetworkTableTimeElapsedCondition object will provide a true indication
/// via the isConditionMet() method when the desired amount of time has elapsed.
///
/// @remarks
/// is near identical to TimeElapsedCondition just with a network table entry for
/// the wait time
////////////////////////////////////////////////////////////////////////////////
class NetworkTableTimeElapsedCondition : public Condition
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Constructor
      ///
      /// @param[in] networkTableWaitTime   Number of seconds before the condition is met
      /// as a network table entry, 
      /// @param[in] default_waitTime is the default number of seconds if the 
      /// network table entry isn't accesible
      //////////////////////////////////////////////////////////////////////////
      NetworkTableTimeElapsedCondition(nt::NetworkTableEntry networkTableWaitTime, double default_waitTime);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~NetworkTableTimeElapsedCondition(void);
      
      //////////////////////////////////////////////////////////////////////////
      /// @see Condition::initialize()
      //////////////////////////////////////////////////////////////////////////
      virtual void initialize(void);
      
      //////////////////////////////////////////////////////////////////////////
      /// @see Condition::isConditionMet()
      //////////////////////////////////////////////////////////////////////////
      virtual bool isConditionMet(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Builds and returns a Conditions object based on the description
      /// given.
      ///
      /// @param[in] rc_desc   The condition description information
      //////////////////////////////////////////////////////////////////////////
      virtual Autonomous::Condition*
      createCondition(Autonomous::condition_description_t const& rc_desc);
      
   private:
      nt::NetworkTableEntry m_networkTableWaitTime;
      double m_waitTimeInSeconds;
      double m_default_waitTimeInSeconds;
      frc::Timer*  mp_timer;
      
   private:
      // Hidden from public view
      DISALLOW_DEFAULT_CTOR(NetworkTableTimeElapsedCondition);
      DISALLOW_COPY_CTOR(NetworkTableTimeElapsedCondition);
      DISALLOW_ASSIGNMENT_OPER(NetworkTableTimeElapsedCondition);
}; // END class NetworkTableTimeElapsedCondition


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb

#endif  // __AutonomousNetworkTableElapsedCondition_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

