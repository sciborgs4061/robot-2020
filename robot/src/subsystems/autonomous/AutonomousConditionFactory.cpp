////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbLogger.h>
#include <PsbStringConverter.h>
#include "AutonomousConditionFactory.h"
#include "AutonomousNetworkTableTimeElapsedCondition.h"
#include "AutonomousController.h"
#include "AutonomousCompositeCondition.h"
#include "AutonomousConstCondition.h"
#include "AutonomousTimeElapsedCondition.h"
#include "frc/smartdashboard/SmartDashboard.h"

// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {

////////////////////////////////////////////////////////////////////////////////
// init statics
ConditionFactory::condition_creator_list_t ConditionFactory::s_conditionCreators;

////////////////////////////////////////////////////////////////////////////////
Condition*
ConditionFactory::createCondition(condition_description_t const& rc_desc)
{
   Condition* p_condition = NULL;

   // Create conditions that we support
   if ("CompositeCondition" == rc_desc.classname)
   {
      // We are building a CompositeCondition, and we need the inputs
      CompositeCondition::operator_t operation = CompositeCondition::OPERATOR_OR;
           if ("OR"   == rc_desc.operation)  { operation = CompositeCondition::OPERATOR_OR;   }
      else if ("NOR"  == rc_desc.operation)  { operation = CompositeCondition::OPERATOR_NOR;  }
      else if ("XOR"  == rc_desc.operation)  { operation = CompositeCondition::OPERATOR_XOR;  }
      else if ("XNOR" == rc_desc.operation)  { operation = CompositeCondition::OPERATOR_XNOR; }
      else if ("AND"  == rc_desc.operation)  { operation = CompositeCondition::OPERATOR_AND;  }
      else if ("NAND" == rc_desc.operation)  { operation = CompositeCondition::OPERATOR_NAND; }
      else                                   { operation = CompositeCondition::OPERATOR_OR;   }

      // Build the object
      p_condition =
         new CompositeCondition(rc_desc.id, operation);
   }
   else if ("ConstCondition" == rc_desc.classname)
   {
      bool flag =
         Psb::StringConverter::toBoolean(rc_desc.params.at("flag").c_str(), true);
      p_condition = new ConstCondition(flag);
   }
   else if ("TimeElapsedCondition" == rc_desc.classname)
   {
      float wait_time = 
         Psb::StringConverter::toFloat(rc_desc.params.at("wait_time").c_str(), 1.0f);
      p_condition = new TimeElapsedCondition(wait_time);
   } 
   else if ("ShuffleboardTimeElapsedCondition" == rc_desc.classname)
   {

     std::string networkTableEntryName = rc_desc.params.at("network_table_entry");
     double default_duration = Psb::StringConverter::toFloat(rc_desc.params.at("default_wait_time").c_str(), 0.0f);



     
      // add the network table stuff to shuffleboard
      frc::ShuffleboardLayout& autoGridLayout =
      frc::Shuffleboard::GetTab("Autonomous").GetLayout("ProgramParameters", "Grid Layout");


      nt::NetworkTableEntry ntEntry = autoGridLayout.Add(networkTableEntryName, default_duration).GetEntry();
   
    /*  nt::NetworkTableEntry example = autoGridLayout
      .Add(widgetName, false)
      .WithWidget(frc::BuiltInWidgets::kToggleButton)
      .GetEntry();
*/
     
     p_condition = new NetworkTableTimeElapsedCondition(ntEntry, default_duration);
   }
   else
   {
      // If there is a conditionCreator registered for this condition
      // the have it make you the condition handler
      condition_creator_list_t::iterator iter = 
         s_conditionCreators.find(rc_desc.classname);
      if (iter != s_conditionCreators.end())
      {
          p_condition = iter->second->createCondition(rc_desc);
      }

      if (p_condition == NULL)
      {
         // Unknown condition!
         PSB_LOG_ERROR(5, Psb::LOG_TYPE_STRING, rc_desc.id.c_str(),
                        Psb::LOG_TYPE_STRING, rc_desc.parent_id.c_str(),
                        Psb::LOG_TYPE_STRING, rc_desc.type.c_str(),
                        Psb::LOG_TYPE_STRING, rc_desc.operation.c_str(),
                        Psb::LOG_TYPE_STRING, rc_desc.classname.c_str());

         // Build the object
         p_condition = new ConstCondition(true);
      }
   }

   return p_condition;
}

//////////////////////////////////////////////////////////////////////////
void ConditionFactory::addConditionCreator(std::string const &conditionName,
                                           ConditionCreator *creator)
{
   s_conditionCreators[conditionName] = creator;
}

} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

