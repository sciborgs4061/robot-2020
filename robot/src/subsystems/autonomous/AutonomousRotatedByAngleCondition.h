////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __AutonomousRotatedByAngleCondition_h__
#define __AutonomousRotatedByAngleCondition_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbMacros.h>
#include "AutonomousCondition.h"
#include "AutonomousConditionFactory.h"
#include "AutonomousXmlDescriptions.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// The RotatedByAngleCondition object will provide a true indication
/// via the isConditionMet() function when the robot has rotated through the
/// prescribed angle.
///
/// @remarks
/// Positive sweep angles indicate a clockwise rotation, negative indicate CCW.
////////////////////////////////////////////////////////////////////////////////
class RotatedByAngleCondition : public Condition
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Constructor
      ///
      /// @param[in] sweepAngle    The expected angle that the robot will rotate
      ///                          through.
      //////////////////////////////////////////////////////////////////////////
      RotatedByAngleCondition(double sweepAngle);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~RotatedByAngleCondition(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see Condition::initialize()
      //////////////////////////////////////////////////////////////////////////
      virtual void initialize(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see Condition::isConditionMet()
      //////////////////////////////////////////////////////////////////////////
      virtual bool isConditionMet(void);

   private:

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Current reading of the Gyro
      //////////////////////////////////////////////////////////////////////////
      double m_current_angle;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// We keep track of the Gyro's starting angle for our rotation
      /// calculations rather than resetting it.
      //////////////////////////////////////////////////////////////////////////
      double m_starting_angle;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This is the target sweep angle.
      ///
      /// @remarks
      /// This condition is met once the robot has rotated by at least this many
      /// degrees in the corresponding direction.
      //////////////////////////////////////////////////////////////////////////
      double m_sweep_angle;

   private:
      // Hidden from public view
      DISALLOW_DEFAULT_CTOR(RotatedByAngleCondition);
      DISALLOW_COPY_CTOR(RotatedByAngleCondition);
      DISALLOW_ASSIGNMENT_OPER(RotatedByAngleCondition);
}; // END class RotatedByAngleCondition

// /////////////////////////////////////////////////////////////////////////////
// @brief
// The class that will create the above RotateByAngleCondition when needed
// /////////////////////////////////////////////////////////////////////////////
class RotatedByAngleConditionCreator :
        public Autonomous::ConditionFactory::ConditionCreator
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief Constructor to create the Creator
      //////////////////////////////////////////////////////////////////////////
      RotatedByAngleConditionCreator(void)
      {
      }

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Builds and returns a Conditions object based on the description
      /// given.
      ///
      /// @param[in] rc_desc   The condition description information
      //////////////////////////////////////////////////////////////////////////
      virtual Autonomous::Condition*
              createCondition(Autonomous::condition_description_t const& rc_desc);

   private:
      // Hidden from public view
      DISALLOW_COPY_CTOR(RotatedByAngleConditionCreator);
      DISALLOW_ASSIGNMENT_OPER(RotatedByAngleConditionCreator);

}; // END class RotateByAngleConditionCreator
} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb

#endif  // __AutonomousRotatedByAngleCondition_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////


