////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//
////////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbLogger.h>
#include "AutonomousDriveStraightWithEncodersCondition.h"
#include "PsbStringConverter.h"
#include "SiDataGrabbers.h"
#include "GlobalMiStore.h"
#include "GlobalSiWpiStore.h"
#include "GlobalOiWpiStore.h"

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {

////////////////////////////////////////////////////////////////////////////////
DriveStraightWithEncodersCondition::
DriveStraightWithEncodersCondition(double sweep_angle,
                                   double slowdown_angle,
                                   double heading,
                                   double minimum_heading,
                                   double angle_deadband,
                                   std::string heading_oi_name,
                                   std::string store_name,
                                   std::string left_encoder_name,
                                   std::string right_encoder_name,
                                   encoder_selection_t encoderSelection,
                                   double left_ticks,
                                   double right_ticks)
   : Condition(__FUNCTION__)
   , m_current_angle(0.0f)
   , m_starting_angle(0.0f)
   , m_sweep_angle(sweep_angle)
   , m_heading(heading)
   , m_slowdown_angle(slowdown_angle)
   , m_minimum_heading(minimum_heading)
   , m_angle_deadband(angle_deadband)
   , m_grabber(NULL)
   , m_leftEncoderInitialTickCount(0.0)
   , m_rightEncoderInitialTickCount(0.0)
   , m_leftEncoderTargetTickCount(left_ticks)
   , m_rightEncoderTargetTickCount(right_ticks)
   , m_encoderSelection(encoderSelection)
{

   GlobalSiWpiStore *si = GlobalSiWpiStore::instance();
   si->attach(new NavigationDataGrabber(&(m_current_angle),
              NavigationDataGrabber::ADJUSTED_ANGLE));

   
   if (store_name == "mi")
   {
      GlobalMiStore *mi = GlobalMiStore::instance();
      mi->mirror(left_encoder_name, new DataMirror<double>(&(m_left_drive_encoder)));
      mi->mirror(right_encoder_name, new DataMirror<double>(&(m_right_drive_encoder)));
   }
   else if (store_name == "si")// if we want si store 
   {

      si->attach(new EncoderDataGrabber(&(m_left_drive_encoder), EncoderDataGrabber::COUNT),
                  left_encoder_name);
      si->attach(new EncoderDataGrabber(&(m_right_drive_encoder), EncoderDataGrabber::COUNT),
                  right_encoder_name);
   }
   else
   {
      //error?
   }
   
   
   




   GlobalOiWpiStore *oi = GlobalOiWpiStore::instance();
   m_grabber = static_cast<JoystickDataGrabber *>(oi->getGrabberByName(heading_oi_name.c_str()));
}


////////////////////////////////////////////////////////////////////////////////
DriveStraightWithEncodersCondition::~DriveStraightWithEncodersCondition(void)
{
   // Intentionally left empty
}


////////////////////////////////////////////////////////////////////////////////
void
DriveStraightWithEncodersCondition::initialize(void)
{
   // Note the current heading angle instead of resetting the gyro
   // m_starting_angle = m_current_angle;
   m_starting_angle = NavigationDataGrabber::get_snapshot();

   // Get the initial Encoder counts from the InputFacade and store them
   m_leftEncoderInitialTickCount  = m_left_drive_encoder;
   m_rightEncoderInitialTickCount  = m_right_drive_encoder;
}

////////////////////////////////////////////////////////////////////////////////
void
DriveStraightWithEncodersCondition::adjustDriveAngle(void)
{
   // double actual_angle = m_starting_angle - m_current_angle;
   double actual_angle = m_current_angle - m_starting_angle;
   // double distance_away = actual_angle;
   double distance_away = m_sweep_angle - actual_angle;
   double correction_value = 1.0;
   double calculated_heading = m_heading;

   if (distance_away < 0)
   {
      distance_away *= -1.0;
      correction_value = -1.0;
   }

   if (distance_away <= m_slowdown_angle)
   {
      calculated_heading = (distance_away / m_slowdown_angle) * m_heading;
      if (calculated_heading < m_minimum_heading)
      {
         calculated_heading = m_minimum_heading;
      }
   }

   if (distance_away < m_angle_deadband)
   {
      calculated_heading = 0.0;
   }

   m_grabber->setValue(calculated_heading * correction_value);


  
}

////////////////////////////////////////////////////////////////////////////////
bool
DriveStraightWithEncodersCondition::isConditionMet(void)
{
   // //////////////////////////////////////////////////////////////////////////
   // Depending on how this object was constructed, the isConditionMet() method
   //  will return different indications.  In call cases, we need to make a
   //  determination as to whether or not both encoders have attained their
   //  target values.  Then, depending on the selection type, we will make a
   //  secondary determination as to whether the overall condition is met.
   //
   // NOTE:
   //  We default to the condition being met to prevent cases where the robot
   //  would run away in an uncontrolled manner.
   // //////////////////////////////////////////////////////////////////////////
   bool is_condition_met = true;

   
   // Now, get the instantaneous encoder counts...
   double left_enc_count  = m_left_drive_encoder;
   double right_enc_count = m_right_drive_encoder;

   // Next, determine the number of ticks traveled by subtracting the initial
   // values
   double left_enc_count_diff  = left_enc_count  - m_leftEncoderInitialTickCount;
   double right_enc_count_diff = right_enc_count - m_rightEncoderInitialTickCount;

   // Next, determine whether or not the left & right encoders have met their
   // target values, irrespective of whether or not they are used in the
   // selection.
   bool left_target_hit =
      (m_leftEncoderTargetTickCount >= 0)
      ? (left_enc_count_diff >= m_leftEncoderTargetTickCount)
      : (left_enc_count_diff <= m_leftEncoderTargetTickCount);

   bool right_target_hit =
      (m_rightEncoderTargetTickCount >= 0)
      ? (right_enc_count_diff >= m_rightEncoderTargetTickCount)
      : (right_enc_count_diff <= m_rightEncoderTargetTickCount);

   // ///////////////////////////////////////////////////////////////////////
   // Finally, the condition is met when the selection is taken into account
   //   +-----------+---+---+--------+
   //   | Selection | L | R | C-Met? |
   //   +-----------+---+---+--------+
   //   |    BOTH   | N | N | No     |
   //   |    BOTH   | N | Y | No     |
   //   |    BOTH   | Y | N | No     |
   //   |    BOTH   | Y | Y | Yes    |
   //   +-----------+---+---+--------+
   //   |    LEFT   | N | N | No     |
   //   |    LEFT   | N | Y | No     |
   //   |    LEFT   | Y | N | Yes    |
   //   |    LEFT   | Y | Y | Yes    |
   //   +-----------+---+---+--------+
   //   |   RIGHT   | N | N | No     |
   //   |   RIGHT   | N | Y | Yes    |
   //   |   RIGHT   | Y | N | No     |
   //   |   RIGHT   | Y | Y | Yes    |
   //   +-----------+---+---+--------+
   //   |  EITHER   | N | N | No     |
   //   |  EITHER   | N | Y | Yes    |
   //   |  EITHER   | Y | N | Yes    |
   //   |  EITHER   | Y | Y | Yes    |
   //   +-----------+---+---+--------+
   //   |  NEITHER  | X | X | Yes    |
   //   +-----------+---+---+--------+
   //   |  INVALID  | X | X | Yes    |
   //   +-----------+---+---+--------+
   // ///////////////////////////////////////////////////////////////////////
   switch (m_encoderSelection)
   {
      case SELECT_BOTH_ENCODERS:
      {
         // Both left and right targets must be hit
         is_condition_met = 
         ( (true == left_target_hit) &&
           (true == right_target_hit) );
      } break;

      case SELECT_LEFT_ENCODER:
      {
         // Just the left target must be hit
         is_condition_met = (true == left_target_hit);
      } break;

      case SELECT_RIGHT_ENCODER:
      {
         // Just the right target must be hit
         is_condition_met = (true == right_target_hit);
      } break;

      case SELECT_EITHER_ENCODER:
      {
         // Either the left OR the right target must be hit
         is_condition_met = 
         ( (true == left_target_hit) ||
           (true == right_target_hit) );
      } break;

      case SELECT_NEITHER_ENCODER:
      {
         // Caller wants to select neither encoder - condition is met already
         PSB_LOG_WARNING(10, Psb::LOG_TYPE_INT32, m_leftEncoderInitialTickCount,
                             Psb::LOG_TYPE_INT32, m_rightEncoderInitialTickCount,
                             Psb::LOG_TYPE_INT32, m_leftEncoderTargetTickCount,
                             Psb::LOG_TYPE_INT32, m_rightEncoderTargetTickCount,
                             Psb::LOG_TYPE_INT32, left_enc_count_diff,
                             Psb::LOG_TYPE_INT32, right_enc_count_diff,
                             Psb::LOG_TYPE_INT32, m_encoderSelection,
                             Psb::LOG_TYPE_BOOL,  left_target_hit,
                             Psb::LOG_TYPE_BOOL,  right_target_hit,
                             Psb::LOG_TYPE_BOOL,  is_condition_met);
         is_condition_met = true;
      } break;

      default:
      {
         // Unsupported Encoder selection mode
         PSB_LOG_ERROR(10, Psb::LOG_TYPE_INT32, m_leftEncoderInitialTickCount,
                           Psb::LOG_TYPE_INT32, m_rightEncoderInitialTickCount,
                           Psb::LOG_TYPE_INT32, m_leftEncoderTargetTickCount,
                           Psb::LOG_TYPE_INT32, m_rightEncoderTargetTickCount,
                           Psb::LOG_TYPE_INT32, left_enc_count_diff,
                           Psb::LOG_TYPE_INT32, right_enc_count_diff,
                           Psb::LOG_TYPE_INT32, m_encoderSelection,
                           Psb::LOG_TYPE_BOOL,  left_target_hit,
                           Psb::LOG_TYPE_BOOL,  right_target_hit,
                           Psb::LOG_TYPE_BOOL,  is_condition_met);
         is_condition_met = true;
      } break;
   }

   // Dump copious amounts of info for debugging...
   PSB_LOG_DEBUG3(10, Psb::LOG_TYPE_INT32, m_leftEncoderInitialTickCount,
                      Psb::LOG_TYPE_INT32, m_rightEncoderInitialTickCount,
                      Psb::LOG_TYPE_INT32, m_leftEncoderTargetTickCount,
                      Psb::LOG_TYPE_INT32, m_rightEncoderTargetTickCount,
                      Psb::LOG_TYPE_INT32, left_enc_count_diff,
                      Psb::LOG_TYPE_INT32, right_enc_count_diff,
                      Psb::LOG_TYPE_INT32, m_encoderSelection,
                      Psb::LOG_TYPE_BOOL,  left_target_hit,
                      Psb::LOG_TYPE_BOOL,  right_target_hit,
                      Psb::LOG_TYPE_BOOL,  is_condition_met);

   // if we arent done calc a drive angle normaly, otherwise set heading to 0.0
   if (!is_condition_met)
   {
      adjustDriveAngle();
   }
   else
   {
      m_grabber->setValue(0.0);
   }
   
   

   // And we're done... return the condition indication
   return is_condition_met;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//                       CONDITION CREATOR
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
static inline
DriveStraightWithEncodersCondition::encoder_selection_t
convertStringToEncoderSelection(std::string& rc_string)
{
   // Default to unknown - should NEVER happen
   DriveStraightWithEncodersCondition::encoder_selection_t selection = DriveStraightWithEncodersCondition::ENCODER_SELECTION_INVALID;

   if      ("SELECT_BOTH_ENCODERS" == rc_string)   { selection = DriveStraightWithEncodersCondition::SELECT_BOTH_ENCODERS; }
   else if ("SELECT_LEFT_ENCODER" == rc_string)    { selection = DriveStraightWithEncodersCondition::SELECT_LEFT_ENCODER; }
   else if ("SELECT_RIGHT_ENCODER" == rc_string)   { selection = DriveStraightWithEncodersCondition::SELECT_RIGHT_ENCODER; }
   else if ("SELECT_EITHER_ENCODER" == rc_string)  { selection = DriveStraightWithEncodersCondition::SELECT_EITHER_ENCODER; }
   else if ("SELECT_NEITHER_ENCODER" == rc_string) { selection = DriveStraightWithEncodersCondition::SELECT_NEITHER_ENCODER; }
   else                                            { selection = DriveStraightWithEncodersCondition::SELECT_BOTH_ENCODERS; }

   return selection;
}


Autonomous::Condition* DriveStraightWithEncodersConditionCreator::createCondition(
                Autonomous::condition_description_t const& rc_desc)
{
   double sweep_angle =
      Psb::StringConverter::toFloat(rc_desc.params.at("sweep_angle").c_str(), 0);
   double slowdown_angle =
      Psb::StringConverter::toFloat(rc_desc.params.at("slowdown_angle").c_str(), 10);
   double heading =
      Psb::StringConverter::toFloat(rc_desc.params.at("heading").c_str(), 1.0);
   double minimum_heading =
      Psb::StringConverter::toFloat(rc_desc.params.at("minimum_heading").c_str(), 0.2);
   double angle_deadband =
      Psb::StringConverter::toFloat(rc_desc.params.at("angle_deadband").c_str(), 1.0);
   std::string store_name = rc_desc.params.at("store_name");
   std::string heading_oi_name = rc_desc.params.at("heading_oi_name");
   std::string left_encoder_name  = rc_desc.params.at("left_encoder_name");
   std::string right_encoder_name = rc_desc.params.at("right_encoder_name");

   double left_ticks = 
      Psb::StringConverter::toFloat(rc_desc.params.at("left_ticks").c_str(), 0);

   double right_ticks = 
      Psb::StringConverter::toFloat(rc_desc.params.at("right_ticks").c_str(), 0);

   std::string selection_str = rc_desc.params.at("enc_selection");

   DriveStraightWithEncodersCondition::encoder_selection_t encoderSelection = 
      convertStringToEncoderSelection(selection_str);

   return new DriveStraightWithEncodersCondition(
                                             sweep_angle,
                                             slowdown_angle,
                                             heading,
                                             minimum_heading,
                                             angle_deadband,
                                             heading_oi_name,
                                             store_name,
                                             left_encoder_name,
                                             right_encoder_name,
                                             encoderSelection,
                                             left_ticks,
                                             right_ticks);
}


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb

