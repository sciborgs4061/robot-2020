////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbLogger.h>
#include "frc/Timer.h"
#include <frc/shuffleboard/Shuffleboard.h>
#include "AutonomousNetworkTableTimeElapsedCondition.h"


// /////////////////////////////////////////////////////////////////////////////
// Defines(s)
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
NetworkTableTimeElapsedCondition::
NetworkTableTimeElapsedCondition(nt::NetworkTableEntry networkTableWaitTime, double default_waitTime)
   : Condition(__FUNCTION__)
   , m_waitTimeInSeconds(default_waitTime)
   , m_default_waitTimeInSeconds(default_waitTime)
   , m_networkTableWaitTime(networkTableWaitTime)
   , mp_timer(NULL)
{
   mp_timer = new frc::Timer();
}


////////////////////////////////////////////////////////////////////////////////
NetworkTableTimeElapsedCondition::
~NetworkTableTimeElapsedCondition(void)
{
   delete mp_timer;
   mp_timer = NULL;
}


////////////////////////////////////////////////////////////////////////////////
void
NetworkTableTimeElapsedCondition::initialize(void)
{
   // start the timer and try fetching the network table entry
   m_waitTimeInSeconds = m_networkTableWaitTime.GetDouble(m_default_waitTimeInSeconds);
   
   mp_timer = new frc::Timer();// this is apparently works better than using their reset funciton
   mp_timer->Start();
   return;
}


////////////////////////////////////////////////////////////////////////////////
bool
NetworkTableTimeElapsedCondition::isConditionMet(void)
{
   // //////////////////////////////////////////////////////////////////////////
   // The WaitForTime condition is met once the provided wait time has elapsed.
   //  We can check this condition by starting a stop-watch timer in the
   //  initialize() method and seeing whether or not the HasPeriodPassed()
   //  method returns true for the given wait time.
   // //////////////////////////////////////////////////////////////////////////
   
   // Read the limit switch state from the InputFacade
   bool is_condition_met = (mp_timer->HasPeriodPassed(m_waitTimeInSeconds));
   


   if (m_waitTimeInSeconds == 0.0)
   {
      return true;
   }




   // Post an entry to the debug log...
   PSB_LOG_DEBUG1(3,
                  Psb::LOG_TYPE_FLOAT64, mp_timer->GetFPGATimestamp(),
                  Psb::LOG_TYPE_FLOAT64, m_waitTimeInSeconds,
                  Psb::LOG_TYPE_BOOL,    is_condition_met);
                
   // And we're done... return the indication
   return is_condition_met;
}



Autonomous::Condition* NetworkTableTimeElapsedCondition::createCondition(
                Autonomous::condition_description_t const& rc_desc)
{
   // extract nt name/table and duration
   std::string networkTableEntryName = rc_desc.params.at("network_table_entry");
   std::string networkTableTableName = rc_desc.params.at("network_table_name");
   double default_duration = Psb::StringConverter::toFloat(rc_desc.params.at("default_wait_time").c_str(), 0.0f);

   nt::NetworkTableEntry ntEntry = nt::NetworkTableInstance::GetDefault().GetTable(networkTableTableName)->GetEntry(networkTableEntryName);

   return new NetworkTableTimeElapsedCondition(ntEntry, default_duration);
}


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

