////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __AutonomousRotatedByArcAndRadiusCondition_h__
#define __AutonomousRotatedByArcAndRadiusCondition_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbMacros.h>
#include "AutonomousCondition.h"
#include "AutonomousConditionFactory.h"
#include "AutonomousXmlDescriptions.h"
#include "OiDataGrabbers.h"
#include "AutonomousDriveStraightWithEncodersCondition.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// The RotatedByArcAndRadiusCondition object will provide a true indication
/// via the isConditionMet() function when the robot has rotated through the
/// prescribed angle.
///
/// @remarks
/// Positive sweep angles indicate a clockwise rotation, negative indicate CCW.
////////////////////////////////////////////////////////////////////////////////
class RotatedByArcAndRadiusCondition : public Condition
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Constructor
      ///
      /// @param[in] arc_length   the arc of the turn
      ///   
      /// @param[in] radius   the radius of the turn
      //////////////////////////////////////////////////////////////////////////
      RotatedByArcAndRadiusCondition(
                              double arc, // in ticks
                              double radius, // in ticks
                              std::string store_name, 
                              std::string left_encoder_name, 
                              std::string right_encoder_name,
                              DriveStraightWithEncodersCondition::encoder_selection_t encoder_selection,
                              std::string heading_oi_name,
                              double deadband,
                              double slow_ratio_band,
                              double min_heading,
                              double max_heading);


      

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~RotatedByArcAndRadiusCondition(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see Condition::initialize()
      //////////////////////////////////////////////////////////////////////////
      virtual void initialize(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see Condition::isConditionMet()
      //////////////////////////////////////////////////////////////////////////
      virtual bool isConditionMet(void);

   private:

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// the value used to help set the sign of the output heading
      //////////////////////////////////////////////////////////////////////////
      double m_default_correction_value;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// the ratio of left ticks to right ticks
      //////////////////////////////////////////////////////////////////////////
      double m_leftToRightRatio;

      
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The data grabber pointer for heading
      //////////////////////////////////////////////////////////////////////////
      JoystickDataGrabber* m_grabber;

      
      // si/mi store value of left and right drive encoders
      double m_left_encoder;
      double m_right_encoder;

      // initials
      double m_left_initial_ticks;
      double m_right_initial_ticks;

      // desired drive ticks
      double m_desired_right_ticks;
      double m_desired_left_ticks;

      // control configs for maintaining the encoder ratio
      double m_slowdown_ratio;// below this ratio distance the min heading is used
      double m_deadband;// below this ratio a zero heading is applied
      double m_min_heading;
      double m_max_heading;


      DriveStraightWithEncodersCondition::encoder_selection_t m_encoder_selection;

      // data to attatch to config. is only needed at start making this var excess
      double m_trackWidth_config;

   private:
      // Hidden from public view
      DISALLOW_DEFAULT_CTOR(RotatedByArcAndRadiusCondition);
      DISALLOW_COPY_CTOR(RotatedByArcAndRadiusCondition);
      DISALLOW_ASSIGNMENT_OPER(RotatedByArcAndRadiusCondition);
}; // END class RotatedByArcAndRadiusCondition

// /////////////////////////////////////////////////////////////////////////////
// @brief
// The class that will create the above RotatedByArcAndRadiusConditionCreator
// when needed
// /////////////////////////////////////////////////////////////////////////////
class RotatedByArcAndRadiusConditionCreator :
        public Autonomous::ConditionFactory::ConditionCreator
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief Constructor to create the Creator
      //////////////////////////////////////////////////////////////////////////
      RotatedByArcAndRadiusConditionCreator(void)
      {
      }

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Builds and returns a Conditions object based on the description
      /// given.
      ///
      /// @param[in] rc_desc   The condition description information
      //////////////////////////////////////////////////////////////////////////
      virtual Autonomous::Condition*
              createCondition(Autonomous::condition_description_t const& rc_desc);

   private:
      // Hidden from public view
      DISALLOW_COPY_CTOR(RotatedByArcAndRadiusConditionCreator);
      DISALLOW_ASSIGNMENT_OPER(RotatedByArcAndRadiusConditionCreator);

}; // END class RotateByAngleConditionCreator
} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb

#endif  // __AutonomousRotatedByArcAndRadiusCondition_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////


