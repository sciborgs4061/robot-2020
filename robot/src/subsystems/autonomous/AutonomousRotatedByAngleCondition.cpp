////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//
////////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbLogger.h>
#include "AutonomousRotatedByAngleCondition.h"
#include "PsbStringConverter.h"
#include "SiDataGrabbers.h"
#include "GlobalSiWpiStore.h"
// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
RotatedByAngleCondition::
RotatedByAngleCondition(double sweep_angle)
   : Condition(__FUNCTION__)
   , m_current_angle(0.0f)
   , m_starting_angle(0.0f)
   , m_sweep_angle(sweep_angle)
{
   GlobalSiWpiStore *si = GlobalSiWpiStore::instance();
   si->attach(new NavigationDataGrabber(&(m_current_angle),
              NavigationDataGrabber::ADJUSTED_ANGLE));
}


////////////////////////////////////////////////////////////////////////////////
RotatedByAngleCondition::~RotatedByAngleCondition(void)
{
   // Intentionally left empty
}


////////////////////////////////////////////////////////////////////////////////
void
RotatedByAngleCondition::initialize(void)
{
   // Note the current heading angle instead of resetting the gyro
   m_starting_angle = m_current_angle;
}


////////////////////////////////////////////////////////////////////////////////
bool
RotatedByAngleCondition::isConditionMet(void)
{
   // //////////////////////////////////////////////////////////////////////////
   // This function will return true when the Gyro has seen a rotation of the
   // desired angle.
   // //////////////////////////////////////////////////////////////////////////
   bool is_condition_met = false;

   if (m_sweep_angle >= 0.0f)
   {
      // The sweep angle is positive, so we look for the delta to be positive
      is_condition_met = ((m_current_angle - m_starting_angle) >= m_sweep_angle);
   }
   else
   {
      // The sweep angle is negative, so we look for the delta to be negative
      is_condition_met = ((m_current_angle - m_starting_angle) <= m_sweep_angle);
   }

   return is_condition_met;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//                       CONDITION CREATOR
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

Autonomous::Condition* RotatedByAngleConditionCreator::createCondition(
                Autonomous::condition_description_t const& rc_desc)
{
   double sweep_angle =
      Psb::StringConverter::toFloat(rc_desc.params.at("sweep_angle").c_str(), 0);

   return new RotatedByAngleCondition(sweep_angle);
}


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////
