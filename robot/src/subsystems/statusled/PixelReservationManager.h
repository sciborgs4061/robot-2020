// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __PixelReservationManager_h__
#define __PixelReservationManager_h__


// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <cstdint>
#include <bitset>
#include <set>
#include "PsbMacros.h"
#include "StatusLedTypes.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace StatusLed {


// /////////////////////////////////////////////////////////////////////////////
// Forward Declaration(s)
// /////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This policy class manages pixel allocation for an LED Strip.
////////////////////////////////////////////////////////////////////////////////
class PixelReservationManager
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Default Constructor
      //////////////////////////////////////////////////////////////////////////
      PixelReservationManager(void);
     
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~PixelReservationManager(void);
     
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Attempts to reserve the provides pixels, returning the reservation
      /// result.
      ///
      /// @remarks
      /// The reservation is checked for success before it is performed.  If the
      /// reservation fails, the registry will remain unchanged (i.e. pixel
      /// reservations are atomic).
      ///
      /// @param[in] pixels   The pixel set to operate on.
      //////////////////////////////////////////////////////////////////////////
      bool reservePixels(pixel_set_t pixels);
     
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Attempts to reserve all pixels, returning the reservation result.
      ///
      /// @see reserveAllPixels()
      //////////////////////////////////////////////////////////////////////////
      bool reserveAllPixels(void);
     
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Cancels the reservation of the requested pixels.
      ///
      /// @remarks
      /// All provided pixels will be left "unreserved" at the end of this
      /// operation, irrespective of their state before this function was
      /// called.
      ///
      /// @param[in] pixels   The pixel set to operate on.
      //////////////////////////////////////////////////////////////////////////
      void unreservePixels(pixel_set_t pixels);
     
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Cancels the reservation of all pixesl.
      ///
      /// @see unreservePixels()
      //////////////////////////////////////////////////////////////////////////
      void unreserveAllPixels(void);

   private:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Keeps track of pixels that are "reserved".
      ///
      /// @remarks
      /// This bitset is sized for MORE than the maximum number of pixels that
      /// could be displayed in the strip.
      //////////////////////////////////////////////////////////////////////////
      std::bitset<NUM_PIXEL_IDS> m_pixelRegistry;

   private:
      DISALLOW_COPY_CTOR(PixelReservationManager);
      DISALLOW_ASSIGNMENT_OPER(PixelReservationManager);
}; // END class PixelReservationManager


} // END namespace StatusLed
} // END namespace Subsystem
} // END namespace Psb


#endif // __PixelReservationManager_h__


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

