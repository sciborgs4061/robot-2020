// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbLogger.h>
#include "PixelReservationManager.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace StatusLed {


////////////////////////////////////////////////////////////////////////////////
PixelReservationManager::PixelReservationManager(void)
   : m_pixelRegistry()
{
}


////////////////////////////////////////////////////////////////////////////////
PixelReservationManager::~PixelReservationManager(void)
{
}


////////////////////////////////////////////////////////////////////////////////
bool
PixelReservationManager::reservePixels(pixel_set_t pixels)
{
   if (pixels.size() > 0)
   {
      // Check the reservation
      for (auto idx : pixels)
      {
         if (idx >= m_pixelRegistry.size())
         {
            // Index out of range
            PSB_LOG_ERROR(1,
                  Psb::LOG_TYPE_UINT32, idx);
            return false;
         }

         if (true == m_pixelRegistry.test(idx))
         {
            // Pixel already reserved (not an error)
            return false;
         }
      }

      // Make the reservation
      for (auto idx : pixels)
      {
         m_pixelRegistry.set(idx);
      }
   }
   else
   {
      // Suspicious... why an empty set?
      PSB_LOG_WARNING(0);
   }

   return true;
}


////////////////////////////////////////////////////////////////////////////////
bool
PixelReservationManager::reserveAllPixels(void)
{
   // Check the reservation
   if (true == m_pixelRegistry.any())
   {
      // Some pixels already set
      return false;
   }

   // Make the reservation (all)
   m_pixelRegistry.set();

   return true;
}


////////////////////////////////////////////////////////////////////////////////
void
PixelReservationManager::unreservePixels(pixel_set_t pixels)
{
   if (pixels.size() > 0)
   {
      for (auto idx : pixels)
      {
         // Verify proper index
         if (idx < m_pixelRegistry.size())
         {
            m_pixelRegistry.reset(idx);
         }
         else
         {
            // Index out of range
            PSB_LOG_ERROR(1,
                  Psb::LOG_TYPE_UINT32, idx);
         }
      }
   }
   else
   {
      // Suspicious... why an empty set?
      PSB_LOG_WARNING(0);
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
void
PixelReservationManager::unreserveAllPixels(void)
{
   m_pixelRegistry.reset();
   return;
}


} // END namespace StatusLed
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

