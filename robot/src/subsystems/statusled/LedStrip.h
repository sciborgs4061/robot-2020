// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __LedStrip_h__
#define __LedStrip_h__


// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <cstdint>
#include <cstddef>
#include "PsbSubsystemTypes.h"

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace StatusLed {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This class represents a strip of LEDs, controlled by the LDP8803 driver.
///
/// @remarks
/// The interface here is heavily based on the AdaFruit reference implementation
/// on github, here: https://github.com/adafruit/LPD8806.
////////////////////////////////////////////////////////////////////////////////
class LedStrip
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Overridden Constructor
      ///
      /// @param[in] numPixels   Number of pixels in this strip.
      /// @param[in] p_memblk    Pre-allocated memory region to use.
      //////////////////////////////////////////////////////////////////////////
      LedStrip(size_t numPixels, memblk_t* p_memblk);
     
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~LedStrip(void);

      size_t getNumPixels(void) const;
     
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Sets the color for the given pixel.
      ///
      /// @param[in] pixel    The ID of the targeted pixel
      /// @param[in] red      Intensity [0:127] of the red pixel
      /// @param[in] green    Intensity [0:127] of the green pixel
      /// @param[in] blue     Intensity [0:127] of the blue pixel
      //////////////////////////////////////////////////////////////////////////
      void setColor(size_t pixel, uint8_t red, uint8_t green, uint8_t blue);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Sets the color for all pixels in the entire strip.
      ///
      /// @param[in] red      Intensity [0:127] of the red pixel
      /// @param[in] green    Intensity [0:127] of the green pixel
      /// @param[in] blue     Intensity [0:127] of the blue pixel
      //////////////////////////////////////////////////////////////////////////
      void setColor(uint8_t red, uint8_t green, uint8_t blue);
     
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Sets the strip data for a blank (all LEDs off).
      //////////////////////////////////////////////////////////////////////////
      void blankOut(void);

   private:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Stores the number of pixels in this strip.
      //////////////////////////////////////////////////////////////////////////
      size_t m_numPixels;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Memory region "descriptor" used to store pixel color bytes and latch
      /// bytes in the proper order for transmission to/through the strip.
      //////////////////////////////////////////////////////////////////////////
      memblk_t* mp_memblk;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The number of pixel data bytes in the stream.
      ///
      /// @remarks
      /// The number of pixel data bytes is 3 times the number of pixels.
      //////////////////////////////////////////////////////////////////////////
      size_t m_numPixelBytes;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The number of latch bytes in the stream.
      ///
      /// @remarks
      /// The number of latch bytes is 1 for each set of 32 LEDs in the strip.
      //////////////////////////////////////////////////////////////////////////
      size_t m_numLatchBytes;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The number of bytes in the transmission.
      ///
      /// @remarks
      /// This byte count includes all pixel data bytes along with all latch
      /// bytes required.
      //////////////////////////////////////////////////////////////////////////
      size_t m_numTotalBytes;

   private:
      DISALLOW_DEFAULT_CTOR(LedStrip);
      DISALLOW_COPY_CTOR(LedStrip);
      DISALLOW_ASSIGNMENT_OPER(LedStrip);
}; // END class LedStrip


} // END namespace StatusLed
} // END namespace Subsystem
} // END namespace Psb


#endif // __LedStrip_h__


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

