// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __StatusLedTypes_h__
#define __StatusLedTypes_h__


// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <list>


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace StatusLed {


typedef enum
{
   #define ROBOT_LED(LED_ENUM) LED_ENUM,
      #include <TheRobotMappings.h>
   #undef ROBOT_LED
   NUM_PIXEL_IDS
} pixel_id_t;


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// Simple collection mechanism for the components of an RGB colors
////////////////////////////////////////////////////////////////////////////////
typedef struct
{
   uint8_t red;
   uint8_t green;
   uint8_t blue;
} rgb_t;


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// Represents an arbitrary ordered set of pixel IDs.
////////////////////////////////////////////////////////////////////////////////
typedef std::list<pixel_id_t> pixel_set_t;


} // END namespace StatusLed
} // END namespace Subsystem
} // END namespace Psb


#endif // __StatusLedTypes_h__


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

