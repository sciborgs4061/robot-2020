// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbLogger.h>
#include <GlobalOiWpiStore.h>
#include <OiDataGrabbers.h>
#include <GlobalAiWpiStore.h>
#include <AiDataPushers.h>
#include <GlobalSiWpiStore.h>
#include <SiDataGrabbers.h>
#include <GlobalIiWpiStore.h>
#include <IiDataPushers.h>

// /////////////////////////////////////////////////////////////////////////////
// This is where we actually build the dashstream interface
// since this is the object that will hand it out if someone wants it
// /////////////////////////////////////////////////////////////////////////////
#define BUILD_IT
#include "StatusLedController.h"
#undef BUILD_IT

#include "StatusLedEvents.h"
#include "StatusLedIndicators.h"

// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////

// Define this to bypass event checking and just loop through all known
// indicators
#define LAMP_TEST
#undef LAMP_TEST


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace StatusLed {


////////////////////////////////////////////////////////////////////////////////
Controller::Controller(void)
   : Controller_I()
   , m_eventTable()
   , m_leds(NUM_PIXEL_IDS, &(m_pixel_memblk))
   , m_manager()
{
   // get a hold of the data so we can use it as needed
   m_data = (DashstreamInterface::DashstreamPOD_t *)m_dashstreamIntf.getData();

   // clear out the data we are in control of
   this->resetData();
}


////////////////////////////////////////////////////////////////////////////////
Controller::~Controller(void)
{
   for (auto pair : m_eventTable)
   {
      // Free up the event
      delete pair.first;

      // Free up the list of indicators associated with that event
      for (auto indicator : pair.second)
      {
         delete indicator;
      }
   }

   m_eventTable.clear();
}

////////////////////////////////////////////////////////////////////////////////
Dashstream::BaseInterface_I *Controller::getDashstreamInterface(void)
{
   return &m_dashstreamIntf;
}

////////////////////////////////////////////////////////////////////////////////
void
Controller::setEnabledState(enabled_state_t /*new_state*/)
{
}

//////////////////////////////////////////////////////////////////////////
void
Controller::resetData(void)
{
   // clear the pixel memory
   memset(&m_pixel_memblk,0,sizeof(memblk_t));

   // For now just forcing everything to 0 is good enough.
   // There could be cases where the reset state is not just 0
   // but we will take that on when it happens
   memset(m_data,0,sizeof(DashstreamInterface::DashstreamPOD_t));

   // Default strip zero
   m_leds.blankOut();
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::init(void)
{
   // Hook up data pushers here (just for the LEDs)
   GlobalIiWpiStore *ii = GlobalIiWpiStore::instance();
   ii->attach(new LPD8806LedStripDataPusher(&(m_pixel_memblk)));

   setupRobotLEDEvents(m_eventTable, m_leds, m_manager);

#if 0

   pixel_set_t all_pixels = 
   {
      PIXEL_ID_00, PIXEL_ID_01, PIXEL_ID_02, PIXEL_ID_03,
      PIXEL_ID_04, PIXEL_ID_05, PIXEL_ID_06, PIXEL_ID_07,
      PIXEL_ID_08, PIXEL_ID_09, PIXEL_ID_10, PIXEL_ID_11,
      PIXEL_ID_12, PIXEL_ID_13, PIXEL_ID_14, PIXEL_ID_15,
      PIXEL_ID_16, PIXEL_ID_17, PIXEL_ID_18, PIXEL_ID_19,
      PIXEL_ID_20, PIXEL_ID_21, PIXEL_ID_22, PIXEL_ID_23,
      PIXEL_ID_24, PIXEL_ID_25, PIXEL_ID_26, PIXEL_ID_27,
      PIXEL_ID_28, PIXEL_ID_29, PIXEL_ID_30, PIXEL_ID_31
   };
   pixel_set_t first_half = 
   {
       PIXEL_ID_00, PIXEL_ID_01, PIXEL_ID_02, PIXEL_ID_03
      ,PIXEL_ID_04, PIXEL_ID_05, PIXEL_ID_06, PIXEL_ID_07
      ,PIXEL_ID_08, PIXEL_ID_09, PIXEL_ID_10, PIXEL_ID_11
      ,PIXEL_ID_12, PIXEL_ID_13, PIXEL_ID_14, PIXEL_ID_15
   };
   pixel_set_t second_half = 
   {
       PIXEL_ID_16, PIXEL_ID_17, PIXEL_ID_18, PIXEL_ID_19
      ,PIXEL_ID_20, PIXEL_ID_21, PIXEL_ID_22, PIXEL_ID_23
      ,PIXEL_ID_24, PIXEL_ID_25, PIXEL_ID_26, PIXEL_ID_27
      ,PIXEL_ID_28, PIXEL_ID_29, PIXEL_ID_30, PIXEL_ID_31
   };

   pixel_set_t first_quarter = 
   {
       PIXEL_ID_00, PIXEL_ID_01, PIXEL_ID_02, PIXEL_ID_03
      ,PIXEL_ID_04, PIXEL_ID_05, PIXEL_ID_06, PIXEL_ID_07
   };
   pixel_set_t second_quarter = 
   {
       PIXEL_ID_08, PIXEL_ID_09, PIXEL_ID_10, PIXEL_ID_11
      ,PIXEL_ID_12, PIXEL_ID_13, PIXEL_ID_14, PIXEL_ID_15
   };
   pixel_set_t third_quarter = 
   {
       PIXEL_ID_16, PIXEL_ID_17, PIXEL_ID_18, PIXEL_ID_19
      ,PIXEL_ID_20, PIXEL_ID_21, PIXEL_ID_22, PIXEL_ID_23
   };
   pixel_set_t fourth_quarter = 
   {
       PIXEL_ID_24, PIXEL_ID_25, PIXEL_ID_26, PIXEL_ID_27
      ,PIXEL_ID_28, PIXEL_ID_29, PIXEL_ID_30, PIXEL_ID_31
   };

   // //////////////////////////////////////////////////////////////////////////
   // Build our event list...
   // Events at the front of the list have the highest priority.
   // //////////////////////////////////////////////////////////////////////////

   ///////////////////////////////////////////////////////
   // Test Mode
   m_eventTable.push_back(
      { new TestModeEvent(),
         { new DigitalInputsIndicator(m_leds,
                                      m_manager,
                                      all_pixels,
                                      DigitalInputsIndicator::POS_BEGIN,
                                      {0x00, 0x7f, 0x00},
                                      {0x7f, 0x00, 0x00}) }}
   );

   ///////////////////////////////////////////////////////
   // Driver Station attach
   // colors for DS Attach indication
   std::list<rgb_t> ds_attach_colors;
   ds_attach_colors.push_back({0x7F,0x7f,0x7f});
   ds_attach_colors.push_back({0x7F,0x7f,0x7f});
   ds_attach_colors.push_back({0x7F,0x7f,0x7f});
   ds_attach_colors.push_back({0x7F,0x7f,0x7f});
   m_eventTable.push_back(
      { new DsDetachedEvent(),
         { new PingPongIndicator(m_leds,
                                 m_manager,
                                 first_half,
                                 ds_attach_colors) }}
   );
   m_eventTable.push_back(
      { new DsDetachedEvent(),
         { new PingPongIndicator(m_leds,
                                 m_manager,
                                 second_half,
                                 ds_attach_colors) }}
   );

   ///////////////////////////////////////////////////////
   // Autonomous color set...it is kinda pink
   m_eventTable.push_back(
      { new RobotStateEvent(1),
         { new SolidColorIndicator(m_leds,
                                   m_manager,
                                   all_pixels,
                                   0xFF, 0x14, 0x93) }}
   );

   ///////////////////////////////////////////////////////
   // Alliance color selection
   std::list<rgb_t> blue_alliance_colors;
   blue_alliance_colors.push_back({0x00,0x00,0x7f});
   blue_alliance_colors.push_back({0x00,0x00,0x7f});
   m_eventTable.push_back(
      { new BlueAllianceEvent(),
         { new PingPongIndicator(m_leds,
                                 m_manager,
                                 first_quarter,
                                 blue_alliance_colors) }}
   );
   m_eventTable.push_back(
      { new BlueAllianceEvent(),
         { new PingPongIndicator(m_leds,
                                 m_manager,
                                 second_quarter,
                                 blue_alliance_colors) }}
   );
   m_eventTable.push_back(
      { new BlueAllianceEvent(),
         { new PingPongIndicator(m_leds,
                                 m_manager,
                                 third_quarter,
                                 blue_alliance_colors) }}
   );
   m_eventTable.push_back(
      { new BlueAllianceEvent(),
         { new PingPongIndicator(m_leds,
                                 m_manager,
                                 fourth_quarter,
                                 blue_alliance_colors) }}
   );

   std::list<rgb_t> red_alliance_colors;
   red_alliance_colors.push_back({0x7f,0x00,0x00});
   red_alliance_colors.push_back({0x7f,0x00,0x00});
   m_eventTable.push_back(
      { new RedAllianceEvent(),
         { new PingPongIndicator(m_leds,
                                 m_manager,
                                 first_quarter,
                                 red_alliance_colors) }}
   );
   m_eventTable.push_back(
      { new RedAllianceEvent(),
         { new PingPongIndicator(m_leds,
                                 m_manager,
                                 second_quarter,
                                 red_alliance_colors) }}
   );
   m_eventTable.push_back(
      { new RedAllianceEvent(),
         { new PingPongIndicator(m_leds,
                                 m_manager,
                                 third_quarter,
                                 red_alliance_colors) }}
   );
   m_eventTable.push_back(
      { new RedAllianceEvent(),
         { new PingPongIndicator(m_leds,
                                 m_manager,
                                 fourth_quarter,
                                 red_alliance_colors) }}
   );
#endif // 0

}


////////////////////////////////////////////////////////////////////////////////
void
Controller::performManualUpdate(void)
{
   // Start with a blank slate...
   m_leds.blankOut();
   m_manager.unreserveAllPixels();

#ifndef LAMP_TEST
   // Allow each event to check if it is active or not
   for (auto eil : m_eventTable)
   {
      EventBase* p_event = eil.first;

      p_event->check();
      PSB_LOG_DEBUG3(3,
                     Psb::LOG_TYPE_STRING, p_event->name().c_str(),
                     Psb::LOG_TYPE_BOOL, p_event->isActiveNow(),
                     Psb::LOG_TYPE_BOOL, p_event->justBecameActive());

      if (true == p_event->isActiveNow())
      {
         // Track whether events need to be cleared based on indicator status
         bool force_clear_flag = true;

         // This event is currently active, render its associated indicators
         for (auto p_indicator : eil.second)
         {
            if (true == p_event->justBecameActive())
            {
               // Special handling for newly-activated events
               p_indicator->initialize();
            }

            // Control the LEDs
            p_indicator->show();

            // Examine the indicator status
            force_clear_flag = (force_clear_flag && p_indicator->isComplete());
         }

         // If all indicators have completed, then the event is over
         if (true == force_clear_flag)
         {
            PSB_LOG_NOTICE(1,
                           Psb::LOG_TYPE_STRING, p_event->name().c_str());
            p_event->clear();
         }
      }
      else
      {
         // This event is not currently active (expected)
      }
   }

#else
   this->runLampTest();
#endif

   // Dump pixel data for debugging
   // PSB_LOG_DEBUG3(1,
                  // Psb::LOG_TYPE_UINT8_A, m_data->pixel_memblk.length,
                                         // m_data->pixel_memblk.block);
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::runLampTest(void)
{
   static uint32_t s_state = 0;
   static Psb::Timer s_timer;
   static std::list<eil_mapping_t>::iterator s_iter = m_eventTable.begin();

   switch (s_state)
   {
      case 0: // Prepare & show
      {
         for (auto p_indicator : s_iter->second)
         {
            PSB_LOG_WARNING(2,
                  Psb::LOG_TYPE_STRING, s_iter->first->name().c_str(),
                  Psb::LOG_TYPE_STRING, p_indicator->name().c_str());

            p_indicator->initialize();
            p_indicator->show();
         }

         s_timer.stop();
         s_timer.setExpiration(5.000);
         s_timer.reset();
         s_timer.start();
         s_state = 1;
      } break;

      case 1: // Show for time
      {
         for (auto p_indicator : s_iter->second)
         {
            p_indicator->show();
         }

         if (s_timer.isExpired())
         {
            PSB_LOG_WARNING(1,
                  Psb::LOG_TYPE_STRING, "Timer Expired, moving on");
            s_state = 2;
         }
      } break;

      case 2: // Move to next
      {
         s_iter++;
         if (m_eventTable.end() == s_iter)
         {
            s_iter = m_eventTable.begin();
         }

         PSB_LOG_WARNING(1,
               Psb::LOG_TYPE_STRING, "Moving to next event");
         s_state = 0;
      } break;

      default:
      {
         s_iter = m_eventTable.begin();
         s_state = 0;
      } break;
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::performAutoUpdate(void)
{
   // This subsystem does not support the Automatic Update state.
   PSB_LOG_ERROR(0);
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::performDisabledUpdate(void)
{
   /// @todo - this is just for testing in lieu of having a driver station
   this->transitionToState(Psb::Subsystem::SUBSYSTEM_MANUAL);
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::transitionToState(subsystem_state_t newState)
{
   // keep track of the old state just in case
   subsystem_state_t prev_state = m_state;

   // update the base class member so the update() function calls
   // the right thing
   m_state = newState;

   /// now do whatever else it is you need to do during a transiton
   switch (m_state)
   {
      case Psb::Subsystem::SUBSYSTEM_AUTOMATIC:
      {
         this->transitionToAutomaticState(prev_state);
      } break;

      case Psb::Subsystem::SUBSYSTEM_DISABLED:
      {
         this->transitionToDisabledState(prev_state);
      } break;

      case Psb::Subsystem::SUBSYSTEM_MANUAL:
      {
         this->transitionToManualState(prev_state);
      } break;

      default:
      {
         // Unknown state!  This is a coding defect!
         PSB_LOG_ERROR(1,
                       Psb::LOG_TYPE_INT32, prev_state,
                       Psb::LOG_TYPE_INT32, newState);
         m_state = Psb::Subsystem::SUBSYSTEM_DISABLED;
      } break;
   }

   // The state has been selected, note it
   PSB_LOG_INFO(2,
                Psb::LOG_TYPE_INT32, prev_state,
                Psb::LOG_TYPE_INT32, m_state);
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::transitionToManualState(subsystem_state_t previousState)
{
   previousState = previousState;
   switch (previousState)
   {
      case Psb::Subsystem::SUBSYSTEM_AUTOMATIC:
      {
         // This is a coding defect
         PSB_LOG_ERROR(1, Psb::LOG_TYPE_INT32, previousState);
      } break;

      case Psb::Subsystem::SUBSYSTEM_DISABLED:
      case Psb::Subsystem::SUBSYSTEM_MANUAL:
      {
         // Nothing special to do here
      } break;

      default:
      {
         PSB_LOG_ERROR(1, Psb::LOG_TYPE_INT32, previousState);
      } break;
   }

   // Note the state change
   m_state = Psb::Subsystem::SUBSYSTEM_MANUAL;
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::transitionToAutomaticState(subsystem_state_t previousState)
{
   previousState = previousState;
   // This subsystem has no automatic modes - this is an error!
   PSB_LOG_ERROR(1, Psb::LOG_TYPE_INT32, previousState);
   m_state = Psb::Subsystem::SUBSYSTEM_DISABLED;
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::transitionToDisabledState(subsystem_state_t previousState)
{
   switch (previousState)
   {
      case Psb::Subsystem::SUBSYSTEM_AUTOMATIC:
      {
         // This is a coding defect
         PSB_LOG_ERROR(1, Psb::LOG_TYPE_INT32, previousState);
      } break;

      case Psb::Subsystem::SUBSYSTEM_DISABLED:
      case Psb::Subsystem::SUBSYSTEM_MANUAL:
      {
         // Nothing special to do here
      } break;

      default:
      {
         PSB_LOG_ERROR(1, Psb::LOG_TYPE_INT32, previousState);
      } break;
   }

   // Note the state change
   m_state = Psb::Subsystem::SUBSYSTEM_DISABLED;

   // We are not enabled - so reset all POD data
   this->resetData();
   return;
}


} // END namespace StatusLed
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

