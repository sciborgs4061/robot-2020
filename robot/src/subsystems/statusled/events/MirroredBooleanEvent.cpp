// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbLogger.h>
#include <GlobalMiStore.h>
#include <SiDataGrabbers.h>

#include "events/MirroredBooleanEvent.h"
#include <iostream>

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace StatusLed {


////////////////////////////////////////////////////////////////////////////////
MirroredBooleanEvent::MirroredBooleanEvent(char const *mirror_name, bool comparisonValue)
   : EventBase(__FUNCTION__)
   , m_mirrorValue(false)
   , m_comparisonValue(comparisonValue)
{
   // Hook into the Test Mode Switch in the SI
   GlobalMiStore *mi = GlobalMiStore::instance();
   mi->mirror(mirror_name, new DataMirror<bool>(&(m_mirrorValue)));
}


////////////////////////////////////////////////////////////////////////////////
MirroredBooleanEvent::~MirroredBooleanEvent(void)
{
}


////////////////////////////////////////////////////////////////////////////////
void
MirroredBooleanEvent::check(void)
{
   m_lastActiveFlag = m_currentActiveFlag;
   m_currentActiveFlag = (m_comparisonValue == m_mirrorValue);
   //std::cout << m_mirrorValue << std::endl;
   return;
}


} // END namespace StatusLed
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

