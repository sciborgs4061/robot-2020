// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbLogger.h>
#include <GlobalSiWpiStore.h>
#include <SiDataGrabbers.h>

#include "events/LimitSwitchEvent.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace StatusLed {


////////////////////////////////////////////////////////////////////////////////
LimitSwitchEvent::LimitSwitchEvent(GlobalSiWpiStore::si_digitalinputs_t diname, 
                                   bool CompareValue)
   : EventBase(__FUNCTION__)
   , m_SwitchState(false)
   , m_CompareValue(CompareValue)
{
   // Hook into the Test Mode Switch in the SI
   GlobalSiWpiStore *si = GlobalSiWpiStore::instance();
   si->attach(new DigitalInputDataGrabber(&(m_SwitchState)), diname);
}


////////////////////////////////////////////////////////////////////////////////
LimitSwitchEvent::~LimitSwitchEvent(void)
{
}


////////////////////////////////////////////////////////////////////////////////
void
LimitSwitchEvent::check(void)
{
   m_lastActiveFlag = m_currentActiveFlag;
   m_currentActiveFlag = (m_CompareValue == m_SwitchState);
   return;
}


} // END namespace StatusLed
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

