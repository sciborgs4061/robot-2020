// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include "LEDColorPickerEvent.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace StatusLed {


////////////////////////////////////////////////////////////////////////////////
LEDColorPickerEvent::LEDColorPickerEvent(int32_t check_state)
   : EventBase(__FUNCTION__)
   , m_red(0)
   , m_green(0)
   , m_blue(0)   
   , m_currentActiveFlag(false)
   , m_lastActiveFlag(false)

{
}


////////////////////////////////////////////////////////////////////////////////
LEDColorPickerEvent::~LEDColorPickerEvent(void)
{
}


////////////////////////////////////////////////////////////////////////////////
void
LEDColorPickerEvent::clear(void)
{
   m_currentActiveFlag = false;
   m_lastActiveFlag = false;
}


////////////////////////////////////////////////////////////////////////////////
bool
LEDColorPickerEvent::isActiveNow(void) const
{
   return m_currentActiveFlag;
}
////////////////////////////////////////////////////////////////////////////////
void
LEDColorPickerEvent::check(void) 
{
      double red = frc::SmartDashboard::GetNumber("DB/Slider 0", 0.0);
      double green = frc::SmartDashboard::GetNumber("DB/Slider 1", 0.0);
      double blue = frc::SmartDashboard::GetNumber("DB/Slider 2", 0.0);
// std::cout << str_red << str_green << str_blue << std::endl;
   // double red = std::stod(str_red);
   // double green = std::stod(str_green);
   // double blue = std::stod(str_blue);

   if(red != m_red || green != m_green || blue != m_blue)
   {
   		m_currentActiveFlag = true;
   } 
}


} // END namespace StatusLed
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////
