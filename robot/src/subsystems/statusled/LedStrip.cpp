// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <cstring>
#include <PsbLogger.h>
#include "StatusLedTypes.h"
#include "LedStrip.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace StatusLed {


// /////////////////////////////////////////////////////////////////////////////
// This is the maximum number of LEDs we COULD support.  The NUM_PIXEL_IDS is
// the current number of supported LEDs.
// /////////////////////////////////////////////////////////////////////////////
#define MAX_POSSIBLE_LEDS (60)
static_assert(NUM_PIXEL_IDS <= MAX_POSSIBLE_LEDS, "Check NUM_PIXEL_LEDs!");


////////////////////////////////////////////////////////////////////////////////
LedStrip::LedStrip(size_t numPixels, memblk_t *p_memblk)
   : m_numPixels(0)
   , mp_memblk(nullptr)
   , m_numPixelBytes(0)
   , m_numLatchBytes(0)
   , m_numTotalBytes(0)
{
   if (numPixels <= NUM_PIXEL_IDS)
   {
      m_numPixels     = numPixels;
      m_numPixelBytes = 3*m_numPixels;          // R,G,B for each pixel
      m_numLatchBytes = (m_numPixels+31) >> 5;  // One latch byte per 32-pixels
      m_numTotalBytes = m_numPixelBytes + m_numLatchBytes;

      if (m_numTotalBytes <= sizeof(p_memblk->block))
      {
         mp_memblk = p_memblk;
      }
      else
      {
         // Insufficient memory region provided!
         PSB_LOG_ERROR(2,
               Psb::LOG_TYPE_UINT32, numPixels,
               Psb::LOG_TYPE_UINT32, sizeof(p_memblk->block));

         // Go back to defaults
         m_numPixels     = 0;
         m_numPixelBytes = 0;
         m_numLatchBytes = 0;
         m_numTotalBytes = 0;
      }

      // Created the strip with m_numPixels
      PSB_LOG_DEBUG3(4,
            Psb::LOG_TYPE_UINT32, m_numPixels,
            Psb::LOG_TYPE_UINT32, m_numPixelBytes,
            Psb::LOG_TYPE_UINT32, m_numLatchBytes,
            Psb::LOG_TYPE_UINT32, m_numTotalBytes);
   }
   else
   {
      // Invalid number of pixels supported
      PSB_LOG_ERROR(1,
            Psb::LOG_TYPE_UINT32, numPixels);
   }
}


////////////////////////////////////////////////////////////////////////////////
LedStrip::~LedStrip(void)
{
}


////////////////////////////////////////////////////////////////////////////////
size_t
LedStrip::getNumPixels(void) const
{
   return m_numPixels;
}


////////////////////////////////////////////////////////////////////////////////
void
LedStrip::setColor(size_t pixel, uint8_t r, uint8_t g, uint8_t b)
{
   if (pixel < m_numPixels)
   {
      uint8_t * p_pixel = &(mp_memblk->block[3*(pixel)]);
      *p_pixel++ = g | 0x80; // Strip color order is Green-Red-Blue
      *p_pixel++ = r | 0x80; // not the more common RGB,
      *p_pixel++ = b | 0x80; // so the order here is intentional; don't "fix" it
      mp_memblk->length = m_numTotalBytes;
   }
   else
   {
      // Invalid pixel index
      PSB_LOG_ERROR(4,
            Psb::LOG_TYPE_UINT32, pixel,
            Psb::LOG_TYPE_HEX8, r,
            Psb::LOG_TYPE_HEX8, g,
            Psb::LOG_TYPE_HEX8, b);
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
void
LedStrip::setColor(uint8_t r, uint8_t g, uint8_t b)
{
   for (size_t idx = 0; idx < m_numPixels; idx++)
   {
      uint8_t * p_pixel = &(mp_memblk->block[3*(idx)]);
      *p_pixel++ = g | 0x80; // Strip color order is Green-Red-Blue
      *p_pixel++ = r | 0x80; // not the more common RGB,
      *p_pixel++ = b | 0x80; // so the order here is intentional; don't "fix" it
   }

   mp_memblk->length = m_numTotalBytes;
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
LedStrip::blankOut(void)
{
   memset(mp_memblk->block,                   0x80, m_numPixelBytes);
   memset(mp_memblk->block + m_numPixelBytes, 0x00, m_numLatchBytes);
   return;
}


} // END namespace StatusLed
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

