// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string>
#include <PsbMacros.h>
#include <PsbLogger.h>

#include "PixelReservationManager.h"
#include "indicators/EcgSinusRhythmIndicator.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace StatusLed {


////////////////////////////////////////////////////////////////////////////////
EcgSinusRhythmIndicator::EcgSinusRhythmIndicator(LedStrip & r_leds,
                                         PixelReservationManager & r_manager,
                                         pixel_set_t pixels,
                                         uint8_t red,
                                         uint8_t green,
                                         uint8_t blue)
   : IndicatorBase(__FUNCTION__, r_leds, r_manager, pixels)
   , m_red(red)
   , m_green(green)
   , m_blue(blue)
{
}


////////////////////////////////////////////////////////////////////////////////
EcgSinusRhythmIndicator::~EcgSinusRhythmIndicator(void)
{
}


////////////////////////////////////////////////////////////////////////////////
void
EcgSinusRhythmIndicator::initialize(void)
{
   m_completeFlag = false;
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
EcgSinusRhythmIndicator::show(void)
{
   /// @todo - a better "Heartbeat Pattern" is required... perhaps using
   /// a Psb::Timer and piecewise functions of time?

   /*static uint8_t sinus_rhythm_samples[] = {
      16, 17, 16, 16, 16, 16, 18, 17, 24, 26,
      33, 33, 30, 25, 21, 16, 18, 16, 18, 12,
      75, 127, 127, 0, 14, 17, 16, 16, 14, 16,
      16, 15, 15, 15, 16, 16, 18, 20, 22, 25,
      24, 30, 31, 37, 40, 42, 50, 52, 55, 54,
      51, 47, 40, 35, 30, 27, 26, 24, 23, 20,
      19, 20, 19, 20, 19, 18, 20, 20, 20, 20,
      20, 20, 20, 20, 20, 20, 20, 20, 20, 20,
      20, 20, 20, 20, 20, 20, 20, 20, 20, 20,
      20, 20, 20, 20, 20, 20, 20, 20, 20, 20
   };*/

   static uint8_t sinus_rhythm_samples[] = {
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 4, 6, 8, 10, 12, 14, 16, 
      18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54,
      56, 58, 60, 62, 64, 66, 68, 70, 72, 74, 76, 78, 80, 82, 84, 86, 88, 90, 92, 94,
      96, 98, 100, 102, 104, 106, 108, 110, 112, 114, 116, 118, 120, 122, 124, 127, 124,
      122, 120, 118, 116, 114, 112, 110, 108, 106, 104, 102, 100, 98, 96, 94, 92, 90,
      88, 86, 84, 82, 80, 78, 76, 74, 72, 70, 68, 66, 64, 62, 60, 58, 56, 54, 52, 50,
      48, 46, 44, 42, 40, 38, 36, 34, 32, 30, 28, 26, 24, 22, 20, 18, 16, 14, 12, 10,
      8, 6, 4, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
   };

   /*static uint8_t sinus_rhythm_samples[] = {
      0
   };*/
   static size_t s_idx = 0;


   // Make sure we control the pixels
   if (true == mr_manager.reservePixels(m_pixels))
   {
      for (auto idx : m_pixels)
      {
         uint8_t value = sinus_rhythm_samples[s_idx];
         mr_leds.setColor(idx, value & m_red, value & m_green, value & m_blue);
      }

      s_idx += 1;
      s_idx = (s_idx < sizeof(sinus_rhythm_samples)) ? s_idx : 0;
   }
   else
   {
      // Pixels already in use - try again next time
      PSB_LOG_DEBUG1(0);
   }

   return;
}


} // END namespace StatusLed
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

