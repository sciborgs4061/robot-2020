// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include "indicators/LEDColorPickerIndicator.h"
#include <PsbMacros.h>
#include <PsbLogger.h>


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace StatusLed {


////////////////////////////////////////////////////////////////////////////////
LEDColorPickerIndicator::LEDColorPickerIndicator(
                             LedStrip & r_leds,
                             PixelReservationManager & r_manager,
                             pixel_set_t pixels)
   : IndicatorBase(__FUNCTION__, r_leds, r_manager, pixels)
   // , mr_leds(r_leds)
   // , mr_manager(r_manager)
   // , m_pixels(pixels)
   
{
}


////////////////////////////////////////////////////////////////////////////////
LEDColorPickerIndicator::~LEDColorPickerIndicator(void)
{
}


////////////////////////////////////////////////////////////////////////////////
std::string
LEDColorPickerIndicator::name(void) const
{
   return m_name;
}


////////////////////////////////////////////////////////////////////////////////
bool
LEDColorPickerIndicator::isComplete(void) const
{
   return m_completeFlag;
}

void
LEDColorPickerIndicator::show(void)
{
   // Make sure we control the pixels
   if (true == mr_manager.reservePixels(m_pixels))
   {
      // std::string s_red = SmartDashboard::GetString("DB/Slider 0", "0.0");
      // std::string s_green = SmartDashboard::GetString("DB/Slider 1", "0.0");
      // std::string s_blue = SmartDashboard::GetString("DB/Slider 2", "0.0");
      int red = (int)(frc::SmartDashboard::GetNumber("DB/Slider 0", 0.0)/5*127);
      int green = (int)(frc::SmartDashboard::GetNumber("DB/Slider 1", 0.0)/5*127);
      int blue = (int)(frc::SmartDashboard::GetNumber("DB/Slider 2", 0.0)/5*127);
      // m_red = std::stod(s_red)/5*255;
      // m_green = std::stod(s_green)/5*255;
      // m_blue = std::stod(s_blue)/5*255;
      // std::cout << "LED Values" << red << green << blue << std::endl; [Jason said to remove cout due to hurting performance]
      frc::SmartDashboard::PutString("DB/String 7", std::to_string(red));
      frc::SmartDashboard::PutString("DB/String 8", std::to_string(green));
      frc::SmartDashboard::PutString("DB/String 9", std::to_string(blue));
      // m_red = 255;
      // m_green = 255;
      // m_blue = 255;
      for (auto idx : m_pixels)
      {
         mr_leds.setColor(idx, red, green, blue);
      }
   }
   else
   {
      // Pixels already in use - try again next time
      PSB_LOG_DEBUG1(0);
   }

   return;
}

void LEDColorPickerIndicator::initialize(void)
 {
 }


} // END namespace StatusLed
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

//Comments from creator:
    //Other than declairing uint8_t s and the show(void), This is similar to Indicator base.
    //This is completed for our project.
