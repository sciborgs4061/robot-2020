// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __DigitalInputsIndicator_h__
#define __DigitalInputsIndicator_h__


// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string>
#include <PsbMacros.h>
#include <GlobalSiWpiStore.h>

#include "StatusLedTypes.h"
#include "LedStrip.h"
#include "PixelReservationManager.h"
#include "indicators/IndicatorBase.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace StatusLed {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This class renders all the digital inputs in the system on LED pixels on
/// the strip, starting from the first pixel given.
////////////////////////////////////////////////////////////////////////////////
class DigitalInputsIndicator : public IndicatorBase
{
   public:
      typedef enum
      {
         POS_BEGIN   ///< Inputs indicator starts at pixel_set beginning
       , POS_CENTER  ///< Inputs indicator starts at pixel_set center
       , POS_END     ///< Inputs indicator starts at pixel_set end

       , NUM_POSITIONS
      } position_t;

   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Overloaded Constructor
      ///
      /// @param[in] r_leds       Reference to the LED Strip to use
      /// @param[in] r_manager    Reference to the pixel reservation object
      /// @param[in] pixels       The set of pixels to operate on
      /// @param[in] pos          Where to "place" the indicator group
      /// @param[in] color_high   Color to display for a logic-high input
      /// @param[in] color_low    Color to display for a logic-low input
      //////////////////////////////////////////////////////////////////////////
      DigitalInputsIndicator(LedStrip & r_leds,
                             PixelReservationManager & r_manager,
                             pixel_set_t pixels,
                             position_t pos,
                             rgb_t color_high,
                             rgb_t color_low);
     
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~DigitalInputsIndicator(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see IndicatorBase::initialize()
      //////////////////////////////////////////////////////////////////////////
      void initialize(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see IndicatorBase::show()
      //////////////////////////////////////////////////////////////////////////
      void show(void);
     
   private:
      /// @todo Document me!
      position_t m_position;
      rgb_t m_colorLogicHigh;
      rgb_t m_colorLogicLow;
      bool m_digitalInputStates[GlobalSiWpiStore::MAX_DIGITAL_INPUTS];


   private:
      DISALLOW_DEFAULT_CTOR(DigitalInputsIndicator);
      DISALLOW_COPY_CTOR(DigitalInputsIndicator);
      DISALLOW_ASSIGNMENT_OPER(DigitalInputsIndicator);
}; // END class DigitalInputsIndicator


} // END namespace StatusLed
} // END namespace Subsystem
} // END namespace Psb


#endif // __DigitalInputsIndicator_h__


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

