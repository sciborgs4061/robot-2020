// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __StatusLedIndicatorBase_h__
#define __StatusLedIndicatorBase_h__


// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string>
#include <PsbMacros.h>

#include "LedStrip.h"
#include "PixelReservationManager.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace StatusLed {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// Base class for all Indicator objects.
////////////////////////////////////////////////////////////////////////////////
class IndicatorBase
{
   protected:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Overridden Constructor
      ///
      /// @param[in] pc_indicatorName   Name of this indicator object
      /// @param[in] r_leds             Reference to the LED Strip
      /// @param[in] r_manager          Reference to the Pixel Reservation Mgr.
      /// @param[in] pixels             Pixels to operate on, in order
      //////////////////////////////////////////////////////////////////////////
      IndicatorBase(const char * pc_indicatorName,
                    LedStrip & r_leds,
                    PixelReservationManager & r_manager,
                    pixel_set_t pixels);
     
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~IndicatorBase(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Retrieves this object's name.
      //////////////////////////////////////////////////////////////////////////
      std::string name(void) const;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// If this indication is a "one-shot" indication, then this method will
      /// return true when the indication is complete.
      //////////////////////////////////////////////////////////////////////////
      bool isComplete(void) const;
     
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Initializes the Indicator object to the "start" of the sequence,
      /// whatever that may be.
      //////////////////////////////////////////////////////////////////////////
      virtual void initialize(void) = 0;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Renders the current indication sequence step onto the LEDs.
      ///
      /// @remarks
      /// For some indicators, this will be a constant pattern (e.g. all blue),
      /// while for others, there might be a light-dancing pattern going on.
      /// Each time this function is called, the LEDs are updated to the "next"
      /// appropriate color(s).
      //////////////////////////////////////////////////////////////////////////
      virtual void show(void) = 0;

   private:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Name of this object
      //////////////////////////////////////////////////////////////////////////
      std::string m_name;
     
   protected:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The LED Strip to use to render our pattern.
      //////////////////////////////////////////////////////////////////////////
      LedStrip & mr_leds;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The reservation manager to use to reserve pixels for our rendering.
      //////////////////////////////////////////////////////////////////////////
      PixelReservationManager & mr_manager;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The reservation manager to use for reserving pixels for our rendering.
      //////////////////////////////////////////////////////////////////////////
      pixel_set_t m_pixels;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Stores whether or not this indication has run its course.
      //////////////////////////////////////////////////////////////////////////
      bool m_completeFlag;

   private:
      DISALLOW_DEFAULT_CTOR(IndicatorBase);
      DISALLOW_COPY_CTOR(IndicatorBase);
      DISALLOW_ASSIGNMENT_OPER(IndicatorBase);
}; // END class IndicatorBase


} // END namespace StatusLed
} // END namespace Subsystem
} // END namespace Psb


#endif // __StatusLedIndicatorBase_h__


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

