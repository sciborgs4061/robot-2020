// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __StatusLedLEDColorPickerIndicator_h__
#define __StatusLedLEDColorPickerIndicator_h__


// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string>
#include <PsbMacros.h>
#include "indicators/IndicatorBase.h"
#include "LedStrip.h"
#include "PixelReservationManager.h"
#include <frc/smartdashboard/SmartDashboard.h>
#include <iostream>


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace StatusLed {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// Base class for all Indicator objects.
////////////////////////////////////////////////////////////////////////////////
class LEDColorPickerIndicator : public IndicatorBase
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Overridden Constructor
      ///
      /// @param[in] pc_indicatorName   Name of this indicator object
      /// @param[in] r_leds             Reference to the LED Strip
      /// @param[in] r_manager          Reference to the Pixel Reservation Mgr.
      /// @param[in] pixels             Pixels to operate on, in order
      //////////////////////////////////////////////////////////////////////////
      LEDColorPickerIndicator(
                    LedStrip & r_leds,
                    PixelReservationManager & r_manager,
                    pixel_set_t pixels);
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~LEDColorPickerIndicator(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Retrieves this object's name.
      //////////////////////////////////////////////////////////////////////////
      std::string name(void) const;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// If this indication is a "one-shot" indication, then this method will
      /// return true when the indication is complete.
      //////////////////////////////////////////////////////////////////////////
      bool isComplete(void) const;
     
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Initializes the Indicator object to the "start" of the sequence,
      /// whatever that may be.
      //////////////////////////////////////////////////////////////////////////
      void initialize(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Renders the current indication sequence step onto the LEDs.
      ///
      /// @remarks
      /// For some indicators, this will be a constant pattern (e.g. all blue),
      /// while for others, there might be a light-dancing pattern going on.
      /// Each time this function is called, the LEDs are updated to the "next"
      /// appropriate color(s).
      //////////////////////////////////////////////////////////////////////////
      void show(void);

   private:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Name of this object
      //////////////////////////////////////////////////////////////////////////
      std::string m_name;
      uint8_t m_red;
      uint8_t m_green;
      uint8_t m_blue;
     
   protected:
      // //////////////////////////////////////////////////////////////////////////
      // /// @brief
      // /// The LED Strip to use to render our pattern.
      // //////////////////////////////////////////////////////////////////////////
      // LedStrip & mr_leds;

      // //////////////////////////////////////////////////////////////////////////
      // /// @brief
      // /// The reservation manager to use to reserve pixels for our rendering.
      // //////////////////////////////////////////////////////////////////////////
      // PixelReservationManager & mr_manager;

      // //////////////////////////////////////////////////////////////////////////
      // /// @brief
      // /// The reservation manager to use for reserving pixels for our rendering.
      // //////////////////////////////////////////////////////////////////////////
      // pixel_set_t m_pixels;

      // //////////////////////////////////////////////////////////////////////////
      // /// @brief
      // /// Stores whether or not this indication has run its course.
      // //////////////////////////////////////////////////////////////////////////
      bool m_completeFlag;

   private:
      DISALLOW_DEFAULT_CTOR(LEDColorPickerIndicator);
      DISALLOW_COPY_CTOR(LEDColorPickerIndicator);
      DISALLOW_ASSIGNMENT_OPER(LEDColorPickerIndicator);
}; // END class LEDColorPickerIndicator


} // END namespace StatusLed
} // END namespace Subsystem
} // END namespace Psb


#endif // __StatusLedLEDColorPickerIndicator_h__


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

//Comments from creator:
    //Other than declairing uint8_t s, This is similar to Indicator base.
    //This is completed for our project.