// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2017-2018 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string>
#include <PsbMacros.h>
#include <PsbLogger.h>
#include <GlobalSiWpiStore.h>
#include <SiDataGrabbers.h>

#include "PixelReservationManager.h"
#include "indicators/PotIndicator.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace StatusLed {


////////////////////////////////////////////////////////////////////////////////
PotIndicator::
PotIndicator(LedStrip & r_leds,
                       PixelReservationManager & r_manager,
                       pixel_set_t pixels,
                       position_t pos,
                       rgb_t color_high,
                       rgb_t color_low)
   : IndicatorBase(__FUNCTION__, r_leds, r_manager, pixels)
   , m_position(pos)
   , m_colorLogicHigh(color_high)
   , m_colorLogicLow(color_low)
{
   // Wipe out any known state
   memset(m_potValues, 0x00, sizeof(m_potValues));

   // It is an error if we have fewer pixels than needed to represent all the configured POTs"
   if (GlobalSiWpiStore::MAX_ANALOG_POTS * 3 > pixels.size())
   {
      PSB_LOG_ERROR(2,
            Psb::LOG_TYPE_INT32, GlobalSiWpiStore::MAX_ANALOG_POTS * 1, //1, 2, or 10
            Psb::LOG_TYPE_INT32, pixels.size());

      // Nothing to change here - we just illuminate the ones we can
   }

   // Set up the data grabbers - one for each pot with no input
   GlobalSiWpiStore *si = GlobalSiWpiStore::instance();
   for (int idx = 0; idx < GlobalSiWpiStore::MAX_ANALOG_POTS; idx++)
   {
      si->attach(new AnalogPotDataGrabber(&(m_potValues[idx])),
                 static_cast<GlobalSiWpiStore::si_analogpots_t>(idx));
   }
}


////////////////////////////////////////////////////////////////////////////////
PotIndicator::~PotIndicator(void)
{
}


////////////////////////////////////////////////////////////////////////////////
void
PotIndicator::initialize(void)
{
   // Mark this indication as in progress
   m_completeFlag = false;

   // Prepare data
   memset(m_potValues, 0x00, sizeof(m_potValues));
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
PotIndicator::show(void)
{
   // Make sure we control the pixels
   if (true == mr_manager.reservePixels(m_pixels))
   {
      // Adjust the iterator if the setting requires it
      // TODO:
      // While this is perfectly functional as it is, we can improve performance
      // By making this decision once in the constructor and storing the "base"
      // iterator here.
      auto iter = m_pixels.begin();
      if (POS_CENTER == m_position)
      {
         // Center the starting point if we have room to do that
         std::advance(iter,
                      (m_pixels.size() > GlobalSiWpiStore::MAX_ANALOG_POTS)
                      ? ((m_pixels.size() - GlobalSiWpiStore::MAX_ANALOG_POTS) >> 1)
                      : 0);
      }
      else if (POS_END == m_position)
      {
         std::advance(iter,
                      (m_pixels.size() > GlobalSiWpiStore::MAX_ANALOG_POTS)
                      ? (m_pixels.size() - GlobalSiWpiStore::MAX_ANALOG_POTS)
                      : 0);
      }
      else
      {
         // Start at the beginning of the pixel set
      }

      // Illuminate the LEDs we know about
      for (int idx = 0; idx < GlobalSiWpiStore::MAX_ANALOG_POTS; idx++)
      {
	 for (int bit = 0; bit < 3; bit++)
         {
            // Choose the color based on logic state
            rgb_t color = 
               ((1 << bit) & (int) m_potValues[idx])
               ? m_colorLogicHigh
               : m_colorLogicLow;

            // Set the color on the pixel, if it exists
            if (iter != m_pixels.end())
            {
               mr_leds.setColor(*iter, color.red, color.green, color.blue);
               iter++;
            }
         } 
      }
   }
   else
   {
      // Pixels already in use - try next time
      PSB_LOG_DEBUG1(0);
   }

   return;
}


} // END namespace StatusLed
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

