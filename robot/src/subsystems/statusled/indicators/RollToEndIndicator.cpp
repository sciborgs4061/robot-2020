// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string>
#include <PsbMacros.h>
#include <PsbLogger.h>

#include "PixelReservationManager.h"
#include "indicators/RollToEndIndicator.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace StatusLed {



////////////////////////////////////////////////////////////////////////////////
RollToEndIndicator::RollToEndIndicator(LedStrip & r_leds,
                                     PixelReservationManager & r_manager,
                                     pixel_set_t pixels,
                                     bool direction,
                                     std::list<rgb_t> & r_colors)
   : IndicatorBase(__FUNCTION__, r_leds, r_manager, pixels)
   , m_colorList(r_colors)
   , m_timer()
   , m_currentPixel(m_pixels.begin())
   , m_direction(direction)
{
   if (0 == m_colorList.size())
   {
      // We need at least 1 pixel...
      PSB_LOG_ERROR(0);
      m_colorList.push_back({0x7F,0x00,0x00});
   }
}


////////////////////////////////////////////////////////////////////////////////
RollToEndIndicator::~RollToEndIndicator(void)
{
}


////////////////////////////////////////////////////////////////////////////////
void
RollToEndIndicator::initialize(void)
{
   // Mark this indication as in progress
   m_completeFlag = false;

   if (AWAY_FROM_CPU == m_direction)
   {
      m_currentPixel = m_pixels.begin();
   }
   else
   {
      m_currentPixel = m_pixels.end();
   }

   /// @todo use #define for "forward" and "backward"
   // m_direction = AWAY_FROM_CPU;

   m_timer.stop();
   m_timer.setExpiration(0.010);
   m_timer.start();
   m_timer.reset();
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
RollToEndIndicator::show(void)
{
   // Make sure we control the pixels
   if (true == mr_manager.reservePixels(m_pixels))
   {
      // Set the "Point" color
      mr_leds.setColor(*m_currentPixel,
                       m_colorList.begin()->red,
                       m_colorList.begin()->green,
                       m_colorList.begin()->blue);

      // Set the "wings" that lead the point
      for (size_t offset = 1; offset < m_colorList.size()+1; offset++)
      {
         auto next_iter = std::next(m_currentPixel,offset);
         if (next_iter == m_pixels.end())
         {
            break;
         }

         mr_leds.setColor(*(next_iter),
                          std::next(m_colorList.begin(),offset-1)->red,
                          std::next(m_colorList.begin(),offset-1)->green,
                          std::next(m_colorList.begin(),offset-1)->blue);
      }

      // Set the "wings" that trail the point
      for (size_t offset = 1; offset < m_colorList.size()+1; offset++)
      {
         auto prev_iter = std::prev(m_currentPixel,offset);
         if (prev_iter == m_pixels.end())
         {
            break;
         }

         mr_leds.setColor(*(prev_iter),
                          std::next(m_colorList.begin(),offset-1)->red,
                          std::next(m_colorList.begin(),offset-1)->green,
                          std::next(m_colorList.begin(),offset-1)->blue);
      }


      // Advance the point Pixel
      if (true == m_timer.isExpired())
      {
         m_timer.reset();

         // Adjust and wrap
         if (AWAY_FROM_CPU == m_direction)
         {
            std::advance(m_currentPixel,1);
            if (m_pixels.end() == m_currentPixel)
            {
               // m_direction = TOWARD_CPU;
               // std::advance(m_currentPixel,-2);
               m_currentPixel = m_pixels.begin();
            }
         }
         else
         {
            if (m_pixels.begin() != m_currentPixel)
            {
               std::advance(m_currentPixel,-1);
            }
            else
            {
               // m_direction = AWAY_FROM_CPU;
               // std::advance(m_currentPixel,1);
               m_currentPixel = m_pixels.end();
            }
         }
      }
   }
   else
   {
      // Pixels already in use - try again next time
   }

   return;
}


} // END namespace StatusLed
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

