// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <algorithm>
#include <string>
#include <map>
#include <PsbMacros.h>
#include <PsbLogger.h>

#include "PixelReservationManager.h"
#include "indicators/MorseCodeIndicator.h"


// ///////////////////////////////////////////////////////////////////////////// 
// Define(s)
// /////////////////////////////////////////////////////////////////////////////
#define DOT_DURATION (0.100)


// ///////////////////////////////////////////////////////////////////////////// 
// Static Data
// /////////////////////////////////////////////////////////////////////////////
static std::map<char,std::string> gv_MorseCodeTable = 
{
   { 'A', std::string(".-")      },
   { 'B', std::string("-...")    },
   { 'C', std::string("-.-.")    },
   { 'D', std::string("-..")     },
   { 'E', std::string(".")       },
   { 'F', std::string("..-.")    },
   { 'G', std::string("--.")     },
   { 'H', std::string("....")    },
   { 'I', std::string("..")      },
   { 'J', std::string(".---")    },
   { 'K', std::string("-.-")     },
   { 'L', std::string(".-..")    },
   { 'M', std::string("--")      },
   { 'N', std::string("-.")      },
   { 'O', std::string("---")     },
   { 'P', std::string(".--.")    },
   { 'Q', std::string("--.-")    },
   { 'R', std::string(".-.")     },
   { 'S', std::string("...")     },
   { 'T', std::string("-")       },
   { 'U', std::string("..-")     },
   { 'V', std::string("...-")    },
   { 'W', std::string(".--")     },
   { 'X', std::string("-..-")    },
   { 'Y', std::string("-.--")    },
   { 'Z', std::string("--..")    },
   { '0', std::string("-----")   },
   { '1', std::string(".----")   },
   { '2', std::string("..---")   },
   { '3', std::string("...--")   },
   { '4', std::string("....-")   },
   { '5', std::string(".....")   },
   { '6', std::string("-....")   },
   { '7', std::string("--...")   },
   { '8', std::string("---..")   },
   { '9', std::string("----.")   },
   { '!', std::string("-.-.--")  }
};


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace StatusLed {


////////////////////////////////////////////////////////////////////////////////
MorseCodeIndicator::MorseCodeIndicator(LedStrip & r_leds,
                                       PixelReservationManager & r_manager,
                                       pixel_set_t pixels,
                                       uint8_t red,
                                       uint8_t green,
                                       uint8_t blue,
                                       std::string text)
   : IndicatorBase(__FUNCTION__, r_leds, r_manager, pixels)
   , m_red(red)
   , m_green(green)
   , m_blue(blue)
   , m_timer()
   , m_morseCodeChars("")
   , m_iter(m_morseCodeChars.begin())
{
   const std::string allowed_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

   // Grab a copy of the user-string, and upper-case it
   std::string squashed_string(text);
   std::transform(squashed_string.begin(),
                  squashed_string.end(),
                  squashed_string.begin(),
                  ::toupper);

   // Loop through the string, and squash any non-conforming characters to '!'
   size_t idx = squashed_string.find_first_not_of(allowed_chars, 0);
   while (std::string::npos != idx)
   {
      squashed_string[idx] = '!';
      idx = squashed_string.find_first_not_of(allowed_chars, idx+1);
   }

   // Warn if the text was modified from the user
   if (text != squashed_string)
   {
      PSB_LOG_WARNING(2,
            Psb::LOG_TYPE_STRING, text.c_str(),
            Psb::LOG_TYPE_STRING, squashed_string.c_str());
   }

   // Now, encode the text into Morse
   for (auto c : squashed_string)
   {
      if (' ' != c)
      {
         // And for each dit/dah in the coded character
         for (auto ccode : gv_MorseCodeTable[c])
         {
            m_morseCodeChars += ('.' == ccode)
               ? "1"      // "dit"
               : "111";   // "dah"

            // Plus one dot duration off
            m_morseCodeChars += "0";
         }

         // Pad 3 DOT_DURATIONs for each character
         m_morseCodeChars += "000";
      }
      else
      {
         // Word spacer (7 dot durations)
         m_morseCodeChars += "0000000";
      }
   }

   // PSB requirement... 20 DOT_DURATIONs between text repeats
   m_morseCodeChars += "00000000000000000000";
}


////////////////////////////////////////////////////////////////////////////////
MorseCodeIndicator::~MorseCodeIndicator(void)
{
}


////////////////////////////////////////////////////////////////////////////////
void
MorseCodeIndicator::initialize(void)
{
   // Mark this indication as in progress
   m_completeFlag = false;

   // Start from the beginning of the Morse-Encoded string
   m_iter = m_morseCodeChars.begin();

   // Ensure our timer is firing at the right pace
   m_timer.stop();
   m_timer.setExpiration(DOT_DURATION);
   m_timer.start();
   m_timer.reset();
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
MorseCodeIndicator::show(void)
{
   // Make sure we control the pixels
   if (true == mr_manager.reservePixels(m_pixels))
   {
      // Update all pixels in our control with the color or off
      uint8_t red   = ('1' == *m_iter) ? m_red : 0;
      uint8_t green = ('1' == *m_iter) ? m_green : 0;
      uint8_t blue  = ('1' == *m_iter) ? m_blue : 0;

      for (auto idx : m_pixels)
      {
         mr_leds.setColor(idx, red, green, blue);
      }

      // Advance to the next encoded character if needed
      if (true == m_timer.isExpired())
      {
         // Kick it again
         m_timer.reset();

         // Increment with wrap
         m_iter++;
         if (m_morseCodeChars.end() == m_iter)
         {
            m_iter = m_morseCodeChars.begin();
         }
      }
   }
   else
   {
      // Pixels already in use - try again next time
      PSB_LOG_DEBUG1(0);
   }

   return;
}


} // END namespace StatusLed
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

