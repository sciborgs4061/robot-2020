// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __BlinkIndicator_h__
#define __BlinkIndicator_h__


// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string>
#include <PsbMacros.h>
#include <PsbTimer.h>

#include "StatusLedTypes.h"
#include "LedStrip.h"
#include "PixelReservationManager.h"
#include "indicators/IndicatorBase.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace StatusLed {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This class renders a solid color across the given pixels.
////////////////////////////////////////////////////////////////////////////////
class BlinkIndicator : public IndicatorBase
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Overloaded Constructor
      ///
      /// @param[in] r_leds     Reference to the LED Strip to use
      /// @param[in] r_manager  Reference to the pixel reservation object
      /// @param[in] pixels     The set of pixels to operate on
      /// @param[in] period     Blink period (on+off time in seconds)
      /// @param[in] count      The number of blinks before complete
      /// @param[in] red        Intensity of the red pixel [0:127]
      /// @param[in] green      Intensity of the red pixel [0:127]
      /// @param[in] blue       Intensity of the red pixel [0:127]
      ///
      /// @remarks
      /// Setting 'count' to 0 indicates that this indicator should blink
      /// indefinitely.
      //////////////////////////////////////////////////////////////////////////
      BlinkIndicator(LedStrip & r_leds,
                          PixelReservationManager & r_manager,
                          pixel_set_t pixels,
                          double period,
                          uint32_t count,
                          /// @todo - consider adding a period adjustment in
                          //here so that we can make fast/slow blink indicators
                          //in a single class
                          uint8_t red,
                          uint8_t green,
                          uint8_t blue);
     
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~BlinkIndicator(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see IndicatorBase::initialize()
      //////////////////////////////////////////////////////////////////////////
      void initialize(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see IndicatorBase::show()
      //////////////////////////////////////////////////////////////////////////
      void show(void);
     
   private:
      /// @todo Document me!
      uint8_t m_red;
      uint8_t m_green;
      uint8_t m_blue;
      Psb::Timer m_timer;
      double m_blinkPeriod;
      uint32_t m_blinkLimit;
      uint32_t m_numBlinksCompleted;
      bool m_lightsOnFlag;

   private:
      DISALLOW_DEFAULT_CTOR(BlinkIndicator);
      DISALLOW_COPY_CTOR(BlinkIndicator);
      DISALLOW_ASSIGNMENT_OPER(BlinkIndicator);
}; // END class BlinkIndicator


} // END namespace StatusLed
} // END namespace Subsystem
} // END namespace Psb


#endif // __BlinkIndicator_h__


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

