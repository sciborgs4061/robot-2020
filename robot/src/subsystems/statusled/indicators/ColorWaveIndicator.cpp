// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string>
#include <PsbMacros.h>
#include <PsbLogger.h>

#include "StatusLedTypes.h"
#include "PixelReservationManager.h"
#include "indicators/ColorWaveIndicator.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace StatusLed {


////////////////////////////////////////////////////////////////////////////////
ColorWaveIndicator::ColorWaveIndicator(LedStrip & r_leds,
                                       PixelReservationManager & r_manager,
                                       pixel_set_t pixels,
                                       double period)
   : IndicatorBase(__FUNCTION__, r_leds, r_manager, pixels)
   , m_timer()
   , m_updatePeriod(period)
   , m_wheelPosition(0)
{
   // 20 milliseconds is the minimum
   if (m_updatePeriod < 0.020)
   {
      PSB_LOG_ERROR(1,
            Psb::LOG_TYPE_FLOAT64, m_updatePeriod);

      // Provide the minimum value
      m_updatePeriod = 0.020;
   }
}


////////////////////////////////////////////////////////////////////////////////
ColorWaveIndicator::~ColorWaveIndicator(void)
{
}


////////////////////////////////////////////////////////////////////////////////
void
ColorWaveIndicator::initialize(void)
{
   // Mark this indication as in progress
   m_completeFlag = false;

   // Prepare data
   m_wheelPosition = 0;

   m_timer.stop();
   m_timer.setExpiration(m_updatePeriod);
   m_timer.start();
   m_timer.reset();
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
ColorWaveIndicator::show(void)
{
   // Make sure we control the pixels
   if (true == mr_manager.reservePixels(m_pixels))
   {
      // Render the pixel values in the stripA
      auto iter = m_pixels.begin();
      for (uint16_t i = 0; i < m_pixels.size(); i++)
      {
         rgb_t color = 
            this->getColorFromWheel((m_wheelPosition + (i<<3)) % 384);
         mr_leds.setColor(*iter, color.red, color.green, color.blue);

         std::advance(iter, 1);
      }

      // Check to see if it's time to update
      if (true == m_timer.isExpired())
      {
         m_timer.reset();

         m_wheelPosition =
            (m_wheelPosition >= 383)
            ? 0
            : m_wheelPosition + (1<<3);
      }
   }
   else
   {
      // Pixels already in use - try next time
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
rgb_t
ColorWaveIndicator::getColorFromWheel(uint16_t pos)
{
   // Default to OFF
   rgb_t color = {0x00, 0x00, 0x00};

   switch (pos >> 7)
   {
      case 0:
      {
         color.red   = 0x7F - (pos & 0x7F); // red down
         color.green = (pos & 0x7F);        // green up
         color.blue  = 0;                   // blue off
      } break;

      case 1:
      {
         color.green = 0x7F - (pos & 0x7F); // green down
         color.blue  = (pos & 0x7F);        // blue up
         color.red   = 0;                   // red off
      } break;

      case 2:
      {
         color.blue  = 0x7F - (pos & 0x7F); // blue down
         color.red   = (pos & 0x7F);        // red up
         color.green = 0;                   // green off
      } break;

      default:
      {
         // We got out of range... not supposed to happen
         PSB_LOG_ERROR(1,
            Psb::LOG_TYPE_UINT16, pos);
      } break;
   } // END switch()

   return color;
}


} // END namespace StatusLed
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

