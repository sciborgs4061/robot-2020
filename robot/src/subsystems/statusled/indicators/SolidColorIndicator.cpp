// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string>
#include <PsbMacros.h>
#include <PsbLogger.h>

#include "PixelReservationManager.h"
#include "indicators/SolidColorIndicator.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace StatusLed {


////////////////////////////////////////////////////////////////////////////////
SolidColorIndicator::SolidColorIndicator(LedStrip & r_leds,
                                         PixelReservationManager & r_manager,
                                         pixel_set_t pixels,
                                         /// @todo Consider adding a time
                                         //duration which, if nullptr, means
                                         //never expire, else is a timeout value
                                         //which will control the "complete"
                                         //flag for this indicator
                                         uint8_t red,
                                         uint8_t green,
                                         uint8_t blue)
   : IndicatorBase(__FUNCTION__, r_leds, r_manager, pixels)
   , m_red(red)
   , m_green(green)
   , m_blue(blue)
{
}


////////////////////////////////////////////////////////////////////////////////
SolidColorIndicator::~SolidColorIndicator(void)
{
}


////////////////////////////////////////////////////////////////////////////////
void
SolidColorIndicator::initialize(void)
{
   // Mark this indication as in progress
   m_completeFlag = false;
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
SolidColorIndicator::show(void)
{
   // Make sure we control the pixels
   if (true == mr_manager.reservePixels(m_pixels))
   {
      for (auto idx : m_pixels)
      {
         mr_leds.setColor(idx, m_red, m_green, m_blue);
      }
   }
   else
   {
      // Pixels already in use - try again next time
      PSB_LOG_DEBUG1(0);
   }

   return;
}


} // END namespace StatusLed
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

