// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __PingPongIndicator_h__
#define __PingPongIndicator_h__


// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string>
#include <list>
#include <PsbMacros.h>
#include <PsbTimer.h>

#include "StatusLedTypes.h"
#include "LedStrip.h"
#include "PixelReservationManager.h"
#include "indicators/IndicatorBase.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace StatusLed {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This class implements a "ping-pong" effect by sending a small group of
/// pixels along the pixel set in one direction and then reflecting that group
/// back down the same set of pixels.
///
/// @remarks
/// While a nearly limitless configuration of pixels is possible, the intent of
/// this object is to "walk" a pixel down a linear stretch of LEDs.  Keep this
/// in mind when constructing the object.
////////////////////////////////////////////////////////////////////////////////
class PingPongIndicator : public IndicatorBase
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Overloaded Constructor
      ///
      /// @param[in] r_leds     Reference to the LED Strip to use
      /// @param[in] r_manager  Reference to the pixel reservation object
      /// @param[in] pixels     The set of pixels to operate on
      /// @param[in] r_colors   RGB color sets for each pixel in the display
      ///
      /// @remarks
      /// The r_colors parameter lists all colors to be displayed - the first
      /// color is the "point" and all others are mirrored about the point.
      //////////////////////////////////////////////////////////////////////////
      PingPongIndicator(LedStrip & r_leds,
                        PixelReservationManager & r_manager,
                        pixel_set_t pixels,
                        std::list<rgb_t> & r_colors);
     
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~PingPongIndicator(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see IndicatorBase::initialize()
      //////////////////////////////////////////////////////////////////////////
      void initialize(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see IndicatorBase::show()
      //////////////////////////////////////////////////////////////////////////
      void show(void);
     
   private:
      /// @todo Document me!
      std::list<rgb_t> m_colorList;
      /// @todo Consider moving this to the IndicatorBase as many indicators
      ///       have a time-based component to them.
      Psb::Timer m_timer;
      pixel_set_t::iterator m_currentPixel;
      bool m_direction;

   private:
      DISALLOW_DEFAULT_CTOR(PingPongIndicator);
      DISALLOW_COPY_CTOR(PingPongIndicator);
      DISALLOW_ASSIGNMENT_OPER(PingPongIndicator);
}; // END class PingPongIndicator


} // END namespace StatusLed
} // END namespace Subsystem
} // END namespace Psb


#endif // __PingPongIndicator_h__


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

