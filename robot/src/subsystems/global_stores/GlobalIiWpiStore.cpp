// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <frc/SPI.h>
#include "GlobalIiWpiStore.h"
#include "IiDataPushers.h"

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {


// ////////////////////////////////////////////////////////////////////////////
// Static Data
// ///////////////////////////////////////////////////////////////////////////// 
GlobalIiWpiStore* GlobalIiWpiStore::msp_instance = nullptr;


////////////////////////////////////////////////////////////////////////////////
GlobalIiWpiStore::GlobalIiWpiStore(void)
   : m_spiMaster(nullptr)
{
   //m_spiMaster = new SPI(SPI::kMXP);
   m_spiMaster = new frc::SPI(frc::SPI::kOnboardCS0);
   m_spiMaster->SetClockRate(1000000.0);
   m_spiMaster->SetMSBFirst();
   m_spiMaster->SetSampleDataOnLeadingEdge();
   m_spiMaster->SetClockActiveHigh();
   m_spiMaster->SetChipSelectActiveLow();
}


////////////////////////////////////////////////////////////////////////////////
GlobalIiWpiStore::~GlobalIiWpiStore(void)
{
   delete m_spiMaster;
   m_spiMaster = nullptr;
}


////////////////////////////////////////////////////////////////////////////////
GlobalIiWpiStore*
GlobalIiWpiStore::instance(void)
{
   if (nullptr == msp_instance)
   {
      msp_instance = new GlobalIiWpiStore();
   }

   return msp_instance;
}


////////////////////////////////////////////////////////////////////////////////
void
GlobalIiWpiStore::attach(LPD8806LedStripDataPusher * p_pusher)
{
   p_pusher->setSpiDevice(m_spiMaster);
   m_pushers.push_back(p_pusher);
}

////////////////////////////////////////////////////////////////////////////////
void
GlobalIiWpiStore::flush(void)
{
   for (auto p_pusher : m_pushers)
   {
      p_pusher->pushData();
   }
}

} // END namespace Subsystem
} // END namespace Psb

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////


