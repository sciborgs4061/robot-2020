// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string.h>  // for NULL
#include "GlobalSiWpiStore.h"
#include "SiDataGrabbers.h"

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {


// ////////////////////////////////////////////////////////////////////////////
// Sensor Data Grabber Static Data
// ///////////////////////////////////////////////////////////////////////////// 
double NavigationDataGrabber::s_old_update_count  = 0;

// ////////////////////////////////////////////////////////////////////////////
// Static Data
// ///////////////////////////////////////////////////////////////////////////// 
GlobalSiWpiStore* GlobalSiWpiStore::msp_instance = NULL;


////////////////////////////////////////////////////////////////////////////////
GlobalSiWpiStore::GlobalSiWpiStore(void)
{
   int x;
   for (x = 0; x < MAX_ENCODERS; x++)
   {
      m_encoder[x] = new Encoder(m_encoder_channels[x].aChannel,
                                 m_encoder_channels[x].bChannel,
                                 m_encoder_channels[x].reverse);
      m_encoder[x]->SetDistancePerPulse(m_encoder_channels[x].distancePerPulse);
      m_encoder[x]->SetSamplesToAverage(m_encoder_channels[x].samplesToAverage);
   }

   for (x = 0; x < MAX_DIGITAL_INPUTS; x++)
   {
      m_digitalInput[x] = new DigitalInput(m_di_mappings[x].idx);
   }

#if MAX_ANALOG_INPUTS
   for (x = 0; x < MAX_ANALOG_INPUTS; x++)
   {
      m_analogInput[x] = new AnalogInput(m_ai_mappings[x].idx);
   }
#endif
#if MAX_ANALOG_POTS
   for (x = 0; x < MAX_ANALOG_POTS; x++)
   {
      m_analogPot[x] = new AnalogPotentiometer(m_ap_mappings[x].idx,
                                               m_ap_mappings[x].fullRange,
											   m_ap_mappings[x].offset);
   }
#endif
   
#ifndef X86_PLATFORM
#if 0
   m_navigation = new AHRS(SerialPort::Port::kMXP,
                           AHRS::SerialDataType::kProcessedData,
                           NavigationDataGrabber::UPDATE_RATE_HZ);
#else
   m_navigation = new AHRS(SPI::kMXP, NavigationDataGrabber::UPDATE_RATE_HZ);
#endif
#endif // X86_PLATFORM
   
   //m_vision = new Vision::VisionProcessor();

   m_pdp = new PowerDistributionPanel(0);

}


////////////////////////////////////////////////////////////////////////////////
GlobalSiWpiStore::~GlobalSiWpiStore(void)
{
   int x;
   for (x = 0; x < MAX_ENCODERS; x++)
   {
      delete m_encoder[x];
      m_encoder[x] = NULL;
   }

   for (x = 0; x < MAX_DIGITAL_INPUTS; x++)
   {
      delete m_digitalInput[x];
      m_digitalInput[x] = NULL;
   }

#if MAX_ANALOG_INPUTS
   for (x = 0; x < MAX_ANALOG_INPUTS; x++)
   {
      delete m_analogInput[x];
      m_analogInput[x] = NULL;
   }
#endif
#if MAX_ANALOG_POTS
   for (x = 0; x < MAX_ANALOG_POTS; x++)
   {
      delete m_analogPot[x];
      m_analogPot[x] = NULL;
   }
#endif
   delete m_navigation;
   m_navigation = NULL;
   delete m_pdp;
   m_pdp = NULL;
}


////////////////////////////////////////////////////////////////////////////////
GlobalSiWpiStore*
GlobalSiWpiStore::instance(void)
{
   if (NULL == msp_instance)
   {
      msp_instance = new GlobalSiWpiStore();
   }

   return msp_instance;
}

////////////////////////////////////////////////////////////////////////////////
void GlobalSiWpiStore::attach(EncoderDataGrabber *edg, si_encoders_t idx)
{
   // printf("\n\n  LOOKING FOR: Encoder idx: %d\n",idx);
   if (idx < MAX_ENCODERS)
   {
      // printf("Found Encoder: %d\n",idx);
         // printf("  NAME: %s\n",m_encoder_channels[idx].name);
         // printf("  A: %d\n",m_encoder_channels[idx].aChannel);
         // printf("  B: %d\n",m_encoder_channels[idx].bChannel);
      edg->setEncoder(m_encoder[idx]);
      m_grabbers.push_back(edg);
   }
   else
   {
      // printf("\n\n  FAILED TO FIND Encoder idx: %d\n",idx);
      // bad index...
   }
}


////////////////////////////////////////////////////////////////////////////////
void GlobalSiWpiStore::attach(EncoderDataGrabber *edg, std::string const &name)
{
   // printf("\n\n  LOOKING FOR: Encoder: %s\n",name.c_str());
   int x;
   for (x = 0; x < MAX_ENCODERS; x++)
   {
      if (name == m_encoder_channels[x].name)
      {
         // printf("Found Encoder: %s\n",name.c_str());
         // printf("  A: %d\n",m_encoder_channels[x].aChannel);
         // printf("  B: %d\n",m_encoder_channels[x].bChannel);
         edg->setEncoder(m_encoder[x]);
         m_grabbers.push_back(edg);
         break;
      }
      else
      {
         // printf("NOT: Found Encoder: %s\n",name.c_str());
         // printf("  LOOKING AT: Encoder: %s\n",m_encoder_channels[x].name);
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void GlobalSiWpiStore::attach(DigitalInputDataGrabber *didg,
                              si_digitalinputs_t idx)
{
   // printf("\n\n  LOOKING FOR: DI idx: %d\n",idx);
   if (idx < MAX_DIGITAL_INPUTS)
   {
      // printf("\n\n  FOUND FOR: DI idx: %d\n",idx);
      didg->setDigitalInput(m_digitalInput[idx]);
      m_grabbers.push_back(didg);
   }
   else
   {
      // bad index...
   }
}

////////////////////////////////////////////////////////////////////////////////
void GlobalSiWpiStore::attach(DigitalInputDataGrabber *didg,
                              std::string const &name)
{
   // printf("\n\n  LOOKING FOR: DI: %s\n",name.c_str());
   int x;
   for (x = 0; x < MAX_DIGITAL_INPUTS; x++)
   {
      if (name == m_di_mappings[x].name)
      {
         // printf("Found DI: %s -- %d\n",name.c_str(),m_di_mappings[x].idx);
         didg->setDigitalInput(m_digitalInput[x]);
         m_grabbers.push_back(didg);
         break;
      }
      else
      {
         // printf("  NOT Found DI: %s\n",name.c_str());
         // printf("  LOOKING AT: DI: %s\n",m_di_mappings[x].name);
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void GlobalSiWpiStore::attach(AnalogInputDataGrabber *aidg,
                              si_analoginputs_t idx)
{
#if MAX_ANALOG_INPUTS
  if (idx < MAX_ANALOG_INPUTS)
   {
      aidg->setInput(m_analogInput[idx]);
      m_grabbers.push_back(aidg);
   }
   else
   {
      // bad index...
   }
#endif
}

////////////////////////////////////////////////////////////////////////////////
void GlobalSiWpiStore::attach(AnalogInputDataGrabber *aidg,
                              std::string const &name)
{
#if MAX_ANALOG_INPUTS
   int x;
   for (x = 0; x < MAX_ANALOG_INPUTS; x++)
   {
      if (name == m_ai_mappings[x].name)
      {
         aidg->setInput(m_analogInput[x]);
         m_grabbers.push_back(aidg);
         break;
      }
   }
#endif
}

////////////////////////////////////////////////////////////////////////////////
void GlobalSiWpiStore::attach(AnalogPotDataGrabber *apdg,
                              si_analogpots_t idx)
{
#if MAX_ANALOG_POTS
   if (idx < MAX_ANALOG_POTS)
   {
      apdg->setInput(m_analogPot[idx]);
      m_grabbers.push_back(apdg);
   }
   else
   {
      // bad index...
   }
#endif 
}

////////////////////////////////////////////////////////////////////////////////
void GlobalSiWpiStore::attach(PDPDataGrabber *pdpdg)
{
   pdpdg->setPDP(m_pdp);
   m_grabbers.push_back(pdpdg);
}

////////////////////////////////////////////////////////////////////////////////
void GlobalSiWpiStore::attach(NavigationDataGrabber *ndg)
{
   ndg->setNavigation(m_navigation);
   m_grabbers.push_back(ndg);
}

////////////////////////////////////////////////////////////////////////////////
void GlobalSiWpiStore::refresh(void)
{
   for (auto p_grabber : m_grabbers)
   {
      p_grabber->grabData();
   }
}

} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

