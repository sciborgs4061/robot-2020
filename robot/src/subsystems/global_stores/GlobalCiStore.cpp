////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string.h> // for NULL
#include <sys/types.h>
#include <dirent.h>
#include "GlobalCiStore.h"
#include "ConfigItem.h"
#include "PsbXmlConfigurationFile.h"

#ifdef X86_PLATFORM
#include <unistd.h>
#include <pwd.h>
#endif

std::string determineProgramsLocation() {
#ifndef X86_PLATFORM
   return "/home/lvuser/psb_settings/";
#else
   // TODO: Use /robot-2017/src/the_robot/config/
   /*
   struct passwd *pw = getpwuid(getuid());
   return std::string(pw->pw_dir) + "/.psb/";
   */
   // assuming we are running from the build directory on x86
   return "../src/subsystems/the_robot/config/";
#endif

} 

// ////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {


// ////////////////////////////////////////////////////////////////////////////
// Static Data
// /////////////////////////////////////////////////////////////////////////////
GlobalCiStore* GlobalCiStore::msp_instance = NULL;


////////////////////////////////////////////////////////////////////////////////
GlobalCiStore::GlobalCiStore(void)
   : m_configValues()
{
   refresh();
}


////////////////////////////////////////////////////////////////////////////////
GlobalCiStore::~GlobalCiStore(void)
{
   cleanupConfigs();
   for (auto item : m_items)
   {
      delete item;
   }
}

////////////////////////////////////////////////////////////////////////////////
void GlobalCiStore::cleanupConfigs(void)
{
   for (auto config : m_configs)
   {
      delete config;
   }
}


////////////////////////////////////////////////////////////////////////////////
GlobalCiStore*
GlobalCiStore::instance(void)
{
   if (NULL == msp_instance)
   {
      msp_instance = new GlobalCiStore();
   }

   return msp_instance;
}

////////////////////////////////////////////////////////////////////////////////
void GlobalCiStore::attach(ConfigItem *item)
{
   m_items.push_back(item);
}

////////////////////////////////////////////////////////////////////////////////
void GlobalCiStore::refresh(void)
{
   // Clean up configuration.
   cleanupConfigs();
   m_configs.clear();

   // Find all the configuration files.
   std::string prefix = determineProgramsLocation();

   DIR *dir = opendir(prefix.c_str());
   struct dirent *dirent;

   if (dir != NULL) {
      while ((dirent = readdir(dir)) != NULL) {
         if ((strlen(dirent->d_name) >= 6) && (strncmp("Config_", dirent->d_name, 6) == 0)) {
            m_configs.push_back(new XmlConfigurationFile(prefix + dirent->d_name));
         }
      }

      for (auto config : m_configs)
      {
         for (auto kp : config->m_configItemsTable)
         {
            m_configValues[kp.first] = kp.second;
         }
      }
   }

   // Grab new data.
   for (auto p_item : m_items)
   {
      p_item->grabData();
   }
}


} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////
