// /////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __GlobalSiWpiStore_h__
#define __GlobalSiWpiStore_h__

// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbMacros.h>
#include <list>
#include "SiDataGrabbers.h"
#include "VisionProcessor.h"
#include "GripPipeline.h"
#include "I2CBreakout.h"
// ///////////////////////////////////////////////////////////////////////////// 
// Namespace(s)
// ///////////////////////////////////////////////////////////////////////////// 
namespace Psb {
namespace Subsystem {

//////////////////////////////////////////////////////////////////////////////// 
/// @brief 
/// Class that provides a single access point for all sensor interface Wpi 
/// objects in this file
//////////////////////////////////////////////////////////////////////////////// 
class GlobalSiWpiStore
{
   public:

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      ///   Encoders.
      //////////////////////////////////////////////////////////////////////////

      typedef struct {
         uint32_t aChannel; // the index of the DI on the Rio for channelA
         uint32_t bChannel; // the index of the DI on the Rio for channelB
         bool reverse;      // should we track reverse movement?
         double distancePerPulse; // the linear/angular distance per encoder pulse
         uint32_t samplesToAverage; // the number of pulse times to average
         char const *name;  // the name of the sensor
      } encoder_channel_t;

      // define the ENCODER macro to just pull out names to use
      // as the enumeration
      #define ENCODER(name,achannel,bchannel,reverse,distancePerPulse,sampleToAverage) name,

      typedef enum {
         #include <TheRobotMappings.h>
         MAX_ENCODERS
      } si_encoders_t;

      // now redefine the ENCODER macro to fill out the structure
      // that is needed by the data grabbers
      #undef ENCODER
      #define ENCODER(name,achannel,bchannel,reverse,distancePerPulse,sampleToAverage) \
           {achannel,bchannel,reverse,distancePerPulse,sampleToAverage,#name},

      const encoder_channel_t m_encoder_channels[MAX_ENCODERS+1] = {
         #include <TheRobotMappings.h>
         {0,0,true,0.0,0,"MAX_ENCODER_PLACE_HOLDER"}
      };

      // undefine the ENCODER macro so it doesn't do anything
      #undef ENCODER

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      ///   Limit Switches.
      //////////////////////////////////////////////////////////////////////////

      typedef struct {
         uint32_t idx; // the index of the DI on the Rio
         char const *name;  // the name of the sensor
      } di_mapping_t;

      // define the DIGITAL_INPUT macro to just pull out names to use
      // as the enumeration
      #define DIGITAL_INPUT(name,idx) name,

      typedef enum {
         #include <TheRobotMappings.h>
         MAX_DIGITAL_INPUTS
      } si_digitalinputs_t;

      // now redefine the DIGITAL_INPUT macro to fill out the structure
      // that is needed by the data grabbers
      #undef DIGITAL_INPUT
      #define DIGITAL_INPUT(name,idx) \
           {idx,#name},

      const di_mapping_t m_di_mappings[MAX_DIGITAL_INPUTS+1] = {
         #include <TheRobotMappings.h>
         {0,"MAX_DIGITAL_INPUT_PLACE_HOLDER"}
      };

      // undefine the DIGITAL_INPUT macro so it doesn't do anything
      #undef DIGITAL_INPUT

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      ///   Analog Inputs.
      //////////////////////////////////////////////////////////////////////////

      typedef struct {
         uint32_t idx;      // the index of the AI on the Rio
         char const *name;  // the name of the sensor
      } ai_mapping_t;

      // define the ANALOG_INPUT macro to just pull out names to use
      // as the enumeration
      #define ANALOG_INPUT(name,idx) name,

      typedef enum {
         #include <TheRobotMappings.h>
         MAX_ANALOG_INPUTS
      } si_analoginputs_t;

      // now redefine the ANALOG_INPUT macro to fill out the structure
      // that is needed by the data grabbers
      #undef ANALOG_INPUT
      #define ANALOG_INPUT(name,idx) \
           {idx,#name},

      const ai_mapping_t m_ai_mappings[MAX_ANALOG_INPUTS+1] = {
         #include <TheRobotMappings.h>
         {0,"MAX_ANALOG_INPUT_PLACE_HOLDER"}
      };

      // undefine the ANALOG_INPUT macro so it doesn't do anything
      #undef ANALOG_INPUT

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      ///   Analog Potentiometers.
      //////////////////////////////////////////////////////////////////////////

      typedef struct {
         uint32_t idx;      // the index of the AI on the Rio
         double fullRange;  // full range
         double offset;     // offset
         char const *name;  // the name of the sensor
      } ap_mapping_t;

      // define the ANALOG_POT macro to just pull out names to use
      // as the enumeration
      #define ANALOG_POT(name,idx,fullRange,offset) name,

      typedef enum {
         #include <TheRobotMappings.h>
         MAX_ANALOG_POTS
      } si_analogpots_t;

      // now redefine the ANALOG_INPUT macro to fill out the structure
      // that is needed by the data grabbers
      #undef ANALOG_POT
      #define ANALOG_POT(name,idx,fullRange,offset) \
           {idx,fullRange,offset,#name},

      const ap_mapping_t m_ap_mappings[MAX_ANALOG_POTS+1] = {
         #include <TheRobotMappings.h>
         {0,1,0,"MAX_ANALOG_POT_PLACE_HOLDER"}
      };

      // undefine the ANALOG_POT macro so it doesn't do anything
      #undef ANALOG_POT
      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      /// Singleton create/access function.
      ////////////////////////////////////////////////////////////////////////// 
      static GlobalSiWpiStore* instance(void);

      ///////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor for GlobalSiWpiStore.
      ///////////////////////////////////////////////////////////////////////////
      ~GlobalSiWpiStore(void);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Attach a memory location to type of SI.
      ///
      /// [in] edg - The encoder data grabber.
      /// [in] idx - The zero based index of the item to push data to
      ///              use the si_encoders_t enumeration.
      ////////////////////////////////////////////////////////////////////////// 
      void attach(EncoderDataGrabber *edg, si_encoders_t idx);
      // string version
      void attach(EncoderDataGrabber *edg, std::string const &name);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Attach a memory location to type of SI.
      ///
      /// [in] lsdg - The limit swith data grabber.
      /// [in] idx - The zero based index of the item to push data to
      ///              use the si_digitalinputs_t enumeration.
      ////////////////////////////////////////////////////////////////////////// 
      void attach(DigitalInputDataGrabber *didg, si_digitalinputs_t idx);
      // string version
      void attach(DigitalInputDataGrabber *didg, std::string const &name);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Attach a memory location to type of SI.
      ///
      /// [in] pdg - The analog input data grabber.
      /// [in] idx - The zero based index of the item to push data to
      ///              use the si_digitalinputs_t enumeration.
      ////////////////////////////////////////////////////////////////////////// 
      void attach(AnalogInputDataGrabber *aidg, si_analoginputs_t idx);
      // string version
      void attach(AnalogInputDataGrabber *aidg, std::string const &name);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Attach a memory location to type of SI.
      ///
      /// [in] apdg - The analog potentiometer data grabber.
      /// [in] idx  - The zero based index of the item to push data to
      ///              use the si_digitalinputs_t enumeration.
      ////////////////////////////////////////////////////////////////////////// 
      void attach(AnalogPotDataGrabber *apdg, si_analogpots_t idx);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Attach a memory location to type of SI.
      ///
      /// [in] pdg - The navigation sensor data grabber.
      ////////////////////////////////////////////////////////////////////////// 
      void attach(NavigationDataGrabber *nsdg);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Attach a memory location to type of SI.
      ///
      /// [in] vdg - The vision data grabber.
      ////////////////////////////////////////////////////////////////////////// 

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Attach the data grabber
      ///
      /// [in] pdpdg - The PDP sensor data grabber.
      ////////////////////////////////////////////////////////////////////////// 
      void attach(PDPDataGrabber *pdpdg);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   Refresh all the values from the physical objects.
      ////////////////////////////////////////////////////////////////////////// 
      void refresh(void);

   private:

      ///////////////////////////////////////////////////////////////////////////
      /// @brief 
      ///   Constructor for GlobalSiWpiStore.
      ///////////////////////////////////////////////////////////////////////////
      GlobalSiWpiStore(void);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief 
      ///   GlobalSiWpiStore's singleton instance.
      ////////////////////////////////////////////////////////////////////////// 
      static GlobalSiWpiStore* msp_instance;

      ///////////////////////////////////////////////////////////////////////////
      /// @brief
      ///   Array of Encoders.
      ///////////////////////////////////////////////////////////////////////////
      frc::Encoder* m_encoder[MAX_ENCODERS];

      ///////////////////////////////////////////////////////////////////////////
      /// @brief
      ///   Array of DigitalInputs.
      ///////////////////////////////////////////////////////////////////////////
      frc::DigitalInput* m_digitalInput[MAX_DIGITAL_INPUTS];

      ///////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Array of AnalogInputs.
      ///////////////////////////////////////////////////////////////////////////
#if MAX_ANALOG_INPUTS
      frc::AnalogInput* m_analogInput[MAX_ANALOG_INPUTS];
#endif
      
      ///////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Array of AnalogPots.
      ///////////////////////////////////////////////////////////////////////////
#if MAX_ANALOG_POTS 
      frc::AnalogPotentiometer* m_analogPot[MAX_ANALOG_POTS];
#endif

      ///////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Navigation Sensor
      ///////////////////////////////////////////////////////////////////////////
      AHRS* m_navigation;

      ///////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Vision Processor
      ///////////////////////////////////////////////////////////////////////////
      Vision::VisionProcessor* m_vision;

      ///////////////////////////////////////////////////////////////////////////
      /// @brief
      /// PDP Sensor
      ///////////////////////////////////////////////////////////////////////////
      PowerDistributionPanel* m_pdp;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief 
      ///   All the data grabbers
      ////////////////////////////////////////////////////////////////////////// 
      std::list<SiDataGrabber *> m_grabbers;

   private:
      DISALLOW_COPY_CTOR(GlobalSiWpiStore);
      DISALLOW_ASSIGNMENT_OPER(GlobalSiWpiStore);
}; // END class GlobalSiWpiStore

} // END namespace Subsystem
} // END namespace Psb

#endif // __GlobalSiWpiStore_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

