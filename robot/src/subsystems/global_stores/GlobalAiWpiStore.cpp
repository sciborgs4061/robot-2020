// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string.h> // needed for NULL
#include "frc/Talon.h"
#include "GlobalAiWpiStore.h"
#include "AiDataPushers.h"

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {


// ////////////////////////////////////////////////////////////////////////////
// Static Data
// ///////////////////////////////////////////////////////////////////////////// 
GlobalAiWpiStore* GlobalAiWpiStore::msp_instance = NULL;


////////////////////////////////////////////////////////////////////////////////
GlobalAiWpiStore::GlobalAiWpiStore(void)
{
   int x;
   for (x = 0; x < MAX_SPEED_CONTROLLER; x++)
   {
      m_speedController[x] = new frc::Talon(m_pwm_mappings[x].idx);
   }
   // should do the same for the relays
   for (x = 0; x < MAX_RELAY; x++)
   {
      m_relay[x] = new frc::Relay(x);
   }
   // and the digital outputs
   for (x = 0; x < MAX_DIGITAL_OUTPUT; x++)
   {
      m_digitalOutput[x] = new frc::DigitalOutput(m_digitaloutput_mappings[x].idx);
   }
}


////////////////////////////////////////////////////////////////////////////////
GlobalAiWpiStore::~GlobalAiWpiStore(void)
{
   int x;
   for (x = 0; x < MAX_SPEED_CONTROLLER; x++)
   {
      delete m_speedController[x];
      m_speedController[x] = NULL;
   }
   for (x = 0; x < MAX_RELAY; x++)
   {
      delete m_relay[x];
      m_relay[x] = NULL;
   }
   for (x = 0; x < MAX_DIGITAL_OUTPUT; x++)
   {
      delete m_digitalOutput[x];
      m_digitalOutput[x] = NULL;
   }
}


////////////////////////////////////////////////////////////////////////////////
GlobalAiWpiStore*
GlobalAiWpiStore::instance(void)
{
   if (NULL == msp_instance)
   {
      msp_instance = new GlobalAiWpiStore();
   }

   return msp_instance;
}


////////////////////////////////////////////////////////////////////////////////
void GlobalAiWpiStore::attach(SpeedControllerDataPusher *scdp, 
                              ai_speedcontrollers_t idx)
{
   if (idx < MAX_SPEED_CONTROLLER)
   {
      scdp->setSpeedController(m_speedController[idx]);
      m_pushers.push_back(scdp);
   }
   else
   {
      // bad index...
   }
}

// Note: the test idx >= 0 in these functions is needed only to prevent
// a compiler warning about a situation that cannot occur anyway
// (because the functions are never called if there are no relays or digitaloutputs.
// In the interest of clean compilation the tests have been added.
// If you actually have any relays or digitaloutputs you can remove them.
////////////////////////////////////////////////////////////////////////////////
void GlobalAiWpiStore::attach(RelayDataPusher *rdp, 
                              ai_relays_t idx)
{
  if (idx >= (ai_relays_t) 0 && idx < MAX_RELAY)
   {
      rdp->setRelay(m_relay[idx]);
      m_pushers.push_back(rdp);
   }
   else
   {
      // bad index...
   }
}


////////////////////////////////////////////////////////////////////////////////
void GlobalAiWpiStore::attach(DigitalOutputDataPusher *dodp,
                              ai_digitaloutput_t idx)
{
  if (idx >= (ai_digitaloutput_t) 0 && idx < MAX_DIGITAL_OUTPUT)
   {
      dodp->setDigitalOutput(m_digitalOutput[idx]);
      m_pushers.push_back(dodp);
   }
   else
   {
      // bad index...
   }
}

////////////////////////////////////////////////////////////////////////////////
void GlobalAiWpiStore::flush(void)
{
   for (auto p_pusher : m_pushers)
   {
      p_pusher->pushData();
   }
}

////////////////////////////////////////////////////////////////////////////////
void GlobalAiWpiStore::reset(void)
{
   for (auto p_pusher : m_pushers)
   {
      p_pusher->reset();
      p_pusher->pushData();
   }
}

} // END namespace Subsystem
} // END namespace Psb

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////


