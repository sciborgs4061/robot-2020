////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string.h> // for NULL
#include "GlobalMiStore.h"
#include "MiDataMirrors.h"

// ////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// ///////////////////////////////////////////////////////////////////////////// 
namespace Psb {
namespace Subsystem {


// ////////////////////////////////////////////////////////////////////////////
// Static Data
// ///////////////////////////////////////////////////////////////////////////// 
GlobalMiStore* GlobalMiStore::msp_instance = NULL;


////////////////////////////////////////////////////////////////////////////////
GlobalMiStore::GlobalMiStore(void)
{
}

////////////////////////////////////////////////////////////////////////////////
GlobalMiStore::~GlobalMiStore(void)
{
   for (auto p_mirrors : m_mirrors)
   {
      for (auto p_mirror : p_mirrors.second)
      {
         delete p_mirror;
      }
   }
   for (auto p_published : m_published)
   {
      delete p_published.second;
   }
}


////////////////////////////////////////////////////////////////////////////////
GlobalMiStore*
GlobalMiStore::instance(void)
{
   if (NULL == msp_instance)
   {
      msp_instance = new GlobalMiStore();
   }

   return msp_instance;
}

////////////////////////////////////////////////////////////////////////////////
void GlobalMiStore::mirror(std::string const &data_name, MiDataMirror *dm)
{
   m_mirrors[data_name].push_back(dm);
}

////////////////////////////////////////////////////////////////////////////////
void GlobalMiStore::publish(std::string const &data_name, MiDataPublisher *dp)
{
   m_published[data_name] = dp;
}

////////////////////////////////////////////////////////////////////////////////
void GlobalMiStore::init(void)
{
   // walk all the items that wish to be mirrored and find their
   // published counterparts and attach them together.
   for (auto p_mirrors : m_mirrors)
   {
      if (m_published.count(p_mirrors.first) == 1)
      {
         // if the requested item exists in the published map
         // the you can grab it and set the publisher for each of the mirrors
         for (auto p_mirror : p_mirrors.second)
         {
            p_mirror->setPublisher(m_published.find(p_mirrors.first)->second);
         }
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
void GlobalMiStore::mirror(void)
{
   for (auto p_mirrors : m_mirrors)
   {
      for (auto p_mirror : p_mirrors.second)
      {
         p_mirror->mirrorData();
      }
   }
}

} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

