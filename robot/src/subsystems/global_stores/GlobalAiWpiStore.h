// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __GlobalAiWpiStore_h__
#define __GlobalAiWpiStore_h__

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbMacros.h>
#include <list>
#include "AiDataPushers.h"

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {

////////////////////////////////////////////////////////////////////////////////
/// @brief
/// Provides a single access point for all WPI objects that will be used to
/// implement the subsystem-specific Actuator Interface classes
////////////////////////////////////////////////////////////////////////////////
class GlobalAiWpiStore
{
   public:

      // define the SPEED_CONTROLLER macro to just pull out names to use
      // as the enumeration
      #define SPEED_CONTROLLER(name,idx) name,

      typedef struct {
         uint32_t idx; // the index of the PWM on the Rio
      } pwm_mapping_t;

      typedef enum {
         #include <TheRobotMappings.h>
         MAX_SPEED_CONTROLLER
      } ai_speedcontrollers_t;

      // now redefine the SPEED_CONTROLLER macro to fill out the structure
      // that is needed by the data grabbers
      #undef SPEED_CONTROLLER
      #define SPEED_CONTROLLER(name,idx) \
           {idx},

      const pwm_mapping_t m_pwm_mappings[MAX_SPEED_CONTROLLER+1] = {
         #include <TheRobotMappings.h>
         {0} // just a placeholder...needs to be here, but isn't used
      };

      // undefine the SPEED_CONTROLLER macro so it doesn't do anything
      #undef SPEED_CONTROLLER


      // define the RELAY macro to use as the enumeration
      #define RELAY(name) name,

      typedef enum {
         #include <TheRobotMappings.h>
         MAX_RELAY
      } ai_relays_t;

      // undefine the RELAY macro so it doesn't do anything
      #undef RELAY

      // define the DIGITAL_OUTPUT macro to just pull out names to use
      // as the enumeration
      #define DIGITAL_OUTPUT(name,idx) name,

      typedef struct {
         uint32_t idx; // the index of the DigitalOutput on the Rio
      } digitaloutput_mapping_t;

      typedef enum {
         #include <TheRobotMappings.h>
         MAX_DIGITAL_OUTPUT
      } ai_digitaloutput_t;

      // now redefine the DIGITAL_OUTPUT macro to fill out the structure
      // that is needed by the data grabbers
      #undef DIGITAL_OUTPUT
      #define DIGITAL_OUTPUT(name,idx) \
           {idx},

      const digitaloutput_mapping_t m_digitaloutput_mappings[MAX_DIGITAL_OUTPUT+1] = {
         #include <TheRobotMappings.h>
         {0} // just a placeholder...needs to be here, but isn't used
      };

      // undefine the DIGITAL_OUTPUT macro so it doesn't do anything
      #undef DIGITAL_OUTPUT
      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      /// Singleton create/access function
      ////////////////////////////////////////////////////////////////////////// 
      static GlobalAiWpiStore* instance(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      ~GlobalAiWpiStore(void);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   attach a memory location to type of AI
      ///
      /// [in] scdp - the speed controller data pusher
      /// [in] idx - the zero based index of the item to push data to
      ///              ... use the ai_speedcontrollers_t enumeration
      ////////////////////////////////////////////////////////////////////////// 
      void attach(SpeedControllerDataPusher *scdp, ai_speedcontrollers_t idx);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   attach a memory location to type of AI
      ///
      /// [in] rdp - the relay data pusher
      /// [in] idx - the zero based index of the item to push data to
      ///              ... use the ai_relays_t enumeration
      ////////////////////////////////////////////////////////////////////////// 
      void attach(RelayDataPusher *rdp, ai_relays_t idx);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   attach a memory location to type of AI
      ///
      /// [in] dodp - the DO data pusher
      /// [in] idx - the zero based index of the item to push data to
      ///              ... use the ai_digitaloutput_t enumeration
      ////////////////////////////////////////////////////////////////////////// 
      void attach(DigitalOutputDataPusher *dodp, ai_digitaloutput_t idx);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   flush all the values to the real actuators
      ////////////////////////////////////////////////////////////////////////// 
      void flush(void);

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      ///   reset all the AIs.  This will call reset on each pusher
      ////////////////////////////////////////////////////////////////////////// 
      void reset(void);

   private:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Default Constructor
      //////////////////////////////////////////////////////////////////////////
      GlobalAiWpiStore(void);

   private:
      ////////////////////////////////////////////////////////////////////////// 
      /// @brief 
      /// The class's singleton instance
      ////////////////////////////////////////////////////////////////////////// 
      static GlobalAiWpiStore* msp_instance;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// All the speed controllers
      //////////////////////////////////////////////////////////////////////////
      frc::SpeedController* m_speedController[MAX_SPEED_CONTROLLER];

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// All the relays
      //////////////////////////////////////////////////////////////////////////
      frc::Relay* m_relay[MAX_RELAY+1];

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// All the relays
      //////////////////////////////////////////////////////////////////////////
      frc::DigitalOutput* m_digitalOutput[MAX_DIGITAL_OUTPUT+1];

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief 
      /// All the data pushers
      ////////////////////////////////////////////////////////////////////////// 
      std::list<AiDataPusher *> m_pushers;

   private:
      DISALLOW_COPY_CTOR(GlobalAiWpiStore);
      DISALLOW_ASSIGNMENT_OPER(GlobalAiWpiStore);

}; // END class GlobalAiWpiStore


////////////////////////////////////////////////////////////////////////////////
} // END namespace Subsystem
} // END namespace Psb


#endif // __GlobalAiWpiStore_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

