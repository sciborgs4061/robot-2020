// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <cstring>
#include <cstdio>
#include <cassert>
#include "PsbLogDataBundle.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {


// /////////////////////////////////////////////////////////////////////////////
// Static Data Init
// /////////////////////////////////////////////////////////////////////////////

// Mapping of log levels to string representations of those levels
static char sv_LogLevelNames[NUM_LOG_LEVELS][8] = 
{
   { 'A', 'L', 'W', 'A', 'Y',  'S',  '\0', '\0' },
   { 'E', 'R', 'R', 'O', 'R',  '\0', '\0', '\0' },
   { 'W', 'A', 'R', 'N', 'I',  'N',  'G',  '\0' },
   { 'N', 'O', 'T', 'I', 'C',  'E',  '\0', '\0' },
   { 'I', 'N', 'F', 'O', '\0', '\0', '\0', '\0' },
   { 'D', 'E', 'B', 'U', 'G',  '1',  '\0', '\0' },
   { 'D', 'E', 'B', 'U', 'G',  '2',  '\0', '\0' },
   { 'D', 'E', 'B', 'U', 'G',  '3',  '\0', '\0' },
};


////////////////////////////////////////////////////////////////////////////////
static inline bool
isDataTypeAndSizeSane(log_type_t data_type,
                      size_t     data_size)
{
   // Default to invalid data/size pairing
   bool is_sane = false;

   switch (data_type)
   {
      case LOG_TYPE_CHAR:
      case LOG_TYPE_BOOL:
      case LOG_TYPE_UINT8:
      case LOG_TYPE_HEX8:
      case LOG_TYPE_INT8:
      {
         // Size should be 1B
         is_sane = (1u == data_size);
      } break;

      case LOG_TYPE_UINT16:
      case LOG_TYPE_HEX16:
      case LOG_TYPE_INT16:
      {
         // Size should be 2B
         is_sane = (2u == data_size);
      } break;

      case LOG_TYPE_UINT32:
      case LOG_TYPE_HEX32:
      case LOG_TYPE_INT32:
      case LOG_TYPE_FLOAT32:
      {
         // Size should be 4B
         is_sane = (4u == data_size);
      } break;

      case LOG_TYPE_FLOAT64:
      {
         // Size should be 8B
         is_sane = (8u == data_size);
      } break;

      case LOG_TYPE_STRING:
      {
         // Size should be at least 1 byte (null-term only)
         is_sane = (data_size >= 1u);
      } break;

      case LOG_TYPE_CHAR_A:
      case LOG_TYPE_BOOL_A:
      case LOG_TYPE_UINT8_A:
      case LOG_TYPE_HEX8_A:
      case LOG_TYPE_INT8_A:
      {
         // Size should be at least 1 byte (array of 1 element only)
         is_sane = (data_size >= 1u);
      } break;

      case LOG_TYPE_UINT16_A:
      case LOG_TYPE_HEX16_A:
      case LOG_TYPE_INT16_A:
      {
         // Size should be a multiple of 2
         is_sane = (0u == (data_size & 1u));
      } break;

      case LOG_TYPE_UINT32_A:
      case LOG_TYPE_HEX32_A:
      case LOG_TYPE_INT32_A:
      case LOG_TYPE_FLOAT32_A:
      {
         // Size should be a multiple of 4
         is_sane = (0u == (data_size & 3u));
      } break;

      case LOG_TYPE_FLOAT64_A:
      {
         // Size should be a multiple of 8
         is_sane = (0u == (data_size & 7u));
      } break;

      default:
      {
         // Oops!
         assert(false);
         is_sane = false;
      } break;
   } // END switch (data_type)

   return is_sane;
}


////////////////////////////////////////////////////////////////////////////////
LogDataBundle::LogDataBundle(struct timespec timeStamp,
                             uint32_t        sequenceNumber,
                             log_level_t     logLevel,
                             char const *    pc_fileName,
                             uint32_t        lineNumber)
   : m_logDataBuffer()
   , m_dataElementLocations()
   , mp_writePtr(&(m_logDataBuffer[0]))
   , mp_logHeader(reinterpret_cast<LogDataHeader*>(mp_writePtr))
{
   // Populate the beginning of our log data with the header info
   mp_logHeader->ts_whole      = timeStamp.tv_sec;
   mp_logHeader->ts_fractional = timeStamp.tv_nsec;
   mp_logHeader->seq_num       = sequenceNumber;
   mp_logHeader->log_level     = logLevel;
   mp_logHeader->line_num      = lineNumber;
   mp_logHeader->element_count = 0;

   // Gently massage the incoming file name as it may be full-path and/or too
   // long to fit into our smaller buffer.
   std::string fname(pc_fileName);
   const size_t pos = fname.rfind('/');
   if (std::string::npos != pos)
   {
      // The string contains a slash, cut everything up to and including it
      fname.erase(0,pos+1);
   }

   // Copy only what will fit into the string buffer
   strncpy(mp_logHeader->file_name, fname.c_str(), MAX_FILENAME_LENGTH);
   mp_logHeader->file_name[MAX_FILENAME_LENGTH] = '\0';

   // Make sure to advance the write pointer appropriately
   mp_writePtr += sizeof(LogDataHeader);

   // And since we have no data yet, the element locations should be zero
   memset(m_dataElementLocations, 0x00, sizeof(m_dataElementLocations));
}


////////////////////////////////////////////////////////////////////////////////
LogDataBundle::LogDataBundle(uint8_t const* pc_data,
                             size_t         length)
   : m_logDataBuffer()
   , m_dataElementLocations()
   , mp_writePtr(&(m_logDataBuffer[0]))
   , mp_logHeader(reinterpret_cast<LogDataHeader*>(mp_writePtr))
{
   // Copy all the data that will fit
   ::memcpy(m_logDataBuffer, pc_data, (length <= sizeof(m_logDataBuffer))
                                      ? length
                                      : sizeof(m_logDataBuffer));

   // Populate the beginning of our log data with the header info

   /// @todo - ensure we don't walk off the end of the buffer!
   // Walk the incoming data buffer and find out where the data elements are
   mp_writePtr = &(m_logDataBuffer[0]) + sizeof(LogDataHeader);
   for (uint8_t idx = 0; idx < mp_logHeader->element_count; idx++)
   {
      // Note the data element location
      m_dataElementLocations[idx] = mp_writePtr;

      // The field is type(1B), size(2B), data(nB)
      mp_writePtr += 1;
      uint16_t size = *(reinterpret_cast<uint16_t*>(mp_writePtr));
      mp_writePtr += 2;
      mp_writePtr += size;
   }
}


////////////////////////////////////////////////////////////////////////////////
LogDataBundle::LogDataBundle(LogDataBundle const& rc_rhs)
   : m_logDataBuffer()
   , m_dataElementLocations()
   , mp_writePtr(&(m_logDataBuffer[0]))
   , mp_logHeader(reinterpret_cast<LogDataHeader*>(mp_writePtr))
{
   // Copy the buffer contents from the incoming object
   ::memcpy(m_logDataBuffer, rc_rhs.m_logDataBuffer, sizeof(m_logDataBuffer));

   // Walk the incoming data buffer and find out where the data elements are
   mp_writePtr = &(m_logDataBuffer[0]) + sizeof(LogDataHeader);
   for (uint8_t idx = 0; idx < mp_logHeader->element_count; idx++)
   {
      // Note the data element location
      m_dataElementLocations[idx] = mp_writePtr;

      // The field is type(1B), size(2B), data(nB)
      mp_writePtr += 1;
      uint16_t size = *(reinterpret_cast<uint16_t*>(mp_writePtr));
      mp_writePtr += 2;
      mp_writePtr += size;
   }
}


////////////////////////////////////////////////////////////////////////////////
LogDataBundle&
LogDataBundle::operator=(LogDataBundle const& rc_rhs)
{
   if (&rc_rhs != this)
   {
      // Deep copy of the buffer
      mp_writePtr  = &(m_logDataBuffer[0]);
      mp_logHeader = reinterpret_cast<LogDataHeader*>(mp_writePtr);
      ::memcpy(mp_writePtr, rc_rhs.m_logDataBuffer, sizeof(m_logDataBuffer));

      // Walk the incoming data buffer and find out where the data elements are
      mp_writePtr = &(m_logDataBuffer[0]) + sizeof(LogDataHeader);
      for (uint8_t idx = 0; idx < mp_logHeader->element_count; idx++)
      {
         // Note the data element location
         m_dataElementLocations[idx] = mp_writePtr;

         // The field is type(1B), size(2B), data(nB)
         mp_writePtr += 1;
         uint16_t size = *(reinterpret_cast<uint16_t*>(mp_writePtr));
         mp_writePtr += 2;
         mp_writePtr += size;
      }
   }

   return *this;
}


////////////////////////////////////////////////////////////////////////////////
LogDataBundle::~LogDataBundle(void)
{
   // Intentionally left empty
}


////////////////////////////////////////////////////////////////////////////////
LogDataHeader*
LogDataBundle::getLogDataHeader(void)
{
   return mp_logHeader;
}


////////////////////////////////////////////////////////////////////////////////
size_t
LogDataBundle::getLogDataHeaderSize(void) const
{
   return sizeof(LogDataHeader);
}


////////////////////////////////////////////////////////////////////////////////
uint8_t*
LogDataBundle::getDataBuffer(void)
{
   return &(m_logDataBuffer[0]);
}


////////////////////////////////////////////////////////////////////////////////
size_t
LogDataBundle::getDataBufferSize(void) const
{
   return (mp_writePtr - &(m_logDataBuffer[0]));
}


////////////////////////////////////////////////////////////////////////////////
DataElementDescriptor
LogDataBundle::getLogDataElementByIndex(size_t index)
{
   // Default to an invalid descriptor
   DataElementDescriptor desc =
   {
      NUM_LOG_TYPES,
      0u,
      nullptr
   };

   if (index < mp_logHeader->element_count)
   {
      DataElementDescriptor* p_desc =
         reinterpret_cast<DataElementDescriptor*>(m_dataElementLocations[index]);

      if (p_desc != NULL)
      {
         // Build the data to return
         desc.data_type = p_desc->data_type;
         desc.data_size = p_desc->data_size;
         desc.p_data    = reinterpret_cast<void*>(m_dataElementLocations[index] + 3u);
      }
      else
      {
         // value at the requested index is NULL
      }
   }
   else
   {
      // Invalid index requested!
   }

   return desc;
}


////////////////////////////////////////////////////////////////////////////////
bool
LogDataBundle::packData(log_type_t      data_type,
                        size_t          data_size,
                        uint8_t const*  pc_data)
{
   // Assume failure to pack data
   bool ret_val = false;

   // Note how many bytes we have left to work with
   const size_t c_unused_bytes = 
      sizeof(m_logDataBuffer) -              // Total buffer size
      (mp_writePtr - &(m_logDataBuffer[0])); // Size of used bytes

   // Note how many bytes we will use when packing this data
   const size_t c_incoming_bytes = 
      sizeof(log_type_t) +  // 1 byte for the logger type
      sizeof(uint16_t)   +  // 2 bytes for the size of this data
      data_size;            // N bytes for the incoming data

   if (c_incoming_bytes < c_unused_bytes)
   {
      // We can accommodate the new data, sanity-check the data type/size
      if (true == isDataTypeAndSizeSane(data_type, data_size))
      {
         // Note the data element location
         m_dataElementLocations[mp_logHeader->element_count] = mp_writePtr;

         // Write in 1 byte for the log data type
         *(reinterpret_cast<log_type_t*>(mp_writePtr)) = data_type;
         mp_writePtr += sizeof(log_type_t);

         // Write in 2 bytes for the data size
         *(reinterpret_cast<uint16_t*>(mp_writePtr)) =
            static_cast<uint16_t>(data_size & 0xFFFF);
         mp_writePtr += sizeof(uint16_t);

         // Write in N bytes for the incoming data
         memcpy(mp_writePtr, pc_data, data_size);
         mp_writePtr += data_size;

         // Update the element count
         mp_logHeader->element_count += 1;

         // Indicate that we were successful
         ret_val = true;
      }
      else
      {
         // Caller of packData() needs to understand the API better...
         ret_val = false;
      }
   }
   else
   {
      // No more room, we can't process the request
      ret_val = false;
   }

   return ret_val;
}


////////////////////////////////////////////////////////////////////////////////
std::string
LogDataBundle::toString(void)
{
   // This is a scratch pad for printing
   char crunched_str[127+1] = { '\0' };

   // Print the contents of the header to a string
   snprintf(crunched_str,
            sizeof(crunched_str),
            "%011u.%09u|%011u|%s|%s|%06u|%03hhu|",
            mp_logHeader->ts_whole,
            mp_logHeader->ts_fractional,
            mp_logHeader->seq_num,
            sv_LogLevelNames[static_cast<size_t>(mp_logHeader->log_level)],
            mp_logHeader->file_name,
            mp_logHeader->line_num,
            mp_logHeader->element_count);

   // Start building our return string
   std::string output = "";
   output            += crunched_str;

   // Iterate over all elements of data, and print them
   for (int idx = 0; idx < mp_logHeader->element_count; idx++)
   {
      DataElementDescriptor desc = this->getLogDataElementByIndex(idx);

      // Bail early if data integrity fails
      if (false == isDataTypeAndSizeSane(desc.data_type, desc.data_size))
      {
         snprintf(crunched_str, sizeof(crunched_str),
                  "Invalid{%u,%u},", desc.data_type, desc.data_size);
         output += crunched_str;
         continue;
      }

      // Type/Size combo is sane, process it
      switch(desc.data_type)
      {
         case LOG_TYPE_CHAR:
         {
            assert(1 == desc.data_size);
            output += *(reinterpret_cast<char*>(desc.p_data));
         } break;

         case LOG_TYPE_BOOL:
         case LOG_TYPE_UINT8:
         {
            assert(1 == desc.data_size);
            snprintf(crunched_str, sizeof(crunched_str),
                     "%hhu", *(reinterpret_cast<uint8_t*>(desc.p_data)));
            output += crunched_str;
         } break;

         case LOG_TYPE_UINT16:
         {
            assert(2 == desc.data_size);
            snprintf(crunched_str, sizeof(crunched_str),
                     "%hu", *(reinterpret_cast<uint16_t*>(desc.p_data)));
            output += crunched_str;
         } break;

         case LOG_TYPE_UINT32:
         {
            assert(4 == desc.data_size);
            snprintf(crunched_str, sizeof(crunched_str),
                     "%u", *(reinterpret_cast<uint32_t*>(desc.p_data)));
            output += crunched_str;
         } break;

         case LOG_TYPE_HEX8:
         {
            assert(1 == desc.data_size);
            snprintf(crunched_str, sizeof(crunched_str),
                     "0x%02x", *(reinterpret_cast<uint8_t*>(desc.p_data)));
            output += crunched_str;
         } break;

         case LOG_TYPE_HEX16:
         {
            assert(2 == desc.data_size);
            snprintf(crunched_str, sizeof(crunched_str),
                     "0x%04x", *(reinterpret_cast<uint16_t*>(desc.p_data)));
            output += crunched_str;
         } break;

         case LOG_TYPE_HEX32:
         {
            assert(4 == desc.data_size);
            snprintf(crunched_str, sizeof(crunched_str),
                     "0x%08x", *(reinterpret_cast<uint32_t*>(desc.p_data)));
            output += crunched_str;
         } break;

         case LOG_TYPE_INT8:
         {
            assert(1 == desc.data_size);
            snprintf(crunched_str, sizeof(crunched_str),
                     "%hhd", *(reinterpret_cast<int8_t*>(desc.p_data)));
            output += crunched_str;
         } break;

         case LOG_TYPE_INT16:
         {
            assert(2 == desc.data_size);
            snprintf(crunched_str, sizeof(crunched_str),
                     "%hd", *(reinterpret_cast<int16_t*>(desc.p_data)));
            output += crunched_str;
         } break;

         case LOG_TYPE_INT32:
         {
            assert(4 == desc.data_size);
            snprintf(crunched_str, sizeof(crunched_str),
                     "%d", *(reinterpret_cast<int32_t*>(desc.p_data)));
            output += crunched_str;
         } break;

         case LOG_TYPE_FLOAT32:
         {
            assert(4 == desc.data_size);
            snprintf(crunched_str, sizeof(crunched_str),
                     "%f", *(reinterpret_cast<float*>(desc.p_data)));
            output += crunched_str;
         } break;

         case LOG_TYPE_FLOAT64:
         {
            assert(8 == desc.data_size);
            snprintf(crunched_str, sizeof(crunched_str),
                     "%f", *(reinterpret_cast<double*>(desc.p_data)));
            output += crunched_str;
         } break;

         case LOG_TYPE_STRING:
         {
            assert(desc.data_size > 0);
            output += reinterpret_cast<char*>(desc.p_data);
         } break;

         case LOG_TYPE_CHAR_A:
         {
            output += "CHAR_A not supported";
         } break;

         case LOG_TYPE_BOOL_A:
         {
            output += "BOOL_A not supported";
         } break;

         case LOG_TYPE_UINT8_A:
         {
            /// @todo - This can likely be a macro
            uint8_t* p_data = reinterpret_cast<uint8_t*>(desc.p_data);

            output += "[";
            for (size_t idx = 0; idx < desc.data_size; idx++)
            {
               snprintf(crunched_str, sizeof(crunched_str),
                        "%03hhu,", p_data[idx]);
               output += crunched_str;
            }
            output.pop_back();
            output += "]";
         } break;

         case LOG_TYPE_UINT16_A:
         {
            output += "UINT16_A not supported";
         } break;

         case LOG_TYPE_UINT32_A:
         {
            output += "UINT32_A not supported";
         } break;

         case LOG_TYPE_HEX8_A:
         {
            /// @todo - This can likely be a macro
            uint8_t* p_data = reinterpret_cast<uint8_t*>(desc.p_data);

            output += "[";
            for (size_t idx = 0; idx < desc.data_size; idx++)
            {
               snprintf(crunched_str, sizeof(crunched_str),
                        "%02X,", p_data[idx]);
               output += crunched_str;
            }
            output.pop_back();
            output += "]";
         } break;

         case LOG_TYPE_HEX16_A:
         {
            output += "HEX16_A not supported";
         } break;

         case LOG_TYPE_HEX32_A:
         {
            output += "HEX32_A not supported";
         } break;

         case LOG_TYPE_INT8_A:
         {
            output += "INT8_A not supported";
         } break;

         case LOG_TYPE_INT16_A:
         {
            output += "INT16_A not supported";
         } break;

         case LOG_TYPE_INT32_A:
         {
            output += "INT32_A not supported";
         } break;

         case LOG_TYPE_FLOAT32_A:
         {
            output += "FLOAT32_A not supported";
         } break;

         case LOG_TYPE_FLOAT64_A:
         {
            output += "FLOAT64_A not supported";
         } break;

         default:
         {
            output += "ERROR! Unknown Type: ";
            snprintf(crunched_str, sizeof(crunched_str), "%hhu", desc.data_type);
            output += crunched_str;
         } break;
      } // END switch (*p_type)

      // Add the trailing pipe for each data type
      output += '|';
   } // END for ()

   // Strip the final pipe off the string
   output.pop_back();

   return output;
} // END toString()


} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

