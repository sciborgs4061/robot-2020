// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2017 PART Sciborgs, FRC Team 4061
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

#include "VisionProcessor.h"

#include "GripPipeline.h"

#include <cmath>
#include <chrono>
#include <thread>
#include <exception>
#include <cstring>
#include <iostream>

// POSIX includes
#include <errno.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/uio.h>

// DEBUG_FRAMES only works on the ARM
#ifndef X86_PLATFORM

// display all the rectangles that were found after filtering
#define DEBUG_FRAMES
//#define DEBUG_RECT_PRINT_DUMPS

#endif // X86_PLATFORM

#ifdef DEBUG_FRAMES
#include <cameraserver/CameraServer.h>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#endif

#include <ctype.h>
#include <cstdio>


namespace Psb {
	namespace Subsystem {
		namespace Vision {
			void OperatorControl(void) {
				grip::GripPipeline grip;
				cv::Mat frame;
				cv::VideoCapture camera;
				camera.set(CV_CAP_PROP_FRAME_WIDTH, 320);
				camera.set(CV_CAP_PROP_FRAME_HEIGHT, 240);
				camera.read(frame);
				grip.GripPipeline::Process(frame);
				}

			
			}
	}
}
