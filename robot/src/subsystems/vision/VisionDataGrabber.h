// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2017 PART Sciborgs, FRC Team 4061
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#pragma once

#include <SiDataGrabbers.h>
#include <VisionProcessor.h>

namespace Psb {
namespace Subsystem {
namespace Vision {

class VisionDataGrabber : public SiDataGrabber
{
   public:
      typedef enum {
         TOP_RIGHT_X,
         TOP_RIGHT_Y,
         //BOTTOM_LEFT_X,
         //BOTTOM_LEFT_Y,
         BOTTOM_RIGHT_Y,
         CENTER_X,
         CENTER_Y,
         DISTANCE,
		 DISTANCE_ALT,
         HEIGHT,
         HEIGHT_GAP,
         PIXELS_FROM_CENTER,
         CAMERA_ANGLE,
         ANGLE,
         NUM_RECTS,
         VISIBLE //Are two aligned rectangles visible, True/False.
      } wanted_data_t;

      typedef enum {
         DOUBLE_DATA,
         BOOL_DATA,
         INT_DATA
      } data_type_t;

      VisionDataGrabber(int *mem, wanted_data_t wanted_data);

      VisionDataGrabber(double *mem, wanted_data_t wanted_data);

      VisionDataGrabber(bool *mem, wanted_data_t wanted_data);

      void setVision(VisionProcessor *vision);

      virtual void grabData(void);

   private:
      VisionProcessor *m_vision;
      void *m_memory;
      wanted_data_t m_wanted_data;
      data_type_t m_type;

};

}
}
}
