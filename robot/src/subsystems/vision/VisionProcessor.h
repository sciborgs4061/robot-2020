// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2017 PART Sciborgs, FRC Team 4061
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#pragma once

#include <netinet/in.h>

#include <exception>
#include <sstream>
#include <thread>

#include <errno.h>
#include <opencv2/core.hpp>


namespace Psb {
namespace Subsystem {
namespace Vision {

class VisionProcessorException : public std::exception
{
        public:
                VisionProcessorException(std::string msg, int errorNum)
                        : std::exception()
                {
                        std::stringstream ss;
                        ss << msg << std::endl << strerror(errorNum);
                        ss >> m_what;
                }

                virtual const char* what() const throw()
                {
                        return m_what.c_str();
                }

        private:
                std::string m_what;
};

class VisionProcessor
{
   public:
      VisionProcessor();
      ~VisionProcessor();


      // pick the target we want to use
      typedef enum {
         HOPPER_TARGET,
         PEG_TARGET
      } target_t;

      static void switchTarget(int target);

   protected:
	void initSocket(void);
	void teardownSocket(void);

   public:
      void thread_body(void);
      int get_num_rects(void) const;
      int get_x_coord(int idx) const;
      int get_y_coord(int idx) const;
      int get_x_coord_center(int idx) const;
      int get_y_coord_center(int idx) const;
      int get_height(int idx) const;
      int get_height_gap(void) const;
      double get_pixels_from_center(void) const;
      bool check_visible(void) const;
      double angle(void) const;              //radians (left and right) from where camera is pointed
      double get_camera_angle(int idx) const;
      double get_distance(int idx) const;
	   double get_distance_alt(void) const;
      void set_camera(int camIndex) const;

   private:
      std::vector<cv::Rect> points;
      bool m_isThreadRunning;
      std::thread m_thread;
      int m_socket;
      int num_rects;
      double m_vision_config_dist = 58;  //distance the team has the robot from the target exactly, in inches, in order to find the fixed camera angle
		struct sockaddr_in m_pi;
      int m_piSocket;

      //
      // TODO: throw these in a structure and select which ones to use
      // based on m_target....
      //
      // These become specific for the hopper
      struct camera_config
      {
         int m_rect_err_tol;           //pixels the alignment test loop will allow the rectangles to be off by and still return true
         double m_minRatio;   // width/height
         double m_maxRatio;   // width/height
         double m_image_width;
         double m_image_height;
         double m_camera_altitude;   //height of camera in inches from the ground, may vary from bot to bot, so should make configurable
         double m_camera_angle;     //number of radians camera is from parellel to ground. MUST be in radians. MUST be configurable.
         double m_target_height;       //altitude of target in inches. 88 for top of tape, 84 for middle of gap
         double m_rads_total;      //total radians the camera can see (vertically).
      };
      
      camera_config m_cameraConfigs[2] = 
         {{ 10, 2.0, 9.0, 768, 432, 19.5, 0.917, 84, 0.5861}, 
          { 10, 0.1, 1.0, 768, 432, 4.0, 0.4363, 10.75, 0.5861}};

      int m_target = HOPPER_TARGET;

      // this is a big hack...but I know I only have one of these in the system right now
      static VisionProcessor *s_myself;

      //NOTE: these are compiling correctly but should be assigned all in one or the other for consistency.
};

}
}
}
