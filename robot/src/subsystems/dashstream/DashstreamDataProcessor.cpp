////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include <errno.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <unistd.h>
#include <iostream>

#include "DashstreamDataProcessor.h"

// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////
#define DASHBOARD_SENDTO_PORT_NUM 5801

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Dashstream {

////////////////////////////////////////////////////////////////////////////////
/// Constructor: DataProcessor
///
/// Description:
///  Creates a DataProcessor object.
///
/// Parameters:
///  None
////////////////////////////////////////////////////////////////////////////////
DataProcessor::DataProcessor(Controller::dataInterfaceList_t &interfaces)
   : m_dataInterfaces(interfaces)
   , m_databuf()
   , m_datasize(0)
{
   // Start with clean data
   memset(m_databuf,0,sizeof(m_databuf));
}

////////////////////////////////////////////////////////////////////////////////
void DataProcessor::packageAllData(void)
{
   char *bufptr = m_databuf;
   static bool printPODsize = true;
   for (auto subsystem: m_dataInterfaces)
   {
      memcpy(bufptr,subsystem->getData(),subsystem->dataSize());
      bufptr += subsystem->dataSize();
   }

   // how far did we move...that is the size
   m_datasize = bufptr - m_databuf;
   if (printPODsize) {
      std::cout << "POD size: " << m_datasize << std::endl;
      if (m_datasize > sizeof(m_databuf)) {
  	 std::cout << "ERROR - POD size is bigger than available buffer: " << sizeof(m_databuf) << std::endl;
      }
      printPODsize = false;
   }
   
}

////////////////////////////////////////////////////////////////////////////////
bool DataProcessor::sendPacket(int sock, struct sockaddr *client)
{
   bool retval = true;
   int send_status = sendto(sock, m_databuf, m_datasize, 0, client, sizeof(*client));

   if (-1 == send_status)
   {
       // perror("failed to send:");
       retval = false;
   }
   return retval;
}

////////////////////////////////////////////////////////////////////////////////
void DataProcessor::collectAllData(void)
{
   // DONT NEED THIS ANYMORE...because it will be handled
   // by the fact that the Global Stores will be automatically
   // updating all the data on their own

}

////////////////////////////////////////////////////////////////////////////////
/// Destructor: ~DataProcessor
///
/// Description
///  Closes the sending socket, default the logging bitmap, and forgets its
///   own instance reference.
////////////////////////////////////////////////////////////////////////////////
DataProcessor::~DataProcessor(void)
{
   // Free up resources
   // ::close(m_socketDescriptor);
   // m_socketDescriptor = -1;
   // delete m_sendToAddress;
   // m_sendToAddress = NULL;
}

} // END namespace Dashstream
} // END namespace Subsystem
} // END namespace Psb

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////
