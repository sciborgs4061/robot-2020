// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

START_NEW_SS(Collector)

   //////  OI  ///////
   
   SS_POD_JOYSTICK_BUTTON(collector_enabled, RAW_BUTTON_10, CCI_ONE,
                          "collector enabled", "CCI 1, button 10")
   SS_POD_JOYSTICK_STICK(collector_deployment_throttle, RAW_AXIS_1, JOYSTICK_ONE,
                         "deployment throttle", "Joystick 1, stick 1 on the left side")
   SS_POD_JOYSTICK_BUTTON(collector_intake, RAW_BUTTON_2, JOYSTICK_ONE,
                          "collector intake", "Joystick 1, button 2")
   

   //////  AI  ///////
   SS_POD_SPEED_CONTROLLER(collector_intake_motor, COLLECTOR_INTAKE_MOTOR,
                           "collector intake motor", "collector intake motor")
   SS_POD_SPEED_CONTROLLER(collector_deployment_motor, COLLECTOR_MOTOR,
                           "collector deployment motor", "collector deployment motor")
                     

   //////  SI  ///////
      
   //////  Publish  ///////
   SS_POD_PUBLISH_BOOL(want_loiter_position, "want conveyor in loiter position",
                           "want conveyor in loiter position", "want conveyor in loiter position")
   SS_POD_PUBLISH_BOOL(want_conveyor_in_low, "want conveyor in low speed",
                           "want conveyor in low speed", "want conveyor in low speed")
   
   //////  MI  ///////
   SS_POD_MIRROR_BOOL(loiter_position, "loiter position",
                           "loiter position", "loiter position")

   //////  CI  ///////
   SS_POD_CONFIG_DOUBLE(collector_deploy_speed, 0.5f, "collector Drive speed", "N/A")
   
   //////  SS  ///////

   //////  CONFIG  //////

   //////  LEDS  //////
  
   //////  DI  //////
   
   SS_POD_DIGITAL_INPUT(collector_retract_limit, COLLECTOR_RETRACT_LIMIT,
                        "is collector retracted", "collector completely retracted")
   SS_POD_DIGITAL_INPUT(collector_deploy_limit, COLLECTOR_DEPLOY_LIMIT,
                        "is collector deployed", "collector completely deployed")

END_SS