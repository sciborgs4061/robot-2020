// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include "GlobalOiWpiStore.h"
#include "OiDataGrabbers.h"
#include "GlobalAiWpiStore.h"
#include "AiDataPushers.h"
#include "GlobalSiWpiStore.h"
#include "SiDataGrabbers.h"
#include "GlobalCiStore.h"
#include "ConfigItem.h"
#include "frc/SerialPort.h"
#include <iostream>

// /////////////////////////////////////////////////////////////////////////////
// This is where we actually build the dashstream interface
// since this is the object that will hand it out if someone wants it
// /////////////////////////////////////////////////////////////////////////////
#define BUILD_IT
#include "outputController.h"
#undef BUILD_IT


// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Output {

////////////////////////////////////////////////////////////////////////////////
Controller::Controller(void)
   : Controller_I()
{
   // get a hold of the data so we can use it as needed
   m_pod = (DashstreamInterface::DashstreamPOD_t *)m_dashstreamIntf.getData();

   // clear out the data we are in control of
   resetData();
}

////////////////////////////////////////////////////////////////////////////////
Dashstream::BaseInterface_I *Controller::getDashstreamInterface(void)
{
   return &m_dashstreamIntf;
}

////////////////////////////////////////////////////////////////////////////////
void Controller::setEnabledState(enabled_state_t new_state)
{
}

//////////////////////////////////////////////////////////////////////////
void Controller::resetData(void)
{
   // For now just forcing everything to 0 is good enough.
   // There could be cases where the reset state is not just 0
   // but we will take that on when it happens
   memset(m_pod,0,sizeof(DashstreamInterface::DashstreamPOD_t));
}

////////////////////////////////////////////////////////////////////////////////
void Controller::init(void)
{
   // This is here in case you need to do something else...
   // like hook up things that aren't automatically hooked up
   // by the dashstream init function
   

}


////////////////////////////////////////////////////////////////////////////////
Controller::~Controller(void)
{
}

////////////////////////////////////////////////////////////////////////////////
void
Controller::performManualUpdate(void)
{
   
   computeAndApplyMotorOutputs();
     
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::performAutoUpdate(void)
{
   
   computeAndApplyMotorOutputs();
  

}


////////////////////////////////////////////////////////////////////////////////
void
Controller::performDisabledUpdate(void)
{
  
  
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::transitionToState(subsystem_state_t new_state)
{
   //keep track of the old state just in case
   subsystem_state_t prev_state __attribute__((unused)) = m_state;

   //update the base class member so the update() function calls
   //the right things
   m_state = new_state;

   ///now do whatever else it is you need to do during a transition

}

////////////////////////////////////////////////////////////////////////////////
void
Controller::computeConveyorRollers(void)
{

   // defaults
   m_pod->conveyor_top=0.0;
   m_pod->conveyor_bottom=0.0;
   

   // handle what user/subsystems want for the conveyor roller speeds
   if(m_pod->conveyor_reverse_button)
   {
      m_pod->conveyor_top = -1*m_pod->conveyor_speed_low_top_motor;
      m_pod->conveyor_bottom = -1*m_pod->conveyor_speed_low_bottom_motor;
   }
   else if (m_pod->conveyor_button || m_want_conveyor_in_high)
   {
      m_pod->conveyor_top = m_pod->conveyor_speed_high_top_motor;
      m_pod->conveyor_bottom = m_pod->conveyor_speed_high_bottom_motor;
   }
   else if (m_pod->want_conveyor_in_low)
   {
      m_pod->conveyor_top = m_pod->conveyor_speed_low_top_motor;
      m_pod->conveyor_bottom = m_pod->conveyor_speed_low_bottom_motor;

   }

}

////////////////////////////////////////////////////////////////////////////////
void
Controller::computeConveyorLoiter(void)
{
   // default
   m_pod->loiter_motor=0.0;
   m_pod->conveyor_current_state=UNKNOWN;


   if (!m_pod->loiterMotor_enabled)
   {
      return;
   }

   if(m_pod->conveyor_loiter_limit && m_pod->conveyor_pinch_limit)
   {
      m_pod->loiter_motor=0.0;
      //error!!
   }
   else if(m_pod->conveyor_loiter_limit)
   {
      m_pod->conveyor_current_state=LOITER;
   }
   else if(m_pod->conveyor_pinch_limit)
   {
      m_pod->conveyor_current_state=PINCH;
   }
   else if(m_pod->conveyor_current_state==PINCH)
   {
      m_pod->conveyor_current_state=AFTERPINCH;
   }
   else if(m_pod->conveyor_current_state==LOITER)
   {
      m_pod->conveyor_current_state=AFTERLOITER;
   }

   // if not in or right after the desired state keep going
   if (m_want_pinch_position && !(m_pod->conveyor_current_state == PINCH || m_pod->conveyor_current_state == AFTERPINCH))
   {
      m_pod->loiter_motor = m_pod->conveyor_loiter_motor_speed;
   }
   else if (m_pod->want_loiter_position && !(m_pod->conveyor_current_state == LOITER || m_pod->conveyor_current_state == AFTERLOITER))
   {
      m_pod->loiter_motor = m_pod->conveyor_loiter_motor_speed;
   }

}

////////////////////////////////////////////////////////////////////////////////
void
Controller::computeAndApplyMotorOutputs(void)
{
   
   computeConveyorLoiter();// has its own enable/disable built in


   //Default Values
   m_pod->output_motor=0.0;



   if (!m_pod->output_enabled)
   {     
      //Intentionaly left empty!
      return;
   }

   computeConveyorRollers();// do rollers only if ouput enabled
   


   if(m_pod->output_reverse_button)
   {
      m_pod->output_motor=-1*m_pod->output_slow_speed;
   }
   else if(m_pod->output_deposit > m_pod->output_deposit_control_threshold)
   {
      m_want_pinch_position=true;

      if(m_pinch_position)
      {
         m_pod->output_motor=1.0;

         if(!m_stallConveyorTimer.isRunning())
         {
            m_stallConveyorTimer.setExpiration(m_pod->conveyor_stall_wait_time);
            m_stallConveyorTimer.start();
         }
         else if(m_stallConveyorTimer.isExpired())
         {
            m_want_conveyor_in_high=true;
            m_pod->conveyor_top=m_pod->conveyor_speed_high_top_motor;
            m_pod->conveyor_bottom=m_pod->conveyor_speed_high_bottom_motor;
         }
          
      }
      
   } 
   else
   {
      m_want_conveyor_in_high=false;
      m_want_pinch_position=false;  
   }

   if(m_pod->want_conveyor_in_low && !m_want_conveyor_in_high)
   {
      if(!m_conveyorPulseOffTimer.isRunning())
      {
         m_conveyorPulseOffTimer.setExpiration(m_pod->conveyor_pulse_off_time);
         m_conveyorPulseOffTimer.start();

         m_pod->conveyor_top=0.0;
         m_pod->conveyor_bottom=0.0;
      }
      else if(m_conveyorPulseOffTimer.isExpired())
      {
         m_conveyor_pulse_off_has_happened=true;
      }
      if(m_conveyor_pulse_off_has_happened)
      {
         if(!m_conveyorPulseOnTimer.isRunning())
         {
            m_conveyorPulseOnTimer.setExpiration(m_pod->conveyor_pulse_on_time);
            m_conveyorPulseOnTimer.start();
         }
         else if(m_conveyorPulseOnTimer.isExpired())
         {
            m_pod->conveyor_top=m_pod->conveyor_speed_low_top_motor;
            m_pod->conveyor_bottom=m_pod->conveyor_speed_low_bottom_motor;
            
            m_conveyor_pulse_off_has_happened=false;
         }
      }
   }
}


} // END namespace Output
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

