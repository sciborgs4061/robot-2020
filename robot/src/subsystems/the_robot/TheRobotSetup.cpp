// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNCTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <RobotSetup.h>
#include <DrivebaseController.h>
#include <StatusLedController.h>
#include <trenchController.h>
#include <outputController.h>
#include <collectorController.h>
#include <climberController.h>
#include <shellController.h>



// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {

void AddSubsystems(Psb::Subsystem::Controller_I::subsystems_t &subsystems)
{  
   subsystems.push_back(new Psb::Subsystem::Drivebase::Controller());
   subsystems.push_back(new Psb::Subsystem::Output::Controller());
   subsystems.push_back(new Psb::Subsystem::Collector::Controller());
   subsystems.push_back(new Psb::Subsystem::Climber::Controller());
   subsystems.push_back(new Psb::Subsystem::StatusLed::Controller());
   subsystems.push_back(new Psb::Subsystem::Trench::Controller());
   subsystems.push_back(new Psb::Subsystem::Shell::Controller());
  

}

} // END namespace Psb

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

