// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

START_NEW_SS(Shell)

   //////  OI  ///////
   
   SS_POD_JOYSTICK_BUTTON(shell_enabled, RAW_BUTTON_11, CCI_ONE,
                          "shell enabled", "CCI 1, button 11")
   SS_POD_JOYSTICK_BUTTON(shell_wheel_enable, RAW_BUTTON_4, JOYSTICK_ZERO,
                          "go forward", "Joystick 0, button 4")
   SS_POD_JOYSTICK_STICK(shell_wheel_forward, RAW_AXIS_1, JOYSTICK_ZERO,
                          "drive forward","Joystick 0, left stick")

   //////  AI  ///////
   /*SS_POD_SPEED_CONTROLLER(shell_wheels_left_motor, SHELL_LEFT_WHEELS,
                           "shell wheels left motor", "shell left wheels")
   SS_POD_SPEED_CONTROLLER(shell_wheels_right_motor, SHELL_RIGHT_WHEELS,
                           "shell wheels right motor", "shell right wheels")*/

   //////  SI  ///////
      

   /*SS_POD_DIGITAL_INPUT(magnet_passing_halleffect, CLIMBER_HALLEFFECT,
                          "magnet passing halleffect", "magnet is passing halleffect sensor")*/

   //////  Publish  ///////
   

   //////  CI  ///////
   SS_POD_CONFIG_DOUBLE(shell_drive_speed, 0.5f, "shell Drive speed", "N/A")
   SS_POD_CONFIG_DOUBLE(shell_deadband, 0.5f, "shell deadband", "N/A")
   

   //////  SS  ///////
   
   //////  CONFIG  //////

   //////  LEDS    //////
  

END_SS