// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2017 PART Sciborgs, FRC Team 4061
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

START_NEW_SS(Drivebase)
   //////  OI  ///////
   // lb-switchdrivebase lt-antiturbovariable(what we have) rb-turbo rt-shoot
   SS_POD_JOYSTICK_BUTTON(drivebase_enabled, RAW_BUTTON_12, CCI_ONE,
                          "Drivebase Enabled", "CCI 1, button 12")
  
   SS_POD_JOYSTICK_BUTTON(drivebase_turbo_button, RAW_BUTTON_6, JOYSTICK_ZERO,
                    "Turbo", "Joystick 0, button 6, right shoulder")

   SS_POD_JOYSTICK_BUTTON(drivebase_switch_controls, RAW_BUTTON_5, JOYSTICK_ZERO,
                          "Switch controls", "Joystick 0, button 5, left shoulder")
   SS_POD_JOYSTICK_STICK(drivebase_custom_multiplier, RAW_AXIS_2, JOYSTICK_ZERO,
                          "Custom Multiplier", "Joystick 0, axis 2, left trigger")
  /* SS_POD_JOYSTICK_BUTTON(drivebase_anti_turbo_button, RAW_BUTTON_5, JOYSTICK_ZERO,
                          "Anti-turbo", "Joystick 1, button 5")*/
  
   SS_POD_JOYSTICK_BUTTON(drivebase_vision_toggle, RAW_BUTTON_2, CCI_ONE,
                    "Vision Direction", "CCI 1, button 1")
   SS_POD_JOYSTICK_STICK(drivebase_throttle, RAW_AXIS_1, JOYSTICK_ZERO,
                         "Throttle", "Joystick 0, stick 1 on the left side")
   SS_POD_JOYSTICK_STICK(drivebase_heading, RAW_AXIS_4, JOYSTICK_ZERO,
                         "Heading", "Joystick 0, stick 4 on the right side")
   SS_POD_JOYSTICK_BUTTON(drivebase_vision_targeting, RAW_BUTTON_8, JOYSTICK_ZERO,
                          "vision targeting enabled", "drive to target with vision steering")
   SS_POD_JOYSTICK_BUTTON(climber_wheel_enable, RAW_BUTTON_4, JOYSTICK_ZERO,
                          "go forward", "Joystick 0, button 4")
                          

   //////  AI  ///////


   // I think I can just leave this here...
   SS_POD_PUBLISH_BOOL(drivebase_drive_direction, "MI",
                    "Drive Direction", "1 == Drive forward, 0 == Drive backwards")
   SS_POD_PUBLISH_DOUBLE(vision_distance_to_target, "vision_distance_to_target",
                      "calculated distance to target", "calculated distance to target (zero if no target)")
   
   
   SS_POD_PUBLISH_DOUBLE(drivebase_left_encoder, "drivebase_left_encoder",
                      "left drivebase encoder", "left drivebase encoder")
   SS_POD_PUBLISH_DOUBLE(drivebase_right_encoder, "drivebase_right_encoder",
                      "right drivebase encoder", "right drivebase encoder")


   //////  MI  ///////   

   SS_POD_MIRROR_BOOL(want_safety_speed, "want_safety_speed",
                    "want drivebase in safety speed", "want drivebase in safety speed")

   SS_DOUBLE_POD_DATA(drivebase_left_motor_speed, "drivebase_left_motor_speed",
                      "left drivebase motor speed", "left drivebase motor speed")
   SS_DOUBLE_POD_DATA(drivebase_right_motor_speed, "drivebase_right_motor_speed",
                      "right drivebase motor speed", "right drivebase motor speed")

   //////  SI  ////////

   SS_POD_NAVX_DOUBLE(gyro_adjusted_angle, ADJUSTED_ANGLE,
                      "GYRO Angle", "The calculated adjusted angle")
   SS_POD_MIRROR_INT32(robot_state_number,"robot_state",
                       "Robot State Number", "Numeric State of the robot")
	

   ////// CONFIG //////
   SS_POD_CONFIG_DOUBLE(drivebase_normal_speed_mod, 0.6f, "Normal Speed Mod", "N/A")
   SS_POD_CONFIG_DOUBLE(drivebase_anti_turbo_speed_mod, 0.2f, "Anti-turbo Speed Mod", "N/A")
   SS_POD_CONFIG_DOUBLE(drivebase_heading_sensitivity, 1.5f, "Heading Sensitivity", "N/A")
   SS_POD_CONFIG_DOUBLE(drivebase_throttle_dead_zone, 0.05f, "Throttle Dead Zone", "N/A")
   SS_POD_CONFIG_DOUBLE(drivebase_heading_dead_zone, 0.05f, "Heading Dead Zone", "N/A")
   
   SS_POD_CONFIG_DOUBLE(drivebase_zpt_speed, 1.0f, "zpt speed", "N/A")
   SS_POD_CONFIG_DOUBLE(drivebase_turbo_speed_mod, 1.0f, "Turbo Speed Mod", "N/A")
   SS_POD_CONFIG_DOUBLE(drivebase_climber_speed, 0.2f, "Drivebase adjust Climb","N/A")
   SS_POD_CONFIG_DOUBLE(drivebase_safety_speed, 0.1f, "Drivebase Safety Speed", "N/A")

   
   SS_POD_CONFIG_DOUBLE(drivebase_encoder_read_sleep, 0.1f, "time to sleep for encoder updating", "N/A")
   

   //Vision Targeting
   SS_POD_CONFIG_DOUBLE(vision_heading_pid_min_value, -1.0f, "Vision heading PID min", "N/A")
   SS_POD_CONFIG_DOUBLE(vision_heading_pid_max_value, 1.0f, "Vision heading PID max", "N/A")
   SS_POD_CONFIG_DOUBLE(vision_heading_angle_offset, 0.0f, "Vision Angle Offset", "N/A")
   SS_POD_CONFIG_DOUBLE(vision_heading_pid_kp, 0.03f, "Vision heading PID kp", "N/A")
   SS_POD_CONFIG_DOUBLE(vision_heading_pid_innerEpsilon, 5.0f, "Vision heading PID innerEpsilon", "N/A")

   SS_POD_CONFIG_DOUBLE(vision_limelight_height_inch, 4.0f, "Vision limelight height (inch)", "N/A")
   SS_POD_CONFIG_DOUBLE(vision_target_height_inch, 5.0f, "Vision target height (inch)", "N/A")
   SS_POD_CONFIG_DOUBLE(vision_limelight_angle, 0.0f, "Vision limelight angle", "N/A")

   SS_POD_CONFIG_DOUBLE(vision_drive_forward_band, 5.0f, "Vision drive forward band", "N/A")
   SS_POD_CONFIG_DOUBLE(vision_throttle_kp, 0.5f, "Vision throttle kp", "N/A")
   SS_POD_CONFIG_DOUBLE(vision_throttle_value_default, 0.1f, "Vision throttle when no visible target", "N/A")

   SS_POD_PUBLISH_BOOL(vision_detecting_contours, "Contours_Detected", "ContoursFound", "N/A")
   SS_POD_PUBLISH_BOOL(vision_not_detecting_contours, "Contours_Not_Detected", "ContoursNotFound", "N/A")

   //Cameras
   SS_POD_CONFIG_DOUBLE(camera_forward_ip, 35.0f, "Camera forward ip", "N/A")
   SS_POD_CONFIG_DOUBLE(camera_backward_ip, 36.0f, "Camera backward ip", "N/A")
   
   // Color Sensor
	

END_SS
