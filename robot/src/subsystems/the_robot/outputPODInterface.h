// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

START_NEW_SS(Output)

   //////  OI  ///////
   
   SS_POD_JOYSTICK_BUTTON(output_enabled, RAW_BUTTON_11, CCI_ONE,
                          "output enabled", "CCI 1, button 11")
   SS_POD_JOYSTICK_BUTTON(loiterMotor_enabled, RAW_BUTTON_11, CCI_ONE,
                          "loiter motor enabled", "CCI 1, button 11")
   SS_POD_JOYSTICK_STICK(output_deposit, RAW_AXIS_3, JOYSTICK_ZERO,
                          "deposit cells", "Joystick 0, axis 3, right trigger")
   SS_POD_JOYSTICK_BUTTON(output_reverse_button, RAW_BUTTON_1, JOYSTICK_ONE,
                          "output reverse button", "Joystick 1, button 1")
   SS_POD_JOYSTICK_BUTTON(conveyor_button, RAW_BUTTON_3, JOYSTICK_ONE,
                          "conveyor button", "Joystick 1, button 3")
   SS_POD_JOYSTICK_BUTTON(conveyor_reverse_button, RAW_BUTTON_4, JOYSTICK_ONE,
                          "conveyor reverse button", "Joystick 1, button 4")

   
   //////  AI  ///////
   SS_POD_SPEED_CONTROLLER(output_motor, OUTPUT_WHEELS,
                           "output wheels", "output wheels")
   SS_POD_SPEED_CONTROLLER(conveyor_top, CONVEYOR_TOP,
                           "conveyor top", "conveyor top")
   SS_POD_SPEED_CONTROLLER(conveyor_bottom, CONVEYOR_BOTTOM,
                           "conveyor bottom", "conveyor bottom")
   SS_POD_SPEED_CONTROLLER(loiter_motor, LOITER_MOTOR,
                           "loiter motor", "loiter motor")

   //////  SI  ///////

   //////  Publish  ///////
   SS_POD_PUBLISH_BOOL(loiter_position, "loiter position",
                           "loiter position", "loiter position")
   
   //////  MI  ///////
   SS_POD_MIRROR_BOOL(want_loiter_position, "want conveyor in loiter position",
                           "want conveyor in loiter position", "want conveyor in loiter position")
   SS_POD_MIRROR_BOOL(want_conveyor_in_low, "want conveyor in low speed",
                           "want conveyor in low speed", "want conveyor in low speed")
   
   //////  CI  ///////
   
   //////  SS  ///////
   SS_INT32_POD_DATA(conveyor_current_state, "conveyor_currentstate", "state the loiter system is currently in", "")

   //////  CONFIG  //////
   SS_POD_CONFIG_DOUBLE(conveyor_speed_low_bottom_motor, 0.2,
                           "conveyor speed low bottom motor", "conveyor speed low bottom motor")
   SS_POD_CONFIG_DOUBLE(conveyor_speed_low_top_motor, 0.2,
                           "conveyor speed low top motor", "conveyor speed low top motor")
   SS_POD_CONFIG_DOUBLE(conveyor_speed_high_top_motor, 1.0,
                           "conveyor speed high top motor", "conveyor speed high top motor")
   SS_POD_CONFIG_DOUBLE(conveyor_speed_high_bottom_motor, 1.0,
                           "conveyor speed high bottom motor", "conveyor speed high bottom motor")
   SS_POD_CONFIG_DOUBLE(conveyor_stall_wait_time, 0.25,
                           "stall conveyor wait time", "stall conveyor wait time")
   SS_POD_CONFIG_DOUBLE(conveyor_pulse_on_time, 0.5,
                           "conveyor pulse on time", "conveyor pulse on time")
   SS_POD_CONFIG_DOUBLE(conveyor_pulse_off_time, 0.5,
                           "conveyor pulse off time", "conveyor pulse off time")
   SS_POD_CONFIG_DOUBLE(conveyor_loiter_motor_speed, 0.5,
                           "conveyor loiter motor speed", "conveyor loiter motor speed")
   SS_POD_CONFIG_DOUBLE(output_deposit_control_threshold, 0.2f, "deposit control Threshold","N/A")

   SS_POD_CONFIG_DOUBLE(output_slow_speed, 0.25,
                           "output slow speed", "output slow speed")

   //////  LEDS    //////

   //////  DI  ///////
   SS_POD_DIGITAL_INPUT(conveyor_pinch_limit, CONVEYOR_PINCH_LIMIT,
                        "is conveyor pinched", "conveyor completely pinched")
   SS_POD_DIGITAL_INPUT(conveyor_loiter_limit, CONVEYOR_LOITER_LIMIT,
                        "is conveyor loitering", "conveyor completely loitering")
  

END_SS