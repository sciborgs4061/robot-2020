// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include "GlobalOiWpiStore.h"
#include "OiDataGrabbers.h"
#include "GlobalAiWpiStore.h"
#include "AiDataPushers.h"
#include "GlobalSiWpiStore.h"
#include "SiDataGrabbers.h"
#include "GlobalCiStore.h"
#include "ConfigItem.h"
#include "frc/SerialPort.h"
#include <iostream>

// /////////////////////////////////////////////////////////////////////////////
// This is where we actually build the dashstream interface
// since this is the object that will hand it out if someone wants it
// /////////////////////////////////////////////////////////////////////////////
#define BUILD_IT
#include "shellController.h"
#undef BUILD_IT


// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Shell {

////////////////////////////////////////////////////////////////////////////////
Controller::Controller(void)
   : Controller_I()
{
   // get a hold of the data so we can use it as needed
   m_pod = (DashstreamInterface::DashstreamPOD_t *)m_dashstreamIntf.getData();

   // clear out the data we are in control of
   resetData();
}

////////////////////////////////////////////////////////////////////////////////
Dashstream::BaseInterface_I *Controller::getDashstreamInterface(void)
{
   return &m_dashstreamIntf;
}

////////////////////////////////////////////////////////////////////////////////
void Controller::setEnabledState(enabled_state_t new_state)
{
}

//////////////////////////////////////////////////////////////////////////
void Controller::resetData(void)
{
   // For now just forcing everything to 0 is good enough.
   // There could be cases where the reset state is not just 0
   // but we will take that on when it happens
   memset(m_pod,0,sizeof(DashstreamInterface::DashstreamPOD_t));
}

////////////////////////////////////////////////////////////////////////////////
void Controller::init(void)
{
   // This is here in case you need to do something else...
   // like hook up things that aren't automatically hooked up
   // by the dashstream init function
   

}


////////////////////////////////////////////////////////////////////////////////
Controller::~Controller(void)
{
}

////////////////////////////////////////////////////////////////////////////////
void
Controller::performManualUpdate(void)
{
   
   computeAndApplyMotorOutputs();
     
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::performAutoUpdate(void)
{
   
   computeAndApplyMotorOutputs();
  

}


////////////////////////////////////////////////////////////////////////////////
void
Controller::performDisabledUpdate(void)
{
  
  
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::transitionToState(subsystem_state_t new_state)
{
   //keep track of the old state just in case
   subsystem_state_t prev_state __attribute__((unused)) = m_state;

   //update the base class member so the update() function calls
   //the right things
   m_state = new_state;

   ///now do whatever else it is you need to do during a transition

}

////////////////////////////////////////////////////////////////////////////////
void
Controller::computeAndApplyMotorOutputs(void)
{
   
   //Default Values
   //m_pod->shell_wheels_left_motor=0.0;
   //m_pod->shell_wheels_right_motor=0.0;
   

   if(!m_pod->shell_enabled)
   {     
      //Intentionaly left empty!
      return;
   }


   /*if (m_pod->shell_wheel_enable)
   {
      if (fabs(m_pod->shell_wheel_forward) > m_pod->shell_deadband){
         m_pod->shell_wheels_left_motor = m_pod->shell_wheel_forward*m_pod->shell_drive_speed*-1;
         m_pod->shell_wheels_right_motor = m_pod->shell_wheel_forward*m_pod->shell_drive_speed*-1;
      }
   }*/ 

}


} // END namespace Shell
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

