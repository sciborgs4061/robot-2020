// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

START_NEW_SS(Trench)

   //////  OI  ///////
   
   SS_POD_JOYSTICK_BUTTON(trench_enabled, RAW_BUTTON_9, CCI_ONE,
                          "trench enabled", "CCI 1, button 9")
   /*SS_POD_JOYSTICK_BUTTON(trench_easyButton, RAW_BUTTON_11, JOYSTICK_TWO,
                          "Drive to control panel", "Joystick 2, button 11")*/
   SS_POD_JOYSTICK_BUTTON(control_panel_raise, RAW_BUTTON_5, CCI_ONE,
                          "Fully raise control panel mechanism", "CCI 1, button 5")
   SS_POD_JOYSTICK_BUTTON(control_panel_lower, RAW_BUTTON_6, CCI_ONE,
                          "Fully lower control panel mechanism", "CCI 1, button 6")
   SS_POD_JOYSTICK_BUTTON(control_panel_stage_two, RAW_BUTTON_3, CCI_ONE,
                          "Manipulates for stage 2", "CCI 1, button 3")
   SS_POD_JOYSTICK_BUTTON(control_panel_stage_three, RAW_BUTTON_4, CCI_ONE,
                          "Manipulates for stage 3", "CCI 1, button 4")

   //////  AI  ///////
   SS_POD_SPEED_CONTROLLER(trench_deploy_motors, TRENCH_DEPLOY_MOTORS,
                           "trench deploy motors", "trench deploy motors")
   SS_POD_SPEED_CONTROLLER(trench_spin_motors, TRENCH_SPIN_MOTORS,
                           "trench spin motors", "trench spin motors")

   //////  SI  ///////
   SS_POD_NAVX_DOUBLE(displacement, DISPLACEMENT_Y,
                           "displacement", "The verticle distance the NavX has traveled")   
   
   // we only need one of these but we don't know which yet so everything!
   SS_POD_NAVX_DOUBLE(yaw, YAW,
                           "yaw", "Navx yaw")
   SS_POD_NAVX_DOUBLE(pitch, PITCH,
                           "pitch", "Navx pitch")
   SS_POD_NAVX_DOUBLE(roll, ROLL,
                           "roll", "Navx roll")

   //////  SS  ///////
   SS_DOUBLE_POD_DATA(distance_left_front, "SS", "Left Front Distance Sensor", "")
   SS_DOUBLE_POD_DATA(distance_left_rear, "SS", "Left Rear Distance Sensor", "")
   // SS_DOUBLE_POD_DATA(distance_right_front, "SS", "Right Front Distance Sensor", "")
   // SS_DOUBLE_POD_DATA(distance_right_rear, "SS", "Right Rear Distance Sensor", "")

   SS_DOUBLE_POD_DATA(color_front_red, "SS", "Front Color Sensor Red", "")
   SS_DOUBLE_POD_DATA(color_front_green, "SS", "Front Color Sensor Green", "")
   SS_DOUBLE_POD_DATA(color_front_blue, "SS", "Front Color Sensor Blue", "")
   SS_DOUBLE_POD_DATA(color_rear_red, "SS", "Rear Color Sensor Red", "")
   SS_DOUBLE_POD_DATA(color_rear_green, "SS", "Rear Color Sensor Green", "")
   SS_DOUBLE_POD_DATA(color_rear_blue, "SS", "Rear Color Sensor Blue", "")
   

   //////  Publish  ///////
   SS_POD_PUBLISH_BOOL(want_safety_speed, "want_safety_speed",
                    "want drivebase in safety speed", "want drivebase in safety speed")

   //////  DI  //////
   SS_POD_DIGITAL_INPUT(control_panel_upper_limit, CONTROL_PANEL_UPPER_LIMIT, "cp upper limit", "control panel upper limit")
   SS_POD_DIGITAL_INPUT(control_panel_lower_limit, CONTROL_PANEL_LOWER_LIMIT, "cp lower limit", "control panel lower limit")
   
   //////  CONFIG  //////
   SS_POD_CONFIG_DOUBLE(panel_spin_speed, 0.75f, "panel spin speed", "N/A")
   SS_POD_CONFIG_DOUBLE(trench_color_threshold, 0.75f, "Color threshold", "N/A")
   SS_POD_CONFIG_DOUBLE(control_panel_stage_two_rotation_time, 0.25,
                           "control panel stage two rotation time", "control panel stage two rotation time")

   //////  LEDS    //////
  

END_SS



