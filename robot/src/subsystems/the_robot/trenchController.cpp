// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include "GlobalOiWpiStore.h"
#include "OiDataGrabbers.h"
#include "GlobalAiWpiStore.h"
#include "AiDataPushers.h"
#include "GlobalSiWpiStore.h"
#include "SiDataGrabbers.h"
#include "GlobalCiStore.h"
#include "ConfigItem.h"
#include "frc/SerialPort.h"
#include "frc/I2C.h"
#include "PsbRev2mDistanceSensor.h"
#include "PsbI2CDistanceSensor.h"
#include "PsbI2CColorSensor.h"
#include "I2CBreakout.h"
#include <iostream>
#include <cmath>
#include <frc/DriverStation.h>

// /////////////////////////////////////////////////////////////////////////////
// This is where we actually build the dashstream interface
// since this is the object that will hand it out if someone wants it
// /////////////////////////////////////////////////////////////////////////////
#define BUILD_IT
#include "trenchController.h"
#undef BUILD_IT


// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Trench {

////////////////////////////////////////////////////////////////////////////////
Psb::I2C::DistanceSensor* makeDistanceSensor(Psb::I2CBreakout* breakout, int channel)
{
   Psb::I2C::DistanceSensor *ds = new Psb::I2C::DistanceSensor(Psb::Rev2mDistanceSensor::Port::kOnboard,
                                                               Psb::Rev2mDistanceSensor::DistanceUnit::kMilliMeters,
                                                               Psb::Rev2mDistanceSensor::RangeProfile::kDefault,
                                                               *breakout, channel);
   if (ds->StatusIsFatal()) { ds = NULL; }
   if (ds) {std::cout << "Created distance sensor on channel " << channel << std::endl;}
   return ds;
}

////////////////////////////////////////////////////////////////////////////////
Psb::I2C::ColorSensor* makeColorSensor(Psb::I2CBreakout* breakout, int channel)
{
   Psb::I2C::ColorSensor *cs = new Psb::I2C::ColorSensor(frc::I2C::Port::kOnboard, *breakout, channel);
   if (cs->StatusIsFatal()) { cs = NULL; }
   if (cs) {std::cout << "Created color sensor on channel " << channel << std::endl;}
   return cs;
}

////////////////////////////////////////////////////////////////////////////////
Controller::Controller(void)
   : Controller_I()
{
   // get a hold of the data so we can use it as needed
   // TBD: get sensor channels info from robot config, not hardcoded
   m_pod = (DashstreamInterface::DashstreamPOD_t *)m_dashstreamIntf.getData();
   m_i2cBreakout = new Psb::I2CBreakout(frc::I2C::Port::kOnboard);
   m_distanceSensor_LF = makeDistanceSensor(m_i2cBreakout, 2);
   m_distanceSensor_LR = makeDistanceSensor(m_i2cBreakout, 3);
   // m_distanceSensor_RF = makeDistanceSensor(4);
   // m_distanceSensor_RR = makeDistanceSensor(5);
   m_colorSensor_Front = makeColorSensor(m_i2cBreakout, 0);
   m_colorSensor_Rear = makeColorSensor(m_i2cBreakout, 1);
    
   // clear out the data we are in control of
   resetData();
}

////////////////////////////////////////////////////////////////////////////////
Dashstream::BaseInterface_I *Controller::getDashstreamInterface(void)
{
   return &m_dashstreamIntf;
}

////////////////////////////////////////////////////////////////////////////////
void Controller::setEnabledState(enabled_state_t new_state)
{
}

//////////////////////////////////////////////////////////////////////////
void Controller::resetData(void)
{
   // For now just forcing everything to 0 is good enough.
   // There could be cases where the reset state is not just 0
   // but we will take that on when it happens
   memset(m_pod,0,sizeof(DashstreamInterface::DashstreamPOD_t));
}

////////////////////////////////////////////////////////////////////////////////
void Controller::init(void)
{
   // This is here in case you need to do something else...
   // like hook up things that aren't automatically hooked up
   // by the dashstream init function
   

}


////////////////////////////////////////////////////////////////////////////////
Controller::~Controller(void)
{
}

////////////////////////////////////////////////////////////////////////////////
double
Controller::calculateLength(double red, double green, double blue)
{
   return red*red + green*green + blue*blue;
}

////////////////////////////////////////////////////////////////////////////////
double
Controller::normalize(double length, double raw)
{
   return raw*raw / length;
}

////////////////////////////////////////////////////////////////////////////////
double
Controller::distance(double x_one, double y_one, double z_one, double x_two, double y_two, double z_two)
{
   return sqrt( pow((x_one - x_two), 2) + pow((y_one - y_two), 2) + pow((z_one - z_two), 2));
}

////////////////////////////////////////////////////////////////////////////////
void
Controller::performManualUpdate(void)
{
  // updateDistanceSensors();
  // updateColorSensors();
   computeAndApplyMotorOutputs();

  /* double length_front = calculateLength(m_pod->red_front, m_pod->green_front, m_pod->blue_front);
   double red_front_prime = normalize(length_front, m_pod->red_front);
   double green_front_prime = normalize(length_front, m_pod->green_front);
   double blue_front_prime = normalize(length_front, m_pod->blue_front);
*/


   //double length_back = calculateLength(m_pod->red_back, m_pod->green_back, m_pod->blue_back);
   //double red_back_prime = normalize(length_back, m_pod->red_back);
   //double green_back_prime = normalize(length_back, m_pod->green_back);
   //double blue_front_prime = normalize(length_back, m_pod->blue_back);

   //std::cout << m_pod->red_front << std::endl;
   //std::cout << m_pod->green_front << std::endl;
   //std::cout << m_pod->blue_front << std::endl;

   //std::cout << red_front_prime << "red front" << std::endl;
   //std::cout << green_front_prime << "blue front" << std::endl;
   //std::cout << blue_front_prime <<"green front" << std::endl;
   
   //std::cout << red_back_prime << "red back" << std::endl;
   //std::cout << blue_back_prime << "blue back" << std::endl;
   //std::cout << green_back_prime << "green back" << std::endl;
   
     
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::performAutoUpdate(void)
{
  // updateDistanceSensors();
  // updateColorSensors();
   computeAndApplyMotorOutputs();
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::performDisabledUpdate(void)
{
 //  updateDistanceSensors();
  // updateColorSensors();
}

////////////////////////////////////////////////////////////////////////////////
void
Controller::updateColorSensors(void)
{
  rev::ColorSensorV3::RawColor detectedColor = m_colorSensor_Front->GetRawColor();
  m_pod->color_front_red = detectedColor.red;
  m_pod->color_front_green = detectedColor.green;
  m_pod->color_front_blue = detectedColor.blue;

  detectedColor = m_colorSensor_Rear->GetRawColor();
  m_pod->color_rear_red = detectedColor.red;
  m_pod->color_rear_green = detectedColor.green;
  m_pod->color_rear_blue = detectedColor.blue;
}

////////////////////////////////////////////////////////////////////////////////                   
// This code runs a vl53l0x distance sensor interfaced through the I2C multiplexer
// using the PSB library.
void
Controller::updateDistanceSensors(void)
{
   if (m_distanceSensor_LF)
   {
      uint32_t distance = m_distanceSensor_LF->GetRange();
      bool isValid = m_distanceSensor_LF->IsRangeValid();
      if (isValid) {
         m_pod->distance_left_front = distance;
      }
   }
   if (m_distanceSensor_LR)
   {
      uint32_t distance = m_distanceSensor_LR->GetRange();
      bool isValid = m_distanceSensor_LR->IsRangeValid();
      if (isValid) {
         m_pod->distance_left_rear = distance;
      }
   }
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::transitionToState(subsystem_state_t new_state)
{
   
   //keep track of the old state just in case
   subsystem_state_t prev_state __attribute__((unused)) = m_state;

   //update the base class member so the update() function calls
   //the right things
   m_state = new_state;

   ///now do whatever else it is you need to do during a transition
   
}

/*
////////////////////////////////////////////////////////////////////////////////
void
Controller::driveToControlPanel(void)
{
  
}
*/

////////////////////////////////////////////////////////////////////////////////
bool 
Controller::isSameColor(double a, double b)
{
   return (a <= b + m_pod->trench_color_threshold && a >= b - m_pod->trench_color_threshold);
}

////////////////////////////////////////////////////////////////////////////////
void
Controller::computeAndApplyMotorOutputs(void)
{
   
   m_pod->trench_deploy_motors=0.0;
   m_pod->trench_spin_motors=0.0;
   m_pod->want_safety_speed=false;

   if(m_pod->control_panel_upper_limit)
   {
      m_pod->want_safety_speed=true;
   }

   if(!m_pod->trench_enabled)
   {
      return;
   }

   if(m_pod->control_panel_raise)
   {
      if(!m_pod->control_panel_upper_limit)
      {
         m_pod->trench_deploy_motors=1;
      }
   }
   
   if(m_pod->control_panel_lower)
   {
      if(!m_pod->control_panel_lower_limit)
      {
         m_pod->trench_deploy_motors=1;
      }
   }

   

   
   std::string gameData;
   gameData = frc::DriverStation::GetInstance().GetGameSpecificMessage();

   // if just hit button once toggle activated state
   if (m_pod->control_panel_stage_two /*&& !m_previousControlPanelStageTwo*/)
   {

      m_pod->trench_spin_motors=m_pod->panel_spin_speed;

      /*
      // if we were not activated before activate
      if (!m_panelStageTwoActivated)
      {

         m_panelStageTwoActivated = true;

         // store current color
         m_frontRedStageTwo = m_pod->color_front_red;
         m_frontGreenStageTwo = m_pod->color_front_green;
         m_frontBlueStageTwo = m_pod->color_front_blue;
         m_rearRedStageTwo = m_pod->color_rear_red;
         m_rearGreenStageTwo = m_pod->color_rear_green;
         m_rearBlueStageTwo = m_pod->color_rear_blue;
      }
      else // we were already activated so de-activate
      {
         // button can be pressed again to cancel
         m_pod->trench_spin_motors=0;
         m_panelStageTwoActivated = false;
         m_stageTwoFrontCounter = 0;
         m_stageTwoRearCounter = 0;
      }
      */
   }
  

   /*if (m_panelStageTwoActivated)
   {
      m_pod->trench_spin_motors=m_pod->panel_spin_speed;

      if(m_stageTwoTimer.isExpired())
      {
         // if we've hit the same color as we started then we've gone half a rotation
         if (isSameColor(m_frontRedStageTwo, m_pod->color_front_red) &&
            isSameColor(m_frontGreenStageTwo, m_pod->color_front_green) &&
            isSameColor(m_frontBlueStageTwo, m_pod->color_front_blue))      
         {
            m_stageTwoFrontCounter++;
         }

         // if we've hit the same color as we started then we've gone half a rotation
         if (isSameColor(m_rearRedStageTwo, m_pod->color_rear_red) &&
            isSameColor(m_rearGreenStageTwo, m_pod->color_rear_green) &&
            isSameColor(m_rearBlueStageTwo, m_pod->color_rear_blue))        
         {
            m_stageTwoRearCounter++;
         }


         m_stageTwoTimer.start();// restart timer for next check
      }
      else if (!m_stageTwoTimer.isRunning())
      {
         m_stageTwoTimer.setExpiration(m_pod->control_panel_stage_two_rotation_time);
         m_stageTwoTimer.start();
      }

      // cancels when we've gone 7 half rotations (aka 3.5 full rotations)
      if (m_stageTwoFrontCounter >= 7 || m_stageTwoRearCounter >= 7) 
      { 

         m_stageTwoTimer.stop();
         m_stageTwoTimer.reset();

         m_pod->trench_spin_motors = 0;
         m_panelStageTwoActivated = false;
         m_stageTwoFrontCounter = 0;
         m_stageTwoRearCounter = 0;
      }
   }*/

   /*
   if(m_pod->control_panel_stage_three)
   {
      //figure out what color we are looking at

      switch(gameData[0])
      {
         case 'R':
         {
            
         }
         case 'B':
         {

         }
         case 'G':
         {

         }
         case 'Y':
         {

         }
         default:
         {

         }
      }
   }
   */

   m_previousControlPanelStageTwo = m_pod->control_panel_stage_two;
}





} // END namespace Trench
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

