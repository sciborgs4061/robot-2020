
#include "StatusLedController.h"
#include "StatusLedTypes.h"
#include "StatusLedEvents.h"
#include "StatusLedIndicators.h"

using namespace Psb::Subsystem::StatusLed;

////////////////////////////////////////////////////////////////////////////////
void setupRobotLEDEvents(std::list<Psb::Subsystem::StatusLed::Controller::eil_mapping_t> &eventTable,
                         LedStrip &leds,
                         PixelReservationManager &manager)
{

   pixel_set_t all_pixels = 
   {
      PIXEL_ID_00, PIXEL_ID_01, PIXEL_ID_02, PIXEL_ID_03,
      PIXEL_ID_04, PIXEL_ID_05, PIXEL_ID_06, PIXEL_ID_07,
      PIXEL_ID_08, PIXEL_ID_09, PIXEL_ID_10, PIXEL_ID_11,
      PIXEL_ID_12, PIXEL_ID_13, PIXEL_ID_14, PIXEL_ID_15,
      PIXEL_ID_16, PIXEL_ID_17, PIXEL_ID_18, PIXEL_ID_19,
      PIXEL_ID_20, PIXEL_ID_21, PIXEL_ID_22, PIXEL_ID_23,
      PIXEL_ID_24, PIXEL_ID_25, PIXEL_ID_26, PIXEL_ID_27,
      PIXEL_ID_28, PIXEL_ID_29, PIXEL_ID_30, PIXEL_ID_31,
      PIXEL_ID_32, PIXEL_ID_33, PIXEL_ID_34, PIXEL_ID_35,
      PIXEL_ID_36, PIXEL_ID_37, PIXEL_ID_38, PIXEL_ID_39,
      PIXEL_ID_40, PIXEL_ID_41, PIXEL_ID_42, PIXEL_ID_43
   };
   pixel_set_t first_half = 
   {
       PIXEL_ID_00, PIXEL_ID_01, PIXEL_ID_02, PIXEL_ID_03
      ,PIXEL_ID_04, PIXEL_ID_05, PIXEL_ID_06, PIXEL_ID_07
      ,PIXEL_ID_08, PIXEL_ID_09, PIXEL_ID_10, PIXEL_ID_11
      ,PIXEL_ID_12, PIXEL_ID_13, PIXEL_ID_14, PIXEL_ID_15
      ,PIXEL_ID_16, PIXEL_ID_17, PIXEL_ID_18, PIXEL_ID_19
      ,PIXEL_ID_20, PIXEL_ID_21
   };
   pixel_set_t second_half = 
   {
       PIXEL_ID_22, PIXEL_ID_23, PIXEL_ID_24, PIXEL_ID_25
      ,PIXEL_ID_26, PIXEL_ID_27, PIXEL_ID_28, PIXEL_ID_29
      ,PIXEL_ID_30, PIXEL_ID_31, PIXEL_ID_32, PIXEL_ID_33
      ,PIXEL_ID_34, PIXEL_ID_35, PIXEL_ID_36, PIXEL_ID_37
      ,PIXEL_ID_38, PIXEL_ID_39, PIXEL_ID_40, PIXEL_ID_41
      ,PIXEL_ID_42, PIXEL_ID_43
   };

   pixel_set_t first_quarter = 
   {
       PIXEL_ID_00, PIXEL_ID_01, PIXEL_ID_02, PIXEL_ID_03
      ,PIXEL_ID_04, PIXEL_ID_05, PIXEL_ID_06, PIXEL_ID_07
      ,PIXEL_ID_08, PIXEL_ID_09, PIXEL_ID_10, PIXEL_ID_11
   };
   pixel_set_t second_quarter = 
   {
       PIXEL_ID_12, PIXEL_ID_13, PIXEL_ID_14, PIXEL_ID_15
      ,PIXEL_ID_16, PIXEL_ID_17, PIXEL_ID_18, PIXEL_ID_19
      ,PIXEL_ID_20, PIXEL_ID_21
   };
   pixel_set_t third_quarter = 
   {
      
   
      PIXEL_ID_31, PIXEL_ID_30, PIXEL_ID_29, PIXEL_ID_28, PIXEL_ID_27,
      PIXEL_ID_26, PIXEL_ID_25, PIXEL_ID_24, PIXEL_ID_23, PIXEL_ID_22

   };
   pixel_set_t fourth_quarter = 
   {

      PIXEL_ID_43, PIXEL_ID_42, PIXEL_ID_41, PIXEL_ID_40, PIXEL_ID_39, 
      PIXEL_ID_38, PIXEL_ID_37, PIXEL_ID_36, PIXEL_ID_35, PIXEL_ID_34,
      PIXEL_ID_33, PIXEL_ID_32
   };
   pixel_set_t target_leds =
   {
       PIXEL_ID_08, PIXEL_ID_09, PIXEL_ID_10, PIXEL_ID_11
      ,PIXEL_ID_12, PIXEL_ID_13, PIXEL_ID_14, PIXEL_ID_15
      ,PIXEL_ID_24, PIXEL_ID_25, PIXEL_ID_26, PIXEL_ID_27
      ,PIXEL_ID_28, PIXEL_ID_29, PIXEL_ID_30, PIXEL_ID_31

   };
   pixel_set_t first_third =
   {
      PIXEL_ID_00, PIXEL_ID_01, PIXEL_ID_02, PIXEL_ID_03,
      PIXEL_ID_04, PIXEL_ID_05, PIXEL_ID_06, PIXEL_ID_07,
      PIXEL_ID_08, PIXEL_ID_09

   };
   pixel_set_t second_third =
   {
      PIXEL_ID_10, PIXEL_ID_11,
      PIXEL_ID_12, PIXEL_ID_13, PIXEL_ID_14, PIXEL_ID_15,
      PIXEL_ID_16, PIXEL_ID_17, PIXEL_ID_18, PIXEL_ID_19,
   };
   pixel_set_t third_third =
   {
      PIXEL_ID_20, PIXEL_ID_21, PIXEL_ID_22, PIXEL_ID_23,
      PIXEL_ID_24, PIXEL_ID_25, PIXEL_ID_26, PIXEL_ID_27,
      PIXEL_ID_28, PIXEL_ID_29, PIXEL_ID_30, PIXEL_ID_31
   };
   pixel_set_t right_light_leds = 
   {
     PIXEL_ID_00, PIXEL_ID_01, PIXEL_ID_02, PIXEL_ID_03
   };
   pixel_set_t right_ramp_leds =
   {
     PIXEL_ID_11, PIXEL_ID_10, PIXEL_ID_09, PIXEL_ID_08,
     PIXEL_ID_07, PIXEL_ID_06, PIXEL_ID_05, PIXEL_ID_04
   };
   pixel_set_t center_ramp_leds =
   {
     PIXEL_ID_12, PIXEL_ID_13, PIXEL_ID_14, PIXEL_ID_15,
     PIXEL_ID_16, PIXEL_ID_17, PIXEL_ID_18, PIXEL_ID_19
   };
   pixel_set_t left_ramp_leds =
   {
     PIXEL_ID_27, PIXEL_ID_26, PIXEL_ID_25, PIXEL_ID_24,
     PIXEL_ID_23, PIXEL_ID_22, PIXEL_ID_21, PIXEL_ID_20
   };
   pixel_set_t left_light_leds =
   {
     PIXEL_ID_28, PIXEL_ID_29, PIXEL_ID_30, PIXEL_ID_31
   };

   // //////////////////////////////////////////////////////////////////////////
   // Build our event list...
   // Events at the front of the list have the highest priority.
   // //////////////////////////////////////////////////////////////////////////

   ///////////////////////////////////////////////////////
   // Test Mode

   ///////////////////////////////////////////////////////
   // Limelight Events
   
   std::list<rgb_t> ButtonPressed;
   ButtonPressed.push_back({0x00,0x7f,0x00});
   //std::list<rgb_t> NoTarget;
   //NoTarget.push_back({0x00,0x38,0x14});
   eventTable.push_back(
      { new MirroredBooleanEvent("Contours_Detected"),
         { new PingPongIndicator(leds,
                                 manager,
                                 first_quarter,
                                 ButtonPressed) }}
   );
   eventTable.push_back(
      { new MirroredBooleanEvent("Contours_Detected"),
         { new PingPongIndicator(leds,
                                 manager,
                                 second_quarter,
                                 ButtonPressed) }}
   );
   eventTable.push_back(
      { new MirroredBooleanEvent("Contours_Detected"),
         { new PingPongIndicator(leds,
                                 manager,
                                 third_quarter,
                                 ButtonPressed) }}
   );
   eventTable.push_back(
      { new MirroredBooleanEvent("Contours_Detected"),
         { new PingPongIndicator(leds,
                                 manager,
                                 fourth_quarter,
                                 ButtonPressed) }}
   );
   eventTable.push_back(
      { new MirroredBooleanEvent("Contours_Not_Detected"),
         { new BlinkIndicator(leds,
                              manager,
                              first_quarter,
                              0.160,
                              2,
                              0x00, 0x38, 0x14) }}
   );
   eventTable.push_back(
      { new MirroredBooleanEvent("Contours_Not_Detected"),
         { new BlinkIndicator(leds,
                              manager,
                              second_quarter,
                              0.160,
                              2,
                              0x00, 0x38, 0x14) }}
   );
   eventTable.push_back(
      { new MirroredBooleanEvent("Contours_Not_Detected"),
         { new BlinkIndicator(leds,
                              manager,
                              third_quarter,
                              0.160,
                              2,
                              0x00, 0x38, 0x14) }}
   );
   eventTable.push_back(
      { new MirroredBooleanEvent("Contours_Not_Detected"),
         { new BlinkIndicator(leds,
                              manager,
                              fourth_quarter,
                              0.160,
                              2,
                              0x00, 0x38, 0x14) }}
   );

   ///////////////////////////////////////////////////////////////////////
   // Limit Switches

   /*eventTable.push_back(
      { new LimitSwitchEvent(Psb::Subsystem::GlobalSiWpiStore::TILT_LIMIT_SWITCH,true),
         { new BlinkIndicator(leds,
                              manager,
                              first_quarter,
                              0.160,
                              2,
                              0x3E, 0x00, 0x0C) }}
   );*/
   
   
   /*eventTable.push_back(
      { new LimitSwitchEvent(Psb::Subsystem::GlobalSiWpiStore::STATUSLED_TESTMODE_SWITCH),
         { new DigitalInputsIndicator(leds,
                                      manager,
                                      second_third,
                                      DigitalInputsIndicator::POS_BEGIN,
                                      {0x00, 0x7f, 0x00},
                                      {0x7f, 0x00, 0x00}) }}
   );*/

   /*eventTable.push_back(
      { new LimitSwitchEvent(Psb::Subsystem::GlobalSiWpiStore::STATUSLED_TESTMODE_SWITCH),
         { new PotIndicator(leds,
                                      manager,
                                      right_ramp_leds,
                                      PotIndicator::POS_BEGIN,
                                      {0x00, 0x7f, 0x00},
                                      {0x7f, 0x00, 0x00}) }}
   );*/

   ///////////////////////////////////////////////////////
   // Bad XML 
   std::list<rgb_t> bad_xml_colors;
   bad_xml_colors.push_back({0x7f,0x00,0x7f});
   bad_xml_colors.push_back({0x7f,0x00,0x7f});
   bad_xml_colors.push_back({0x7f,0x00,0x7f});
   bad_xml_colors.push_back({0x7f,0x00,0x7f});
   eventTable.push_back(
      { new MirroredBooleanEvent("bad_auto_xml"),
         { new PingPongIndicator(leds,
                                 manager,
                                 first_quarter,
                                 bad_xml_colors) }}
   );
   eventTable.push_back(
      { new MirroredBooleanEvent("bad_auto_xml"),
         { new PingPongIndicator(leds,
                                 manager,
                                 second_quarter,
                                 bad_xml_colors) }}
   );
   eventTable.push_back(
      { new MirroredBooleanEvent("bad_auto_xml"),
         { new PingPongIndicator(leds,
                                 manager,
                                 third_quarter,
                                 bad_xml_colors) }}
   );
   eventTable.push_back(
      { new MirroredBooleanEvent("bad_auto_xml"),
         { new PingPongIndicator(leds,
                                 manager,
                                 fourth_quarter,
                                 bad_xml_colors) }}
   );

   ///////////////////////////////////////////////////////
   // Driver Station attach
   // colors for DS Attach indication
   std::list<rgb_t> ds_attach_colors;
   ds_attach_colors.push_back({0x7F,0x7f,0x7f});
   ds_attach_colors.push_back({0x7F,0x7f,0x7f});
   eventTable.push_back(
      { new DsDetachedEvent(),
         { new PingPongIndicator(leds,
                                 manager,
                                 first_quarter,
                                 ds_attach_colors) }}
   );
   eventTable.push_back(
      { new DsDetachedEvent(),
         { new PingPongIndicator(leds,
                                 manager,
                                 second_quarter,
                                 ds_attach_colors) }}
   );
   eventTable.push_back(
      { new DsDetachedEvent(),
         { new PingPongIndicator(leds,
                                 manager,
                                 third_quarter,
                                 ds_attach_colors) }}
   );
   eventTable.push_back(
      { new DsDetachedEvent(),
         { new PingPongIndicator(leds,
                                 manager,
                                 fourth_quarter,
                                 ds_attach_colors) }}
   );

   ///////////////////////////////////////////////////////
   // Autonomous color set...it is kinda pink

   // eventTable.push_back(
      // { new RobotStateEvent(1),
         // { new SolidColorIndicator(leds,
                                   // manager,
                                   // all_pixels,
                                   // 0xFF, 0x14, 0x93) }}
   // );

   ///////////////////////////////////////////////////////
   // Game Events

   ///////////////////////////////////////////////////////
   // Alliance color selection
   //   NOTE: These should always be last
   std::list<rgb_t> blue_alliance_colors;
   blue_alliance_colors.push_back({0x00,0x00,0x7f});
   blue_alliance_colors.push_back({0x00,0x00,0x7f});
   eventTable.push_back(
      { new BlueAllianceEvent(),
         { new PingPongIndicator(leds,
                                 manager,
                                 first_quarter,
                                 blue_alliance_colors) }}
   );
   eventTable.push_back(
      { new BlueAllianceEvent(),
         { new PingPongIndicator(leds,
                                 manager,
                                 second_quarter,
                                 blue_alliance_colors) }}
   );
   eventTable.push_back(
      { new BlueAllianceEvent(),
         { new PingPongIndicator(leds,
                                 manager,
                                 third_quarter,
                                 blue_alliance_colors) }}
   );
   eventTable.push_back(
      { new BlueAllianceEvent(),
         { new PingPongIndicator(leds,
                                 manager,
                                 fourth_quarter,
                                 blue_alliance_colors) }}
   );

   std::list<rgb_t> red_alliance_colors;
   red_alliance_colors.push_back({0x7f,0x00,0x00});
   eventTable.push_back(
      { new RedAllianceEvent(),
         { new PingPongIndicator(leds,
                                 manager,
                                 first_quarter,
                                 red_alliance_colors) }}
   );
   eventTable.push_back(
      { new RedAllianceEvent(),
         { new PingPongIndicator(leds,
                                 manager,
                                 second_quarter,
                                 red_alliance_colors) }}
   );
   eventTable.push_back(
      { new RedAllianceEvent(),
         { new PingPongIndicator(leds,
                                 manager,
                                 third_quarter,
                                 red_alliance_colors) }}
   );
   eventTable.push_back(
      { new RedAllianceEvent(),
         { new PingPongIndicator(leds,
                                 manager,
                                 fourth_quarter,
                                 red_alliance_colors) }}
   );
}
