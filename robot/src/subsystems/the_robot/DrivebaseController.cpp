// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include "GlobalOiWpiStore.h"
#include "OiDataGrabbers.h"
#include "GlobalAiWpiStore.h"
#include "AiDataPushers.h"
#include "GlobalSiWpiStore.h"
#include "SiDataGrabbers.h"
#include "GlobalCiStore.h"
#include "ConfigItem.h"
#include <networktables/NetworkTableInstance.h>
#include <opencv2/core/core.hpp>

#include <frc/smartdashboard/SmartDashboard.h>
#include <string>
#include <cscore_oo.h>
#include <frc/shuffleboard/Shuffleboard.h>
#include <frc/shuffleboard/BuiltInWidgets.h>
#include <cameraserver/CameraServer.h>
#include <iostream>
#include "PsbNumberManipulator.h"


// /////////////////////////////////////////////////////////////////////////////
// This is where we actually build the dashstream interface
// since this is the object that will hand it out if someone wants it
// /////////////////////////////////////////////////////////////////////////////
#define BUILD_IT
#include "DrivebaseController.h"
#undef BUILD_IT


// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Drivebase {

////////////////////////////////////////////////////////////////////////////////
Controller::Controller(void)
   : Controller_I()
{
   // get a hold of the data so we can use it as needed
   m_pod = (DashstreamInterface::DashstreamPOD_t *)m_dashstreamIntf.getData();


   // clear out the data we are in control of
   resetData();

   

}

////////////////////////////////////////////////////////////////////////////////
Dashstream::BaseInterface_I *Controller::getDashstreamInterface(void)
{
   return &m_dashstreamIntf;
}

////////////////////////////////////////////////////////////////////////////////
void Controller::setEnabledState(enabled_state_t new_state)
{
   if (new_state == DISABLED)
   {
      m_pod->drivebase_drive_direction = FORWARD;
   }
  
   
}

//////////////////////////////////////////////////////////////////////////
void Controller::resetData(void)
{
   // For now just forcing everything to 0 is good enough.
   // There could be cases where the reset state is not just 0
   // but we will take that on when it happens
   memset(m_pod,0,sizeof(DashstreamInterface::DashstreamPOD_t));
}

////////////////////////////////////////////////////////////////////////////////
void Controller::init(void)
{
   // This is here in case you need to do something else...
   // like hook up things that aren't automatically hooked up
   // by the dashstream init function

   // add limelight camera to dashboard might already be their but ?? sometimes it isn't?
   // 
   
   //default limelight off - this only sometimes works but when it does :)
   nt::NetworkTableInstance::GetDefault().GetTable("limelight")->PutNumber("ledMode",1);

   
   

   addShuffleboardScreenSizeChanger();

   // clear random neo faults (We've needed this code to handle little problems may not be needed now but is here to uncomment at anytime)
   /*m_motor_left_follow1.ClearFaults();
   m_motor_left_follow2.ClearFaults();
   m_motor_left_lead.ClearFaults();
   m_motor_right_follow1.ClearFaults();
   m_motor_right_follow2.ClearFaults();
   m_motor_right_lead.ClearFaults();*/

   m_motor_left_follow1.RestoreFactoryDefaults();
   m_motor_left_follow2.RestoreFactoryDefaults();
   m_motor_left_lead.RestoreFactoryDefaults();
   m_motor_right_follow1.RestoreFactoryDefaults();
   m_motor_right_follow2.RestoreFactoryDefaults();
   m_motor_right_lead.RestoreFactoryDefaults();

  /* @TODO does this reduce can errors? if so we should use it
   m_motor_left_follow1.SetControlFramePeriodMs(50);
   m_motor_left_follow2.SetControlFramePeriodMs(50);
   m_motor_left_lead.SetControlFramePeriodMs(50);
   m_motor_right_follow1.SetControlFramePeriodMs(50);
   m_motor_right_follow2.SetControlFramePeriodMs(50);
   m_motor_right_lead.SetControlFramePeriodMs(50);*/


   // Configure Neos in follow the leader configuration
   m_motor_left_follow1.Follow(m_motor_left_lead);
   m_motor_left_follow2.Follow(m_motor_left_lead);
   
   
   m_motor_right_follow1.Follow(m_motor_right_lead);
   m_motor_right_follow2.Follow(m_motor_right_lead);


   // reverse right side 
   m_motor_right_lead.SetInverted(true);
}


////////////////////////////////////////////////////////////////////////////////
Controller::~Controller(void)
{
}

////////////////////////////////////////////////////////////////////////////////
void
Controller::performManualUpdate(void)
{
   updateEncoders();

   // update the motors
   computeAndApplyMotorOutputs();
   
//   tryLimelightLEDToggle();   
   trySwapCameras();
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::performAutoUpdate(void)
{
   updateEncoders();
   computeAndApplyMotorOutputs();
   
  // tryLimelightLEDToggle();   
   trySwapCameras();
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::performDisabledUpdate(void)
{
   
//Debug Output
//std::cout<<"horizontal distance: "<<getVisionDistance(NetworkTable::GetTable("limelight")->GetEntry("ty").GetDouble(0.0))<<std::endl;

   updateEncoders();
  // tryLimelightLEDToggle();



   if (m_needCameraSetup == true)
   {
      //limelight setup may already be done in h but this is fine here for now
     // m_limelight = cs::HttpCamera("LIMELIGHT", "http://10.40.61.11:5800/", cs::HttpCamera::HttpCameraKind::kMJPGStreamer);
      //CameraServer::GetInstance()->AddCamera(m_limelight);

      m_axis_cam1 = addNewAxisCam("AXIS_CAM_1", ("10.40.61." + std::to_string((int) m_pod->camera_forward_ip)));
      m_axis_cam2 = addNewAxisCam("AXIS_CAM_2", ("10.40.61." + std::to_string((int) m_pod->camera_backward_ip)));
      
      addShuffleboardCameras();
      
      m_needCameraSetup = false;
   }
   else// else do normal camera maintenance
   {
      trySwapCameras();

       //if user wan'ts to update camera screen size
      if (m_shuffleboard_reset_screen_size.GetBoolean(false) == true)
      {
         
         addShuffleboardCameras();
         m_shuffleboard_reset_screen_size.SetBoolean(false);//reset button

      }
   }
}


////////////////////////////////////////////////////////////////////////////////
double
Controller::getVisionDistance(double a2)
{

   //using inches
   double h1 = m_pod->vision_limelight_height_inch;
   double h2 = m_pod->vision_target_height_inch;

   double a1 = m_pod->vision_limelight_angle;
   //double a2 = ty




   double d = (h2-h1) / tan((a2+a1)*3.14159265358979323846/180);

   return d;
}

/*
////////////////////////////////////////////////////////////////////////////////
void
Controller::tryLimelightLEDToggle(void)
{
  
   if (m_pod->drivebase_vision_targeting || SmartDashboard::GetBoolean("DB/Button 3", false ))//toggle limelight
   {
      nt::NetworkTableInstance::GetDefault().GetTable("limelight")->PutNumber("ledMode",0);

   }
   else
   {
      
      nt::NetworkTableInstance::GetDefault().GetTable("limelight")->PutNumber("ledMode",1);

   }
}*/

////////////////////////////////////////////////////////////////////////////////
void
Controller::updateEncoders(void)
{
   // calc and push data out to mi store
   if (m_canEncoderTimer.isExpired())
   {

      
      m_pod->drivebase_left_encoder = 
      (m_encoder_left_lead.GetPosition() +
       m_encoder_left_follow1.GetPosition() + 
       m_encoder_left_follow2.GetPosition())/3;
         
      
      m_pod->drivebase_right_encoder = 
      (m_encoder_right_lead.GetPosition() + 
       m_encoder_right_follow1.GetPosition() + 
       m_encoder_right_follow2.GetPosition())/3;
    
      //reset timer 
      m_canEncoderTimer.setExpiration(m_pod->drivebase_encoder_read_sleep);
      m_canEncoderTimer.start();
   }
   else if (m_canEncoderTimer.isRunning() == false)// if not even running then start it @TODO this could be turned into a first time thing that happens in the init
   {
      m_canEncoderTimer.setExpiration(m_pod->drivebase_encoder_read_sleep);
      m_canEncoderTimer.start();
   }
   
}

/*
////////////////////////////////////////////////////////////////////////////////
double
Controller::getCombinedEncoderValue(double enc1, double enc2, double enc3)
{
   /*double min_bad, max_bad;
   double enc_arr[] = {enc1, enc2, enc3};
   getOutlierRange(enc_arr, 2, min_bad, max_bad);
   std::cout <<"min:" + std::to_string(min_bad) + ", max:" + std::to_string(max_bad) << std::endl;


   std::cout <<"en1:" + std::to_string(enc1) + ", en2:" + std::to_string(enc2) + ", en3:" + std::to_string(enc3) << std::endl<<std::endl;

   // average and ignore outliers@TODO fix this code was outputed
   double sum = 0;
   int added = 0;
   for (int i = 0; i < 3; i++)
   {
      // if it is not out in the probably an outlier range then add to sum
      /*if (enc_arr[i] > min_bad && enc_arr[i] < max_bad)
      {
         sum += enc_arr[i];
         added++;
      }
      else if (added == 0 && i == 2)
      {
         // @TODO if all encoders bad then through error or something??????????
      }
      sum += enc_arr[i];
      added++;
   }
   
   return sum / added;
}

////////////////////////////////////////////////////////////////////////////////
void
Controller::getOutlierRange(double values[], double outlier_threshhold, double& min_ref, double& max_ref)
{
   
   int n = sizeof(values)/sizeof(values[0]); 

   std::sort(values, values+n);// sort encoders smallest to largest

   double quartile1 = values[n/4];
   double quartile3 = values[n - n/4];
   double interQuartile = quartile3 - quartile1;// middle 50%

   min_ref = quartile1 - outlier_threshhold * interQuartile;
   max_ref = quartile3 + outlier_threshhold * interQuartile;
  
}
*/

////////////////////////////////////////////////////////////////////////////////
void
Controller::transitionToState(subsystem_state_t new_state)
{
   //keep track of the old state just in case
   subsystem_state_t prev_state __attribute__((unused)) = m_state;

   //update the base class member so the update() function calls
   //the right things
   m_state = new_state;

   if (m_state == subsystem_state_t::SUBSYSTEM_MANUAL)// automatically switch to camtab 1 when enabled in teliop
   {
      Shuffleboard::SelectTab("CamTab1");
      Shuffleboard::Update();
   
   }

}

////////////////////////////////////////////////////////////////////////////////
void
Controller::computeAndApplyMotorOutputs(void)
{
    
   ///////////////////////////////////////////////////////////
   // check if someone has hit the "switch controls" button
   // even if this subsystem is disabled. 
   ////////////////////////////////////////////////////////////////
   
   if (!m_prev_switch_controls && m_pod->drivebase_switch_controls)
   {
      // someone HAS hit the "switch controls button"
      // ...so switch the direction
      if (m_pod->drivebase_drive_direction == FORWARD)
      {
         m_pod->drivebase_drive_direction = BACKWARD;
      }
      else
      {
         m_pod->drivebase_drive_direction = FORWARD;
      }
   }
   // save the previous values so we can detect the rising edge
   m_prev_switch_controls = m_pod->drivebase_switch_controls;
      

   ///////////////////////////////////////////////////////////
   // check if we are disabled and bail if we are
   if (!m_pod->drivebase_enabled)
   {
      m_motor_left_lead.Set(0.0);
      m_motor_right_lead.Set(0.0);
      return;
   }


   // configure default values
   double mult = m_pod->drivebase_normal_speed_mod;
   double left_motor_speed = 0;
   double right_motor_speed = 0;
   double throttle = 0;
   double heading = 0;



   /////////////////////////////////////////////////////////////////////
   // Here we mirror the controls based on if we want to drive forward
   // or backwards.  The cool thing is we can just overwrite the values
   // in the POD and the rest off the code just works as it used to...
   // easy-peasy
   //
   // forward is "normal" so nothing to do...backward requires flipping
   // both controls
   ////////////////////////////////////////////////////////////////////

   if (m_pod->drivebase_drive_direction == BACKWARD)
   {
      m_pod->drivebase_throttle *= -1.0;
   }



   ////////////////////////////////////////////////////////////////////////////////////////////
   // Figure out what our multiplier is for turbo or anti-turbo. Take anti turbo by default
   // (this is so when we inevitably forget which is which we can press both and have it be
   // anti turbo)
   //////////////////////////////////////////////////////////////////////////////////////////

   if(m_pod->want_safety_speed)
   {
      mult = m_pod->drivebase_safety_speed;
   }
   /*else if (m_pod->drivebase_anti_turbo_button == 1)
   {
      mult = m_pod->drivebase_anti_turbo_speed_mod;
      
   }*/
   else if (m_pod->drivebase_turbo_button == 1)
   {
      mult = m_pod->drivebase_turbo_speed_mod;
   }
   

   // if not using anti turbo (ie multiplier is bigger than anti turbo) then
   //if (m_pod->drivebase_anti_turbo_button != 1)
   //{
      // apply custom multiplier limiter by scaling it to [min_mult, current_mult] and subtracting it from the current mult
      mult = ((mult - m_pod->drivebase_anti_turbo_speed_mod)
          * (1-m_pod->drivebase_custom_multiplier) + m_pod->drivebase_anti_turbo_speed_mod);
   //}
  
  
   
   heading = fabs(m_pod->drivebase_heading) > m_pod->drivebase_heading_dead_zone ? m_pod->drivebase_heading : 0.0;
   heading *= heading; // square it

   throttle = fabs(m_pod->drivebase_throttle) > m_pod->drivebase_throttle_dead_zone ? m_pod->drivebase_throttle : 0.0;
   throttle *= throttle; // square it

  
   // Since we squared the number, we lost the negative sign, we have to
   // re-apply it to keep the direction
   if (m_pod->drivebase_heading  < 0.0f) heading  *= -1.0f;
   if (m_pod->drivebase_throttle < 0.0f) throttle *= -1.0f;

      
   


   // Scale by the multiplier
   //   (The -1 is for the wiring on the 2015 drivebase) @TODO its 2020 now so should we remove this?
   throttle *= mult * -1;
   heading  *= mult;


   // Blend vision heading and throttle to actual heading and throttle, also ignore speed mod
   /*
   if (m_pod->drivebase_vision_targeting)
   {    
      std::shared_ptr<nt::NetworkTable> visionNetworkTable = nt::NetworkTableInstance::GetDefault().GetTable("limelight");

      

      if (visionNetworkTable->GetEntry("tv").GetDouble(0.0) == 1.0)//if limelight has a visible target
      {
         m_vision_isTargeting = true;
         
         
         //extract network table info we care about
         double tx = visionNetworkTable->GetEntry("tx").GetDouble(0.0);// horizontal angle from crosshair +- 27
         double ty = visionNetworkTable->GetEntry("ty").GetDouble(0.0);// vertical angle from corsshair +- 20.5

         //update vision mirrors
         m_pod->vision_distance_to_target = getVisionDistance(ty);
         
         heading += getVisionHeading(tx);
         throttle += getVisionThrottle(tx, ty);

         m_vision_wasTargeting = true;
      }
      else//if no visible target
      {
         m_vision_isTargeting = false;
         m_vision_notTargeting = true;
         
         //update vision mirrors
         m_pod->vision_distance_to_target = 0.0;

         //just go forward into wall 
         throttle += m_pod->vision_throttle_value_default;
      }
      
   }
   else//disable vision driving
   {
      if (m_vision_wasTargeting)//if were coming out of vision then disable stuff (this if statement probably isn't neccesary)
      {   

         m_vision_heading_pidController->disable();      
         
         m_vision_wasTargeting = false;

      }
      m_vision_notTargeting = false;
   }
   m_pod->vision_detecting_contours = m_vision_isTargeting;
   m_pod->vision_not_detecting_contours = m_vision_notTargeting;*/

   // TODO
   //throttle = throttleFilter(throttle);
   //heading = headingFilter(throttle);

   // scale the calculated values to acount for the deadzone
   double doneHeading = heading;//NumberManipulator::applyDeadZone(m_pod->drivebase_heading, m_pod->drivebase_heading_dead_zone);@TODO why doesn't this work?
   double doneThrottle = throttle;// NumberManipulator::applyDeadZone(m_pod->drivebase_throttle, m_pod->drivebase_throttle_dead_zone);

   // Zero point turn
   if (fabs(heading) > m_pod->drivebase_heading_dead_zone && fabs(throttle) < m_pod->drivebase_throttle_dead_zone)
   {
      left_motor_speed = doneHeading * m_pod->drivebase_zpt_speed;
      right_motor_speed = -doneHeading * m_pod->drivebase_zpt_speed;
      
     
   }
   else
   {
      double modifier = doneThrottle * doneHeading;

      if (doneThrottle > 0.0f)
      {
         left_motor_speed = doneThrottle + (modifier * m_pod->drivebase_heading_sensitivity);
         right_motor_speed = doneThrottle - modifier;

         // We're trying to go forward, so make sure no motor goes backwards
         left_motor_speed = (left_motor_speed < 0.0f) ? 0.0f : left_motor_speed;
         right_motor_speed = (right_motor_speed < 0.0f) ? 0.0f : right_motor_speed;
      }
      else
      {
         left_motor_speed = doneThrottle - modifier;
         right_motor_speed = doneThrottle + (modifier* m_pod->drivebase_heading_sensitivity);

         // We're trying to go backwards, so make sure no motor goes forward
         left_motor_speed = (left_motor_speed > 0.0f) ? 0.0f : left_motor_speed;
         right_motor_speed = (right_motor_speed > 0.0f) ? 0.0f : right_motor_speed;
      }
   }


   // Limit the outputs to -1.0 <= output <= 1.0
   left_motor_speed = (left_motor_speed > 1.0) ? 1.0f : left_motor_speed;
   left_motor_speed = (left_motor_speed < -1.0) ? -1.0f : left_motor_speed;

   // Limit the outputs to -1.0 <= output <= 1.0
   right_motor_speed = (right_motor_speed > 1.0) ? 1.0f : right_motor_speed;
   right_motor_speed = (right_motor_speed < -1.0) ? -1.0f : right_motor_speed;


   // push stuff out to the dashboard and the sparkmaxs
   m_motor_left_lead.Set(left_motor_speed);
   m_motor_right_lead.Set(right_motor_speed);
   m_pod->drivebase_left_motor_speed = left_motor_speed;
   m_pod->drivebase_right_motor_speed = right_motor_speed;
}




/*
////////////////////////////////////////////////////////////////////////////////
double Controller::getVisionThrottle(double tx, double ty)
{

  
   if (std::abs(tx) <= m_pod->vision_drive_forward_band)
   {
     
      double visionThrottle = m_pod->vision_throttle_kp * m_pod->vision_distance_to_target;

      //Debug Output
      //SmartDashboard::PutNumber("Distance To Target", distanceFromTarget);
      //SmartDashboard::PutNumber("Vision Throttle", visionThrottle);
      //std::cout<<"Vision Throttle: " << visionThrottle << std::endl;



      return visionThrottle;
   }  
   else
   {
      
      return 0.0;
   }


 
}


////////////////////////////////////////////////////////////////////////////////
double Controller::getVisionHeading(double tx)
{
   if (m_vision_wasTargeting == false)//if we just dropped into a new vision target run so do setup
   {    

     m_vision_heading_pidController = new PsbPidController(
      (float) m_pod->vision_heading_pid_kp,
      0,
      0,
      0,//we want to go to 0.0 degrees
      -27,//min input
      27,//max input
      (float) m_pod->vision_heading_pid_min_value,//min output
      (float) m_pod->vision_heading_pid_max_value,//max output
      -0.5,
      0.5,
      (float) m_pod->vision_heading_pid_innerEpsilon,
      10);
     
      m_vision_heading_pidController->enable();
   }
  
   
   
   double angle_away = m_pod->vision_heading_angle_offset - tx;
   float visionHeading = m_vision_heading_pidController->calcPid(angle_away);    

 
  
   //Debug Output:
   //SmartDashboard::PutNumber("vision heading pid", visionHeading);
   //std::cout<<"vision heading pid"<< visionHeading <<std::endl;
   
 
   return visionHeading;
 }*/


////////////////////////////////////////////////////////////////////////////////
cs::AxisCamera
Controller::addNewAxisCam(std::string name, std::string ip)
{
   cs::AxisCamera axis_cam = CameraServer::GetInstance()->AddAxisCamera(name, ip);
   //CameraServer::GetInstance()->StartAutomaticCapture(axis_cam);
   //CameraServer::GetInstance()->GetServer().SetSource(axis_cam);
  

   return axis_cam;
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::addShuffleboardScreenSizeChanger(void)
{
   /////////////////////////////////////////////////////////////////////////////////////////
   /// Note: shuffleboard doesn't have a maximize function or a way of detecting sizes so 
   /// the size of the cameras is currently constant between monitors meaning that it will
   /// need to be adjusted accordingly through the shuffleboard screen config layout below
   /////////////////////////////////////////////////////////////////////////////////////////

   // add options to specifiy what the max widget size is for the given instance 
   frc::ShuffleboardLayout& shuffleboardScreenConfiglayout =
        frc::Shuffleboard::GetTab("Configuration").GetLayout("Camera Stream Size", "Grid Layout")
            .WithPosition(0, 0)
            .WithSize(3, 2);

  // m_shuffleboard_max_column = shuffleboardScreenConfiglayout.AddPersistent("Column Size",3).GetEntry();
  // m_shuffleboard_max_row = shuffleboardScreenConfiglayout.AddPersistent("Row Size",3).GetEntry();

   m_shuffleboard_columns = shuffleboardScreenConfiglayout.AddPersistent("number of columns",3).GetEntry();
   m_shuffleboard_rows = shuffleboardScreenConfiglayout.AddPersistent("number of rows",3).GetEntry();

  

   /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   /// \/ add button for updating screen size, Note: User will still need to click file->new on the 
   /// shuffleboard in order to show the camera streams with the new size
   /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   m_shuffleboard_reset_screen_size = shuffleboardScreenConfiglayout
      .Add("Reset", false)
      .WithWidget(BuiltInWidgets::kToggleButton)
      .GetEntry();

}


////////////////////////////////////////////////////////////////////////////////
void
Controller::addShuffleboardCameras(void)
{
   // try to get values from shuffleboardScreenConfiglayout
   int column = m_shuffleboard_columns.GetDouble(3);//default send a 3 by 3 sized stream
   int row = m_shuffleboard_rows.GetDouble(3);


   
  
   
   frc::Shuffleboard::GetTab("CamTab1")
      .Add("Axis1", m_axis_cam1)
      .WithSize(column,row)
      .WithPosition(0,0);

   /*frc::Shuffleboard::GetTab("CamTab1")
      .Add("Limelight", m_limelight)
      .WithSize(column_split2,row_split)
      .WithPosition(column_split1,0);*/
      
   frc::Shuffleboard::GetTab("CamTab2")
      .Add("Axis2", m_axis_cam2)
      .WithSize(column,row)
      .WithPosition(0,0);



   
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::trySwapCameras(void)
{
   // if vision toggle is off forward is forward otherwise forward is backward etc
   if (m_pod->drivebase_drive_direction == !m_pod->drivebase_vision_toggle)
   {
      Shuffleboard::SelectTab("CamTab1");
      Shuffleboard::Update();
   }
   else
   {
      Shuffleboard::SelectTab("CamTab2");
      Shuffleboard::Update();
   }
 
}


} // END namespace Drivebase
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////