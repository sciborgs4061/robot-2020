// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

START_NEW_SS(Pneumatic)

   //////  OI  ///////
   
   SS_POD_JOYSTICK_BUTTON(pneumatic_enabled, RAW_BUTTON_11, CCI_ONE,
                          "climber enabled", "CCI 1, button 11")
   SS_POD_JOYSTICK_BUTTON(single_by_gyro, RAW_BUTTON_6, JOYSTICK_TWO,
                          "stop the single valves via gyro", "Joystick 2, button 6")
   SS_POD_JOYSTICK_BUTTON(empty_then_climb, RAW_BUTTON_7, JOYSTICK_TWO,
                          "empty cylinders then climbs", "Joystick 2, button 7")
   SS_POD_JOYSTICK_BUTTON(retract, RAW_BUTTON_15, JOYSTICK_TWO,
                          "retract front pistons", "Joystick 2, button 7")
   SS_POD_JOYSTICK_BUTTON(extend, RAW_BUTTON_12, JOYSTICK_TWO,
                          "extend_front pistons", "Joystick 2, button 12")
   SS_POD_JOYSTICK_BUTTON(pause_all, RAW_BUTTON_11, JOYSTICK_TWO,
                          "stop single solenoids", "Joystick 2, button 1")



   //////  AI  ///////
   

   //////  SI  ///////
   SS_POD_NAVX_DOUBLE(displacement, DISPLACEMENT_Y,
                           "displacement", "The verticle distance the NavX has traveled")   
   
   // we only need one of these but we don't know which yet so everything!
   SS_POD_NAVX_DOUBLE(yaw, YAW,
                           "yaw", "Navx yaw")
   SS_POD_NAVX_DOUBLE(pitch, PITCH,
                           "pitch", "Navx pitch")
   SS_POD_NAVX_DOUBLE(roll, ROLL,
                           "roll", "Navx roll")

   /*SS_POD_DIGITAL_INPUT(magnet_passing_halleffect, CLIMBER_HALLEFFECT,
                          "magnet passing halleffect", "magnet is passing halleffect sensor")*/

   //////  Publish  ///////
   SS_POD_PUBLISH_BOOL(drive_fast_shoot, "drive_fast_shoot",
                           "drive fast shoot", "drive fast shoot")

   //////  CI  ///////
   SS_POD_CONFIG_DOUBLE(pneumatic_third_level_displacement, 0.1f, "third level climb displacement", "N/A")
   SS_POD_CONFIG_DOUBLE(pneumatic_second_level_displacement, 0.1f, "second level climb displacement", "N/A")
   SS_POD_CONFIG_DOUBLE(pneumatic_tilt_forward_deadband, 0.0f, "tilt forward deadband", "N/A")
   SS_POD_CONFIG_DOUBLE(pneumatic_tilt_backward_deadband, -5.0f, "tilt backward deadband", "N/A")

   /*SS_POD_CONFIG_INT(pneumatic_front_double_open, 7, "front open port", "N/A")
   SS_POD_CONFIG_INT(pneumatic_front_double_closed, 0, "front closed port", "N/A")
   SS_POD_CONFIG_INT(pneumatic_back_double_open, 6, "back open port", "N/A")
   SS_POD_CONFIG_INT(pneumatic_back_double_closed, 1, "back closed port", "N/A")
   SS_POD_CONFIG_INT(pneumatic_back_single, 6, "back single port", "N/A")
   SS_POD_CONFIG_INT(pneumatic_front_single, 1, "front single port", "N/A")*/

   //////  SS  ///////
   SS_DOUBLE_POD_DATA(infrared_distance,"SS","Infrared Distance","")
   

   //////  CONFIG  //////

   //////  LEDS    //////
  

END_SS



