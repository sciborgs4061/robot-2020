// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

START_NEW_SS(Climber)

   //////  OI  ///////
   SS_POD_JOYSTICK_BUTTON(climber_enabled, RAW_BUTTON_1, CCI_ONE,
                          "climber enabled", "CCI 1, button 1")
   SS_POD_JOYSTICK_STICK(climber_left_deploy, RAW_AXIS_2, JOYSTICK_ONE,
                          "deploy left climber", "Joystick 1, button 7")
   SS_POD_JOYSTICK_BUTTON(climber_left_retract, RAW_BUTTON_5, JOYSTICK_ONE,
                          "retract left climber", "Joystick 1, button 5")
   SS_POD_JOYSTICK_STICK(climber_right_deploy, RAW_AXIS_3, JOYSTICK_ONE,
                          "deploy right climber", "Joystick 1, button 8")
   SS_POD_JOYSTICK_BUTTON(climber_right_retract, RAW_BUTTON_6, JOYSTICK_ONE,
                          "retract right climber", "Joystick 1, button 6")
   SS_POD_JOYSTICK_BUTTON(climber_override, RAW_BUTTON_7, JOYSTICK_ONE,
                          "overrides climber limits", "Joystick 1, button 7")

   //////  AI  ///////

   //////  SI  ///////

   //////  Publish  ///////

   //////  CI  ///////
   
   

   //////  SS  ///////
   SS_DOUBLE_POD_DATA(left_climber_encoder_count, "SS", "left climber encoder count", "left climber encoder count")
   SS_DOUBLE_POD_DATA(right_climber_encoder_count, "SS", "right climber encoder count", "right climber encoder count")

   //////  CONFIG  //////
   SS_POD_CONFIG_DOUBLE(climber_release_speed, 0.1f, "climber release speed", "N/A")
   SS_POD_CONFIG_DOUBLE(climber_retract_speed, 0.1f, "climber retract speed", "N/A")
   SS_POD_CONFIG_DOUBLE(climber_encoder_limit, 412, "climber encoder count", "N/A")
   SS_POD_CONFIG_DOUBLE(climber_control_deploy_threshold, 0.1f, "climber control deploy threshold", "N/A")

   //////  LEDS    //////
  
   //////  DI    //////
   SS_POD_DIGITAL_INPUT(climber_left_climb_ok, CLIMBER_LEFT_CLIMB_OK,
                          "climber left climb ok", "left climber is not fully retracted")
   SS_POD_DIGITAL_INPUT(climber_right_climb_ok, CLIMBER_RIGHT_CLIMB_OK,
                          "climber right climb ok", "right climber is not fully retracted")

   /*SS_POD_DIGITAL_INPUT(magnet_passing_halleffect, CLIMBER_HALLEFFECT,
                          "magnet passing halleffect", "magnet is passing halleffect sensor")*/

END_SS
