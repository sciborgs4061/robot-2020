// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016-2017 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///                                                                          ///
///              SI - SENSOR INTERFACE                                       ///
///                                                                          ///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef ENCODER
///////////////////////////////////////////////////////////////////////////////
/// ENCODERS
/// @inputs
///    name: Encoder Name - also used as the Enumeration name
///                       ...must be all caps with underscores
///    a_channel: di for the A channel
///    b_channel: di for the B channel
///    reverse - true/false - track reverse movement?  (always use true)
///////////////////////////////////////////////////////////////////////////////
   
   
#endif // ENCODER

#ifdef DIGITAL_INPUT
///////////////////////////////////////////////////////////////////////////////
/// DIGITAL_INPUTS
/// @inputs
///    name: Limit Switch Name - also used as the Enumeration name
///                            ...must be all caps with underscores
///    idx: di for the limit switch
///////////////////////////////////////////////////////////////////////////////
   DIGITAL_INPUT(CLIMBER_LEFT_CLIMB_OK,1)
   DIGITAL_INPUT(CLIMBER_RIGHT_CLIMB_OK,2)
   DIGITAL_INPUT(CONVEYOR_PINCH_LIMIT,3)
   DIGITAL_INPUT(CONVEYOR_LOITER_LIMIT,4)
   DIGITAL_INPUT(CONTROL_PANEL_UPPER_LIMIT,5)
   DIGITAL_INPUT(CONTROL_PANEL_LOWER_LIMIT,6)
   DIGITAL_INPUT(COLLECTOR_RETRACT_LIMIT,8)
   DIGITAL_INPUT(COLLECTOR_DEPLOY_LIMIT,9)
   DIGITAL_INPUT(STATUSLED_TESTMODE_SWITCH,10)

#endif // DIGITAL_INPUT

#ifdef ANALOG_INPUT
///////////////////////////////////////////////////////////////////////////////
/// ANALOG_INPUTS
/// @inputs
///    name: Analog Input Name - also used as the Enumeration name
///                            ...must be all caps with underscores
///    idx: ai for the limit switch
///////////////////////////////////////////////////////////////////////////////
   // ANALOG_INPUT(SOME_IMPORTANT_ANALOG_INPUT,0)
#endif // ANALOG_INPUT

#ifdef ANALOG_POT
///////////////////////////////////////////////////////////////////////////////
/// ANALOG_POTS
/// @inputs
///    name: Analog Potentiometer Name - also used as the Enumeration name
///                                    ...must be all caps with underscores
///    channel:   analog input channel
///    fullRange: full scale range
///    offset:    angle offset
///////////////////////////////////////////////////////////////////////////////
#endif // ANALOG_POT


///////////////////////////////////////////////////////////////////////////////
/// PDP DIAGNOSTICS
///
/// These are POD entries that get pulled into the base robot
///   Just uncomment the ones we want to have
///////////////////////////////////////////////////////////////////////////////
#ifdef PDP_DIAGNOSTICS
   SS_POD_PDP_CURRENT(pdp_ch0_current,0,"PDP Ch 0 Current")
   SS_POD_PDP_CURRENT(pdp_ch1_current,1,"PDP Ch 1 Current")
   SS_POD_PDP_CURRENT(pdp_ch2_current,2,"PDP Ch 2 Current")
   SS_POD_PDP_CURRENT(pdp_ch3_current,3,"PDP Ch 3 Current")
   SS_POD_PDP_CURRENT(pdp_ch4_current,4,"PDP Ch 4 Current")
   SS_POD_PDP_CURRENT(pdp_ch5_current,5,"PDP Ch 5 Current")
   SS_POD_PDP_CURRENT(pdp_ch6_current,6,"PDP Ch 6 Current")
   SS_POD_PDP_CURRENT(pdp_ch7_current,7,"PDP Ch 7 Current")
   SS_POD_PDP_CURRENT(pdp_ch8_current,8,"PDP Ch 8 Current")
   SS_POD_PDP_CURRENT(pdp_ch9_current,9,"PDP Ch 9 Current")
   SS_POD_PDP_CURRENT(pdp_ch10_current,10,"PDP Ch 10 Current")
   SS_POD_PDP_CURRENT(pdp_ch11_current,11,"PDP Ch 11 Current")
   SS_POD_PDP_CURRENT(pdp_ch12_current,12,"PDP Ch 12 Current")
   SS_POD_PDP_CURRENT(pdp_ch13_current,13,"PDP Ch 13 Current")
   SS_POD_PDP_CURRENT(pdp_ch14_current,14,"PDP Ch 14 Current")
   SS_POD_PDP_CURRENT(pdp_ch15_current,15,"PDP Ch 15 Current")
#endif // PDP_DIAGNOSTICS


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///                                                                          ///
///              OI - OPERATOR INTERFACE                                     ///
///                                                                          ///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef JOYSTICK
///////////////////////////////////////////////////////////////////////////////
/// JOYSTICKS
/// @inputs
///    name: Joystick Name - used as the Enumeration name
///                          ...must be all caps with underscores
///                          ...these are usually something like
///                              JOYSTICK_ZERO
///                                 or
///                              CCI_ONE
///
///  NOTE!!!!
///     The ORDER MATTERS...it is the order they will enumerate
///     You can call them whatever you want...but enumerate in the order
///     they are listed here.
///////////////////////////////////////////////////////////////////////////////
   JOYSTICK(JOYSTICK_ZERO)
   JOYSTICK(JOYSTICK_ONE)
   JOYSTICK(CCI_ONE)
#endif // JOYSTICK



////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///                                                                          ///
///              AI - ACTUATOR INTERFACE                                     ///
///                                                                          ///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef SPEED_CONTROLLER
///////////////////////////////////////////////////////////////////////////////
/// SPEED_CONTROLLERS
/// @inputs
///    name: Speed Controller Name - also used as the Enumeration name
///                                ...must be all caps with underscores
///    idx: pwm index for the speed controller
///////////////////////////////////////////////////////////////////////////////
   SPEED_CONTROLLER(CONVEYOR_TOP,0)
   SPEED_CONTROLLER(CONVEYOR_BOTTOM,1)
   SPEED_CONTROLLER(LOITER_MOTOR,2)
   SPEED_CONTROLLER(TRENCH_DEPLOY_MOTORS,3)
   SPEED_CONTROLLER(TRENCH_SPIN_MOTORS,4)
   SPEED_CONTROLLER(OUTPUT_WHEELS,5)
   SPEED_CONTROLLER(COLLECTOR_INTAKE_MOTOR,6)
   SPEED_CONTROLLER(COLLECTOR_MOTOR,7)
   
#endif // SPEED_CONTROLLER

#ifdef RELAY
///////////////////////////////////////////////////////////////////////////////
/// RELAYS
/// @inputs
///    name: Relay Name - used as the Enumeration name
///                          ...must be all caps with underscores
///
///  NOTE!!!!
///     The ORDER MATTERS...it is the order they will enumerate
///     You can call them whatever you want...but enumerate in the order
///     they are listed here.
///////////////////////////////////////////////////////////////////////////////

#endif // RELAY

#ifdef DIGITAL_OUTPUT
///////////////////////////////////////////////////////////////////////////////
/// DIGITAL_OUTPUTS
/// @inputs
///    name: Digital Output Name - also used as the Enumeration name
///                                ...must be all caps with underscores
///    idx: index for the digital output you want to use
///
///  remember...these are shared with the DIs
///////////////////////////////////////////////////////////////////////////////
//   DIGITAL_OUTPUT(SHOOTER_LED_RING,1)
#endif // DIGITAL_OUTPUT

///////////////////////////////////////////////////////////////////////////////
/// LEDs
/// @inputs
///    name: PIXEL_NAME - used as the Enumeration name
///                       ...must be all caps with underscores
///
///  NOTE!!!!
///     The ORDER MATTERS...it is the order they will enumerate
///     You can call them whatever you want...but enumerate in the order
///     they are listed here.
///////////////////////////////////////////////////////////////////////////////
#ifdef ROBOT_LED
   ROBOT_LED(PIXEL_ID_00)
   ROBOT_LED(PIXEL_ID_01)
   ROBOT_LED(PIXEL_ID_02)
   ROBOT_LED(PIXEL_ID_03)
   ROBOT_LED(PIXEL_ID_04)
   ROBOT_LED(PIXEL_ID_05)
   ROBOT_LED(PIXEL_ID_06)
   ROBOT_LED(PIXEL_ID_07)
   ROBOT_LED(PIXEL_ID_08)
   ROBOT_LED(PIXEL_ID_09)
   ROBOT_LED(PIXEL_ID_10)
   ROBOT_LED(PIXEL_ID_11)

   ROBOT_LED(PIXEL_ID_12)
   ROBOT_LED(PIXEL_ID_13)
   ROBOT_LED(PIXEL_ID_14)
   ROBOT_LED(PIXEL_ID_15)
   ROBOT_LED(PIXEL_ID_16)
   ROBOT_LED(PIXEL_ID_17)
   ROBOT_LED(PIXEL_ID_18)
   ROBOT_LED(PIXEL_ID_19)
   ROBOT_LED(PIXEL_ID_20)
   ROBOT_LED(PIXEL_ID_21)

   ROBOT_LED(PIXEL_ID_22)
   ROBOT_LED(PIXEL_ID_23)
   ROBOT_LED(PIXEL_ID_24)
   ROBOT_LED(PIXEL_ID_25)
   ROBOT_LED(PIXEL_ID_26)
   ROBOT_LED(PIXEL_ID_27)
   ROBOT_LED(PIXEL_ID_28)
   ROBOT_LED(PIXEL_ID_29)
   ROBOT_LED(PIXEL_ID_30)
   ROBOT_LED(PIXEL_ID_31)
   ROBOT_LED(PIXEL_ID_32)
   ROBOT_LED(PIXEL_ID_33)

   ROBOT_LED(PIXEL_ID_34)
   ROBOT_LED(PIXEL_ID_35)
   ROBOT_LED(PIXEL_ID_36)
   ROBOT_LED(PIXEL_ID_37)
   ROBOT_LED(PIXEL_ID_38)
   ROBOT_LED(PIXEL_ID_39)
   ROBOT_LED(PIXEL_ID_40)
   ROBOT_LED(PIXEL_ID_41)
   ROBOT_LED(PIXEL_ID_42)
   ROBOT_LED(PIXEL_ID_43)
   




#endif // ROBOT_LED

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////
