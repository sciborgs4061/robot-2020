# This code is designed for the Teensy 3.2 to run vl53l0x distance sensor

You need an Arduino development environment to compile and upload the sketch

## Wiring:
   * Refer to the Teensy 3.2 diagram for doing the wiring; the pins on the vl53l0x are
   labelled on the board; the vl53l0x is an I2C device; it is possible to change its
   I2C address but not very convenient. This code uses it at its default (startup) address

   * Wire Vcc on vl53l0x to the 3.3V output pin of the teensy 
   * Wire Gnd on vl53l0x to the AGND pin of the teensy
   * Wire Scl on vl53l0x to the SCL0 pin of the teensy
   * Wire Sda on vl53l0x to the SDA0 pin of the teensy

## Programming:
   * You need an Arduino IDE with Teensy modifications and the vl53l0x library from Adafruit
      * Install the Arduino IDE from https://www.arduino.cc/en/Main/Software
      * Install the Teensyduino from https://www.pjrc.com/teensy/td_download.html
      * Install the vl53l0x library using instructions on this page:
          * https://learn.adafruit.com/adafruit-vl53l0x-micro-lidar-distance-sensor-breakout/arduino-code
   * Plug the Teensy in to a USB port on your development computer
   * Upload the sketch (which will compile it) in the usual Arduino way
   * Use the Arduino IDE Serial Monitor function to check the output (make sure to choose
   the right serial port)

## Testing:
   Once the sketch is uploaded the Teensy waits for a serial connection and then
   begins reading the distance sensor approx. 10 times per second. It outputs the 
   distance as a sequence of ASCII digits followed by a newline (and carriage return?)
   You should be able to see these measurements in the IDE serial monitor.
   Also, the LED on the teensy provides rudimentary feedback. It blinks rapidly
   while waiting for the serial connection. This is normal and should go away
   once the serial connection is made. It blinks more slowly if it is unable to access
   the vl53l0x, in which case you should remove power, check the connections, and reapply
   power. This state will not go away until power is cycled.
   It then turns ON when the measured distance is > 50mm and < 1500mm.

## Use on the robot:
   * Plug a micro-usb cord into the teensy and the usb-A end into the robo-rio. 
   * The teensy appears as a serial port identified as kUSB1 in the wpilib serial class.