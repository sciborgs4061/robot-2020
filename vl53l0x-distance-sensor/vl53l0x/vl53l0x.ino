#include "Adafruit_VL53L0X.h"
// Time-of-flight IR distance sensor for 2019 FRC robot
//
// Output is 4 ASCII digits per line plus \n with leading zeros
// Distance is in millimeters
//
// Teensy LED: 
//   Blinking rapidly -- waiting for serial connection
//   Blinking slowly -- sensor initialization failed; correct
//                      the issue and power cycle
//   Off -- sensed distance is < 50mm or > 1500mm
//   On -- sensed distance is between 50 and 1500mm inclusive
// 
Adafruit_VL53L0X lox = Adafruit_VL53L0X();
#define LED 13
void setup() {
  Serial.begin(115200);
  pinMode(LED, OUTPUT);           // set pin to input
  digitalWrite(LED, HIGH);
  // wait until serial port opens for native USB devices
  while (! Serial) {
    digitalWrite(LED, LOW);
    delay(20);
    digitalWrite(LED, HIGH);
    delay(20);
  }
  digitalWrite(LED, LOW);  
  // Serial.println("Adafruit VL53L0X test");
  if (!lox.begin()) {
    Serial.println(F("Failed to boot VL53L0X"));
    while(1) {
      digitalWrite(LED, HIGH);
      delay(50);
      digitalWrite(LED,LOW);
      delay(25);
    }
  }
  // power 
  // Serial.println(F("VL53L0X API Simple Ranging example\n\n")); 
}


void loop() {
  VL53L0X_RangingMeasurementData_t measure;
  int range;  
  char strBuf[5];
  lox.rangingTest(&measure, false); // pass in 'true' to get debug data printout!
  if (measure.RangeStatus != 4) {  // phase failures have incorrect data
    range = measure.RangeMilliMeter;
    if (range > 9999) { range = 9999; }
    sprintf(strBuf, "%04d\n", range);
    Serial.print(strBuf);
    if ((range < 50) || (range > 1500)) {
      digitalWrite(LED, LOW);
    } else {
      digitalWrite(LED, HIGH);
    }
  } else {
    Serial.print("9999\n");
    digitalWrite(LED, LOW);
  }
    
  delay(100);
}
